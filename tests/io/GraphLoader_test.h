#ifndef RELATIONS_GRAPHLOADER_TEST_H
#define RELATIONS_GRAPHLOADER_TEST_H

#include <src/dto/EnumType.h>
#include "gtest/gtest.h"
#include "gmock/gmock.h"
#include "src/io/GraphLoader.h"
#include "src/dto/CustomEventType.h"

TEST( GraphLoader_Tests, A ) {
    using relations::enums::GenderBioType;
    using relations::enums::GenderIdentityType;
    using relations::enums::NamePartType;
    using relations::enums::NameType;
    using relations::dto::Person;
    using relations::dto::NamePart;
    using relations::dto::NameForm;
    using relations::dto::Name;
    std::string file_name = "test.db";
    auto db_access    = relations::storage::DatabaseAccess();
    auto graph_loader = relations::io::GraphLoader();
    auto graph        = relations::io::GraphLoader::RelationsGraph_t();
    auto relationship_types = std::vector<relations::dto::RelationshipType>();
    auto progress     = eadlib::tool::Progress();

    db_access.connect( file_name );
    ASSERT_TRUE( graph_loader.populateGraphNodes( progress, db_access, graph ) );
    ASSERT_TRUE( graph_loader.populateGraphEdge( progress, db_access, graph, relationship_types ) );

    std::cout << "=============================Inside GraphLoader TEST=============================" << std::endl;
    for( auto it = graph.cbegin(); it != graph.cend(); ++it ) {
        std::cout << "--------------------------------------------" << std::endl;
        std::cout << it->first << ": " << it->second << std::endl;
    }
    std::cout << "> RelationshipTypes list:" << std::endl;
    for( auto t : relationship_types ) {
        std::cout << t << std::endl;
    }
}

#endif //RELATIONS_GRAPHLOADER_TEST_H
