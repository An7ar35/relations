#ifndef RELATIONS_DATAMANAGER_TEST_H
#define RELATIONS_DATAMANAGER_TEST_H

#include "src/io/DataManager.h"
#include "src/io/PersistentDataIO.h"
#include "gtest/gtest.h"
#include "gmock/gmock.h"
namespace unit_tests::DataManager_Tests {
    void deleteDbFile( const std::string &file_name ) {
        if( std::ifstream( file_name ) ) {
            std::remove( file_name.c_str() );
        }
    }
}

TEST( DataManager_Tests, test ) {
//    using relations::enums::GenderBioType;
//    using relations::enums::GenderIdentityType;
//    using relations::enums::NamePartType;
//    using relations::enums::NameType;
//    using relations::dto::Person;
//    using relations::dto::NamePart;
//    using relations::dto::NameForm;
//    using relations::dto::Name;
//    using unit_tests::DataManager_Tests::deleteDbFile;
//    std::string file_name = "test.db";
//    deleteDbFile( file_name );
//    auto db_access = relations::storage::DatabaseAccess();
//    db_access.connect( file_name );
//    if( db_access.createNewStructure() && db_access.checkDbIntegrity() ) {
//        auto pd = relations::io::PersistentDataIO( db_access );
//        auto data_manager = relations::io::DataManager( pd );
//
//        data_manager.add( Person( GenderBioType::MALE,
//                                  GenderIdentityType::UNSPECIFIED,
//                                  new relations::dto::Note( "First note", "This is my first note." ) ) );
//        data_manager.add( Person( GenderBioType::FEMALE,
//                                  GenderIdentityType::UNSPECIFIED,
//                                  new relations::dto::Note( "Second note", "This is my second note." ) ) );
//        data_manager.add( Person( GenderBioType::UNKNOWN,
//                                  GenderIdentityType::TRIGENDER ) );
//
//        ASSERT_EQ( 3, data_manager._graph->nodeCount() );
//        ASSERT_EQ( 0, data_manager._graph->size() );
//
//        auto relationship_type = relations::dto::RelationshipType( "Friend" );
//        auto relationship = relations::dto::Relationship( 1, 2, relationship_type );
//        data_manager.add( relationship );
//
//        ASSERT_EQ( 1, data_manager._graph->size() );
//
//        auto name_form = new NameForm( "eng", "Es Davison" );
//        name_form->_name_parts = { NamePart( "Esmond", NamePartType::FIRST ),
//                                   NamePart( "Nathaniel", NamePartType::MIDDLE ),
//                                   NamePart( "Peter", NamePartType::MIDDLE ),
//                                   NamePart( "Alwyn", NamePartType::MIDDLE ),
//                                   NamePart( "Davison", NamePartType::SURNAME ) };
//        auto name = Name( 1, NameType::BIRTHNAME, true, name_form );
//        data_manager.add( name );
//
//        for( auto n : *data_manager._graph ) {
//            std::cout << "--------------------------------------------" << std::endl;
//            std::cout << n.first << ": " << n.second << std::endl;
//        }
//    }
}

#endif //RELATIONS_DATAMANAGER_TEST_H
