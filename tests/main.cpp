#include "gtest/gtest.h"

#include "tests/datastructure/DMDGraph_test.h"
#include "tests/datastructure/UpdateTracker2_test.h"

int main(int argc, char **argv) {
    testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}