#ifndef RELATIONS_UPDATETRACKER2_TEST_H
#define RELATIONS_UPDATETRACKER2_TEST_H

#include "gtest/gtest.h"
#include "gmock/gmock.h"
#include "src/datastructure/UpdateTracker2.h"

TEST( UpdateTracker2_Tests, toAdd ) {
    auto tracker = relations::UpdateTracker2<int, int, std::string>();
    for( int i = 0; i < 10; i++ ) {
        for( int j = 0; j < 5; j++ ) {
            tracker.toAdd( { i, j },  "(" + std::to_string( i ) + "," + std::to_string( j ) + ")" );
        }
    }
    ASSERT_EQ( 50, tracker.additionsCount() );
}

TEST( UpdateTracker2_Tests, toAdd_iterator_empty ) {
    auto tracker = relations::UpdateTracker2<int, int, std::string>();
    for( auto it = tracker.additions_begin(); it != tracker.additions_end(); ++it ) {
        ASSERT_FALSE( true );
    }
    ASSERT_TRUE( true );
}

TEST( UpdateTracker2_Tests, toAdd_iterator_1element ) {
    auto tracker = relations::UpdateTracker2<int, int, std::string>();
    tracker.toAdd( { 0, 0 }, "(0,0)" );
    auto count = 0;
    for( auto it = tracker.additions_begin(); it != tracker.additions_end(); ++it ) {
        count++;
        ASSERT_TRUE( count < 2 );
    }
    ASSERT_EQ( 1, count );
}

TEST( UpdateTracker2_Tests, toAdd_iterator_50elements ) {
    auto tracker = relations::UpdateTracker2<int, int, std::string>();
    for( int i = 0; i < 10; i++ ) {
        for( int j = 0; j < 5; j++ ) {
            tracker.toAdd( { i, j },  "(" + std::to_string( i ) + "," + std::to_string( j ) + ")" );
        }
    }
    ASSERT_EQ( 50, tracker.additionsCount() );
    auto count = 0;
    for( auto it = tracker.additions_begin(); it != tracker.additions_end(); ++it ) {
        count++;
    }
    ASSERT_EQ( 50, count );
}

TEST( UpdateTracker2_Tests, toAdd_iterator_erase_1element ) {
    auto tracker = relations::UpdateTracker2<int, int, std::string>();
    tracker.toAdd( { 0, 0 }, "(0,0)" );
    auto count = 0;
    auto it = tracker.additions_begin();
    while( it != tracker.additions_end() ) {
        it = tracker.removals_erase( it );
        count++;
        ASSERT_TRUE( count < 2 );
    }
    ASSERT_EQ( 1, count );
}

TEST( UpdateTracker2_Tests, toAdd_iterator_erase_50elements ) {
    auto tracker = relations::UpdateTracker2<int, int, std::string>();
    for( int i = 0; i < 10; i++ ) {
        for( int j = 0; j < 5; j++ ) {
            tracker.toAdd( { i, j },  "(" + std::to_string( i ) + "," + std::to_string( j ) + ")" );
        }
    }
    ASSERT_EQ( 50, tracker.additionsCount() );
    auto count = 0;
    auto it = tracker.additions_begin();
    while( it != tracker.additions_end() ) {
        it = tracker.removals_erase( it );
        count++;
    }
    ASSERT_EQ( 50, count );
}

TEST( UpdateTracker2_Tests, toUpdate ) {
    auto tracker = relations::UpdateTracker2<int, int, std::string>();
    for( int i = 0; i < 10; i++ ) {
        for( int j = 0; j < 5; j++ ) {
            tracker.toUpdate( { i, j },  "(" + std::to_string( i ) + "," + std::to_string( j ) + ")" );
        }
    }
    ASSERT_EQ( 50, tracker.updatesCount() );
}

TEST( UpdateTracker2_Tests, toRemove ) {
    auto tracker = relations::UpdateTracker2<int, int, std::string>();
    for( int i = 0; i < 10; i++ ) {
        for( int j = 0; j < 5; j++ ) {
            tracker.toRemove( { i, j },  "(" + std::to_string( i ) + "," + std::to_string( j ) + ")" );
        }
    }
    ASSERT_EQ( 50, tracker.removalCount() );
}


#endif //RELATIONS_UPDATETRACKER2_TEST_H
