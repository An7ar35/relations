#ifndef RELATIONS_RELATIONSDBSETUP_TEST_H
#define RELATIONS_RELATIONSDBSETUP_TEST_H

#include "gtest/gtest.h"
#include "src/storage/ReLationsDbSetup.h"

namespace unit_tests::RelationsDbSetup_Tests {
    void deleteDbFile( const std::string &file_name ) {
        if( std::ifstream( file_name ) ) {
            std::remove( file_name.c_str() );
        }
    }
}

TEST( RelationsDbSetup_Tests, table_build_and_check ) {
    using unit_tests::RelationsDbSetup_Tests::deleteDbFile;
    std::string file_name = "test.db";
    deleteDbFile( file_name );
    auto db = eadlib::wrapper::SQLite();
    db.open( file_name );
    db.push( "PRAGMA foreign_keys = ON;" );
    auto setup = relations::storage::ReLationsDbSetup();
    LOG_TRACE( "DB filename: ", db.getFileName(), ", ", db.connected() ? "connected." : "not connected." );
    ASSERT_TRUE( setup.buildTableStructure( db ) );
    ASSERT_TRUE( setup.checkTableStructure( db ) );
    db.close();
}

#endif //RELATIONS_RELATIONSDBSETUP_TEST_H
