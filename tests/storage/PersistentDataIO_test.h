#ifndef RELATIONS_PERSISTENTDATAIO_TEST_H
#define RELATIONS_PERSISTENTDATAIO_TEST_H

#include "gtest/gtest.h"
#include "gmock/gmock.h"
#include "src/io/PersistentDataIO.h"
#include <eadlib/wrapper/SQLite/SQLite.h>
#include <omp.h>

class PersistentDataIO_TestEnvironment : public ::testing::Environment {
  public:
    virtual void SetUp() {}

    static relations::storage::DatabaseAccess * getDbAccess() {
        static auto db_access = relations::storage::DatabaseAccess();
        return &db_access;
    }

    static std::string & getFileName() {
        static std::string file_name { "test.db" };
        return file_name;
    }
};

class PersistentDataIO_Tests : public ::testing::Test {
  public:
    typedef PersistentDataIO_TestEnvironment TestEnvironment;
  protected:
    PersistentDataIO_Tests() {};
    virtual ~PersistentDataIO_Tests() {};

    virtual void SetUp() {
        if( std::ifstream( TestEnvironment::getFileName() ) )
            std::remove( TestEnvironment::getFileName().c_str() );
        TestEnvironment::getDbAccess()->connect( TestEnvironment::getFileName() );
        ASSERT_TRUE( TestEnvironment::getDbAccess()->createNewStructure() );
        ASSERT_TRUE( TestEnvironment::getDbAccess()->checkDbIntegrity() );
    };

    virtual void TearDown() {
        ASSERT_TRUE( TestEnvironment::getDbAccess()->disconnect() );
    };
};

TEST_F( PersistentDataIO_Tests, add_Person ) {
    typedef PersistentDataIO_TestEnvironment TestEnvironment;
    using relations::enums::GenderBioType;
    using relations::enums::GenderIdentityType;
    using relations::enums::NamePartType;
    using relations::enums::NameType;
    using relations::dto::Person;
    using relations::dto::NamePart;
    using relations::dto::NameForm;
    using relations::dto::Name;

    auto mockDBAccess = std::make_unique<TestEnvironment>( TestEnvironment::getDbAccess() );
    relations::io::PersistentDataIO dataIO = relations::io::PersistentDataIO( mockDBAccess );

    auto start = omp_get_wtime(); //Benchmark start

    auto person1 = Person( GenderBioType::MALE,
                           GenderIdentityType::UNSPECIFIED,
                           new relations::dto::Note( "First note", "This is my first note." ) );
    auto person2 = Person( GenderBioType::FEMALE,
                           GenderIdentityType::UNSPECIFIED,
                           new relations::dto::Note( "Second note", "This is my second note." ) );
    auto person3 = Person( GenderBioType::UNKNOWN,
                           GenderIdentityType::TRIGENDER );

    ASSERT_TRUE( dataIO.add( person1 ) );
    ASSERT_TRUE( dataIO.add( person2 ) );
    ASSERT_TRUE( dataIO.add( person3 ) );

    auto relationship_type = relations::dto::RelationshipType( "ParentChild" );
    ASSERT_TRUE( dataIO.try_add( relationship_type ) );
    ASSERT_EQ( 2, relationship_type._id );
    auto relationship = relations::dto::Relationship( 1, 2, relationship_type );
    ASSERT_TRUE( dataIO.add( relationship ) );


    auto name_form = new NameForm( "eng", "Es Davison" );
    name_form->_name_parts = { NamePart( "Esmond", NamePartType::FIRST ),
                               NamePart( "Nathaniel", NamePartType::MIDDLE ),
                               NamePart( "Peter", NamePartType::MIDDLE ),
                               NamePart( "Alwyn", NamePartType::MIDDLE ),
                               NamePart( "Davison", NamePartType::SURNAME ) };
    auto name = Name( 1, NameType::BIRTHNAME, true, name_form );
    ASSERT_TRUE( dataIO.add( name ) );

    auto end = omp_get_wtime(); //Benchmark end
    std::cout << "¬ Execution time: " << end - start << "s" << std::endl;
}

TEST_F( PersistentDataIO_Tests, B ) {
    ASSERT_TRUE( true );
}

#endif //RELATIONS_PERSISTENTDATAIO_TEST_H
