#include "TableDBModel.h"

TableDBModel::TableDBModel( QObject *parent ) :
    QAbstractTableModel( parent )
{}

TableDBModel::~TableDBModel() {}

/**
 * Sort the table
 * @param column Column index to sort by
 * @param order  Order of sort
 */
void TableDBModel::sort( int column, Qt::SortOrder order ) {
    if( column < 0 ) {
        LOG_ERROR( "TableDBModel::sort( ", column, ", ", order, " )] Column index is < 0. Sorting aborted." );
        return;
    }
    switch( order ) {
        case Qt::SortOrder::AscendingOrder:
            _table.sort( static_cast<size_t>( column ), []( eadlib::TableDBCell const &a, eadlib::TableDBCell const &b ) {
               return a < b;
            });
            break;
        case Qt::SortOrder::DescendingOrder:
            _table.sort( static_cast<size_t>( column ), []( eadlib::TableDBCell const &a, eadlib::TableDBCell const &b ) {
                return a > b;
            });
            break;
    }
}

int TableDBModel::rowCount( const QModelIndex &parent ) const {
    return _table.getRowCount();
}

int TableDBModel::columnCount( const QModelIndex &parent ) const {
    return _table.getColCount();
}

/**
 *
 * @param index
 * @param role
 * @return
 */
QVariant TableDBModel::data( const QModelIndex &index, int role ) const {
    if (role == Qt::DisplayRole)
    {
        return QString::fromStdString( _table.at( index.column(), index.row() ).getString() );
    }
    return QVariant();
}

void TableDBModel::setTable( const eadlib::TableDB &table ) {
    _table = table;
}

QVariant TableDBModel::headerData( int section, Qt::Orientation orientation, int role ) const {
    if( orientation == Qt::Horizontal ) {
        if( role == Qt::DisplayRole ) {
            if( section >= 0 && section <= _table.getColCount() ) {
                return QString::fromStdString( _table.getHeading( static_cast<size_t>( section ) ) );
            } else {
                return QString( "Column %1" ).arg( section + 1 );
            }
        }
    }
    return QVariant();
}
