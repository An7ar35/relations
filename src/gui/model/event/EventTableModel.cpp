#include "EventTableModel.h"

/**
 * Constructor
 * @param data_manager DataManager instance
 * @param events       List of Events
 */
EventTableModel::EventTableModel( std::shared_ptr<relations::io::DataManager> data_manager ) :
    _data_manager( data_manager ),
    _headings( { QString( "ID" ), QString( "Date" ), QString( "Type" ), QString( "Note heading" ) } )
{
    _data = std::move( _data_manager->getEvents() );
}

/**
 * Destructor
 */
EventTableModel::~EventTableModel() {}

int EventTableModel::rowCount( const QModelIndex &parent ) const {
    return _data->size();
}

int EventTableModel::columnCount( const QModelIndex &parent ) const {
    return _headings.size();
}

QVariant EventTableModel::headerData( int section, Qt::Orientation orientation, int role ) const {
    if( orientation == Qt::Horizontal ) {
        if( role == Qt::DisplayRole ) {
            try {
                return _headings.at( section );
            } catch( std::out_of_range ) {
                return QString( "Column %1" ).arg( section + 1 );
            }
        }
    }
    return QVariant::Invalid;
}

QVariant EventTableModel::data( const QModelIndex &index, int role ) const {
    try {
        if( !index.isValid() ) {
            return QVariant();
        }
        //TODO adapt for custom event types in the future
        auto cache       = _data_manager->getDataCache();
        auto cached_type = std::find_if( cache->_event_types.begin(),
                                         cache->_event_types.end(),
                                         ( [&]( const relations::dto::EnumType<relations::enums::EventType> &event_type ) {
                                             return event_type._type == _data->at( index.row() )._type ;
                                         } )
        );
        if( role == Qt::DisplayRole ) {
            switch( index.column() ) {
                case 0: //ID
                    return QString::number( _data->at( index.row() )._id );
                case 1: //Date
                    return QString::fromStdString( _data->at( index.row() )._date->_original );
                case 2: //Type
                    if( cached_type != cache->_event_types.end() )
                        return QString::fromStdString( cached_type->_value );
                    else {
                        LOG_ERROR( "[relations::Ui::EventTableModel::data( {", index.column(), ", ", index.row(), "} , Qt::DisplayRole )] "
                            "Could not find EventType in cache. Row #", index.row(), "." );
                        return QString( "Not Found" );
                    }
                case 3: //Note heading
                        if( _data->at( index.row() )._note )
                            return QString::fromStdString( _data->at( index.row() )._note->_subject );
                        else
                            return QVariant();
                default:
                    return QVariant();
            }
        }
        return QVariant();
    } catch( std::out_of_range ) {
        LOG_ERROR( "[relations::Ui::EventParticipantsTableModel::data( {", index.column(), ", ", index.row(), "} , Qt::DisplayRole )] "
            "Row ", index.row(), " in data store does not exist." );
        return QString( "Error" );
    }
}

const relations::dto::Event & EventTableModel::getEvent( int row ) {
    return _data->at( row );
}

size_t EventTableModel::rows() {
    return _data->size();
}

bool EventTableModel::empty() {
    return _data->empty();
}
