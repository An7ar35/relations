#ifndef RELATIONS_EVENTTABLEMODEL_H
#define RELATIONS_EVENTTABLEMODEL_H

#include <QAbstractListModel>
#include "src/io/DataManager.h"

namespace relations::Ui {
    class EventTableModel;
}

class EventTableModel : public QAbstractTableModel {
  Q_OBJECT

  public:
    EventTableModel( std::shared_ptr<relations::io::DataManager> data_manager );
    ~EventTableModel();

    int rowCount( const QModelIndex &parent = QModelIndex() ) const ;
    int columnCount( const QModelIndex &parent = QModelIndex() ) const;
    QVariant headerData( int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const;
    QVariant data( const QModelIndex &index, int role ) const override;

    const relations::dto::Event & getEvent( int row );

    size_t rows();
    bool empty();
  private:
    std::shared_ptr<relations::io::DataManager>         _data_manager;
    std::unique_ptr<std::vector<relations::dto::Event>> _data;
    std::vector<QString>                                _headings;
};


#endif //RELATIONS_EVENTTABLEMODEL_H
