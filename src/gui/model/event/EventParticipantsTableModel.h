#ifndef RELATIONS_EVENTPARTICIPANTSTABLEMODEL_H
#define RELATIONS_EVENTPARTICIPANTSTABLEMODEL_H

#include <QAbstractTableModel>
#include <src/io/DataManager.h>
#include <src/datastructure/UpdateTracker2.h>

namespace relations::Ui {
    class EventParticipantsTableModel;
}

class EventParticipantsTableModel : public QAbstractTableModel  {
  Q_OBJECT

  public:
    EventParticipantsTableModel( std::shared_ptr<relations::io::DataManager> data_manager,
                                 const relations::dto::Event &event );
    ~EventParticipantsTableModel();

    int rowCount( const QModelIndex &parent = QModelIndex() ) const ;
    int columnCount( const QModelIndex &parent = QModelIndex() ) const;
    QVariant headerData( int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const;
    QVariant data( const QModelIndex &index, int role ) const override;

    bool addParticipant( int row,
                         const int64_t &person_id,
                         const int64_t &event_id,
                         const relations::enums::RoleType &type,
                         const QString &details );

    bool updateParticipant( int row,
                            const int64_t &person_id,
                            const int64_t &event_id,
                            const relations::enums::RoleType &type,
                            const QString &details );

    bool removeParticipant( int row );

    int64_t getPersonId( int row ) const;
    relations::enums::RoleType getRoleType( int row ) const;
    QString getDetails( int row ) const;

    void setEventId( const int64_t &id );
    bool commitChangesToDb();

    bool exists( const int64_t &person_id, relations::enums::RoleType &role_type ) const;
    size_t size();
    bool empty();

  private:
    typedef relations::UpdateTracker2<int64_t, relations::enums::RoleType, relations::dto::EventRole> UpdateTracker2_t;
    int64_t                                                                _event_id;
    bool                                                                   _event_ids_are_set_flag;
    std::shared_ptr<relations::io::DataManager>                            _data_manager;
    std::unique_ptr<std::vector<relations::dto::EventRole>>                _data;
    std::vector<QString>                                                   _headings;
    UpdateTracker2_t                                                       _updates;
};


#endif //RELATIONS_EVENTPARTICIPANTSTABLEMODEL_H
