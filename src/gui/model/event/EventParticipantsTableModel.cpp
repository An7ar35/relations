#include "EventParticipantsTableModel.h"
#include "QMessageBox"

/**
 * Constructor
 * @param data_manager DataManager instance
 */
EventParticipantsTableModel::EventParticipantsTableModel( std::shared_ptr<relations::io::DataManager> data_manager,
                                                          const relations::dto::Event &event ) :
    _data_manager( data_manager ),
    _headings( { QString( "Person" ), QString( "Role" ), QString( "Details" ) } ),
    _event_id( event._id )
{
    _data = std::move( _data_manager->getEventParticipants( event ) );
}

/**
 * Destructor
 */
EventParticipantsTableModel::~EventParticipantsTableModel() {}

int EventParticipantsTableModel::rowCount( const QModelIndex &parent ) const {
    return _data->size();
}

int EventParticipantsTableModel::columnCount( const QModelIndex &parent ) const {
    return _headings.size();
}

QVariant EventParticipantsTableModel::headerData( int section, Qt::Orientation orientation, int role ) const {
    if( orientation == Qt::Horizontal ) {
        if( role == Qt::DisplayRole ) {
            try {
                return _headings.at( section );
            } catch( std::out_of_range ) {
                return QString( "Column %1" ).arg( section + 1 );
            }
        }
    }
    return QVariant::Invalid;
}

QVariant EventParticipantsTableModel::data( const QModelIndex &index, int role ) const {
    try {
        if( !index.isValid() ) {
            return QVariant();
        }
        auto cache       = _data_manager->getDataCache();
        auto cached_type = std::find_if( cache->_role_types.begin(),
                                         cache->_role_types.end(),
                                         ( [&]( const relations::dto::EnumType<relations::enums::RoleType> &role_type ) {
                                             return role_type._type == _data->at( index.row() )._type ;
                                         } )
        );
        if( role == Qt::DisplayRole ) {
            switch( index.column() ) {
                case 0:
                    try {
                        auto person = _data_manager->at( _data->at( index.row() )._person_id );
                        QString name = QString::fromStdString( _data_manager->getPreferedName( person ) );
                        return QString( "%1 (%2)" ).arg( name ).arg( person._id );
                    } catch( std::out_of_range ) {
                        LOG_ERROR( "[relations::Ui::EventParticipantsTableModel::data( {", index.column(), ", ", index.row(), "} , Qt::DisplayRole )] "
                            "Could not find Person [", _data->at( index.row() )._person_id, " ] in Graph." );
                        return QString( "Error" );
                    }
                case 1:
                    if( cached_type != cache->_role_types.end() )
                        return QString::fromStdString( cached_type->_value );
                    else {
                        LOG_ERROR( "[relations::Ui::EventParticipantsTableModel::data( {", index.column(), ", ", index.row(), "} , Qt::DisplayRole )] "
                                       "Could not find RoleType in cache. Row #", index.row(), "." );
                        return QString( "Not Found" );
                    }
                case 2:
                    return QString::fromStdString( _data->at( index.row() )._details );
                default:
                    return QVariant();
            }
        }
        return QVariant();
    } catch( std::out_of_range ) {
        LOG_ERROR( "[relations::Ui::EventParticipantsTableModel::data( {", index.column(), ", ", index.row(), "} , Qt::DisplayRole )] "
            "Row ", index.row(), " in data store does not exist." );
        return QString( "Error" );
    }
}

bool EventParticipantsTableModel::addParticipant( int row,
                                                  const int64_t &person_id,
                                                  const int64_t &event_id,
                                                  const relations::enums::RoleType &type,
                                                  const QString &details )
{
    _event_ids_are_set_flag = false;
    beginInsertRows( QModelIndex(), _data->size() - 1, _data->size() - 1 );
    _data->emplace_back( relations::dto::EventRole( event_id, person_id, type, details.toStdString() ) );
    relations::dto::EventRole & entry = _data->at( _data->size() - 1 );
    _updates.toAdd( { entry._person_id, entry._type }, entry );
    endInsertRows();
    LOG_TRACE( "[relations::Ui::NameTableModel::addParticipant(..)] Added EventRole for Person [", person_id, "]" );
    return true;
}

bool EventParticipantsTableModel::updateParticipant( int row,
                                                     const int64_t &person_id,
                                                     const int64_t &event_id,
                                                     const relations::enums::RoleType &type,
                                                     const QString &details )
{
    _event_ids_are_set_flag = false;
    auto event_role = std::find_if( _data->begin(),
                              _data->end(),
                              ( [&]( const relations::dto::EventRole &er ) {
                                  return er._person_id == person_id && er._event_id == event_id && er._type == type;
                              } )
    );
    if( event_role == _data->end() ) {
        LOG_ERROR( "[relations::Ui::EventParticipantsTableModel::updateParticipant(..)] EventRole could not be found in current names." );
        return false;
    }
    event_role->_details = details.toStdString();
    relations::dto::EventRole & entry = _data->at( row );
    _updates.toUpdate( { entry._person_id, entry._type }, entry  );
    dataChanged( createIndex( row, 0 ), createIndex( row, 8 ) );
    return true;
}

bool EventParticipantsTableModel::removeParticipant( int row ) {
    try {
        if( row >= 0 && !_data->empty() ) {
            beginRemoveRows(QModelIndex(), row, row );
            relations::dto::EventRole & entry = _data->at( row );
            _updates.toRemove( { entry._person_id, entry._type }, entry );
            _data->erase( _data->begin() + row );
            endRemoveRows();
            return true;
        }
    } catch( std::out_of_range ) {
        LOG_ERROR( "[relations::Ui::EventParticipantsTableModel::removeParticipant( ", row, " )] "
            "TableModel row #", row, " is out of bounds of the data vector (size: ", _data->size(), ")." );
    }
    return false;
}

int64_t EventParticipantsTableModel::getPersonId( int row ) const {
    return _data->at( row )._person_id;
}

relations::enums::RoleType EventParticipantsTableModel::getRoleType( int row ) const {
    return _data->at( row )._type;
}

QString EventParticipantsTableModel::getDetails( int row ) const {
    return QString::fromStdString( _data->at( row )._details );
}

void EventParticipantsTableModel::setEventId( const int64_t &id ) {
    for( auto it = _updates.additions_begin(); it != _updates.additions_end(); ++it ) {
        LOG_TRACE( "[relations::Ui::EventParticipantsTableModel::setEventId( ", id, " )] "
            "Setting EventRole (Person [", it->_person_id, "]) Event ID to [", id, "]." );
        it->_event_id = id;
    }
    _event_ids_are_set_flag = true;
}

bool EventParticipantsTableModel::commitChangesToDb() {
    if( !_event_ids_are_set_flag ) {
        LOG_ERROR( "[relations::Ui::EventParticipantsTableModel::commitChangesToDb()] "
                       "Event IDs have not been set or changes have been made since they were." );
        return false;
    }
    auto add_count     = 0;
    auto update_count  = 0;
    auto removal_count = 0;
    auto add_total     = _updates.additionsCount();
    auto update_total  = _updates.updatesCount();
    auto removal_total = _updates.removalCount();
    //Additions
    auto addition_it = _updates.additions_begin();
    while( addition_it != _updates.additions_end() ) {
        if( _data_manager->add( *addition_it ) ) {
            addition_it = _updates.additions_erase( addition_it );
            add_count++;
        } else {
            addition_it = std::next( addition_it );
        }
    }
    //Updates
    auto update_it = _updates.updates_begin();
    while( update_it != _updates.updates_end() ) {
        if( _data_manager->update( *update_it ) ) {
            update_it = _updates.updates_erase( update_it );
            update_count++;
        } else {
            update_it = std::next( update_it );
        }
    }
    //Removals
    auto removal_it = _updates.removals_begin();
    while( removal_it != _updates.removals_end() ) {
        if( _data_manager->remove( *removal_it ) ) {
            removal_it = _updates.removals_erase( removal_it );
            removal_count++;
        } else {
            removal_it = std::next( removal_it );
        }
    }
    LOG( "[relations::Ui::EventParticipantsTableModel::commitChangesToDb()] ", add_count, "/", add_total, " EventRole(s) added." );
    LOG( "[relations::Ui::EventParticipantsTableModel::commitChangesToDb()] ", update_count, "/", update_total, " EventRole(s) updated." );
    LOG( "[relations::Ui::EventParticipantsTableModel::commitChangesToDb()] ", removal_count, "/", removal_total, " EventRole(s) removed." );
    if( !_updates.empty() ) {
        LOG_ERROR( "[relations::Ui::EventParticipantsTableModel::commitChangesToDb()] "
                       "Could not commit some/all the changes. ", _updates.size(), " remain." );
        return false;
    }
    return true;
}

bool EventParticipantsTableModel::exists( const int64_t &person_id, relations::enums::RoleType &role_type ) const {
    if( _event_id > 0 ) {
        auto event_role = std::find_if( _data->begin(),
                                        _data->end(),
                                        ( [&]( const relations::dto::EventRole &er ) {
                                            return er._person_id == person_id && er._type == role_type;
                                        } )
        );
        return event_role != _data->end();
    }
    return false;
}

size_t EventParticipantsTableModel::size() {
    return _data->size();
}

bool EventParticipantsTableModel::empty() {
    return _data->empty();
}
