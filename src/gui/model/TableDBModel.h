#ifndef RELATIONS_TABLEDBMODEL_H
#define RELATIONS_TABLEDBMODEL_H

#include <eadlib/wrapper/SQLite/TableDB.h>
#include <QAbstractTableModel>

namespace relations::Ui {
    class TableDBModel;
}

class TableDBModel : public QAbstractTableModel {
  Q_OBJECT

  public:
    TableDBModel( QObject *parent );
    virtual ~TableDBModel();

    void setTable( const eadlib::TableDB &table );

    void sort( int column, Qt::SortOrder order ) override;

    int rowCount( const QModelIndex &parent = QModelIndex() ) const ;
    int columnCount( const QModelIndex &parent = QModelIndex() ) const;
    QVariant data( const QModelIndex &index, int role = Qt::DisplayRole ) const;
    QVariant headerData( int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const;

  private:
    eadlib::TableDB _table;
};

#endif //RELATIONS_TABLEDBMODEL_H
