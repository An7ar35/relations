#ifndef RELATIONS_MEDIAACTORSTABLEMODEL_H
#define RELATIONS_MEDIAACTORSTABLEMODEL_H

#include <QAbstractTableModel>
#include <src/datastructure/UpdateTracker.h>
#include "src/io/DataManager.h"

namespace relations::Ui {
    class MediaActorsTableModel;
}

class MediaActorsTableModel : public QAbstractTableModel {
  Q_OBJECT

  public:
    MediaActorsTableModel( std::shared_ptr<relations::io::DataManager> data_manager,
                           const relations::dto::Media &media );
    ~MediaActorsTableModel();

    int rowCount( const QModelIndex &parent = QModelIndex() ) const ;
    int columnCount( const QModelIndex &parent = QModelIndex() ) const;
    QVariant headerData( int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const;
    QVariant data( const QModelIndex &index, int role ) const override;

    bool addParticipant( int row,
                         const int64_t &person_id );

    bool removeParticipant( int row );

    void setMediaId( const int64_t &id );

    bool commitChangesToDb();

    bool exists( const int64_t &person_id ) const;
    size_t rows();
    bool empty();
  private:
    int64_t                                                          _media_id;
    bool                                                             _media_ids_are_set_flag;
    std::shared_ptr<relations::io::DataManager>                      _data_manager;
    std::unique_ptr<std::vector<int64_t>>                            _data;
    std::vector<QString>                                             _headings;
    relations::UpdateTracker<int64_t, relations::dto::MediaRelation> _updates;
};


#endif //RELATIONS_MEDIAACTORSTABLEMODEL_H
