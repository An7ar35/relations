#include "MediaTableModel.h"

/**
 * Constructor
 * @param data_manager DataManager instance
 */
MediaTableModel::MediaTableModel( std::shared_ptr<relations::io::DataManager> data_manager ) :
    _data_manager( data_manager ),
    _headings( { QString( "ID" ), QString( "Type" ), QString( "Description" ) } )
{
    _data = std::move( _data_manager->getMedia() );
}

/**
 * Destructor
 */
MediaTableModel::~MediaTableModel() {}

int MediaTableModel::rowCount( const QModelIndex &parent ) const {
    return _data->size();
}

int MediaTableModel::columnCount( const QModelIndex &parent ) const {
    return _headings.size();
}

QVariant MediaTableModel::headerData( int section, Qt::Orientation orientation, int role ) const {
    if( orientation == Qt::Horizontal ) {
        if( role == Qt::DisplayRole ) {
            try {
                return _headings.at( section );
            } catch( std::out_of_range ) {
                return QString( "Column %1" ).arg( section + 1 );
            }
        }
    }
    return QVariant::Invalid;
}

QVariant MediaTableModel::data( const QModelIndex &index, int role ) const {
    try {
        if( !index.isValid() ) {
            return QVariant();
        }
        auto cache       = _data_manager->getDataCache();
        auto cached_type = std::find_if( cache->_media_types.begin(),
                                         cache->_media_types.end(),
                                         ( [&]( const relations::dto::EnumType<relations::enums::MediaType> &media_type ) {
                                             return media_type._type == _data->at( index.row() )._type ;
                                         } )
        );
        if( role == Qt::DisplayRole ) {
            switch( index.column() ) {
                case 0: //ID
                    return QString::number( _data->at( index.row() )._id );
                case 1: //Type
                    if( cached_type != cache->_media_types.end() )
                        return QString::fromStdString( cached_type->_value );
                    else {
                        LOG_ERROR( "[relations::Ui::MediaTableModel::data( {", index.column(), ", ", index.row(), "} , Qt::DisplayRole )] "
                            "Could not find MediaType in cache. Row #", index.row(), "." );
                        return QString( "Not Found" );
                    }
                case 2: //Description
                    return QString::fromStdString( _data->at( index.row() )._description );
                default:
                    return QVariant();
            }
        }
        return QVariant();
    } catch( std::out_of_range ) {
        LOG_ERROR( "[relations::Ui::MediaTableModel::data( {", index.column(), ", ", index.row(), "} , Qt::DisplayRole )] "
            "Row ", index.row(), " in data store does not exist." );
        return QString( "Error" );
    }
}

const relations::dto::Media &MediaTableModel::getMedia( int row ) {
    return _data->at( row );
}

size_t MediaTableModel::rows() {
    return _data->size();
}

bool MediaTableModel::empty() {
    return _data->empty();
}
