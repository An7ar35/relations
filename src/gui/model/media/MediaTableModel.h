#ifndef RELATIONS_MEDIATABLEMODEL_H
#define RELATIONS_MEDIATABLEMODEL_H

#include <QAbstractTableModel>
#include "src/io/DataManager.h"

namespace relations::Ui {
    class MediaTableModel;
}

class MediaTableModel : public QAbstractTableModel {
  Q_OBJECT

  public:
    MediaTableModel( std::shared_ptr<relations::io::DataManager> data_manager );
    ~MediaTableModel();

    int rowCount( const QModelIndex &parent = QModelIndex() ) const ;
    int columnCount( const QModelIndex &parent = QModelIndex() ) const;
    QVariant headerData( int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const;
    QVariant data( const QModelIndex &index, int role ) const override;

    const relations::dto::Media & getMedia( int row );

    size_t rows();
    bool empty();
  private:
    std::shared_ptr<relations::io::DataManager>         _data_manager;
    std::unique_ptr<std::vector<relations::dto::Media>> _data;
    std::vector<QString>                                _headings;
};


#endif //RELATIONS_MEDIATABLEMODEL_H
