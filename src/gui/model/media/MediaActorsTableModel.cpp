#include "MediaActorsTableModel.h"

/**
 * Constructor
 * @param data_manager DataManager instance
 * @param media        Media DTO
 */
MediaActorsTableModel::MediaActorsTableModel( std::shared_ptr<relations::io::DataManager> data_manager,
                                              const relations::dto::Media &media ) :
    _data_manager( data_manager ),
    _headings( { QString( "ID" ), QString( "Person" ) } ),
    _media_id( media._id )
{
    if( media._id > 0 ) {
        _data = std::move( _data_manager->getMediaActors( media ) );
    } else {
        _data = std::make_unique<std::vector<int64_t>>();
    }
}

/**
 * Destructor
 */
MediaActorsTableModel::~MediaActorsTableModel() {}

int MediaActorsTableModel::rowCount( const QModelIndex &parent ) const {
    return _data->size();
}

int MediaActorsTableModel::columnCount( const QModelIndex &parent ) const {
    return _headings.size();
}

QVariant MediaActorsTableModel::headerData( int section, Qt::Orientation orientation, int role ) const {
    if( orientation == Qt::Horizontal ) {
        if( role == Qt::DisplayRole ) {
            try {
                return _headings.at( section );
            } catch( std::out_of_range ) {
                return QString( "Column %1" ).arg( section + 1 );
            }
        }
    }
    return QVariant::Invalid;
}

QVariant MediaActorsTableModel::data( const QModelIndex &index, int role ) const {
    try {
        if( !index.isValid() ) {
            return QVariant();
        }
        if( role == Qt::DisplayRole ) {
            auto person = _data_manager->at( _data->at( index.row() ) );
            switch( index.column() ) {
                case 0:
                    return QString::number( _data->at( index.row() ) );
                case 1:
                    return QString::fromStdString( _data_manager->getPreferedName( person ) );
                default:
                    return QVariant();
            }
        }
        return QVariant();
    } catch( std::out_of_range ) {
        LOG_ERROR( "[relations::Ui::MediaActorsTableModel::data( {", index.column(), ", ", index.row(), "} , Qt::DisplayRole )] "
            "Row ", index.row(), " data unreachable." );
        return QString( "Error" );
    }
}

bool MediaActorsTableModel::addParticipant( int row, const int64_t &person_id ) {
    _media_ids_are_set_flag = false;
    const relations::dto::Person & person = _data_manager->at( person_id );
    beginInsertRows( QModelIndex(), _data->size() - 1, _data->size() - 1 );
    _data->emplace_back( person_id );
    _updates.toAdd( person_id, relations::dto::MediaRelation( person._subject_id, _media_id ) );
    endInsertRows();
    LOG_TRACE( "[relations::Ui::MediaActorsTableModel::addParticipant( ", row, ", ", person_id, " )] "
        "Added MediaRelation [", person._subject_id, ", ", _media_id, "]" );
    return true;
}

bool MediaActorsTableModel::removeParticipant( int row ) {
    try {
        if( row >= 0 && !_data->empty() ) {
            int64_t person_id = _data->at( row );
            const relations::dto::Person & person = _data_manager->at( person_id );
            beginRemoveRows(QModelIndex(), row, row );
            _updates.toRemove( person_id, relations::dto::MediaRelation( person._subject_id, _media_id ) );
            _data->erase( _data->begin() + row );
            endRemoveRows();
            return true;
        }
    } catch( std::out_of_range ) {
        LOG_ERROR( "[relations::Ui::EventParticipantsTableModel::removeParticipant( ", row, " )] "
            "TableModel row #", row, " is out of bounds of the data vector (size: ", _data->size(), ") "
            "or Person with ID at row is not in Graph." );
    }
    return false;
}

void MediaActorsTableModel::setMediaId( const int64_t &id ) {
    for( auto it = _updates.additions_begin(); it != _updates.additions_end(); ++it ) {
        LOG_TRACE( "[relations::Ui::MediaActorsTableModel::setMediaId( ", id, " )] "
            "Setting MediaRelations (Subject [", it->second._subject_id, "]) Media ID to [", id, "]." );
        it->second._media_id = id;
    }
    _media_ids_are_set_flag = true;

}

bool MediaActorsTableModel::exists( const int64_t &person_id ) const {
    if( _media_id > 0 ) {
        return ( std::find( _data->begin(), _data->end(), person_id ) != _data->end() );
    }
    return false;
}

size_t MediaActorsTableModel::rows() {
    return _data->size();
}

bool MediaActorsTableModel::empty() {
    return _data->empty();
}

bool MediaActorsTableModel::commitChangesToDb() {
    if( !_media_ids_are_set_flag ) {
        LOG_ERROR( "[relations::Ui::MediaActorsTableModel::commitChangesToDb()] "
                       "Media IDs have not been set or changes have been made since they were." );
        return false;
    }
    //TODO adapt to this
    auto add_count     = 0;
    auto removal_count = 0;
    auto add_total     = _updates.additionsCount();
    auto update_total  = _updates.updatesCount();
    auto removal_total = _updates.removalCount();
    //Additions
    auto addition_it = _updates.additions_begin();
    while( addition_it != _updates.additions_end() ) {
        if( _data_manager->add( addition_it->second ) ) {
            addition_it = _updates.additions_erase( addition_it );
            add_count++;
        } else {
            addition_it = std::next( addition_it );
        }
    }
    //Removals
    auto removal_it = _updates.removals_begin();
    while( removal_it != _updates.removals_end() ) {
        if( _data_manager->remove( removal_it->second ) ) {
            removal_it = _updates.removals_erase( removal_it );
            removal_count++;
        } else {
            removal_it = std::next( removal_it );
        }
    }
    LOG( "[relations::Ui::MediaActorsTableModel::commitChangesToDb()] ", add_count, "/", add_total, " MediaRelation(s) added." );
    LOG( "[relations::Ui::MediaActorsTableModel::commitChangesToDb()] ", removal_count, "/", removal_total, " MediaRelation(s) removed." );
    if( !_updates.empty() ) {
        LOG_ERROR( "[relations::Ui::MediaActorsTableModel::commitChangesToDb()] "
                       "Could not commit some/all the changes. ", _updates.size(), " remain." );
        return false;
    }
    return true;
}