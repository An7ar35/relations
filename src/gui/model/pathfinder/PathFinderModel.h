#ifndef RELATIONS_PATHFINDERMODEL_H
#define RELATIONS_PATHFINDERMODEL_H

#include <QAbstractTableModel>
#include <src/algorithm/containers/EdgeInfo.h>
#include "src/io/DataManager.h"

namespace relations::Ui {
    class PathFinderModel;
}

class PathFinderModel : public QAbstractTableModel {
  Q_OBJECT

  public:
    PathFinderModel( std::shared_ptr<relations::io::DataManager> data_manager,
                     const int64_t &from,
                     const int64_t &to );
    ~PathFinderModel();

    int rowCount( const QModelIndex &parent = QModelIndex() ) const ;
    int columnCount( const QModelIndex &parent = QModelIndex() ) const;
    QVariant headerData( int section, Qt::Orientation orientation, int role = Qt::DisplayRole ) const;
    QVariant data( const QModelIndex &index, int role ) const override;

  private:
    typedef relations::EdgeInfo<int64_t, relations::dto::RelationshipType> EdgeInfo_t;
    std::shared_ptr<relations::io::DataManager> _data_manager;
    std::vector<EdgeInfo_t>                     _data;
    std::vector<QString>                        _headings;
};


#endif //RELATIONS_PATHFINDERMODEL_H
