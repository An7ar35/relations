#include <src/algorithm/RelationshipPath.h>
#include "PathFinderModel.h"

PathFinderModel::PathFinderModel( std::shared_ptr<relations::io::DataManager> data_manager,
                                  const int64_t &from,
                                  const int64_t &to ) :
    _data_manager( data_manager ),
    _headings( { QString( "From" ), QString( "Edge" ), QString( "To" ), QString( "Relationship" ) } )
{
    auto path = _data_manager->getRelationship( from, to );
    if( !path->empty() ) {
        _data = { std::make_move_iterator( std::begin( *path ) ),
                  std::make_move_iterator( std::end( *path ) )
        };
    }
    path.reset();
}

PathFinderModel::~PathFinderModel() {}

int PathFinderModel::rowCount( const QModelIndex &parent ) const {
    return _data.size();
}

int PathFinderModel::columnCount( const QModelIndex &parent ) const {
    return _headings.size();
}

QVariant PathFinderModel::headerData( int section, Qt::Orientation orientation, int role ) const {
    if( orientation == Qt::Horizontal ) {
        if( role == Qt::DisplayRole ) {
            try {
                return _headings.at( section );
            } catch( std::out_of_range ) {
                return QString( "Column %1" ).arg( section + 1 );
            }
        }
    }
    return QVariant::Invalid;
}

QVariant PathFinderModel::data( const QModelIndex &index, int role ) const {
    try {
        if( !index.isValid() ) {
            return QVariant();
        }
        if( role == Qt::TextAlignmentRole ) {
            switch( index.column() ) {
                case 1:
                    return Qt::AlignCenter;
                default:
                    return Qt::AlignLeft;
            }
        }
        if( role == Qt::DisplayRole ) {
            auto person1      = _data_manager->at( _data.at( index.row() )._origin );
            auto person1_name = QString::fromStdString( _data_manager->getPreferedName( person1 ) );
            auto person2      = _data_manager->at( _data.at( index.row() )._destination );
            auto person2_name = QString::fromStdString( _data_manager->getPreferedName( person2 ) );
            auto relationship = _data.at( index.row() )._relationship;
            auto direction    = _data.at( index.row() )._direction;
            switch( index.column() ) {
                case 0:
                    return QString( "%1 (%2)" ).arg( person1_name ).arg( person1._id );
                case 1:
                    switch( direction ) {
                        case EdgeInfo_t::Direction::PARENT:
                            return QString( "←" );
                        case EdgeInfo_t::Direction::CHILD:
                            return QString( "→" );
                    }
                case 2:
                    return QString( "%1 (%2)" ).arg( person2_name ).arg( person2._id );
                case 3:
                    return QString::fromStdString( relationship._value );
                default:
                    return QVariant();
            }
        }
        return QVariant();
    } catch( std::out_of_range ) {
        LOG_ERROR( "[relations::Ui::PathFinderModel::data( {", index.column(), ", ", index.row(), "} , Qt::DisplayRole )] "
            "Row ", index.row(), " data unreachable." );
        return QString( "Error" );
    }
}
