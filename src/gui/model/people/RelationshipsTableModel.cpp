#include "RelationshipsTableModel.h"


RelationshipsTableModel::RelationshipsTableModel( std::shared_ptr<relations::io::DataManager> data_manager ) :
    _data_manager( data_manager ),
    _children( std::make_unique<std::vector<relations::dto::Relationship>>() ),
    _parents( std::make_unique<std::vector<relations::dto::Relationship>>() ),
    _local_person_name( QString( "New Person" ) )
{
    LOG_TRACE( "[relations::Ui::RelationshipsTableModel::RelationshipsTableModel(..)] Empty relationship model created." );
}

RelationshipsTableModel::RelationshipsTableModel( std::shared_ptr<relations::io::DataManager> data_manager,
                                                  std::unique_ptr<std::vector<relations::dto::Relationship>> children,
                                                  std::unique_ptr<std::vector<relations::dto::Relationship>> parents ) :
    _data_manager( data_manager ),
    _children( std::move( children ) ),
    _parents( std::move( parents ) ),
    _local_person_name( QString() )
{
    LOG_TRACE( "[relations::Ui::RelationshipsTableModel::RelationshipsTableModel(..)] "
                   "Passed ", ( _children ? _children->size() : 0 ), " children and ", ( _parents ? _parents->size() : 0 ), " parents to relationship model." );
    size_t children_index { 0 };
    for( auto e : *_children ) {
        _index_map.emplace_back( std::make_pair( children_index, _children.get() ) );
        children_index++;
    }
    size_t parent_index { 0 };
    for( auto e : *_parents ) {
        _index_map.emplace_back( std::make_pair( parent_index, _parents.get() ) );
        parent_index++;
    }
    LOG_DEBUG( "[relations::Ui::RelationshipsTableModel::RelationshipsTableModel(..)] "
                   "Mapped ", children_index, " children and ", parent_index, " parents to relationship model." );
}

RelationshipsTableModel::~RelationshipsTableModel() {}

int RelationshipsTableModel::rowCount( const QModelIndex &parent ) const {
    return _children->size() + _parents->size();
}

int RelationshipsTableModel::columnCount( const QModelIndex &parent ) const {
    return 5;
}

QVariant RelationshipsTableModel::data( const QModelIndex &index, int role ) const {
    if ( !index.isValid() ) {
        return QVariant();
    }
    if( role == Qt::DisplayRole ) {
        auto data_cache   = _data_manager->getDataCache();
        auto mapped_index = _index_map.at( index.row() );
        const relations::dto::Relationship &relationship = mapped_index.second->at( mapped_index.first );
        auto cached_type = std::find_if( data_cache->_relationship_types.begin(),
                                         data_cache->_relationship_types.end(),
                                         ( [&]( const relations::dto::RelationshipType &type ) { return type._id == relationship._type_id; } )
        );
        switch( index.column() ) {
            case 0:
                try {
                    if( _local_person_name.isEmpty() ) {
                        QString name1 = QString::fromStdString( _data_manager->getPreferedName( _data_manager->at( relationship._person1 ) ) );
                        return QString( "%1 (%2)" ).arg( name1 ).arg( relationship._person1 );
                    } else {
                        return QString( "%1 (N/A)" ).arg( _local_person_name );
                    }
                } catch( std::out_of_range e ) {
                    LOG_ERROR( "[relations::Ui::RelationshipsTableModel::data( {", index.column(), ", ", index.row(), "} , Qt::DisplayRole )] "
                        "Inconsistency detected. Failed to get Person [", relationship._person1, "]'s preferred name." );
                    return QString( "Error" );
                }
            case 1:
                if( cached_type != data_cache->_relationship_types.end() )
                    return QString::fromStdString( cached_type->_value );
                else {
                    LOG_ERROR( "[relations::Ui::RelationshipsTableModel::data( const QModelIndex &, int )] "
                                   "Could not find RelationshipType [", relationship._type_id, "] in cache. Row #", index.row(), "." );
                    return QString( "Not Found" );
                }
            case 2:
                try {
                    QString name2 = QString::fromStdString( _data_manager->getPreferedName( _data_manager->at( relationship._person2 ) ) );
                    return QString( "%1 (%2)" ).arg( name2 ).arg( relationship._person2 );
                } catch( std::out_of_range e ) {
                    LOG_ERROR( "[relations::Ui::RelationshipsTableModel::data( {", index.column(), ", ", index.row(), "} , Qt::DisplayRole )] "
                        "Inconsistency detected. Failed to get Person [", relationship._person2, "]'s preferred name." );
                    return QString( "Error" );
                }
            case 3:
                return relationship._note
                       ? QString( "\u2713" )
                       : QString();
            case 4:
                return  QString::number( relationship._subject_id );
            default:
                return QVariant();
        }
    }
    return QVariant();
}

QVariant RelationshipsTableModel::headerData( int section, Qt::Orientation orientation, int role ) const {
    if( orientation == Qt::Horizontal ) {
        if( role == Qt::DisplayRole ) {
            switch( section ) {
                case 0:
                    return QString( "Person A" );
                case 1:
                    return QString( "Type" );
                case 2:
                    return QString( "Person B" );
                case 3:
                    return QString( "Note" );
                case 4:
                    return QString( "Subject ID" );
                default:
                    return QString( "Column %1" ).arg( section + 1 );
            }
        }
    }
    return QVariant::Invalid;
}

bool RelationshipsTableModel::addRelationship( int row,
                                               const int64_t &person_id,
                                               const int64_t &type_id,
                                               const int64_t &target_id,
                                               const std::string &note_subject,
                                               const std::string &note_text )
{
    _person_ids_are_set_flag = false;
    beginInsertRows( QModelIndex(), _index_map.size() - 1, _index_map.size() - 1 );
    if( note_subject.empty() && note_text.empty() ) {
        _children->emplace_back( relations::dto::Relationship( _new_relatonship_subject_id_count,
                                                               person_id,
                                                               target_id,
                                                               type_id )
        );
    } else {
        auto note = new relations::dto::Note( note_subject, note_text );
        _children->emplace_back( relations::dto::Relationship( _new_relatonship_subject_id_count,
                                                               person_id,
                                                               target_id,
                                                               type_id,
                                                               note )
        );
    }
    _index_map.emplace_back( std::make_pair( _children->size() - 1, _children.get() ) );
    relations::dto::Relationship & entry = _children->at( _children->size() - 1 );
    _updates.toAdd( _new_relatonship_subject_id_count, entry );
    _new_relatonship_subject_id_count--;
    endInsertRows();
    return true;
}

bool RelationshipsTableModel::updateRelationship( int row,
                                                  const int64_t &type_id,
                                                  const int64_t &target_id,
                                                  const std::string &note_subject,
                                                  const std::string &note_text )
{
    _person_ids_are_set_flag = false;
    auto mapped_index = _index_map.at( row );
    relations::dto::Relationship & relationship = mapped_index.second->at( mapped_index.first );
    relationship._person2 = target_id;
    relationship._type_id = type_id;
    if( note_subject.empty() && note_text.empty() ) {
        if( relationship._note ) {
            relationship._note.reset();
        }
    } else {
        if( relationship._note ) {
            relationship._note->_subject = note_subject;
            relationship._note->_text = note_text;
        } else {
            auto note = new relations::dto::Note( note_subject, note_text );
            relationship._note = std::unique_ptr < relations::dto::Note > ( note );
        }
    }
    _updates.toUpdate( relationship._subject_id, relationship );
    dataChanged( createIndex( row, 0 ), createIndex( row, 3 ) );
    return true;
}

bool RelationshipsTableModel::removeRelationship( int row ) {
    beginRemoveRows( QModelIndex(), row, row );
    auto pair = _index_map.at( row );
    std::cout << "RelationshipsTableModel::removeRelationship): To delete: " << pair.second->at( pair.first ) << std::endl;
    relations::dto::Relationship & entry = pair.second->at( pair.first );
    _updates.toRemove( entry._subject_id, entry );
    _index_map.erase( _index_map.begin() + row );
    endRemoveRows();
    return true;
}

const relations::dto::Relationship & RelationshipsTableModel::getRelationship( int row ) {
    if( row >= 0 ) {
        auto mapped_index = _index_map.at( row );
        return mapped_index.second->at( mapped_index.first );
    } else {
        LOG_ERROR( "[relations::Ui::RelationshipsTableModel::getRelationship( ", row, " )] No row selected prior to calling the method." );
        throw std::out_of_range( "[relations::Ui::RelationshipsTableModel::getRelationship( int )] No row selected prior to calling the method." );
    }
}

void RelationshipsTableModel::setPersonID( const int64_t &id ) {
    for( auto it = _children->begin(); it != _children->end(); ++it ) {
        if( it->_subject_id < 0 ) {
            it->_person1 = id;
            LOG_TRACE( "[relations::Ui::NameTableModel::setPersonID( ", id, " )] Setting Relationship (Subject [", it->_subject_id,
                       "]) Person1 ID to [", id, "]." );
        }
    }
    for( auto it = _updates.additions_begin(); it != _updates.additions_end(); ++it ) {
        it->second._person1 = id;
        LOG_TRACE( "[relations::Ui::NameTableModel::setPersonID( ", id, " )] Setting Relationship (Subject [", it->second._subject_id, "]) Person1 ID to [", id, "]." );
    }
    _person_ids_are_set_flag = true;
}

bool RelationshipsTableModel::commitChangesToDb() {
    if( !_person_ids_are_set_flag ) {
        LOG_ERROR( "[relations::Ui::RelationshipsTableModel::commitChangesToDb()] "
                       "Person IDs have not been set or changes have been made since it was." );
        return false;
    }
    auto add_count     = 0;
    auto update_count  = 0;
    auto removal_count = 0;
    auto add_total     = _updates.additionsCount();
    auto update_total  = _updates.updatesCount();
    auto removal_total = _updates.removalCount();
    //Additions
    auto addition_it = _updates.additions_begin();
    while( addition_it != _updates.additions_end() ) {
        if( _data_manager->add( addition_it->second ) ) {
            addition_it = _updates.additions_erase( addition_it );
            add_count++;
        } else {
            addition_it = std::next( addition_it );
        }
    }
    //Updates
    auto update_it = _updates.updates_begin();
    while( update_it != _updates.updates_end() ) {
        if( _data_manager->update( update_it->second ) ) {
            update_it = _updates.updates_erase( update_it );
            update_count++;
        } else {
            update_it = std::next( update_it );
        }
    }
    //Removals
    auto removal_it = _updates.removals_begin();
    while( removal_it != _updates.removals_end() ) {
        if( _data_manager->remove( removal_it->second ) ) {
            removal_it = _updates.removals_erase( removal_it );
            removal_count++;
        } else {
            removal_it = std::next( removal_it );
        }
    }
    LOG( "[relations::Ui::RelationshipsTableModel::commitChangesToDb()] ", add_count, "/", add_total, " Relationship(s) added." );
    LOG( "[relations::Ui::RelationshipsTableModel::commitChangesToDb()] ", update_count, "/", update_total, " Relationship(s) updated." );
    LOG( "[relations::Ui::RelationshipsTableModel::commitChangesToDb()] ", removal_count, "/", removal_total, " Relationship(s) removed." );
    if( !_updates.empty() ) {
        LOG_ERROR( "[relations::Ui::RelationshipsTableModel::commitChangesToDb()] "
                       "Could not commit some/all the changes. ", _updates.size(), " remain." );
        return false;
    }
    return true;
}

bool RelationshipsTableModel::exists( const int64_t &person, const int64_t &type, const int64_t &target ) {
    auto search = std::find_if( _children->begin(),
                                _children->end(),
                                ( [&]( const relations::dto::Relationship &r ) {
                                    return r._person1 == person && r._person2 == target && r._type_id == type;
                                } )
    );
    return search != _children->end();
}
