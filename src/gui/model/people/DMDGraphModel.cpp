#include "DMDGraphModel.h"

/**
 * Constructor
 * @param data_manager DataManager instance
 */
DMDGraphModel::DMDGraphModel( std::shared_ptr<relations::io::DataManager> data_manager ) :
    _data_manager( data_manager )
{
    int64_t rows { 0 };
    for( auto it = _data_manager->cbegin(); it != _data_manager->cend(); ++it ) {
        rows++;
        _index_id_mapper.emplace_back( it->second.value._id );
    }
    std::sort( _index_id_mapper.begin(), _index_id_mapper.end() );
}

int DMDGraphModel::rowCount( const QModelIndex &parent ) const {
    if( _data_manager->personCount() > std::numeric_limits<int>::max() ) {
        LOG_ERROR( "[DMDGraphModel::rowCount(..)] Underlying size larger than int limit. Qt's fault for having int as a return type on row count..." );
        return std::numeric_limits<int>::max();
    } else {
        return static_cast<int>( _data_manager->personCount() );
    }
}

int DMDGraphModel::columnCount( const QModelIndex &parent ) const {
    return 2;
}

QVariant DMDGraphModel::data( const QModelIndex &index, int role ) const {
    if ( !index.isValid() ) {
        return QVariant();
    }
    if (role == Qt::DisplayRole) {
        switch( index.column() ) {
            case 0:
                return QString("%1").arg( _index_id_mapper.at( index.row() ) );
            case 1:
                return QString::fromStdString( _data_manager->getPreferedName( _data_manager->at( _index_id_mapper.at( index.row() ) ) ) );
            default:
                return QVariant();
        }
    }
    return QVariant();
}

QVariant DMDGraphModel::headerData( int section, Qt::Orientation orientation, int role ) const {
    if( orientation == Qt::Horizontal ) {
        if( role == Qt::DisplayRole ) {
            switch( section ) {
                case 0:
                    return QString( "ID" );
                case 1:
                    return QString( "Name" );
                default:
                    return QString( "Column %1" ).arg( section + 1 );
            }
        }
    }
    return QVariant::Invalid;
}

int64_t DMDGraphModel::getPersonID( int row ) {
    return _index_id_mapper.at( row );
}

int DMDGraphModel::getRowId( const int64_t &person_id ) {
    ptrdiff_t pos = std::distance( _index_id_mapper.begin(),
                                   std::find( _index_id_mapper.begin(),
                                              _index_id_mapper.end(),
                                              person_id )
    );
    return pos >= _index_id_mapper.size()
           ? -1
           : pos;
}

size_t DMDGraphModel::rows() const {
    return _index_id_mapper.size();
}
