#ifndef RELATIONS_NAMETABLEMODEL_H
#define RELATIONS_NAMETABLEMODEL_H

#include <QAbstractTableModel>
#include <src/dto/Name.h>
#include <src/io/DataManager.h>
#include <src/datastructure/UpdateTracker.h>

namespace relations::Ui {
    class NameTableModel;
}

class NameTableModel : public QAbstractTableModel {
    Q_OBJECT

  public:
    NameTableModel( std::shared_ptr<relations::io::DataManager> data_manager );
    NameTableModel( std::shared_ptr<relations::io::DataManager> data_manager,
                    std::unique_ptr<std::vector<relations::dto::Name>> names );
    virtual ~NameTableModel();

    int rowCount( const QModelIndex &parent = QModelIndex() ) const ;
    int columnCount( const QModelIndex &parent = QModelIndex() ) const;
    QVariant data( const QModelIndex &index, int role = Qt::DisplayRole ) const;
    QVariant headerData( int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const;

    bool addName( int row,
                  const int64_t &person_id,
                  const relations::enums::NameType &type,
                  const bool &preferred_flag,
                  const QString &language,
                  const QString &display,
                  const QString &prefix,
                  const QString &first,
                  const QString &middle,
                  const QString &last,
                  const QString &suffix );

    bool updateName( int row,
                     const int64_t &person_id,
                     const int64_t &name_id,
                     const relations::enums::NameType &type,
                     const bool &preferred_flag,
                     const QString &language,
                     const QString &display,
                     const QString &prefix,
                     const QString &first,
                     const QString &middle,
                     const QString &last,
                     const QString &suffix );

    bool setDefault( int row );
    bool removeName( int row );

    void setPersonID( const int64_t &id );
    bool commitChangesToDb();

    const relations::dto::Name & getName( int row );
    bool getPreferedFlag( int row );
    size_t size();
    bool empty();

  private:
    int64_t                                                 _new_name_id_count { -1 }; //new names have negative IDs assigned to them
    bool                                                    _person_ids_are_set_flag { false };
    std::shared_ptr<relations::io::DataManager>             _data_manager;
    std::unique_ptr<std::vector<relations::dto::Name>>      _names;
    relations::UpdateTracker<int64_t, relations::dto::Name> _updates;
};


#endif //RELATIONS_NAMETABLEMODEL_H
