#include <QMessageBox>
#include "NameTableModel.h"
#include "src/gui/tool/tools.h"

NameTableModel::NameTableModel( std::shared_ptr<relations::io::DataManager> data_manager ) :
    _data_manager( data_manager ),
    _names( std::make_unique<std::vector<relations::dto::Name>>() )
{
    LOG_TRACE( "[relations::Ui::NameTableModel::NameTableModel(..)] Empty name model created." );
}

NameTableModel::NameTableModel( std::shared_ptr<relations::io::DataManager> data_manager,
                                std::unique_ptr<std::vector<relations::dto::Name>> names ) :
    _data_manager( data_manager ),
    _names( std::move( names ) )
{
    LOG_TRACE( "[relations::Ui::NameTableModel::NameTableModel(..)] Passed ", _names->size(), " names to model." );
}

NameTableModel::~NameTableModel() {}

int NameTableModel::rowCount( const QModelIndex &parent ) const {
    return _names->size();
}

int NameTableModel::columnCount( const QModelIndex &parent ) const {
    return 9;
}

QVariant NameTableModel::data( const QModelIndex &index, int role ) const {
    using relations::Ui::tool::unpack;
    if ( !index.isValid() ) {
        return QVariant();
    }
    if (role == Qt::DisplayRole) {
        switch( index.column() ) {
            case 0:
                return unpack( _names->at( index.row() )._name_form->_name_parts_prefix );
            case 1:
                return unpack( _names->at( index.row() )._name_form->_name_parts_first );
            case 2:
                return unpack( _names->at( index.row() )._name_form->_name_parts_middle );
            case 3:
                return unpack( _names->at( index.row() )._name_form->_name_parts_surname );
            case 4:
                return unpack( _names->at( index.row() )._name_form->_name_parts_suffix );
            case 5:
                return QString::fromStdString( _names->at( index.row() )._name_form->_full_text );
            case 6:
                return QString::fromStdString( _names->at( index.row() )._name_form->_language );
            case 7:
                return unpack( _data_manager->getDataCache(), _names->at( index.row() )._type );
            case 8:
                return _names->at( index.row() )._preferred_flag
                       ? QString( "\u2713" )
                       : QString();
            default:
                return QVariant();
        }
    }
    return QVariant();
}

QVariant NameTableModel::headerData( int section, Qt::Orientation orientation, int role ) const {
    if( orientation == Qt::Horizontal ) {
        if( role == Qt::DisplayRole ) {
            switch( section ) {
                case 0:
                    return QString( "Prefix" );
                case 1:
                    return QString( "First" );
                case 2:
                    return QString( "Middle" );
                case 3:
                    return QString( "Surname" );
                case 4:
                    return QString( "Suffix" );
                case 5:
                    return QString( "Display as" );
                case 6:
                    return QString( "Language" );
                case 7:
                    return QString( "Type" );
                case 8:
                    return QString( "Preferred" );
                default:
                    return QString( "Column %1" ).arg( section + 1 );
            }
        }
    }
    return QVariant::Invalid;
}

bool NameTableModel::addName( int row,
                              const int64_t &person_id,
                              const relations::enums::NameType &type,
                              const bool &preferred_flag,
                              const QString &language,
                              const QString &display,
                              const QString &prefix,
                              const QString &first,
                              const QString &middle,
                              const QString &last,
                              const QString &suffix )
{
    using relations::Ui::tool::unpack;
    _person_ids_are_set_flag = false;
    auto form = std::make_unique<relations::dto::NameForm>( language.toStdString(), display.toStdString() );
    for( auto e : unpack( prefix.toStdString() ) )
        form->_name_parts_prefix.emplace_back( relations::dto::NamePart( e, relations::enums::NamePartType::PREFIX ) );
    for( auto e : unpack( first.toStdString() ) )
        form->_name_parts_first.emplace_back( relations::dto::NamePart( e, relations::enums::NamePartType::FIRST ) );
    for( auto e : unpack( middle.toStdString() ) )
        form->_name_parts_middle.emplace_back( relations::dto::NamePart( e, relations::enums::NamePartType::MIDDLE ) );
    for( auto e : unpack( last.toStdString() ) )
        form->_name_parts_surname.emplace_back( relations::dto::NamePart( e, relations::enums::NamePartType::SURNAME ) );
    for( auto e : unpack( suffix.toStdString() ) )
        form->_name_parts_suffix.emplace_back( relations::dto::NamePart( e, relations::enums::NamePartType::SUFFIX ) );

    beginInsertRows( QModelIndex(), _names->size() - 1, _names->size() - 1 );
    _names->emplace_back( relations::dto::Name( _new_name_id_count, person_id, type, preferred_flag, form.release() ) );
    _new_name_id_count--;
    relations::dto::Name & entry = _names->at( _names->size() - 1 );
    _updates.toAdd( entry._id, entry );
    endInsertRows();
    LOG_TRACE( "[relations::Ui::NameTableModel::addName] Added '", display.toStdString(), "' to Person [", person_id, "]" );
}

bool NameTableModel::updateName( int row,
                                 const int64_t &person_id,
                                 const int64_t &name_id,
                                 const relations::enums::NameType &type,
                                 const bool &preferred_flag,
                                 const QString &language,
                                 const QString &display,
                                 const QString &prefix,
                                 const QString &first,
                                 const QString &middle,
                                 const QString &last,
                                 const QString &suffix )
{
    using relations::Ui::tool::unpack;
    _person_ids_are_set_flag = false;
    auto name = std::find_if( _names->begin(),
                              _names->end(),
                              ( [&]( const relations::dto::Name &name ) { return name._id == name_id; } )
    );
    if( name == _names->end() ) {
        LOG_ERROR( "[relations::Ui::NameTableModel::updateName(..)] Name [", name_id, "] could not be found in current names." );
        return false;
    }
    name->_type = type;
    name->_name_form->_language = language.toStdString();
    name->_name_form->_full_text = display.toStdString();
    name->_name_form->_name_parts_prefix.clear();
    name->_name_form->_name_parts_first.clear();
    name->_name_form->_name_parts_middle.clear();
    name->_name_form->_name_parts_surname.clear();
    name->_name_form->_name_parts_suffix.clear();
    for( auto e : unpack( prefix.toStdString() ) )
        name->_name_form->_name_parts_prefix.emplace_back( relations::dto::NamePart( e, relations::enums::NamePartType::PREFIX ) );
    for( auto e : unpack( first.toStdString() ) )
        name->_name_form->_name_parts_first.emplace_back( relations::dto::NamePart( e, relations::enums::NamePartType::FIRST ) );
    for( auto e : unpack( middle.toStdString() ) )
        name->_name_form->_name_parts_middle.emplace_back( relations::dto::NamePart( e, relations::enums::NamePartType::MIDDLE ) );
    for( auto e : unpack( last.toStdString() ) )
        name->_name_form->_name_parts_surname.emplace_back( relations::dto::NamePart( e, relations::enums::NamePartType::SURNAME ) );
    for( auto e : unpack( suffix.toStdString() ) )
        name->_name_form->_name_parts_suffix.emplace_back( relations::dto::NamePart( e, relations::enums::NamePartType::SUFFIX ) );

    relations::dto::Name & entry = _names->at( row );
    _updates.toUpdate( entry._id, entry );
    dataChanged( createIndex( row, 0 ), createIndex( row, 8 ) );
    LOG_TRACE( "[relations::Ui::NameTableModel::updateName(..)] Edited '", display.toStdString(), "' Name [", name_id, "]." );
    return true;
}

bool NameTableModel::setDefault( int row ) {
    _person_ids_are_set_flag = false;
    if( row >= 0 && !_names->empty() ) {
        auto old_preferred = std::find_if( _names->begin(),
                                           _names->end(),
                                           ( [ & ]( const relations::dto::Name &name ) { return name._preferred_flag; } )
        );
        if( old_preferred == _names->end() ) {
            LOG_WARNING( "[relations::Ui::NameTableModel::setDefault( ", row, " )] Could not find a current preferred name." );
        } else {
            old_preferred->_preferred_flag = false;
        }
        try {
            //_names->at( row )._preferred_flag = true;
            relations::dto::Name & entry = _names->at( row );
            entry._preferred_flag = true;
            _updates.toUpdate( entry._id, entry );
            dataChanged( createIndex( row, 0 ), createIndex( row, 8 ) );
            return true;
        } catch( std::out_of_range ) {
            LOG_ERROR( "[relations::Ui::NameTableModel::setDefault( ", row, " )] "
                           "TableModel row #", row, " is out of bounds of the name vector (size: ", _names->size(), ")." );
            if( old_preferred != _names->end() ) {
                old_preferred->_preferred_flag = true;
            }
        }
    }
    return false;
}

bool NameTableModel::removeName( int row ) {
    try {
        if( row >= 0 && !_names->empty() ) {
            //Error control
            if( _names->size() == 1 ) {
                QMessageBox msg_box;
                msg_box.setWindowTitle( "ReLations Error" );
                msg_box.setText( "There must be at least one name. Cannot delete." );
                msg_box.exec();
                return false;
            }
            if( _names->at( row )._preferred_flag ) {
                QMessageBox msg_box;
                msg_box.setWindowTitle( "ReLations Error" );
                msg_box.setText( "Another name must be set as the default before removing this one." );
                msg_box.exec();
                return false;
            }
            //Removal
            beginRemoveRows(QModelIndex(), row, row );
            relations::dto::Name & entry = _names->at( row );
            _updates.toRemove( entry._id, entry );
            _names->erase( _names->begin() + row );
            endRemoveRows();
        }
    } catch( std::out_of_range ) {
        LOG_ERROR( "[relations::Ui::NameTableModel::removeName( ", row, " )] "
                       "TableModel row #", row, " is out of bounds of the name vector (size: ", _names->size(), ")." );
    }
}

void NameTableModel::setPersonID( const int64_t &id ) {
    for( auto it = _updates.additions_begin(); it != _updates.additions_end(); ++it ) {
        LOG_TRACE( "[relations::Ui::NameTableModel::setPersonID( ", id, " )] Setting Name [", it->second._id, "] Person ID to [", id, "]." );
        it->second._person_id = id;
    }
    _person_ids_are_set_flag = true;
}

bool NameTableModel::commitChangesToDb() {
    if( !_person_ids_are_set_flag ) {
        LOG_ERROR( "[relations::Ui::RelationshipsTableModel::commitChangesToDb()] "
                       "Person IDs have not been set or changes have been made since it was." );
        return false;
    }
    auto add_count     = 0;
    auto update_count  = 0;
    auto removal_count = 0;
    auto add_total     = _updates.additionsCount();
    auto update_total  = _updates.updatesCount();
    auto removal_total = _updates.removalCount();
    //Additions
    auto addition_it = _updates.additions_begin();
    while( addition_it != _updates.additions_end() ) {
        if( _data_manager->add( addition_it->second ) ) {
            addition_it = _updates.additions_erase( addition_it );
            add_count++;
        } else {
            addition_it = std::next( addition_it );
        }
    }
    //Updates
    auto update_it = _updates.updates_begin();
    while( update_it != _updates.updates_end() ) {
        if( _data_manager->update( update_it->second ) ) {
            update_it = _updates.updates_erase( update_it );
            update_count++;
        } else {
            update_it = std::next( update_it );
        }
    }
    //Removals
    auto removal_it = _updates.removals_begin();
    while( removal_it != _updates.removals_end() ) {
        if( _data_manager->remove( removal_it->second ) ) {
            removal_it = _updates.removals_erase( removal_it );
            removal_count++;
        } else {
            removal_it = std::next( removal_it );
        }
    }
    LOG( "[relations::Ui::NameTableModel::commitChangesToDb()] ", add_count, "/", add_total, " Names added." );
    LOG( "[relations::Ui::NameTableModel::commitChangesToDb()] ", update_count, "/", update_total, " Names updated." );
    LOG( "[relations::Ui::NameTableModel::commitChangesToDb()] ", removal_count, "/", removal_total, " Names removed." );
    if( !_updates.empty() ) {
        LOG_ERROR( "[relations::Ui::NameTableModel::commitChangesToDb()] "
                       "Could not commit some/all the changes. ", _updates.size(), " remain." );
        return false;
    }
    return true;
}

const relations::dto::Name &NameTableModel::getName( int row ) {
    return _names->at( row );
}

bool NameTableModel::getPreferedFlag( int row ) {
    return getName( row )._preferred_flag;
}

size_t NameTableModel::size() {
    return _names->size();
}

bool NameTableModel::empty() {
    return _names->empty();
}
