#ifndef RELATIONS_RELATIONSHIPSTABLEMODEL_H
#define RELATIONS_RELATIONSHIPSTABLEMODEL_H

#include <QAbstractTableModel>
#include <src/io/DataManager.h>
#include <src/datastructure/UpdateTracker.h>

namespace relations::Ui {
    class RelationshipsTableModel;
}

class RelationshipsTableModel : public QAbstractTableModel {
    Q_OBJECT

  public:
    RelationshipsTableModel( std::shared_ptr<relations::io::DataManager> data_manager );
    RelationshipsTableModel( std::shared_ptr<relations::io::DataManager> data_manager,
                             std::unique_ptr<std::vector<relations::dto::Relationship>> children,
                             std::unique_ptr<std::vector<relations::dto::Relationship>> parents );
    virtual ~RelationshipsTableModel();
    int rowCount( const QModelIndex &parent ) const override;
    int columnCount( const QModelIndex &parent ) const override;
    QVariant data( const QModelIndex &index, int role ) const override;
    QVariant headerData( int section, Qt::Orientation orientation, int role ) const override;

    bool addRelationship( int row,
                          const int64_t &person_id,
                          const int64_t &type_id,
                          const int64_t &target_id,
                          const std::string &note_subject,
                          const std::string &note_text );
    bool updateRelationship( int row,
                             const int64_t &type_id,
                             const int64_t &target_id,
                             const std::string &note_subject,
                             const std::string &note_text );
    bool removeRelationship( int row );
    void setPersonID( const int64_t &id );
    void setPersonName( const QString &name );
    bool commitChangesToDb();

    const relations::dto::Relationship & getRelationship( int row );

    bool exists( const int64_t &person, const int64_t &type, const int64_t &target );

  private:
    QString _local_person_name;
    int64_t _new_relatonship_subject_id_count { -1 }; //new relationships have negative IDs assigned to them
    bool    _person_ids_are_set_flag           { false };
    std::shared_ptr<relations::io::DataManager>                                 _data_manager;
    std::unique_ptr<std::vector<relations::dto::Relationship>>                  _children;
    std::unique_ptr<std::vector<relations::dto::Relationship>>                  _parents;
    std::vector<std::pair<size_t, std::vector<relations::dto::Relationship> *>> _index_map;
    relations::UpdateTracker<int64_t, relations::dto::Relationship>             _updates;
};

#endif //RELATIONS_RELATIONSHIPSTABLEMODEL_H
