#ifndef RELATIONS_DMDGRAPHMODEL_H
#define RELATIONS_DMDGRAPHMODEL_H

#include <QAbstractListModel>
#include "src/io/DataManager.h"

namespace relations::Ui {
    class DMDGraphModel;
}

class DMDGraphModel : public QAbstractTableModel {
  Q_OBJECT

  public:
    DMDGraphModel( std::shared_ptr<relations::io::DataManager> data_manager );
    int rowCount( const QModelIndex &parent ) const override;
    int columnCount( const QModelIndex &parent ) const override;
    QVariant data( const QModelIndex &index, int role ) const override;
    QVariant headerData( int section, Qt::Orientation orientation, int role ) const override;

    size_t rows() const;
    int64_t getPersonID( int row );
    int getRowId( const int64_t &person_id );

  private:
    std::shared_ptr<relations::io::DataManager> _data_manager;
    std::vector<int64_t>                        _index_id_mapper;
};

#endif //RELATIONS_DMDGRAPHMODEL_H
