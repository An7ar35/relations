#include <src/gui/view/BrowserTabWidget.h>
#include "mainwindow.h"
#include "ui_mainwindow.h"

/**
 * Constructor
 * @param parent Parent widget
 */
MainWindow::MainWindow( QWidget *parent ) :
    QMainWindow( parent ),
    _ui( new relations::Ui::MainWindow ),
    _data_manager( std::make_shared<relations::io::DataManager>() ),
    _status_bar( new QStatusBar( this ) )
{
    _ui->setupUi( this );
    setStatusBar( _status_bar );
    switchToDataView();
    //File Menu
    connect( _ui->actionOpen, SIGNAL( triggered() ), this, SLOT( openFile() ) );
    connect( _ui->actionNew, SIGNAL( triggered() ), this, SLOT( newFile() ) );
    connect( _ui->actionExit, SIGNAL( triggered() ), this, SLOT( close() ) );
    //View Menu
    connect( _ui->actionViewData, SIGNAL( triggered() ), this, SLOT( switchToDataView() ) );
    connect( _ui->actionViewGraph, SIGNAL( triggered() ), this, SLOT( switchToBloodGraphView() ) );
    //Tools Menu
    connect( _ui->actionPathFinder, SIGNAL( triggered() ), this, SLOT( openPathFinderDialog() ) );
    _ui->actionViewGraph->setEnabled( false ); //TODO temporary
    _ui->menuTools->setEnabled( false );
    //Stacked widget content
    _ui->stackedWidget->hide();
    _ui->menuView->setDisabled( true );
    //Data browser section
    auto data_layout = new QGridLayout();
    _browser_tabs    = new BrowserTabWidget( _ui->browser_page, _data_manager );
    _ui->browser_page->setLayout( data_layout );
    data_layout->addWidget( _browser_tabs );
    connect( _browser_tabs,
             SIGNAL( openBloodLineGraphSignal( int64_t ) ),
             this,
             SLOT( switchToBloodGraphView( int64_t ) )
    );
    connect( _browser_tabs,
             SIGNAL( openGenericGraphSignal( int64_t, int64_t ) ),
             this,
             SLOT( switchToGenericGraphView( int64_t , int64_t) )
    );
    //Graph section
    auto graph_layout = new QGridLayout();
    _graph_widget     = new GraphForm( _ui->graph_page, _data_manager );
    _ui->graph_page->setLayout( graph_layout );
    graph_layout->addWidget( _graph_widget );
    connect( _graph_widget,
             SIGNAL( showPersonInformationSignal( int64_t ) ),
             this,
             SLOT( switchToDataView( int64_t ) )
    );
}

/**
 * Destructor
 */
MainWindow::~MainWindow() {
    delete _ui;
    delete _status_bar;
}

/**
 * Opens a database file
 * Main menu > File > Open
 */
void MainWindow::openFile() {
    auto file_name = QFileDialog::getOpenFileName( this,
                                                   tr( "Open Database" ),
                                                   "./",
                                                   tr( "Database Files (*.db)" ) );

    if( file_name.isEmpty() ) {
        return;
    }
    if( !_data_manager->open( file_name.toStdString() ) ) {
        LOG_ERROR( "[MainWindow::openFile()] Could not open database file '", file_name.toStdString(), "'." );
        return;
    }
    if( !_data_manager->checkTableStructure() ) {
        LOG_ERROR( "[MainWindow::openFile()] '", file_name.toStdString(), "' is not passing table structure checks." );
    }
    auto progress = eadlib::tool::Progress(); //TODO implement observers in Ui
    _data_manager->constructGraph( progress );
    _browser_tabs->refreshAllViews();
    _status_bar->showMessage( QString( "Opened '" + file_name + "'" ) );
    _ui->stackedWidget->show();
    _ui->menuView->setDisabled( false );
    _ui->menuTools->setDisabled( false );
}

/**
 * Creates a new database file
 * Main menu > File > New
 */
void MainWindow::newFile() {
    auto file_name = QFileDialog::getSaveFileName( NULL, "Create New File", "genealogy.db", "Database Files (*.db)" );

    if( file_name.isEmpty() ) {
        LOG_ERROR( "[MainWindow::newFile()] File name not specified." );
    } else {
        if( _data_manager->connected() ){
            if( !_data_manager->close() ) {
                LOG_ERROR( "[relations::Ui::MainWindow::newFile()] Could not close currently opened file." );
            }
        }
        if( !_data_manager->open( file_name.toStdString() ) ) {
            LOG_ERROR( "[MainWindow::newFile()] Problem creating new database file '", file_name.toStdString(), "'." );
            return;
        }
        if( !_data_manager->createTableStructure() ) {
            LOG_ERROR( "[MainWindow::newFile()] Failed to build tables in '", file_name.toStdString(), "'." );
            _data_manager->close();
            return;
        }
        if( !_data_manager->checkTableStructure() ) {
            LOG_ERROR( "[MainWindow::newFile()] '", file_name.toStdString(), "' is not passing structure checks." );
            _data_manager->close();
            return;
        }
    }
    auto progress = eadlib::tool::Progress(); //TODO implement observers in Ui
    _data_manager->constructGraph( progress );
    _browser_tabs->refreshAllViews();
    _status_bar->showMessage( QString( "Created '" + file_name + "'" ) );
    _ui->stackedWidget->show();
    _ui->menuView->setDisabled( false );
    _ui->menuTools->setDisabled( false );
}

void MainWindow::openPathFinderDialog() {
    _path_finder_dialog = new PathFinderDialog( this, _data_manager );
    _path_finder_dialog->exec();
}

void MainWindow::switchToDataView() {
    _ui->stackedWidget->setCurrentIndex( 1 );
}

void MainWindow::switchToDataView( int64_t subject_id ) {
    LOG( "@MainWindow::switchToDataView( ", subject_id, " )" );
    _ui->stackedWidget->setCurrentIndex( 1 );
    _browser_tabs->openPersonView( subject_id );
}

void MainWindow::switchToBloodGraphView( int64_t subject_id ) {
    _ui->stackedWidget->setCurrentIndex( 0 );
    _graph_widget->loadBloodGraph( subject_id );
}

void MainWindow::switchToGenericGraphView( int64_t subject_id,
                                           int64_t relationship_id )
{
    auto cache = _data_manager->getDataCache();
    auto relationship = std::find_if( cache->_relationship_types.begin(),
                                      cache->_relationship_types.end(),
                                      ( [&]( const relations::dto::RelationshipType &r ) {
                                          return r._id == relationship_id;
                                      } )
    );
    if( relationship != cache->_relationship_types.end() ) {
        _ui->stackedWidget->setCurrentIndex( 0 );
            _graph_widget->loadGenericGraph( subject_id, *relationship );
    } else {
        LOG_ERROR( "[relations::Ui::MainWindow::switchToGenericGraphView( ", subject_id, ", ", relationship_id, ")] "
            "Relationship ID [", relationship_id, "] has disappeared from the cache!" );
    }

}