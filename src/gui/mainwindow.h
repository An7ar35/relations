#ifndef RELATIONS_MAINWINDOW_H
#define RELATIONS_MAINWINDOW_H

#include <QMainWindow>
#include <QFileDialog>
#include <eadlib/logger/Logger.h>
#include <src/gui/view/BrowserTabWidget.h>
//#include <src/gui/view/GraphFrame.h>
#include <src/gui/view/GraphForm.h>
#include <src/io/DataManager.h>
#include <src/gui/view/pathfinder/PathFinderDialog.h>

namespace relations::Ui {
    class MainWindow;
    class PathFinderDialog;
}

class MainWindow : public QMainWindow {
  Q_OBJECT

  public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

  private slots:
    //MenuFile
    void openFile();
    void newFile();
    void openPathFinderDialog();
    //View
    void switchToDataView();
    void switchToDataView( int64_t subject_id );
    void switchToBloodGraphView( int64_t subject_id );
    void switchToGenericGraphView( int64_t subject_id, int64_t relationship_id );

  private:
    relations::Ui::MainWindow                  *_ui;
    QStatusBar                                 *_status_bar;
    BrowserTabWidget                           *_browser_tabs;
    //GraphFrame                                 *_graph_frame;
    GraphForm                                  *_graph_widget;
    std::shared_ptr<relations::io::DataManager> _data_manager;
    PathFinderDialog                           *_path_finder_dialog;

};

#endif //RELATIONS_MAINWINDOW_H
