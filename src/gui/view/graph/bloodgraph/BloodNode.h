#ifndef RELATIONS_BLOODNODE_H
#define RELATIONS_BLOODNODE_H

#include <QGraphicsItem>
#include <src/enums/GenderBioType.h>
#include <src/enums/GenderIdentityType.h>
#include "BloodGraph.h"
#include "BloodEdge.h"

class QGraphicsSceneMouseEvent;
class BloodEdge;
class BloodGraph;

namespace relations::Ui {
    class BloodNode;
}

class BloodNode : public QGraphicsItem {
  public:
    BloodNode( BloodGraph *blood_graph,
               const int64_t &person_id,
               const QString &display_name,
               const relations::enums::GenderBioType &gender,
               const relations::enums::GenderIdentityType &gender_identity );

    void addEdge( BloodEdge *edge );
    void insertEdgeFront( BloodEdge *edge );
    void insertEdgeBack( BloodEdge *edge );
    void insertLevelEdgeFront( BloodEdge *edge );
    void insertLevelEdgeBack( BloodEdge *edge );
    QList<BloodEdge *> childEdges() const;
    QList<BloodEdge *> parentEdges() const;
    QList<BloodEdge *> levelEdges() const;

    int64_t id() const;
    int maxChildWidth() const;
    int maxParentWidth() const;

    enum { Type = UserType + 3 };
    int type() const override { return Type; }

    QRectF boundingRect() const override;
    QPainterPath shape() const override;
    void paint( QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget ) override;

  protected:
    QVariant itemChange( GraphicsItemChange change, const QVariant &value ) override;
    void mousePressEvent( QGraphicsSceneMouseEvent *event ) override;
    void mouseReleaseEvent( QGraphicsSceneMouseEvent *event ) override;

  private:
    int64_t            _id;
    QString            _name;
    QColor             _gender_colour;
    QList<BloodEdge *> _child_edges;
    QList<BloodEdge *> _parent_edges;
    QList<BloodEdge *> _level_edges;
    BloodGraph        *_graph;
};

#endif //RELATIONS_BLOODNODE_H
