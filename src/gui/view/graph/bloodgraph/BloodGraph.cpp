#include "BloodGraph.h"
#include <QMenu>
#include <QKeyEvent>
#include <src/algorithm/BloodLine.h>

BloodGraph::BloodGraph( QWidget *parent,
                        std::shared_ptr<relations::io::DataManager> data_manager ) :
    QGraphicsView( parent ),
    _data_manager( data_manager )
{
    this->setContextMenuPolicy( Qt::CustomContextMenu );
    _scene = new QGraphicsScene( this );
    _scene->setItemIndexMethod( QGraphicsScene::NoIndex );
    viewport()->childrenRect().setSize( parent->size() );
    _scene->setSceneRect( viewport()->childrenRect() );
    setScene( _scene );
    setCacheMode( CacheBackground );
    setViewportUpdateMode( BoundingRectViewportUpdate );
    setRenderHint( QPainter::Antialiasing );
    setTransformationAnchor( AnchorUnderMouse );
    scale( qreal( 0.8 ), qreal( 0.8 ) );
    setMinimumSize( parent->width(), parent->height() );
}

void BloodGraph::zoomIn() {
    scaleView( qreal( 1.2 ) );
}

void BloodGraph::zoomOut() {
    scaleView( 1 / qreal( 1.2 ) );
}

bool BloodGraph::loadGraph( const int64_t &subject_id ) {
    LOG_DEBUG( "[relations::Ui::BloodGraph::loadBloodGraph( ", subject_id, ")] Loading Person [", subject_id, "]'s blood line \\m/.");
    auto data_cache = _data_manager->getDataCache();
    //Gets the relationship types for blood graph
    auto parent_child = std::find_if( data_cache->_relationship_types.begin(),
                                      data_cache->_relationship_types.end(),
                                      ( []( const relations::dto::RelationshipType &type ) {
                                          return type._value == "ParentChild";
                                      } )
    );
    auto couple = std::find_if( data_cache->_relationship_types.begin(),
                                data_cache->_relationship_types.end(),
                                ( []( const relations::dto::RelationshipType &type ) {
                                    return type._value == "Couple";
                                } )
    );
    //Error control
    if( parent_child == data_cache->_relationship_types.end() ) {
        LOG_ERROR( "[relations::Ui::BloodGraph::loadBloodGraph( ", subject_id, ")] 'ParentChild' relationship type not found in data cache." );
        return false;
    } else {
        _parentchild_type = std::make_unique<relations::dto::RelationshipType>( *parent_child );
    }
    if( couple == data_cache->_relationship_types.end() ) {
        LOG_ERROR( "[relations::Ui::BloodGraph::loadBloodGraph( ", subject_id, ")] 'Couple' relationship type not found in data cache." );
        return false;
    } else {
        _couple_type = std::make_unique<relations::dto::RelationshipType>( *couple );
    }
    //Creating BloodLine algo instance
    _bloodline = std::make_unique<relations::algo::BloodLine>( _data_manager );
    //Loading...
    try {
        //Getting subject Person (S)
        auto person       = _data_manager->at( subject_id );
        auto display_name = QString::fromStdString( _data_manager->getPreferedName( person ) );
        auto blood_node   = std::make_unique<BloodNode>( this,
                                                         person._id,
                                                         display_name,
                                                         person._gender_biological,
                                                         person._gender_identity
        );
        auto node_it      = _nodes.insert( typename NodeMap_t::value_type( subject_id, std::move( blood_node ) ) ).first;
        _subject_node     = node_it->second.get();
        //Setting center position
        node_it->second->setPos( 0, 0 );
        node_it->second->setZValue( 0.1 );

        createAncestors( _subject_node );
        createDescendants( _subject_node );
        positionAncestors( _subject_node );
        positionDescendants( _subject_node );
        positionLevelRelations( _subject_node,
                                std::move( createPartners( _subject_node ) ),
                                std::move( createSiblings( _subject_node ) )
        );

        for( auto it = _nodes.begin(); it != _nodes.end(); ++it ) {
            _scene->addItem( it->second.get() );
        }
    } catch( std::out_of_range ) {
        LOG_ERROR( "[relations::Ui::BloodGraph::loadBloodGraph( ", subject_id, ")] Subject [", subject_id, "] not in DMDGraph.");
        return false;
    }
}

void BloodGraph::clearGraph() {
    LOG_DEBUG( "[relations::Ui::BloodGraph::clearGraph()] Clearing graph scene" );
    _nodes.clear();
    _scene->clear();
    viewport()->update();
}

void BloodGraph::goToProfile( BloodNode *new_subject ) {
    emit showProfileSignal( new_subject->id() );
}

void BloodGraph::goToEvents() {
    //TODO when filtering is available in future version
}

void BloodGraph::goToMedia() {
    //TODO when filtering is available in future version
}

void BloodGraph::centerOnNewSubject( BloodNode *new_subject ) {
    try {
        auto person = _data_manager->at( new_subject->id() );
        clearGraph();
        loadGraph( person._id );
    } catch( std::out_of_range ) {
        LOG_ERROR( "[relations::Ui::BloodGraph::centerOnNewSubject( BloodNode * )] "
                       "DataManager could not find Person [", new_subject->id(), "]." );
    }
}

void BloodGraph::keyPressEvent( QKeyEvent *event ) {
    switch( event->key() ) {
        case Qt::Key_Plus:
            zoomIn();
            break;
        case Qt::Key_Minus:
            zoomOut();
            break;
        default:
            QGraphicsView::keyPressEvent( event );
    }
}

#ifndef QT_NO_WHEELEVENT
void BloodGraph::wheelEvent( QWheelEvent *event ) {
    scaleView( pow( ( double ) 2, -event->delta() / 240.0 ) );
}
#endif

void BloodGraph::drawBackground( QPainter *painter, const QRectF &rect ) {
    Q_UNUSED( rect );
    QRectF sceneRect = this->sceneRect();
    // Fill
    QLinearGradient gradient( sceneRect.topLeft(), sceneRect.bottomRight() );
    gradient.setColorAt( 0, Qt::white );
    gradient.setColorAt( 1, Qt::lightGray );
    painter->fillRect( rect.intersected( sceneRect ), gradient );
    painter->setBrush( Qt::NoBrush );
    painter->drawRect( sceneRect );
}

void BloodGraph::scaleView( qreal scaleFactor ) {
    qreal factor = transform().scale( scaleFactor, scaleFactor ).mapRect( QRectF( 0, 0, 1, 1 ) ).width();
    if( factor < 0.07 || factor > 100 )
        return;

    scale( scaleFactor, scaleFactor );
}

void BloodGraph::resizeEvent( QResizeEvent *event ) {
    QGraphicsView::resizeEvent( event );
}

void BloodGraph::mousePressEvent( QMouseEvent *event ) {
    if( !_nodes.empty() && event->button() == Qt::RightButton ) {
        QGraphicsItem *item = itemAt( event->pos() );
        if( item && item->type() == _nodes.begin()->second->type() ) {
            try {
                auto node = dynamic_cast<BloodNode *>( item );
                QMenu menu( this );
//                QAction action_GoToProfile( "Profile", this );
//                QAction action_GoToEvents( "Events", this );
//                QAction action_GoToMedia( "Media", this );
//                connect( &action_GoToProfile, SIGNAL( triggered() ), this, SLOT( goToProfile() ) );
//                connect( &action_GoToEvents, SIGNAL( triggered() ), this, SLOT( goToEvents() ) );
//                connect( &action_GoToMedia, SIGNAL( triggered() ), this, SLOT( goToMedia() ) );
                menu.addAction( QIcon( ":/resource/open-iconic/person.svg"),
                                "Person info.",
                                this,
                                [this, node]() { goToProfile( node ); }
                );
                menu.addAction( QIcon( ":/resource/open-iconic/target.svg" ),
                                "Center Graph",
                                this,
                                [this, node]() { centerOnNewSubject( node ); }
                );
//                menu.addAction( &action_GoToProfile );
//                menu.addAction( &action_GoToEvents );
//                menu.addAction( &action_GoToMedia );
                menu.exec( mapToGlobal( event->pos() ) );

            } catch( std::bad_cast ) {
                LOG_ERROR( "[relations::Ui::BloodGraph::mousePressEvent(..)] "
                               "Couldn't cast QGraphicItem into BloodNode despite passing the type checks." );
            }
        }
    } else {
        QGraphicsView::mousePressEvent( event );
    }
}

/**
 * Creates ancestor nodes recursively
 * @param current Starting subject node
 */
void BloodGraph::createAncestors( BloodNode *current ) {
    auto parents = _bloodline->getParents( current->id() );
    auto s = parents->size();
    for( auto p : *parents ) {
        auto person       = _data_manager->at( p );
        auto display_name = QString::fromStdString( _data_manager->getPreferedName( person ) );
        auto blood_node   = std::make_unique<BloodNode>( this,
                                                         person._id,
                                                         display_name,
                                                         person._gender_biological,
                                                         person._gender_identity
        );
        auto node_it      = _nodes.insert( typename NodeMap_t::value_type( p, std::move( blood_node ) ) ).first;
        _scene->addItem( new BloodEdge( node_it->second.get(),
                                        current,
                                        BloodEdge::EdgeListPos::BACK,
                                        BloodEdge::EdgeType::BLOOD )
        );
        node_it->second->setZValue( 0.1 );
        LOG_TRACE( "[relations::Ui::BloodGraph::createAncestors( BloodNode * )] "
                       "Created Person [", current->id(), "]'s Parent [", p, "]." );
        createAncestors( node_it->second.get() );
    }
    //Link up couples
    auto couples = _bloodline->generateUniquePairs( *parents );
    for( auto c : *couples ) {
        if( _data_manager->exists( c.first, _couple_type->_id, c.second ) || _data_manager->exists( c.second, _couple_type->_id, c.first ) ) {
            auto first  = _nodes.find( c.first );
            auto second = _nodes.find( c.second );
            if( first != _nodes.end() && second != _nodes.end() ) {
                _scene->addItem( new BloodEdge( first->second.get(),
                                                second->second.get(),
                                                BloodEdge::EdgeListPos::FRONT,
                                                BloodEdge::EdgeType::PARTNER )
                );
                LOG_TRACE( "[relations::Ui::BloodGraph::createAncestors( BloodNode * )] "
                               "Couple [", c.first, "] and [", c.second, "] edge created in BloodGraph." );
            } else {
                LOG_ERROR( "[relations::Ui::BloodGraph::createAncestors( BloodNode * )] "
                               "BloodNode(s) of couple [", c.first, "] and [", c.second, "] not in BloodGraph." );
            }
        }
    }
}

/**
 * Creates descendant nodes recursively
 * @param current Starting subject node
 */
void BloodGraph::createDescendants( BloodNode *current ) {
    auto children = _bloodline->getChildren( current->id() );
    for( auto c : *children ) {
        auto person       = _data_manager->at( c );
        auto display_name = QString::fromStdString( _data_manager->getPreferedName( person ) );
        auto blood_node   = std::make_unique<BloodNode>( this,
                                                         person._id,
                                                         display_name,
                                                         person._gender_biological,
                                                         person._gender_identity
        );
        auto node_it      = _nodes.insert( typename NodeMap_t::value_type( c, std::move( blood_node ) ) ).first;
        _scene->addItem( new BloodEdge( current,
                                        node_it->second.get(),
                                        BloodEdge::EdgeListPos::BACK,
                                        BloodEdge::EdgeType::BLOOD )
        );
        node_it->second->setZValue( 0.1 );
        LOG_TRACE( "[relations::Ui::BloodGraph::createDescendants( BloodNode * )] "
                       "Created Person [", current->id(), "]'s Parent [", c, "]." );
        createDescendants( node_it->second.get() );
    }
}

/**
 * Creates partners of a subject node
 * @param current Subject node
 * @return List of partners BloodNodes created
 */
std::unique_ptr<std::list<BloodNode *>> BloodGraph::createPartners( BloodNode *current ) {
    auto level_nodes = std::make_unique<std::list<BloodNode *>>();
    auto partners    = _bloodline->getPartners( current->id() );
    size_t count = 0;
    for( auto p : *partners ) {
        auto person       = _data_manager->at( p );
        auto display_name = QString::fromStdString( _data_manager->getPreferedName( person ) );
        auto partner_node = std::make_unique<BloodNode>( this,
                                                         person._id,
                                                         display_name,
                                                         person._gender_biological,
                                                         person._gender_identity
        );
        auto partner_node_it = _nodes.insert( typename NodeMap_t::value_type( p, std::move( partner_node ) ) ).first;
        partner_node_it->second->setZValue( 0.1 );

        if( count % 2 ) { //Left of subject
            _scene->addItem( new BloodEdge( current,
                                            partner_node_it->second.get(),
                                            BloodEdge::EdgeListPos::FRONT,
                                            BloodEdge::EdgeType::PARTNER )
            );
            level_nodes->emplace_front( partner_node_it->second.get() );
        } else { //Right of subject
            _scene->addItem( new BloodEdge( current,
                                            partner_node_it->second.get(),
                                            BloodEdge::EdgeListPos::BACK,
                                            BloodEdge::EdgeType::PARTNER )
            );
            level_nodes->emplace_back( partner_node_it->second.get() );
        }

        //Adds partner->blood child edge if blood child exists already in graph
        auto partner_children = _bloodline->getChildren( p );
        for( auto c : *partner_children ) {
            auto child = _nodes.find( c );
            if( child != _nodes.end() ) {
                _scene->addItem( new BloodEdge( partner_node_it->second.get(),
                                                child->second.get(),
                                                BloodEdge::EdgeListPos::BACK,
                                                BloodEdge::EdgeType::BLOOD )
                );
                LOG_TRACE( "[relations::Ui::BloodGraph::createPartners( BloodNode * )] "
                               "Created Person [", p, "] child link to [", c, "]." );
            }
        }

        LOG_TRACE( "[relations::Ui::BloodGraph::createPartners( BloodNode * )] "
                       "Created Person [", current->id(), "]'s partner [", p, "]." );
        count++;
    }
    return std::move( level_nodes );
}

/**
 * Creates Sibling nodes for a Subject
 * @param current Subject node
 * @return List of sibling BloodNodes created
 */
std::unique_ptr<std::list<BloodNode *>> BloodGraph::createSiblings( BloodNode *current ) {
    auto level_nodes = std::make_unique<std::list<BloodNode *>>();
    auto parents     = _bloodline->getParents( current->id() );
    auto siblings    = _bloodline->getSiblings( current->id(), *parents );
    size_t mid_point_full = siblings->_full.size() / 2; //Cut full sibling list in half
    size_t mid_poind_half = siblings->_half.size() / 2; //Cut half sibling list in half

    //Create a list of (weak) pointers to the BloodNodes of the parents
    std::list<BloodNode *> parent_nodes;
    for( auto p : *parents ) {
        auto node_it = _nodes.find( p );
        if( node_it !=  _nodes.end() ) {
            parent_nodes.emplace_back( node_it->second.get() );
        } else {
            LOG_ERROR( "[BloodGraph::createLeveledNodes( BloodNode * )] "
                           "Parent [", p, "] of BloodNode [", current->id(), "] has not been created yet." );
            return std::move( level_nodes );
        }
    }
    //Full sibling first half in reverse: put half the siblings to the left of the subject
    for( auto it = std::next( siblings->_full.rbegin(), mid_point_full ); it != siblings->_full.rend(); ++it ) {
        auto person       = _data_manager->at( *it );
        auto display_name = QString::fromStdString( _data_manager->getPreferedName( person ) );
        auto sibling_node = std::make_unique<BloodNode>( this,
                                                         person._id,
                                                         display_name,
                                                         person._gender_biological,
                                                         person._gender_identity
        );
        auto node_it      = _nodes.insert( typename NodeMap_t::value_type( person._id, std::move( sibling_node ) ) ).first;
        level_nodes->emplace_front( node_it->second.get() );
        LOG_TRACE( "[relations::Ui::BloodGraph::createLeveledNodes( BloodNode * )] "
                       "Created Person [", current->id(), "]'s Full-sibling [", person._id, "]." );
        //Connect to all biological parents
        for( auto parent_node : parent_nodes ) {
            _scene->addItem( new BloodEdge( parent_node,
                                            node_it->second.get(),
                                            BloodEdge::EdgeListPos::FRONT,
                                            BloodEdge::EdgeType::BLOOD )
            );
        }
        node_it->second->setZValue( 0.1 );
    }
    //Hlaf sibling first half in reverse: put half the siblings to the left of the subject
    for( auto it = std::next( siblings->_half.rbegin(), mid_poind_half ); it != siblings->_half.rend(); ++it ) {
        auto person       = _data_manager->at( it->first );
        auto display_name = QString::fromStdString( _data_manager->getPreferedName( person ) );
        auto sibling_node = std::make_unique<BloodNode>( this,
                                                         person._id,
                                                         display_name,
                                                         person._gender_biological,
                                                         person._gender_identity
        );
        auto node_it      = _nodes.insert( typename NodeMap_t::value_type( person._id, std::move( sibling_node ) ) ).first;
        level_nodes->emplace_front( node_it->second.get() );
        LOG_TRACE( "[relations::Ui::BloodGraph::createLeveledNodes( BloodNode * )] "
                       "Created Person [", current->id(), "]'s Half-sibling [", person._id, "]." );
        //Connect to all biological parents
        for( auto parent_node : parent_nodes ) {
            if( parent_node->id() == it->second ) {
                _scene->addItem( new BloodEdge( parent_node,
                                                node_it->second.get(),
                                                BloodEdge::EdgeListPos::FRONT,
                                                BloodEdge::EdgeType::BLOOD )
                );
            }
        }
        node_it->second->setZValue( 0.1 );
    }
    //Full siblings second half forward: put the other half of the siblings to the right of the subject
    for( auto it = std::next( siblings->_full.begin(), siblings->_full.size() - mid_point_full ); it != siblings->_full.end(); ++it ) {
        auto person       = _data_manager->at( *it );
        auto display_name = QString::fromStdString( _data_manager->getPreferedName( person ) );
        auto sibling_node = std::make_unique<BloodNode>( this,
                                                         person._id,
                                                         display_name,
                                                         person._gender_biological,
                                                         person._gender_identity
        );
        auto node_it      = _nodes.insert( typename NodeMap_t::value_type( person._id, std::move( sibling_node ) ) ).first;
        level_nodes->emplace_back( node_it->second.get() );
        LOG_TRACE( "[relations::Ui::BloodGraph::createLeveledNodes( BloodNode * )] "
                       "Created Person [", current->id(), "]'s Full-sibling [", person._id, "]." );
        //Connect to all biological parents
        for( auto parent_node : parent_nodes ) {
            _scene->addItem( new BloodEdge( parent_node,
                                            node_it->second.get(),
                                            BloodEdge::EdgeListPos::BACK,
                                            BloodEdge::EdgeType::BLOOD )
            );
        }
        node_it->second->setZValue( 0.1 );
    }
    //Half siblings second half forward: put the other half of the siblings to the right of the subject
    for( auto it = std::next( siblings->_half.begin(), siblings->_half.size() - mid_poind_half ); it != siblings->_half.end(); ++it ) {
        auto person       = _data_manager->at( it->first );
        auto display_name = QString::fromStdString( _data_manager->getPreferedName( person ) );
        auto sibling_node = std::make_unique<BloodNode>( this,
                                                         person._id,
                                                         display_name,
                                                         person._gender_biological,
                                                         person._gender_identity
        );
        auto node_it      = _nodes.insert( typename NodeMap_t::value_type( person._id, std::move( sibling_node ) ) ).first;
        level_nodes->emplace_back( node_it->second.get() );
        LOG_TRACE( "[relations::Ui::BloodGraph::createLeveledNodes( BloodNode * )] "
                       "Created Person [", current->id(), "]'s Half-sibling [", person._id, "]." );
        //Connect to all biological parents
        for( auto parent_node : parent_nodes ) {
            if( parent_node->id() == it->second ) {
                _scene->addItem( new BloodEdge( parent_node,
                                                node_it->second.get(),
                                                BloodEdge::EdgeListPos::BACK,
                                                BloodEdge::EdgeType::BLOOD )
                );
            }
        }
        node_it->second->setZValue( 0.1 );
    }
    return std::move( level_nodes );
}

/**
 * Positions ancestor nodes recursively
 * @param current Starting subject node
 */
void BloodGraph::positionAncestors( BloodNode *current ) {
    auto max_tree_width = current->maxParentWidth();
    auto parent_edges   = current->parentEdges();
    QPointF current_pos = current->pos();
    QPointF left_offset = current_pos - QPointF( ( ( max_tree_width * _pos_offset.x ) / 2 ), _pos_offset.y );
    //Position nodes
    for( auto e : parent_edges ) {
        auto parent_max_width = e->sourceNode()->maxParentWidth();
        QPointF node_pos = left_offset + QPointF( ( ( parent_max_width * _pos_offset.x ) / 2 ), 0 );
        e->sourceNode()->setPos( node_pos );
        left_offset = left_offset + QPointF( parent_max_width * _pos_offset.x, 0 );
    }
    //Recurse on next level of the sub-graph
    for( BloodEdge * n : parent_edges ) {
        positionAncestors( n->sourceNode() );
    }
}

/**
 * Positions descendant nodes recursively
 * @param current Starting subject node
 */
void BloodGraph::positionDescendants( BloodNode *current ) {
    auto max_tree_width = current->maxChildWidth();
    auto children_edges = current->childEdges();
    QPointF current_pos = current->pos();
    QPointF left_offset = current_pos - QPointF( ( ( max_tree_width * _pos_offset.x ) / 2 ), -_pos_offset.y );
    //Position nodes
    for( auto e : children_edges ) {
        auto child_max_width = e->destNode()->maxChildWidth();
        QPointF node_pos = left_offset + QPointF( ( ( child_max_width * _pos_offset.x ) / 2 ), 0 );
        e->destNode()->setPos( node_pos );
        left_offset = left_offset + QPointF( child_max_width * _pos_offset.x, 0 );
    }
    //Recurse on next level of the sub-graph
    for( BloodEdge * n : children_edges ) {
        positionDescendants( n->destNode() );
    }
}

/**
 * Positions siblings and partners of a subject on a row
 * @param current       Subject node
 * @param partner_nodes List of partner BloodNodes
 * @param sibling_nodes List of sibling BloodNodes
 */
void BloodGraph::positionLevelRelations( BloodNode *current,
                                         std::unique_ptr<std::list<BloodNode *>> partner_nodes,
                                         std::unique_ptr<std::list<BloodNode *>> sibling_nodes )
{
    auto parent_edges   = current->parentEdges();
    QPointF current_pos = current->pos();
    QPointF left_pos    = current_pos - QPointF( _pos_offset.x , 0 );
    QPointF right_pos   = current_pos + QPointF(  _pos_offset.x , 0 );

    //Partners
    size_t mid_point = partner_nodes->size() / 2; //Cut full sibling list in half
    //First half in reverse: put half the partners to the left of the subject
    for( auto it = std::next( partner_nodes->rbegin(), mid_point ); it != partner_nodes->rend(); ++it ) {
        ( *it )->setPos( left_pos );
        left_pos = left_pos - QPointF( _pos_offset.x , 0 );
    }
    //Second half forward: put the other half of the partners to the right of the subject
    for( auto it = std::next( partner_nodes->begin(), partner_nodes->size() - mid_point ); it != partner_nodes->end(); ++it ) {
        ( *it )->setPos( right_pos );
        right_pos = right_pos + QPointF( _pos_offset.x , 0 );
    }

    //Siblings
    mid_point = sibling_nodes->size() / 2; //Cut full sibling list in half
    //First half in reverse: put half the siblings to the left of the subject
    for( auto it = std::next( sibling_nodes->rbegin(), mid_point ); it != sibling_nodes->rend(); ++it ) {
        ( *it )->setPos( left_pos );
        left_pos = left_pos - QPointF( _pos_offset.x , 0 );
    }
    //Second half forward: put the other half of the siblings to the right of the subject
    for( auto it = std::next( sibling_nodes->begin(), sibling_nodes->size() - mid_point ); it != sibling_nodes->end(); ++it ) {
        ( *it )->setPos( right_pos );
        right_pos = right_pos + QPointF( _pos_offset.x , 0 );
    }
}