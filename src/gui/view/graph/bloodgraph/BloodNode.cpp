#include "BloodNode.h"
#include <QGraphicsSceneMouseEvent>
#include <QStyleOption>
#include <QtSvg/QSvgRenderer>

BloodNode::BloodNode( BloodGraph *blood_graph,
                      const int64_t &person_id,
                      const QString &display_name,
                      const relations::enums::GenderBioType &gender,
                      const relations::enums::GenderIdentityType &gender_identity ) :
    _graph( blood_graph ),
    _id( person_id ),
    _name( display_name )
{
    using relations::enums::GenderBioType;
    using relations::enums::GenderIdentityType;
    if( gender_identity == GenderIdentityType::UNSPECIFIED ) {
        switch( gender ) {
            case GenderBioType::MALE:
                _gender_colour = QColor( Qt::GlobalColor::blue );
                break;
            case GenderBioType::FEMALE:
                _gender_colour = QColor( Qt::GlobalColor::red );
                break;
            case GenderBioType::INTERSEX:
                _gender_colour = QColor( Qt::GlobalColor::cyan );
                break;
            case GenderBioType::UNKNOWN:
                _gender_colour = QColor( Qt::GlobalColor::gray );
                break;
        }
    } else {
        _gender_colour = QColor( Qt::GlobalColor::green );
    }
    setFlag( ItemIsMovable );
    setFlag( ItemSendsGeometryChanges );
    setCacheMode( DeviceCoordinateCache );
    setZValue( -1 );
}

void BloodNode::addEdge( BloodEdge *edge ) {
    if( edge->destNode() == this )
        _parent_edges << edge;
    else
        _child_edges << edge;
    edge->adjust();
}

void BloodNode::insertEdgeFront( BloodEdge *edge ) {
    if( edge->destNode() == this )
        _parent_edges.push_front( edge );
    else
        _child_edges.push_front( edge );
    edge->adjust();
}

void BloodNode::insertEdgeBack( BloodEdge *edge ) {
    if( edge->destNode() == this )
        _parent_edges.push_back( edge );
    else
        _child_edges.push_back( edge );
    edge->adjust();
}

void BloodNode::insertLevelEdgeFront( BloodEdge *edge ) {
    _level_edges.push_front( edge );
    edge->adjust();
}

void BloodNode::insertLevelEdgeBack( BloodEdge *edge ) {
    _level_edges.push_back( edge );
}

QList<BloodEdge *> BloodNode::childEdges() const {
    return _child_edges;
}

QList<BloodEdge *> BloodNode::parentEdges() const {
    return _parent_edges;
}

QList<BloodEdge *> BloodNode::levelEdges() const {
    return _level_edges;
}

int64_t BloodNode::id() const {
    return _id;
}

int BloodNode::maxChildWidth() const {
    auto local = _child_edges.size();
    if( local == 0 ) { //Leaf, return own weight
        return 1;
    } else {
        auto total = 0;
        for( auto e : _child_edges ) {
            if( e->destNode() )
                total += e->destNode()->maxChildWidth();
        }
        return local > total ? local : total;
    }
}

int BloodNode::maxParentWidth() const {
    auto local = _parent_edges.size();
    if( local == 0 ) { //Leaf, return own weight
        return 1;
    } else {
        auto total = 0;
        for( auto e : _parent_edges ) {
            if( e->sourceNode() )
                total += e->sourceNode()->maxParentWidth();
        }
        return local > total ? local : total;
    }
}

QRectF BloodNode::boundingRect() const {
    qreal adjust = 2;
    return QRectF( -48 - adjust, -48 - adjust, 96 + adjust, 96 + adjust );
}

QPainterPath BloodNode::shape() const {
    QPainterPath path;
    path.addRect( -48, -48, 96, 96 );
    return path;
}

void BloodNode::paint( QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget ) {
    //Portrait outline
    painter->setPen( QPen( _gender_colour, 1 ) );
    painter->drawRect( -32, -48, 64, 64 );
    //Portrait icon
    QSvgRenderer renderer( QString( ":/resource/open-iconic/person.svg" ) );
    QImage image(64, 64, QImage::Format_ARGB32);
    image.fill(0);
    painter->drawImage( QRect( -32, -48, 64, 64), image );
    renderer.render( painter, QRectF( -32, -48, 64, 64 ) );
    //Person ID
    painter->setPen( QPen( Qt::white, 0 ) );
    painter->drawText(QRect( -32, -48, 64, 32 ), Qt::AlignCenter, QString::number( _id ) );
    //Display name
    painter->setPen( QPen( Qt::black, 0 ) );
    painter->drawText(QRect( -48, 18, 96, 30 ), Qt::AlignCenter, _name );
}

QVariant BloodNode::itemChange( QGraphicsItem::GraphicsItemChange change, const QVariant &value ) {
    switch( change ) {
        case ItemPositionHasChanged:
            foreach( BloodEdge *edge, _child_edges ) edge->adjust();
            foreach( BloodEdge *edge, _parent_edges ) edge->adjust();
            foreach( BloodEdge *edge, _level_edges ) edge->adjust();
            break;
        default:
            break;
    };
    return QGraphicsItem::itemChange( change, value );
}

void BloodNode::mousePressEvent( QGraphicsSceneMouseEvent *event ) {
    update();
    QGraphicsItem::mousePressEvent( event );
}

void BloodNode::mouseReleaseEvent( QGraphicsSceneMouseEvent *event ) {
    update();
    QGraphicsItem::mouseReleaseEvent( event );
}
