#include "BloodEdge.h"
#include <math.h>

static const double PI = M_PI;
static double TWO_PI = 2.0 * M_PI;

BloodEdge::BloodEdge( BloodNode *sourceNode,
                      BloodNode *destNode,
                      BloodEdge::EdgeListPos list_pos,
                      BloodEdge::EdgeType edge_type ) :
    _arrow_size( 10 )
{
    setAcceptedMouseButtons( 0 );
    _source = sourceNode;
    _dest   = destNode;
    switch( edge_type ) {
        case BloodEdge::EdgeType::BLOOD:
            _line_type = BloodEdge::Line::UNIDIRECTIONAL_SOLID;
            switch( list_pos ) {
                case BloodEdge::EdgeListPos::FRONT:
                    _source->insertEdgeFront( this );
                    _dest->insertEdgeFront( this );
                    break;
                case BloodEdge::EdgeListPos::BACK:
                    _source->insertEdgeBack( this );
                    _dest->insertEdgeBack( this );
                    break;
            }
            break;
        case BloodEdge::EdgeType::PARTNER:
            _line_type = BloodEdge::Line::BIDIRECTIONAL_DOTTED;
            switch( list_pos ) {
                case BloodEdge::EdgeListPos::FRONT:
                    _source->insertLevelEdgeFront( this );
                    _dest->insertLevelEdgeFront( this );
                    break;
                case BloodEdge::EdgeListPos::BACK:
                    _source->insertLevelEdgeBack( this );
                    _dest->insertLevelEdgeBack( this );
                    break;
            }
            break;
    }
    adjust();
}

BloodNode * BloodEdge::sourceNode() const {
    return _source;
}

BloodNode * BloodEdge::destNode() const {
    return _dest;
}

void BloodEdge::adjust() {
    if( !_source || !_dest )
        return;

    QLineF line( mapFromItem( _source, 0, 0 ), mapFromItem( _dest, 0, 0 ) );
    qreal length = line.length();

    prepareGeometryChange();

    if( length > qreal( 10. ) ) {
        QPointF edgeOffset( ( line.dx() * 48 ) / length, ( line.dy() * 48 ) / length );
        sourcePoint = line.p1() + edgeOffset;
        destPoint = line.p2() - edgeOffset;
    } else {
        sourcePoint = destPoint = line.p1();
    }
}

QRectF BloodEdge::boundingRect() const {
    if( !_source || !_dest )
        return QRectF();

    qreal penWidth = 1;
    qreal extra = ( penWidth + _arrow_size ) / 2.0;

    return QRectF( sourcePoint, QSizeF( destPoint.x() - sourcePoint.x(),
                                        destPoint.y() - sourcePoint.y() ) )
        .normalized()
        .adjusted( -extra, -extra, extra, extra );
}

void BloodEdge::paint( QPainter *painter, const QStyleOptionGraphicsItem *, QWidget * ) {
    if( !_source || !_dest )
        return;

    QLineF line( sourcePoint, destPoint );
    if( qFuzzyCompare( line.length(), qreal( 0. ) ) )
        return;

    double angle = ::acos( line.dx() / line.length() );
    if( line.dy() >= 0 )
        angle = TWO_PI - angle;

    QPointF destArrowP1, destArrowP2;
    // Draw the line itself
    switch( _line_type ) {
        case Line::UNIDIRECTIONAL_SOLID:
            painter->setPen( QPen( Qt::black, 1, Qt::SolidLine, Qt::RoundCap, Qt::RoundJoin ) );
            painter->drawLine( line );
            destArrowP1 = destPoint + QPointF( sin( angle - PI / 3 ) * _arrow_size,
                                               cos( angle - PI / 3 ) * _arrow_size );
            destArrowP2 = destPoint + QPointF( sin( angle - PI + PI / 3 ) * _arrow_size,
                                               cos( angle - PI + PI / 3 ) * _arrow_size );

            painter->setBrush( Qt::black );
            painter->drawPolygon( QPolygonF() << line.p1() );
            painter->drawPolygon( QPolygonF() << line.p2() << destArrowP1 << destArrowP2 );
            break;
        case Line::UNIDIRECTIONAL_DOTTED:
            painter->setPen( QPen( Qt::black, 1, Qt::DotLine, Qt::RoundCap, Qt::RoundJoin ) );
            painter->drawLine( line );
            destArrowP1 = destPoint + QPointF( sin( angle - PI / 3 ) * _arrow_size,
                                               cos( angle - PI / 3 ) * _arrow_size );
            destArrowP2 = destPoint + QPointF( sin( angle - PI + PI / 3 ) * _arrow_size,
                                               cos( angle - PI + PI / 3 ) * _arrow_size );

            painter->setBrush( Qt::black );
            painter->drawPolygon( QPolygonF() << line.p1() );
            painter->drawPolygon( QPolygonF() << line.p2() << destArrowP1 << destArrowP2 );
            break;
        case Line::BIDIRECTIONAL_DOTTED:
            painter->setPen( QPen( Qt::black, 1, Qt::DotLine, Qt::RoundCap, Qt::RoundJoin ) );
            painter->drawLine( line );
            painter->setBrush( Qt::black );
            painter->drawPolygon( QPolygonF() << line.p1() );
            painter->drawPolygon( QPolygonF() << line.p2() );
            break;
        case Line::BIDIRECTIONAL_SOLID:
            painter->setPen( QPen( Qt::black, 1, Qt::SolidLine, Qt::RoundCap, Qt::RoundJoin ) );
            painter->drawLine( line );
            painter->setBrush( Qt::black );
            painter->drawPolygon( QPolygonF() << line.p1() );
            painter->drawPolygon( QPolygonF() << line.p2() );
            break;
    }
}