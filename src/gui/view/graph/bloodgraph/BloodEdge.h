#ifndef RELATIONS_BLOODGRAPHEDGE_H
#define RELATIONS_BLOODGRAPHEDGE_H

#include <QGraphicsItem>
#include "BloodNode.h"
#include "BloodGraph.h"

class BloodNode;

namespace relations::Ui {
    class BloodEdge;
}

class BloodEdge : public QGraphicsItem {
  public:
    enum class EdgeListPos { FRONT, BACK };
    enum class EdgeType { BLOOD, PARTNER };
    enum class Line {
        UNIDIRECTIONAL_SOLID,
        UNIDIRECTIONAL_DOTTED,
        BIDIRECTIONAL_SOLID,
        BIDIRECTIONAL_DOTTED
    };

    BloodEdge( BloodNode *sourceNode,
               BloodNode *destNode,
               EdgeListPos list_pos,
               EdgeType line );

    BloodNode *sourceNode() const;
    BloodNode *destNode() const;

    void adjust();

    enum { Type = UserType + 4 };
    int type() const override { return Type; }

  protected:
    QRectF boundingRect() const override;
    void paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget) override;

  private:
    Line      _line_type;
    BloodNode *_source, *_dest;
    QPointF   sourcePoint;
    QPointF   destPoint;
    qreal     _arrow_size;
};

#endif //RELATIONS_BLOODGRAPHEDGE_H
