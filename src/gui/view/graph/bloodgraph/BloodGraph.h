#ifndef RELATIONS_BLOODGRAPH_H
#define RELATIONS_BLOODGRAPH_H

#include <QGraphicsView>
#include <eadlib/logger/Logger.h>
#include <src/algorithm/BloodLine.h>
#include <src/algorithm/containers/Position.h>
#include "src/io/DataManager.h"
#include "src/gui/view/graph/bloodgraph/BloodNode.h"
#include "src/gui/view/graph/bloodgraph/BloodEdge.h"

class BloodNode;
class BloodEdge;

namespace relations::Ui {
    class BloodGraph;
}

class BloodGraph : public QGraphicsView {
  Q_OBJECT

  public:
    typedef std::unordered_map<int64_t, std::unique_ptr<BloodNode>> NodeMap_t;
    BloodGraph( QWidget *parent,
                std::shared_ptr<relations::io::DataManager> data_manager );

  signals:
    void showProfileSignal( int64_t );

  public slots:
    void zoomIn();
    void zoomOut();
    bool loadGraph( const int64_t &subject_id );
    void clearGraph();
    void goToProfile( BloodNode *new_subject );
    void goToEvents();
    void goToMedia();

  private slots:
    void centerOnNewSubject( BloodNode *subject );

  protected:
    void keyPressEvent( QKeyEvent *event ) override;

#ifndef QT_NO_WHEELEVENT
    void wheelEvent( QWheelEvent *event ) override;
#endif

    void drawBackground( QPainter *painter, const QRectF &rect ) override;
    void scaleView( qreal scaleFactor );
    void resizeEvent( QResizeEvent *event ) override;
    void mousePressEvent( QMouseEvent *event ) override;

  private:
    //BloodNode creation methods
    void createAncestors( BloodNode *current );
    void createDescendants( BloodNode *current );
    std::unique_ptr<std::list<BloodNode *>> createPartners( BloodNode *current );
    std::unique_ptr<std::list<BloodNode *>> createSiblings( BloodNode *current );
    //Positioning methods
    void positionAncestors( BloodNode *current );
    void positionDescendants( BloodNode *current );
    void positionLevelRelations( BloodNode *current,
                                 std::unique_ptr<std::list<BloodNode *>> partner_nodes,
                                 std::unique_ptr<std::list<BloodNode *>> sibling_nodes );
    //Private variables
    const relations::Position<int64_t>          _pos_offset { 128, 128 };
    QGraphicsScene                             *_scene;
    BloodNode                                  *_subject_node;
    std::shared_ptr<relations::io::DataManager> _data_manager;
    NodeMap_t                                   _nodes;

    std::unique_ptr<relations::dto::RelationshipType> _couple_type;
    std::unique_ptr<relations::dto::RelationshipType> _parentchild_type;
    std::unique_ptr<relations::algo::BloodLine>       _bloodline;

};

#endif //RELATIONS_BLOODGRAPH_H
