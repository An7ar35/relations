#include "GraphNode.h"

#include <QGraphicsSceneMouseEvent>
#include <QStyleOption>
#include <QtSvg/QSvgRenderer>

GraphNode::GraphNode( GraphWidget *graphWidget,
                      const int64_t &person_id,
                      const QString &display_name,
                      const relations::enums::GenderBioType &gender,
                      const relations::enums::GenderIdentityType &gender_identity ) :
    _graph( graphWidget ),
    _id( person_id ),
    _name( display_name )
{
    using relations::enums::GenderBioType;
    using relations::enums::GenderIdentityType;
    if( gender_identity == GenderIdentityType::UNSPECIFIED ) {
        switch( gender ) {
            case GenderBioType::MALE:
                _gender_colour = QColor( Qt::GlobalColor::blue );
                break;
            case GenderBioType::FEMALE:
                _gender_colour = QColor( Qt::GlobalColor::red );
                break;
            case GenderBioType::INTERSEX:
                _gender_colour = QColor( Qt::GlobalColor::cyan );
                break;
            case GenderBioType::UNKNOWN:
                _gender_colour = QColor( Qt::GlobalColor::gray );
                break;
        }
    } else {
        _gender_colour = QColor( Qt::GlobalColor::green );
    }
    //TODO assign preferred name to Node
    setFlag( ItemIsMovable );
    setFlag( ItemSendsGeometryChanges );
    setCacheMode( DeviceCoordinateCache );
    setZValue( -1 );
}

int64_t GraphNode::id() const {
    return _id;
}

void GraphNode::addEdge( GraphEdge *edge ) {
    _edges << edge;
    edge->adjust();
}

QList<GraphEdge *> GraphNode::edges() const {
    return _edges;
}

void GraphNode::calculateForces() {
    if( !scene() || scene()->mouseGrabberItem() == this ) {
        _new_position = pos();
        return;
    }

    // Sum up all forces pushing this item away
    qreal xvel = 0;
    qreal yvel = 0;
        foreach ( QGraphicsItem *item, scene()->items() ) {
            GraphNode *node = qgraphicsitem_cast<GraphNode *>( item );
            if( !node )
                continue;

            QPointF vec = mapToItem( node, 0, 0 );
            qreal dx = vec.x();
            qreal dy = vec.y();
            double l = 2.0 * ( dx * dx + dy * dy );
            if( l > 0 ) {
                xvel += ( dx * 150.0 ) / l;
                yvel += ( dy * 150.0 ) / l;
            }
        }

    // Now subtract all forces pulling items together
    double weight = ( _edges.size() + 1 ) * 10;
        foreach ( GraphEdge *edge, _edges ) {
            QPointF vec;
            if( edge->sourceNode() == this )
                vec = mapToItem( edge->destNode(), 0, 0 );
            else
                vec = mapToItem( edge->sourceNode(), 0, 0 );
            xvel -= vec.x() / weight;
            yvel -= vec.y() / weight;
        }

    if( qAbs( xvel ) < 0.1 && qAbs( yvel ) < 0.1 )
        xvel = yvel = 0;

    QRectF sceneRect = scene()->sceneRect();
    _new_position = pos() + QPointF( xvel, /*yvel*/ 0 );
    //_new_position.setX( qMin( qMax( _new_position.x(), sceneRect.left() + 10 ), sceneRect.right() - 10 ) );
    _new_position.setY( qMin( qMax( _new_position.y(), sceneRect.top() + 128 ), sceneRect.bottom() - 128 ) );
}

bool GraphNode::advance() {
    if( _new_position == pos() )
        return false;

    setPos( _new_position );
    return true;
}

QRectF GraphNode::boundingRect() const {
    qreal adjust = 2;
//    return QRectF( -10 - adjust, -10 - adjust, 23 + adjust, 23 + adjust );
    return QRectF( -48 - adjust, -48 - adjust, 96 + adjust, 96 + adjust );
}

/**
 * Shape of the clickable area
 * @return Shape of Node
 */
QPainterPath GraphNode::shape() const {
    QPainterPath path;
//    path.addEllipse( -10, -10, 20, 20 );
    path.addRect( -48, -48, 96, 96 );
    return path;
}

void GraphNode::paint( QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget * ) {
    //Portrait outline
    painter->setPen( QPen( _gender_colour, 1 ) );
    painter->drawRect( -32, -48, 64, 64 );
    //Portrait icon
    QSvgRenderer renderer( QString( ":/resource/open-iconic/person.svg" ) );
    QImage image(64, 64, QImage::Format_ARGB32);
    image.fill(0);
    painter->drawImage( QRect( -32, -48, 64, 64), image );
    renderer.render( painter, QRectF( -32, -48, 64, 64 ) );
    //Person ID
    painter->setPen( QPen( Qt::white, 0 ) );
    painter->drawText(QRect( -32, -48, 64, 32 ), Qt::AlignCenter, QString::number( _id ) );
    //Display name
    painter->setPen( QPen( Qt::black, 0 ) );
    painter->drawText(QRect( -48, 18, 96, 30 ), Qt::AlignCenter, _name );

    //Helper box
//    painter->setPen( QPen( Qt::green, 2 ) );
//    painter->drawRect(-48, -48, 96, 96 );
}

QVariant GraphNode::itemChange( GraphicsItemChange change, const QVariant &value ) {
    switch( change ) {
        case ItemPositionHasChanged:
                foreach ( GraphEdge *edge, _edges )edge->adjust();
            _graph->itemMoved();
            break;
        default:
            break;
    };

    return QGraphicsItem::itemChange( change, value );
}

void GraphNode::mousePressEvent( QGraphicsSceneMouseEvent *event ) {
    update();
    QGraphicsItem::mousePressEvent( event );
}

void GraphNode::mouseReleaseEvent( QGraphicsSceneMouseEvent *event ) {
    update();
    QGraphicsItem::mouseReleaseEvent( event );
}
