#ifndef RELATIONS_GRAPHNODE_H
#define RELATIONS_GRAPHNODE_H

#include <QGraphicsItem>
#include "GraphWidget.h"
#include "GraphEdge.h"

class QGraphicsSceneMouseEvent;

class GraphWidget;
class GraphEdge;

namespace relations::Ui {
    class GraphNode;
}

class GraphNode : public QGraphicsItem {
  public:
    GraphNode( GraphWidget *graphWidget,
               const int64_t &person_id,
               const QString &display_name,
               const relations::enums::GenderBioType &gender,
               const relations::enums::GenderIdentityType &gender_identity );

    void addEdge( GraphEdge *edge );
    QList<GraphEdge *> edges() const;

    int64_t id() const;

    enum { Type = UserType + 1 };

    int type() const override { return Type; }

    void calculateForces();
    bool advance();

    QRectF boundingRect() const override;
    QPainterPath shape() const override;
    void paint( QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget ) override;

  protected:
    QVariant itemChange( GraphicsItemChange change, const QVariant &value ) override;

    void mousePressEvent( QGraphicsSceneMouseEvent *event ) override;
    void mouseReleaseEvent( QGraphicsSceneMouseEvent *event ) override;

  private:
    int64_t            _id;
    QString            _name;
    QColor             _gender_colour;
    QList<GraphEdge *> _edges;
    QPointF            _new_position;
    GraphWidget       *_graph;
};


#endif //RELATIONS_GRAPHNODE_H
