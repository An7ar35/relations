#ifndef RELATIONS_GRAPHWIDGET_H
#define RELATIONS_GRAPHWIDGET_H

#include <QGraphicsView>
#include <QAbstractListModel>
#include <eadlib/logger/Logger.h>
#include "src/io/DataManager.h"
#include "GraphEdge.h"
#include "GraphNode.h"

class GraphEdge;
class GraphNode;

namespace relations::Ui {
    class GraphWidget;
}

//TODO Positional calculations algo
//TODO Add nodes and edges to graph

class GraphWidget : public QGraphicsView {
  Q_OBJECT

  public:
    typedef std::unordered_map<int64_t, std::unique_ptr<GraphNode>> NodeMap_t;
    GraphWidget( QWidget *parent = 0 );
    void itemMoved();

  public slots:
    void shuffle();
    void zoomIn();
    void zoomOut();
    void loadGraph( std::shared_ptr<relations::io::DataManager> data_manager, const int64_t &subject_id );
    void loadGraph( std::shared_ptr<relations::io::DataManager> data_manager, const relations::dto::RelationshipType &relationship_type );
    void clearGraph();
    void goToProfile();
    void goToEvents();
    void goToMedia();

  protected:
    void contextMenuEvent( const QPoint & );
    void keyPressEvent( QKeyEvent *event ) override;
    void timerEvent( QTimerEvent *event ) override;

#ifndef QT_NO_WHEELEVENT
    void wheelEvent( QWheelEvent *event ) override;
#endif
    void drawBackground( QPainter *painter, const QRectF &rect ) override;

    void scaleView( qreal scaleFactor );
    void resizeEvent( QResizeEvent *event ) override;
    void mousePressEvent( QMouseEvent *event ) override;

  private:
    int timerId;
    QGraphicsScene                             *_scene;
    GraphNode                                  *centerNode;
    std::shared_ptr<relations::io::DataManager> _data_manager;
    NodeMap_t                                   _nodes;
};

#endif //RELATIONS_GRAPHWIDGET_H
