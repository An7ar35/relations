#ifndef RELATIONS_GRAPHEDGE_H
#define RELATIONS_GRAPHEDGE_H

#include <QGraphicsItem>
#include "GraphNode.h"

class GraphNode;

namespace relations::Ui {
    class GraphEdge;
}

class GraphEdge : public QGraphicsItem
{
  public:
    enum class Line {
        UNIDIRECTIONAL_SOLID,
        UNIDIRECTIONAL_DOTTED,
        BIDIRECTIONAL_SOLID,
        BIDIRECTIONAL_DOTTED
    };
    GraphEdge(GraphNode *sourceNode, GraphNode *destNode);
    GraphEdge(GraphNode *sourceNode, GraphNode *destNode, const Line &line );

    GraphNode *sourceNode() const;
    GraphNode *destNode() const;

    void adjust();

    enum { Type = UserType + 2 };
    int type() const override { return Type; }

  protected:
    QRectF boundingRect() const override;
    void paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget) override;

  private:
    Line       _line_type;
    GraphNode *source, *dest;
    QPointF    sourcePoint;
    QPointF    destPoint;
    qreal      arrowSize;
};

#endif //RELATIONS_GRAPHEDGE_H
