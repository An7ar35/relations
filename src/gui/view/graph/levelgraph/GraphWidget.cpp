#include "GraphWidget.h"

#include <math.h>
#include <QMenu>
#include <QKeyEvent>
#include <src/algorithm/HierarchyPosition.h>
#include <src/algorithm/BloodLine.h>

GraphWidget::GraphWidget( QWidget *parent ) :
    QGraphicsView( parent ), timerId( 0 )
{
    this->setContextMenuPolicy( Qt::CustomContextMenu );
    connect(this, SIGNAL( customContextMenuRequested( const QPoint & ) ), this, SLOT( ShowContextMenu( const QPoint & ) ) );
    _scene = new QGraphicsScene( this );
    _scene->setItemIndexMethod( QGraphicsScene::NoIndex );
    //_scene->setSceneRect( this->rect() );
    viewport()->childrenRect().setSize( parent->size() );
    _scene->setSceneRect(viewport()->childrenRect());
    setScene( _scene );
    setCacheMode( CacheBackground );
    setViewportUpdateMode( BoundingRectViewportUpdate );
    setRenderHint( QPainter::Antialiasing );
    setTransformationAnchor( AnchorUnderMouse );
    scale( qreal( 0.8 ), qreal( 0.8 ) );
    setMinimumSize( parent->width(), parent->height() );
    //this->fitInView( _scene->sceneRect(), Qt::KeepAspectRatio );
}

void GraphWidget::itemMoved() {
    if( !timerId )
        timerId = startTimer( 1000 / 25 );
}

void GraphWidget::keyPressEvent( QKeyEvent *event ) {
    switch( event->key() ) {
        case Qt::Key_Up:
            centerNode->moveBy( 0, -20 );
            break;
        case Qt::Key_Down:
            centerNode->moveBy( 0, 20 );
            break;
        case Qt::Key_Left:
            centerNode->moveBy( -20, 0 );
            break;
        case Qt::Key_Right:
            centerNode->moveBy( 20, 0 );
            break;
        case Qt::Key_Plus:
            zoomIn();
            break;
        case Qt::Key_Minus:
            zoomOut();
            break;
        case Qt::Key_Space:
        case Qt::Key_Enter:
            shuffle();
            break;
        default:
            QGraphicsView::keyPressEvent( event );
    }
}

void GraphWidget::timerEvent( QTimerEvent *event ) {
//    Q_UNUSED( event );
//
//    QList<GraphNode *> nodes;
//        foreach ( QGraphicsItem *item, scene()->items() ) {
//            if( GraphNode *node = qgraphicsitem_cast<GraphNode *>( item ) )
//                nodes << node;
//        }
//
//        foreach ( GraphNode *node, nodes )node->calculateForces();
//
//    bool itemsMoved = false;
//        foreach ( GraphNode *node, nodes ) {
//            if( node->advance() )
//                itemsMoved = true;
//        }
//
//    if( !itemsMoved ) {
//        killTimer( timerId );
//        timerId = 0;
//    }
}

#ifndef QT_NO_WHEELEVENT

void GraphWidget::wheelEvent( QWheelEvent *event ) {
    scaleView( pow( ( double ) 2, -event->delta() / 240.0 ) );
}

#endif

void GraphWidget::drawBackground( QPainter *painter, const QRectF &rect ) {
    Q_UNUSED( rect );

    // Shadow
    QRectF sceneRect = this->sceneRect();
    //QRectF rightShadow( sceneRect.right(), sceneRect.top() + 5, 5, sceneRect.height() );
    //QRectF bottomShadow( sceneRect.left() + 5, sceneRect.bottom(), sceneRect.width(), 5 );
//    if( rightShadow.intersects( rect ) || rightShadow.contains( rect ) )
//        painter->fillRect( rightShadow, Qt::darkGray );
//    if( bottomShadow.intersects( rect ) || bottomShadow.contains( rect ) )
//        painter->fillRect( bottomShadow, Qt::darkGray );

    // Fill
    QLinearGradient gradient( sceneRect.topLeft(), sceneRect.bottomRight() );
    gradient.setColorAt( 0, Qt::white );
    gradient.setColorAt( 1, Qt::lightGray );
    painter->fillRect( rect.intersected( sceneRect ), gradient );
    painter->setBrush( Qt::NoBrush );
    painter->drawRect( sceneRect );

    // Text
    //QRectF textRect( sceneRect.left() + 4, sceneRect.top() + 4, sceneRect.width() - 4, sceneRect.height() - 4 );
    //QString message( tr( "Click and drag the nodes around, and zoom with the mouse wheel or the '+' and '-' keys" ) );

//    QFont font = painter->font();
//    font.setBold( true );
//    font.setPointSize( 14 );
//    painter->setFont( font );
//    painter->setPen( Qt::lightGray );
//    painter->drawText( textRect.translated( 2, 2 ), message );
//    painter->setPen( Qt::black );
//    painter->drawText( textRect, message );
}

void GraphWidget::scaleView( qreal scaleFactor ) {
    qreal factor = transform().scale( scaleFactor, scaleFactor ).mapRect( QRectF( 0, 0, 1, 1 ) ).width();
    if( factor < 0.07 || factor > 100 )
        return;

    scale( scaleFactor, scaleFactor );
}

void GraphWidget::shuffle() {
        foreach ( QGraphicsItem *item, scene()->items() ) {
            if( qgraphicsitem_cast<GraphNode *>( item ) )
                item->setPos( -150 + qrand() % 300, -150 + qrand() % 300 );
        }
}

void GraphWidget::zoomIn() {
    scaleView( qreal( 1.2 ) );
}

void GraphWidget::zoomOut() {
    scaleView( 1 / qreal( 1.2 ) );
}

/**
 * Loads all nodes with existing 'ParentChild' and 'Couple' types
 * @param data_manager DataManager instance
 */
void GraphWidget::loadGraph( std::shared_ptr<relations::io::DataManager> data_manager,
                             const int64_t &subject_id )
{ //TODO add center subject var to signature
    LOG( "@loadBloodGraph()" );
    _data_manager = data_manager;
    //TODO need to create an index with the height of each nodes. As we don't have a definite root we can perhaps use a +/- relative height number
    // from the starting node (the one picked at the beginning) -> we could use the person subject focus as a anchor positioned in the the middle.
    // This way we can recalculate all nodes in graph if the subject focus changes by transforming each node coordinates by the (x,y) distance
    // changed between the old subject focus node and the new one in the graph. Or, you know, just shift the scene's camera by the offset...
    //TODO create a set of structures holding the node ids of each heights into their own data-structure layer as double linked list nodes that
    // can then be sorted
    auto hierarchy_calculator = relations::algo::HierarchyPosition();
    hierarchy_calculator.init( data_manager ); //TODO check it ran properly (return is true)

    std::unordered_map<int, int> level_positions;
    level_positions.insert( { 0, 0 } ); //TODO maybe add all level when the level count is implemented in the hierarchy algo...


    std::pair<int, int> starting_pos = { 0, 0 };

    //Find relevant relationship types from cache
    auto parent_child = std::find_if( _data_manager->getDataCache()->_relationship_types.begin(),
                                      _data_manager->getDataCache()->_relationship_types.end(),
                                      ( []( const relations::dto::RelationshipType &type ) { return type._value == "ParentChild"; } )
    );
    auto couple = std::find_if( _data_manager->getDataCache()->_relationship_types.begin(),
                                _data_manager->getDataCache()->_relationship_types.end(),
                                ( []( const relations::dto::RelationshipType &type ) { return type._value == "Couple"; } )
    );
    //Load nodes
    for( auto it = _data_manager->cbegin(); it != _data_manager->cend(); ++it ) {
        if( _data_manager->exists( it->second.value, parent_child->_id )
            ||  _data_manager->exists( it->second.value, couple->_id ) )
        {
            auto id         = it->second.value._id;
            auto name       = QString::fromStdString( _data_manager->getPreferedName( it->second.value ) );
            auto gender_bio = it->second.value._gender_biological;
            auto gender_id  = it->second.value._gender_identity;

            try {
                auto node_it = _nodes.insert( typename NodeMap_t::value_type( it->first,
                                                                              std::make_unique<GraphNode>( this, id, name, gender_bio, gender_id ) )
                ).first;

                //node_it->second->setPos( ( pos.x * 128 ), ( pos.y * 128 ) );


                node_it->second->setZValue( 0.1 );
                _scene->addItem( node_it->second.get() );

                auto level = hierarchy_calculator.at( it->first );

                auto pos = level_positions.find( level.y );
                if( pos != level_positions.end() ) {
                    node_it->second->setPos( pos->second, ( level.y * 128 ) );
                    pos->second += 128;
                } else {
                    auto pos_it = level_positions.insert( { level.y, 0 } );
                    node_it->second->setPos( pos_it.first->second, ( level.y * 128 ) );
                    pos_it.first->second += 128;
                }
            } catch( std::out_of_range ) {

            }
        }
    }
    //Load edges
    for( auto parent = _nodes.begin(); parent != _nodes.end(); ++parent ) {
        auto person = _data_manager->at( parent->second->id() );
        for( auto child_id : _data_manager->getChildList( person, *parent_child ) ) { //Load 'Parent/Child' edges
            auto child = _nodes.find( child_id );
            if( child != _nodes.end() ) {
                _scene->addItem( new GraphEdge( parent->second.get(), child->second.get(), GraphEdge::Line::UNIDIRECTIONAL_SOLID ) );
            }
        }
        for( auto child_id : _data_manager->getChildList( person, *couple ) ) { //Load 'Couple' edges
            auto child = _nodes.find( child_id );
            if( child != _nodes.end() ) {
                _scene->addItem( new GraphEdge( parent->second.get(), child->second.get(), GraphEdge::Line::BIDIRECTIONAL_DOTTED ) );
            }
        }
    }

    //_data_manager->getRelationship( 34, 35 );
}

void GraphWidget::loadGraph( std::shared_ptr<relations::io::DataManager> data_manager,
                             const relations::dto::RelationshipType &relationship_type )
{
    //TODO add center subject var to signature
    _data_manager = data_manager;
    auto cached_type = std::find_if( _data_manager->getDataCache()->_relationship_types.begin(),
                                     _data_manager->getDataCache()->_relationship_types.end(),
                                     ( [&relationship_type]( const relations::dto::RelationshipType &type ) { return relationship_type._value == type._value; } )
    );
    //Load nodes
    for( auto it = _data_manager->cbegin(); it != _data_manager->cend(); ++it ) {
        if( _data_manager->exists( it->second.value, cached_type->_id ) ) {

            auto id         = it->second.value._id;
            auto name       = QString::fromStdString( _data_manager->getPreferedName( it->second.value ) );
            auto gender_bio = it->second.value._gender_biological;
            auto gender_id  = it->second.value._gender_identity;

            auto node_it = _nodes.insert( typename NodeMap_t::value_type( it->first,
                                                                          std::make_unique<GraphNode>( this,
                                                                                                       id,
                                                                                                       name,
                                                                                                       gender_bio,
                                                                                                       gender_id ) )
            ).first;

            _scene->addItem( node_it->second.get() );
        }
    }
    //Load edges
    for( auto parent = _nodes.begin(); parent != _nodes.end(); ++parent ) {
        auto person = _data_manager->at( parent->second->id() );
        for( auto child_id : _data_manager->getChildList( person, *cached_type ) ) {
            auto child = _nodes.find( child_id );
            if( child != _nodes.end() ) {
                _scene->addItem( new GraphEdge( parent->second.get(), child->second.get() ) );
            }
        }
    }
}

void GraphWidget::resizeEvent( QResizeEvent *event ) {

    QGraphicsView::resizeEvent(event);
}

void GraphWidget::clearGraph() {
    LOG_DEBUG( "[relations::Ui::GraphWidget::clearGraph()] Clearing graph scene" );
    _nodes.clear();
    _scene->clear();
    viewport()->update();
}

void GraphWidget::contextMenuEvent( const QPoint & position ) {
    QMenu menu( this );
    QAction action_GoToProfile("Profile", this);
    QAction action_GoToEvents("Events", this);
    QAction action_GoToMedia("Media", this);
    connect( &action_GoToProfile, SIGNAL(triggered()), this, SLOT( goToProfile() ) );
    connect( &action_GoToEvents, SIGNAL(triggered()), this, SLOT( goToEvents() ) );
    connect( &action_GoToMedia, SIGNAL(triggered()), this, SLOT( goToMedia() ) );
    menu.addAction( &action_GoToProfile );
    menu.addAction( &action_GoToEvents );
    menu.addAction( &action_GoToMedia );
    menu.exec( mapToGlobal( position ) );
}

void GraphWidget::goToProfile() {
    LOG( "@GraphWidget::goToProfile()");
}

void GraphWidget::goToEvents() {
    LOG( "@GraphWidget::goToEvents()");
}

void GraphWidget::goToMedia() {
    LOG( "@GraphWidget::goToMedia()");
}

void GraphWidget::mousePressEvent( QMouseEvent *event ) {
    if( !_nodes.empty() && event->button() == Qt::RightButton ) {
        QGraphicsItem *item = itemAt( event->pos() );
        if( item && item->type() == _nodes.begin()->second->type() ) {
            try {
                auto node = dynamic_cast<GraphNode *>( item );
                //TODO pass person id to goto methods.
                QMenu menu( this );
                QAction action_GoToProfile( "Profile", this );
                QAction action_GoToEvents( "Events", this );
                QAction action_GoToMedia( "Media", this );
                connect( &action_GoToProfile, SIGNAL( triggered() ), this, SLOT( goToProfile() ) );
                connect( &action_GoToEvents, SIGNAL( triggered() ), this, SLOT( goToEvents() ) );
                connect( &action_GoToMedia, SIGNAL( triggered() ), this, SLOT( goToMedia() ) );
                menu.addAction( &action_GoToProfile );
                menu.addAction( &action_GoToEvents );
                menu.addAction( &action_GoToMedia );
                menu.exec( mapToGlobal( event->pos() ) );
            } catch( std::bad_cast ) {
                LOG_ERROR( "[relations::Ui::GraphWidget::mousePressEvent(..)] "
                               "Couldn't cast QGraphicItem into GraphNode despite passing the type checks." );
            }
        }
    } else {
        QGraphicsView::mousePressEvent( event );
    }
}