#include "GraphEdge.h"

#include <math.h>

static const double PI = 3.14159265358979323846264338327950288419717;
static double TWO_PI = 2.0 * PI;

GraphEdge::GraphEdge( GraphNode *sourceNode, GraphNode *destNode ) //TODO Add line type var in method
    : arrowSize( 10 ), _line_type( Line::UNIDIRECTIONAL_SOLID ) {
    setAcceptedMouseButtons( 0 );
    source = sourceNode;
    dest = destNode;
    source->addEdge( this );
    dest->addEdge( this );
    adjust();
}

GraphEdge::GraphEdge( GraphNode *sourceNode, GraphNode *destNode, const Line &line )
    : arrowSize( 10 ), _line_type( line ) {
    setAcceptedMouseButtons( 0 );
    source = sourceNode;
    dest = destNode;
    source->addEdge( this );
    dest->addEdge( this );
    adjust();
}

GraphNode *GraphEdge::sourceNode() const {
    return source;
}

GraphNode *GraphEdge::destNode() const {
    return dest;
}

void GraphEdge::adjust() {
    if( !source || !dest )
        return;

    QLineF line( mapFromItem( source, 0, 0 ), mapFromItem( dest, 0, 0 ) );
    qreal length = line.length();

    prepareGeometryChange();

    if( length > qreal( 10. ) ) {
        QPointF edgeOffset( ( line.dx() * 48 ) / length, ( line.dy() * 48 ) / length );
        sourcePoint = line.p1() + edgeOffset;
        destPoint = line.p2() - edgeOffset;
    } else {
        sourcePoint = destPoint = line.p1();
    }
}

QRectF GraphEdge::boundingRect() const {
    if( !source || !dest )
        return QRectF();

    qreal penWidth = 1;
    qreal extra = ( penWidth + arrowSize ) / 2.0;

    return QRectF( sourcePoint, QSizeF( destPoint.x() - sourcePoint.x(),
                                        destPoint.y() - sourcePoint.y() ) )
        .normalized()
        .adjusted( -extra, -extra, extra, extra );
}

void GraphEdge::paint( QPainter *painter, const QStyleOptionGraphicsItem *, QWidget * ) {
    if( !source || !dest )
        return;

    QLineF line( sourcePoint, destPoint );
    if( qFuzzyCompare( line.length(), qreal( 0. ) ) )
        return;

    double angle = ::acos( line.dx() / line.length() );
    if( line.dy() >= 0 )
        angle = TWO_PI - angle;

    QPointF destArrowP1, destArrowP2;
    // Draw the line itself
    switch( _line_type ) {
        case Line::UNIDIRECTIONAL_SOLID:
            painter->setPen( QPen( Qt::black, 1, Qt::SolidLine, Qt::RoundCap, Qt::RoundJoin ) );
            painter->drawLine( line );
            destArrowP1 = destPoint + QPointF( sin( angle - PI / 3 ) * arrowSize,
                                               cos( angle - PI / 3 ) * arrowSize );
            destArrowP2 = destPoint + QPointF( sin( angle - PI + PI / 3 ) * arrowSize,
                                               cos( angle - PI + PI / 3 ) * arrowSize );

            painter->setBrush( Qt::black );
            painter->drawPolygon( QPolygonF() << line.p1() );
            painter->drawPolygon( QPolygonF() << line.p2() << destArrowP1 << destArrowP2 );
            break;
        case Line::UNIDIRECTIONAL_DOTTED:
            painter->setPen( QPen( Qt::black, 1, Qt::DotLine, Qt::RoundCap, Qt::RoundJoin ) );
            painter->drawLine( line );
            destArrowP1 = destPoint + QPointF( sin( angle - PI / 3 ) * arrowSize,
                                               cos( angle - PI / 3 ) * arrowSize );
            destArrowP2 = destPoint + QPointF( sin( angle - PI + PI / 3 ) * arrowSize,
                                               cos( angle - PI + PI / 3 ) * arrowSize );

            painter->setBrush( Qt::black );
            painter->drawPolygon( QPolygonF() << line.p1() );
            painter->drawPolygon( QPolygonF() << line.p2() << destArrowP1 << destArrowP2 );
            break;
        case Line::BIDIRECTIONAL_DOTTED:
            painter->setPen( QPen( Qt::black, 1, Qt::DotLine, Qt::RoundCap, Qt::RoundJoin ) );
            painter->drawLine( line );
            painter->setBrush( Qt::black );
            painter->drawPolygon( QPolygonF() << line.p1() );
            painter->drawPolygon( QPolygonF() << line.p2() );
            break;
        case Line::BIDIRECTIONAL_SOLID:
            painter->setPen( QPen( Qt::black, 1, Qt::SolidLine, Qt::RoundCap, Qt::RoundJoin ) );
            painter->drawLine( line );
            painter->setBrush( Qt::black );
            painter->drawPolygon( QPolygonF() << line.p1() );
            painter->drawPolygon( QPolygonF() << line.p2() );
            break;
    }

    //Helper box
//    painter->setPen( QPen( Qt::cyan ) );
//    painter->drawRect( boundingRect() );

    //painter->setPen(QPen(Qt::black, 1, Qt::SolidLine, Qt::RoundCap, Qt::RoundJoin));
    //painter->drawLine(line);
    // Draw the arrows
//    double angle = ::acos(line.dx() / line.length());
//    if (line.dy() >= 0)
//        angle = TWO_PI - angle;
//
//    QPointF sourceArrowP1 = sourcePoint + QPointF(sin(angle + PI / 3) * arrowSize,
//                                                  cos(angle + PI / 3) * arrowSize);
//    QPointF sourceArrowP2 = sourcePoint + QPointF(sin(angle + PI - PI / 3) * arrowSize,
//                                                  cos(angle + PI - PI / 3) * arrowSize);
//    QPointF destArrowP1 = destPoint + QPointF(sin(angle - PI / 3) * arrowSize,
//                                              cos(angle - PI / 3) * arrowSize);
//    QPointF destArrowP2 = destPoint + QPointF(sin(angle - PI + PI / 3) * arrowSize,
//                                              cos(angle - PI + PI / 3) * arrowSize);
//
//    painter->setBrush(Qt::black);
////    painter->drawPolygon(QPolygonF() << line.p1() << sourceArrowP1 << sourceArrowP2);
////    painter->drawPolygon(QPolygonF() << line.p2() << destArrowP1 << destArrowP2);
//    painter->drawPolygon(QPolygonF() << line.p1() );
//    painter->drawPolygon(QPolygonF() << line.p2() << destArrowP1 << destArrowP2);
}