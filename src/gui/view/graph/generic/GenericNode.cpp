#include <QtSvg/QSvgRenderer>
#include "GenericNode.h"

GenericNode::GenericNode( GenericGraph *graphWidget,
                          const int64_t &id,
                          const QString &display_name,
                          const QColor &colour ) :
    _graph( graphWidget ),
    _id( id ),
    _name( display_name ),
    _colour( colour )
{
    setFlag( ItemIsMovable );
    setFlag( ItemSendsGeometryChanges );
    setCacheMode( DeviceCoordinateCache );
    setZValue( -1 );
}

void GenericNode::addEdge( GenericEdge *edge ) {
    if( edge->destNode() == this )
        _parent_edges << edge;
    else
        _child_edges << edge;
    edge->adjust();
}

QList<GenericEdge *> GenericNode::childEdges() const {
    return _child_edges;
}

QList<GenericEdge *> GenericNode::parentEdges() const {
    return _parent_edges;
}

int64_t GenericNode::id() const {
    return _id;
}

int GenericNode::maxChildWidth() const {
    auto local = _child_edges.size();
    if( local == 0 ) { //Leaf, return own weight
        return 1;
    } else {
        auto total = 0;
        for( auto e : _child_edges ) {
            if( e->destNode() )
                total += e->destNode()->maxChildWidth();
        }
        return local > total ? local : total;
    }
}

int GenericNode::maxParentWidth() const {
    auto local = _parent_edges.size();
    if( local == 0 ) { //Leaf, return own weight
        return 1;
    } else {
        auto total = 0;
        for( auto e : _parent_edges ) {
            if( e->sourceNode() )
                total += e->sourceNode()->maxParentWidth();
        }
        return local > total ? local : total;
    }
}

QRectF GenericNode::boundingRect() const {
    qreal adjust = 2;
    return QRectF( -48 - adjust, -48 - adjust, 96 + adjust, 96 + adjust );
}

QPainterPath GenericNode::shape() const {
    QPainterPath path;
    path.addRect( -48, -48, 96, 96 );
    return path;
}

void GenericNode::paint( QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget ) {
    //Portrait outline
    painter->setPen( QPen( _colour, 1 ) );
    painter->drawRect( -32, -48, 64, 64 );
    //Portrait icon
    QSvgRenderer renderer( QString( ":/resource/open-iconic/person.svg" ) );
    QImage image(64, 64, QImage::Format_ARGB32);
    image.fill(0);
    painter->drawImage( QRect( -32, -48, 64, 64), image );
    renderer.render( painter, QRectF( -32, -48, 64, 64 ) );
    //Person ID
    painter->setPen( QPen( Qt::white, 0 ) );
    painter->drawText(QRect( -32, -48, 64, 32 ), Qt::AlignCenter, QString::number( _id ) );
    //Display name
    painter->setPen( QPen( Qt::black, 0 ) );
    painter->drawText(QRect( -48, 18, 96, 30 ), Qt::AlignCenter, _name );

    //Helper box
//    painter->setPen( QPen( Qt::green, 2 ) );
//    painter->drawRect(-48, -48, 96, 96 );
}

QVariant GenericNode::itemChange( QGraphicsItem::GraphicsItemChange change, const QVariant &value ) {
    switch( change ) {
        case ItemPositionHasChanged:
                foreach ( GenericEdge *edge, _child_edges ) edge->adjust();
                foreach ( GenericEdge *edge, _parent_edges ) edge->adjust();
            break;
        default:
            break;
    };
    return QGraphicsItem::itemChange( change, value );
}

void GenericNode::mousePressEvent( QGraphicsSceneMouseEvent *event ) {
    update();
    QGraphicsItem::mousePressEvent( event );
}

void GenericNode::mouseReleaseEvent( QGraphicsSceneMouseEvent *event ) {
    update();
    QGraphicsItem::mouseReleaseEvent( event );
}