#ifndef RELATIONS_GENERICGRAPH_H
#define RELATIONS_GENERICGRAPH_H

#include <QGraphicsView>
#include <QAbstractListModel>
#include <eadlib/logger/Logger.h>
#include "src/io/DataManager.h"
#include "GenericEdge.h"
#include "GenericNode.h"
#include <src/algorithm/containers/Position.h>

class GenericEdge;
class GenericNode;

namespace relations::Ui {
    class GenericGraph;
}
class GenericGraph : public QGraphicsView {
  Q_OBJECT

  public:
    typedef std::unordered_map<int64_t, std::unique_ptr<GenericNode>> NodeMap_t;
    GenericGraph( QWidget *parent, std::shared_ptr<relations::io::DataManager> data_manager );

  signals:
    void showProfileSignal( int64_t );

  public slots:
    void zoomIn();
    void zoomOut();
    void loadGraph( const int64_t &subject_id, const relations::dto::RelationshipType &relationship_type );
    void clearGraph();
    void goToProfile( GenericNode *new_subject );

  private slots:
    void centerOnNewSubject( GenericNode *new_subject );

  protected:
    void keyPressEvent( QKeyEvent *event ) override;

#ifndef QT_NO_WHEELEVENT
    void wheelEvent( QWheelEvent *event ) override;
#endif

    void drawBackground( QPainter *painter, const QRectF &rect ) override;
    void scaleView( qreal scaleFactor );
    void resizeEvent( QResizeEvent *event ) override;
    void mousePressEvent( QMouseEvent *event ) override;

  private:
    QGraphicsScene                                   *_scene;
    GenericNode                                      *_centerNode;
    std::shared_ptr<relations::io::DataManager>       _data_manager;
    NodeMap_t                                         _nodes;
    std::unique_ptr<relations::dto::RelationshipType> _relationship_type;
    const relations::Position<int64_t>                _pos_offset { 128, 128 };
};


#endif //RELATIONS_GENERICGRAPH_H
