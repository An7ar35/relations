#include "GenericGraph.h"
#include <math.h>
#include <QMenu>
#include <QKeyEvent>
#include <src/algorithm/CircularPosition.h>

GenericGraph::GenericGraph( QWidget *parent,
                            std::shared_ptr<relations::io::DataManager> data_manager ) :
    QGraphicsView( parent ),
    _data_manager( data_manager )
{
    this->setContextMenuPolicy( Qt::CustomContextMenu );
    _scene = new QGraphicsScene( this );
    _scene->setItemIndexMethod( QGraphicsScene::NoIndex );
    viewport()->childrenRect().setSize( parent->size() );
    _scene->setSceneRect(viewport()->childrenRect());
    setScene( _scene );
    setCacheMode( CacheBackground );
    setViewportUpdateMode( BoundingRectViewportUpdate );
    setRenderHint( QPainter::Antialiasing );
    setTransformationAnchor( AnchorUnderMouse );
    scale( qreal( 0.8 ), qreal( 0.8 ) );
    setMinimumSize( parent->width(), parent->height() );
}

void GenericGraph::zoomIn() {
    scaleView( qreal( 1.2 ) );
}

void GenericGraph::zoomOut() {
    scaleView( 1 / qreal( 1.2 ) );
}

void GenericGraph::loadGraph( const int64_t &subject_id,
                              const relations::dto::RelationshipType &relationship_type )
{
    auto cached_type = std::find_if( _data_manager->getDataCache()->_relationship_types.begin(),
                                     _data_manager->getDataCache()->_relationship_types.end(),
                                     ( [&relationship_type]( const relations::dto::RelationshipType &type ) {
                                         return relationship_type._value == type._value;
                                     } )
    );
    _relationship_type.reset();
    _relationship_type = std::make_unique<relations::dto::RelationshipType>( *cached_type );
    //Load subject
    auto subject        = _data_manager->at( subject_id );
    auto subject_name   = QString::fromStdString( _data_manager->getPreferedName( subject ) );
    auto subject_colour = QColor( Qt::GlobalColor::darkMagenta );
    auto subject_it = _nodes.insert( typename NodeMap_t::value_type( subject._id,
                                                                     std::make_unique<GenericNode>( this,
                                                                                                    subject._id,
                                                                                                    subject_name,
                                                                                                    subject_colour ) )
    ).first;
    _centerNode = subject_it->second.get();
    _scene->addItem( subject_it->second.get() );
    //Load connected nodes
    auto children = _data_manager->getChildList( subject, *cached_type );
    for( auto c : children ) {
        auto child        = _data_manager->at( c );
        auto child_name   = QString::fromStdString( _data_manager->getPreferedName( child ) );
        auto child_colour = QColor( Qt::GlobalColor::cyan );
        auto child_it = _nodes.insert( typename NodeMap_t::value_type( child._id,
                                                                       std::make_unique<GenericNode>( this,
                                                                                                      child._id,
                                                                                                      child_name,
                                                                                                      child_colour ) )
        ).first;
        _scene->addItem( child_it->second.get() );
        _scene->addItem( new GenericEdge( subject_it->second.get(), child_it->second.get() ) );
    }
    auto parents  = _data_manager->getParentList( subject, *cached_type );
    for( auto p : parents ) {
        auto parent        = _data_manager->at( p );
        auto parent_name   = QString::fromStdString( _data_manager->getPreferedName( parent ) );
        auto parent_colour = QColor( Qt::GlobalColor::darkCyan );
        auto parent_it = _nodes.insert( typename NodeMap_t::value_type( parent._id,
                                                                        std::make_unique<GenericNode>( this,
                                                                                                       parent._id,
                                                                                                       parent_name,
                                                                                                       parent_colour ) )
        ).first;
        _scene->addItem( parent_it->second.get() );
        _scene->addItem( new GenericEdge( parent_it->second.get(), subject_it->second.get() ) );
    }

    auto positions = relations::algo::circularPosition( _nodes.size() - 1, _pos_offset );
    auto pos_it    = positions->begin();
    for( auto it = _nodes.begin(); it != _nodes.end(); ++it ) {
        if( it->second.get() != _centerNode ) {
            if( pos_it != positions->end() ) {
                LOG_TRACE( "[relations::GenericGraph::loadGraph( ", subject_id, ", ", relationship_type._value, " )] "
                    "Positioning [", it->second->id(), "] at (", pos_it->x, ", ", pos_it->y, ")." );
                it->second->setPos( pos_it->x, pos_it->y );
                ++pos_it;
            } else {
                LOG_ERROR( "[relations::GenericGraph::loadGraph( ", subject_id, ", ", relationship_type._value, " )] "
                    "Ran out of positions to dispense before running out of nodes." );
            }
        } else { //subject node
            LOG_TRACE( "[relations::GenericGraph::loadGraph( ", subject_id, ", ", relationship_type._value, " )] "
                "Positioning subject [", it->second->id(), "] at (0, 0)." );
            it->second->setPos( 0, 0 );
        }
    }
}

void GenericGraph::clearGraph() {
    LOG_DEBUG( "[relations::Ui::GenericGraph::clearGraph()] Clearing graph scene" );
    if( !_nodes.empty() ) {
        _nodes.clear();
    }
    _scene->clear();
    viewport()->update();
}

void GenericGraph::goToProfile( GenericNode *new_subject ) {
    emit showProfileSignal( new_subject->id() );
}

void GenericGraph::centerOnNewSubject( GenericNode *new_subject ) {
    try {
        auto person = _data_manager->at( new_subject->id() );
        clearGraph();
        loadGraph( person._id, *_relationship_type );
    } catch( std::out_of_range ) {
        LOG_ERROR( "[relations::Ui::BloodGraph::centerOnNewSubject( BloodNode * )] "
                       "DataManager could not find Person [", new_subject->id(), "]." );
    }
}

void GenericGraph::keyPressEvent( QKeyEvent *event ) {
    switch( event->key() ) {
        case Qt::Key_Plus:
            zoomIn();
            break;
        case Qt::Key_Minus:
            zoomOut();
            break;
        default:
            QGraphicsView::keyPressEvent( event );
    }
}

#ifndef QT_NO_WHEELEVENT
void GenericGraph::wheelEvent( QWheelEvent *event ) {
    scaleView( pow( ( double ) 2, -event->delta() / 240.0 ) );
}
#endif

void GenericGraph::drawBackground( QPainter *painter, const QRectF &rect ) {
    QLinearGradient gradient( rect.topLeft(), rect.bottomRight() );
    gradient.setColorAt( 0, Qt::white );
    gradient.setColorAt( 1, Qt::lightGray );
    painter->fillRect( rect.intersected( rect ), gradient );
    painter->setBrush( Qt::NoBrush );
    painter->drawRect( rect );
}

void GenericGraph::scaleView( qreal scaleFactor ) {
    qreal factor = transform().scale( scaleFactor, scaleFactor ).mapRect( QRectF( 0, 0, 1, 1 ) ).width();
    if( factor < 0.07 || factor > 100 )
        return;

    scale( scaleFactor, scaleFactor );
}

void GenericGraph::resizeEvent( QResizeEvent *event ) {
    QGraphicsView::resizeEvent( event );
}

void GenericGraph::mousePressEvent( QMouseEvent *event ) {
    if( !_nodes.empty() && event->button() == Qt::RightButton ) {
        QGraphicsItem *item = itemAt( event->pos() );
        if( item && item->type() == _nodes.begin()->second->type() ) {
            try {
                auto node = dynamic_cast<GenericNode *>( item );
                QMenu menu( this );
                menu.addAction( QIcon( ":/resource/open-iconic/person.svg"),
                                "Person info.",
                                this,
                                [this, node]() { goToProfile( node ); }
                );
                menu.addAction( QIcon( ":/resource/open-iconic/target.svg" ),
                                "Center Graph",
                                this,
                                [this, node]() { centerOnNewSubject( node ); }
                );
                menu.exec( mapToGlobal( event->pos() ) );

            } catch( std::bad_cast ) {
                LOG_ERROR( "[relations::Ui::GenericGraph::mousePressEvent(..)] "
                               "Couldn't cast QGraphicItem into GenericNode despite passing the type checks." );
            }
        }
    } else {
        QGraphicsView::mousePressEvent( event );
    }
}
