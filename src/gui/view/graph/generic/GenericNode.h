#ifndef RELATIONS_GENERICNODE_H
#define RELATIONS_GENERICNODE_H

#include <QGraphicsItem>
#include "GenericEdge.h"
#include "GenericGraph.h"

class QGraphicsSceneMouseEvent;
class GenericEdge;
class GenericGraph;

namespace relations::Ui {
    class GenericNode;
}

class GenericNode : public QGraphicsItem {
  public:
    GenericNode( GenericGraph *graphWidget,
                 const int64_t &id,
                 const QString &display_name,
                 const QColor &colour );

    void addEdge( GenericEdge *edge );
    QList<GenericEdge *> childEdges() const;
    QList<GenericEdge *> parentEdges() const;

    int64_t id() const;
    int maxChildWidth() const;
    int maxParentWidth() const;

    enum { Type = UserType + 3 };
    int type() const override { return Type; }

    QRectF boundingRect() const override;
    QPainterPath shape() const override;

    void paint( QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget ) override;

  protected:
    QVariant itemChange( GraphicsItemChange change, const QVariant &value ) override;
    void mousePressEvent( QGraphicsSceneMouseEvent *event ) override;
    void mouseReleaseEvent( QGraphicsSceneMouseEvent *event ) override;

    int64_t              _id;
    QString              _name;
    QColor               _colour;
    QList<GenericEdge *> _child_edges;
    QList<GenericEdge *> _parent_edges;
    GenericGraph        *_graph;
};


#endif //RELATIONS_GENERICNODE_H
