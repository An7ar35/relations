#include "GenericEdge.h"

GenericEdge::GenericEdge( GenericNode *sourceNode, GenericNode *destNode ) :
    _arrow_size( 10 )
{
    setAcceptedMouseButtons( 0 );
    _source = sourceNode;
    _dest   = destNode;
    _source->addEdge( this );
    _dest->addEdge( this );
    adjust();
}


GenericNode *GenericEdge::sourceNode() const {
    return _source;
}

GenericNode *GenericEdge::destNode() const {
    return _dest;
}

void GenericEdge::adjust() {
    if( !_source || !_dest )
        return;

    QLineF line( mapFromItem( _source, 0, 0 ), mapFromItem( _dest, 0, 0 ) );
    qreal length = line.length();

    prepareGeometryChange();

    if( length > qreal( 10. ) ) {
        QPointF edgeOffset( ( line.dx() * 48 ) / length, ( line.dy() * 48 ) / length );
        sourcePoint = line.p1() + edgeOffset;
        destPoint = line.p2() - edgeOffset;
    } else {
        sourcePoint = destPoint = line.p1();
    }
}

QRectF GenericEdge::boundingRect() const {
    if( !_source || !_dest )
        return QRectF();

    qreal penWidth = 1;
    qreal extra = ( penWidth + _arrow_size ) / 2.0;

    return QRectF( sourcePoint, QSizeF( destPoint.x() - sourcePoint.x(),
                                        destPoint.y() - sourcePoint.y() ) )
        .normalized()
        .adjusted( -extra, -extra, extra, extra );
}

void GenericEdge::paint( QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget ) {
    if( !_source || !_dest )
        return;

    QLineF line( sourcePoint, destPoint );
    if( qFuzzyCompare( line.length(), qreal( 0. ) ) )
        return;

    double angle = ::acos( line.dx() / line.length() );
    if( line.dy() >= 0 )
        angle = TWO_PI - angle;

    QPointF destArrowP1, destArrowP2;
    // Draw the line itself
    painter->setPen( QPen( Qt::black, 1, Qt::SolidLine, Qt::RoundCap, Qt::RoundJoin ) );
    painter->drawLine( line );
    destArrowP1 = destPoint + QPointF( sin( angle - PI / 3 ) * _arrow_size,
                                       cos( angle - PI / 3 ) * _arrow_size );
    destArrowP2 = destPoint + QPointF( sin( angle - PI + PI / 3 ) * _arrow_size,
                                       cos( angle - PI + PI / 3 ) * _arrow_size );

    painter->setBrush( Qt::black );
    painter->drawPolygon( QPolygonF() << line.p1() );
    painter->drawPolygon( QPolygonF() << line.p2() << destArrowP1 << destArrowP2 );
}