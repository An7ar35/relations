#ifndef RELATIONS_GENERICEDGE_H
#define RELATIONS_GENERICEDGE_H

#include <QGraphicsItem>
#include <QPainter>
#include "GenericNode.h"
#include <math.h>

class GenericNode;

namespace relations::Ui {
    class GenericEdge;
}

static const double PI = M_PI;
static double TWO_PI = 2.0 * M_PI;

class GenericEdge : public QGraphicsItem {
  public:
    GenericEdge(GenericNode *sourceNode,
                GenericNode *destNode);

    virtual GenericNode *sourceNode() const;
    virtual GenericNode *destNode() const;

    virtual void adjust();

    enum { Type = UserType + 4 };
    virtual int type() const override { return Type; }

  protected:
    QRectF boundingRect() const override;
    void paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget) override;

    GenericNode *_source, *_dest;
    QPointF      sourcePoint;
    QPointF      destPoint;
    qreal        _arrow_size;
};

#endif //RELATIONS_GENERICEDGE_H
