#ifndef RELATIONS_BROWSER_TABWIDGET_H
#define RELATIONS_BROWSER_TABWIDGET_H

#include <QTabWidget>
#include <QTableView>
#include <eadlib/logger/Logger.h>
#include <external/eadlib/wrapper/SQLite/TableDB.h>
#include <src/gui/model/people/DMDGraphModel.h>
#include <src/gui/model/event/EventTableModel.h>
#include <src/gui/model/media/MediaTableModel.h>
#include <src/gui/view/people/PersonEdit.h>
#include <src/gui/view/people/PersonView.h>
#include <src/gui/view/event/EventView.h>
#include <src/gui/view/event/EventEdit.h>
#include <src/gui/view/media/MediaView.h>
#include <src/gui/view/media/MediaEdit.h>
#include "src/io/DataManager.h"

namespace relations::Ui {
    class BrowserTabWidget;
    class PersonEdit;
    class PersonView;
    class EventEdit;
    class EventView;
    class MediaView;
    class MediaEdit;
    class DMDGraphModel;
    class EventTableModel;
    class MediaTableModel;
}

class BrowserTabWidget : public QTabWidget {
  Q_OBJECT

  public:
    explicit BrowserTabWidget( QWidget *parent, std::shared_ptr<relations::io::DataManager> data_manager );
    ~BrowserTabWidget();

  public slots:
    //People Tab
    void switchToAddPersonPane();
    void switchToEditPersonPane();
    void switchToEditPersonPane( relations::dto::Person &person );
    void switchToViewPersonPane();
    void switchToViewPersonPane( relations::dto::Person &person );
    void saveEditPerson();
    void cancelEditPerson();
    void removePerson();
    void personRightClickMenu( QPoint point );
    //Event Tab
    void switchToAddEventPane();
    void switchToEditEventPane();
    void switchToEditEventPane( relations::dto::Event &event );
    void switchToViewEventPane();
    void switchToViewEventPane( relations::dto::Event &event );
    void saveEditEvent();
    void cancelEditEvent();
    void removeEvent();
    //Media tab
    void switchToAddMediaPane();
    void switchToEditMediaPane();
    void switchToEditMediaPane( relations::dto::Media &media );
    void switchToViewMediaPane();
    void switchToViewMediaPane( relations::dto::Media &media );
    void saveEditMedia();
    void cancelEditMedia();
    void removeMedia();
    //Refresh
    void refreshAllViews();

    //Access from Graph
    void openPersonView( int64_t id );

  private slots:
    void refreshPersonListModel();
    void refreshEventListModel();
    void refreshMediaListModel();
    void onTabChanged( int index );

  signals:
    void openBloodLineGraphSignal( int64_t subject_id );
    void openGenericGraphSignal( int64_t subject_id, int64_t relationship_id );


  private:
    void clearLayout( QLayout *layout );
    relations::Ui::BrowserTabWidget            *_ui;
    std::shared_ptr<relations::io::DataManager> _data_manager;
    PersonEdit                                 *_person_edit;
    PersonView                                 *_person_view;
    EventEdit                                  *_event_edit;
    EventView                                  *_event_view;
    MediaView                                  *_media_view;
    MediaEdit                                  *_media_edit;
    DMDGraphModel                              *_graph_model;
    EventTableModel                            *_event_model;
    MediaTableModel                            *_media_model;
    relations::io::DataCache                   *_data_cache;
};

#endif //RELATIONS_BROWSER_TABWIDGET_H
