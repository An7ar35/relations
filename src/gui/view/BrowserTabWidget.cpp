#include <src/gui/model/TableDBModel.h>
#include <src/gui/model/people/DMDGraphModel.h>
#include <src/gui/view/people/PersonEdit.h>
#include <QMenu>
#include "BrowserTabWidget.h"
#include "ui_BrowserTabWidget.h"

BrowserTabWidget::BrowserTabWidget( QWidget *parent,
                                    std::shared_ptr<relations::io::DataManager> data_manager ) :
    QTabWidget( parent ),
    _ui( new relations::Ui::BrowserTabWidget ),
    _data_manager( data_manager ),
    _data_cache( data_manager->getDataCache() )
{
    _ui->setupUi( this );
    connect( this, SIGNAL( currentChanged( int ) ), this, SLOT( onTabChanged( int ) ) );
    _ui->personStackedWidget->setCurrentIndex( 0 );
    _ui->eventStackedWidget->setCurrentIndex( 0 );
    setCurrentIndex( 0 );
    //Person view area buttons
    connect( _ui->addPersonButton, SIGNAL( clicked() ), this, SLOT( switchToAddPersonPane() ) );
    connect( _ui->editPersonButton, SIGNAL( clicked() ), this, SLOT( switchToEditPersonPane() ) );
    connect( _ui->removePersonButton, SIGNAL( clicked() ), this, SLOT( removePerson() ) );
    //Event view area button
    connect( _ui->addEventButton, SIGNAL( clicked() ), this, SLOT( switchToAddEventPane() ) );
    connect( _ui->editEventButton, SIGNAL( clicked() ), this, SLOT( switchToEditEventPane() ) );
    connect( _ui->removeEventButton, SIGNAL( clicked() ), this, SLOT( removeEvent() ) );
    //Media view area buttons
    connect( _ui->addMediaButton, SIGNAL( clicked() ), this, SLOT( switchToAddMediaPane() ) );
    connect( _ui->editMediaButton, SIGNAL( clicked() ), this, SLOT( switchToEditMediaPane() ) );
    connect( _ui->removeMediaButton, SIGNAL( clicked() ), this, SLOT( removeMedia() ) );
}

BrowserTabWidget::~BrowserTabWidget() {
    delete _ui;
}

void BrowserTabWidget::switchToAddPersonPane() {
    clearLayout( _ui->editPersonPaneLayout );
    _person_edit = new PersonEdit( this, _data_manager, -1 );
    connect( _person_edit, SIGNAL( cancelPersonEditSignal() ), this, SLOT( cancelEditPerson() ) );
    connect( _person_edit, SIGNAL( savePersonEditSignal() ), this, SLOT( saveEditPerson() ) );
    _ui->personStackedWidget->setCurrentIndex( 1 );
    _ui->editPersonPaneLayout->addWidget( _person_edit );
    _person_edit->show();
}

void BrowserTabWidget::switchToEditPersonPane() {
    //Grabbing the id of the row selected
    auto selected_row = _ui->peopleTableView->selectionModel()->currentIndex().row();
    if( selected_row >= 0 ) {
        //Getting the correct Person DTO from the graph
        relations::dto::Person person = _data_manager->at( _graph_model->getPersonID( selected_row ) );
        switchToEditPersonPane( person );
    }
}

void BrowserTabWidget::switchToEditPersonPane( relations::dto::Person &person ) {
    clearLayout( _ui->editPersonPaneLayout );
    _person_edit = new PersonEdit( this, _data_manager, person._id );
    connect( _person_edit, SIGNAL( cancelPersonEditSignal() ), this, SLOT( cancelEditPerson() ) );
    connect( _person_edit, SIGNAL( savePersonEditSignal() ), this, SLOT( saveEditPerson() ) );
    _ui->personStackedWidget->setCurrentIndex( 1 );
    _ui->editPersonPaneLayout->addWidget( _person_edit );
    _person_edit->show();
}

void BrowserTabWidget::switchToViewPersonPane() {
    auto selected_row = _ui->peopleTableView->selectionModel()->currentIndex().row();
    if( selected_row >= 0 ) {
        //Getting the correct Person DTO from the graph
        auto id = _graph_model->getPersonID( selected_row );
        relations::dto::Person person = _data_manager->at( id );
        switchToViewPersonPane( person );
    }
}

void BrowserTabWidget::switchToViewPersonPane( relations::dto::Person &person ) {
    clearLayout( _ui->viewPersonPaneLayout );
    _person_view = new PersonView( this, _data_manager, person );
    _ui->personStackedWidget->setCurrentIndex( 0 );
    _ui->viewPersonPaneLayout->addWidget( _person_view );
    connect( _person_view,
             SIGNAL( showBloodGraphSignal( int64_t ) ),
             SIGNAL( openBloodLineGraphSignal( int64_t ) )
    );
    connect( _person_view,
             SIGNAL( showGenericGraphSignal( int64_t, int64_t ) ),
             SIGNAL( openGenericGraphSignal( int64_t, int64_t ) )
    );
    _person_view->show();
}

void BrowserTabWidget::saveEditPerson() {
    LOG( "@BrowserTabWidget::saveEditPerson()" );
    clearLayout( _ui->editPersonPaneLayout );
    switchToViewPersonPane();
    refreshPersonListModel();
}

void BrowserTabWidget::cancelEditPerson() {
    LOG( "@BrowserTabWidget::cancelEditPerson()" );
    clearLayout( _ui->editPersonPaneLayout );
    switchToViewPersonPane();
}

void BrowserTabWidget::removePerson() {
    auto selected_row = _ui->peopleTableView->selectionModel()->currentIndex().row();
    if( selected_row >= 0 ) {
        auto person_id = _graph_model->getPersonID( selected_row );
        try {
            relations::dto::Person person = _data_manager->at( person_id );
            _person_view->hide();
            _ui->viewPersonPaneLayout->removeWidget( _person_view );
            if( !_data_manager->remove( person ) ) {
                LOG_ERROR( "[relations::Ui::BrowserTabWidget::removePerson()] Failed to remove Person [", person_id, "]." );
            }
            refreshPersonListModel();
            if( selected_row < _graph_model->rows() ) {
                _ui->peopleTableView->selectRow( selected_row );
            } else if( selected_row > 0 ) {
                _ui->peopleTableView->selectRow( selected_row - 1 );
            }
        } catch( std::out_of_range ) {
            LOG_ERROR( "[relations::Ui::BrowserTabWidget::removePerson()] Person [", person_id, "] not in Graph." );
        }
    }
}

void BrowserTabWidget::personRightClickMenu( QPoint point ) {
    auto row = _ui->peopleTableView->rowAt( point.y() );
    auto person_id = _graph_model->getPersonID( row );
    QMenu menu( this );
    menu.addAction( QIcon( ":/resource/open-iconic/eye.svg" ),
                    "Show blood line",
                    this,
                    [this, person_id]() { emit openBloodLineGraphSignal( person_id ); }
    );
    menu.popup( _ui->peopleTableView->viewport()->mapToGlobal( point ) );
    menu.exec();
}

void BrowserTabWidget::switchToAddEventPane() {
    LOG( "@BrowserTabWidget::switchToAddEventPane()" );
    clearLayout( _ui->editEventPaneLayout );
    _event_edit = new EventEdit( this, _data_manager, relations::dto::Event() );
    connect( _event_edit, SIGNAL( cancelEventEditSignal() ), this, SLOT( cancelEditEvent() ) );
    connect( _event_edit, SIGNAL( saveEventEditSignal() ), this, SLOT( saveEditEvent() ) );
    _ui->eventStackedWidget->setCurrentIndex( 1 );
    _ui->editEventPaneLayout->addWidget( _event_edit );
    _event_edit->show();
}

void BrowserTabWidget::switchToEditEventPane() {
    LOG( "@BrowserTabWidget::switchToEditEventPane()" );
    //Grabbing the id of the row selected
    if( _ui->eventsTableView->selectionModel() ) {
        auto selected_row = _ui->eventsTableView->selectionModel()->currentIndex().row();
        if( selected_row >= 0 ) {
            //Getting the correct Event DTO from the Model
            relations::dto::Event event = _event_model->getEvent( selected_row );
            switchToEditEventPane( event );
        }
    }
}

void BrowserTabWidget::switchToEditEventPane( relations::dto::Event &event ) {
    LOG( "@BrowserTabWidget::switchToEditEventPane()" );
    clearLayout( _ui->editEventPaneLayout );
    _event_edit = new EventEdit( this, _data_manager, event );
    connect( _event_edit, SIGNAL( cancelEventEditSignal() ), this, SLOT( cancelEditEvent() ) );
    connect( _event_edit, SIGNAL( saveEventEditSignal() ), this, SLOT( saveEditEvent() ) );
    _ui->eventStackedWidget->setCurrentIndex( 1 );
    _ui->editEventPaneLayout->addWidget( _event_edit );
    _event_edit->show();
}

void BrowserTabWidget::switchToViewEventPane() {
    LOG( "@BrowserTabWidget::switchToViewEventPane()" );
    auto selected_row = _ui->eventsTableView->selectionModel()->currentIndex().row();
    if( selected_row >= 0 ) {
        //Getting the correct Event DTO from the TableModel
        auto event = _event_model->getEvent( selected_row );
        switchToViewEventPane( event );
    }
}

void BrowserTabWidget::switchToViewEventPane( relations::dto::Event &event ) {
    LOG( "@BrowserTabWidget::switchToViewEventPane()" );
    clearLayout( _ui->viewEventPaneLayout );
    _event_view = new EventView( this, _data_manager, event );
    _ui->eventStackedWidget->setCurrentIndex( 0 );
    _ui->viewEventPaneLayout->addWidget( _event_view );
    _event_view->show();
}

void BrowserTabWidget::saveEditEvent() {
    LOG( "@BrowserTabWidget::saveEditEvent()" );
    clearLayout( _ui->editPersonPaneLayout );
    switchToViewEventPane();
    refreshEventListModel();
}

void BrowserTabWidget::cancelEditEvent() {
    LOG( "@BrowserTabWidget::cancelEditEvent()" );
    clearLayout( _ui->editEventPaneLayout );
    switchToViewEventPane();
}

void BrowserTabWidget::removeEvent() {
    LOG( "@BrowserTabWidget::removeEvent()" );
    auto selected_row = _ui->eventsTableView->selectionModel()->currentIndex().row();
    if( selected_row >= 0 ) {
        auto event = _event_model->getEvent( selected_row );
        _event_view->hide();
        clearLayout( _ui->viewEventPaneLayout );
        if( !_data_manager->remove( event ) ) {
            LOG_ERROR( "[relations::Ui::BrowserTabWidget::removeEvent()] Failed to remove Event [", event._id, "]." );
        }
        refreshEventListModel();
        if( selected_row < _event_model->rows() ) {
            _ui->eventsTableView->selectRow( selected_row );
        } else if( selected_row > 0 ) {
            _ui->eventsTableView->selectRow( selected_row - 1 );
        }
    }
}

void BrowserTabWidget::switchToAddMediaPane() {
    LOG( "@BrowserTabWidget::switchToAddMediaPane()" );
    clearLayout( _ui->editMediaPaneLayout );
    _media_edit = new MediaEdit( this, _data_manager, relations::dto::Media() );
    connect( _media_edit, SIGNAL( cancelMediaEditSignal() ), this, SLOT( cancelEditMedia() ) );
    connect( _media_edit, SIGNAL( saveMediaEditSignal() ), this, SLOT( saveEditMedia() ) );
    _ui->mediaStackedWidget->setCurrentIndex( 1 );
    _ui->editMediaPaneLayout->addWidget( _media_edit );
    _media_edit->show();
}

void BrowserTabWidget::switchToEditMediaPane() {
    LOG( "@BrowserTabWidget::switchToEditMediaPane()" );
    //Grabbing the id of the row selected
    if( _ui->mediaTableView->selectionModel() ) {
        auto selected_row = _ui->mediaTableView->selectionModel()->currentIndex().row();
        if( selected_row >= 0 ) {
            //Getting the correct Media DTO from the Model
            relations::dto::Media media = _media_model->getMedia( selected_row );
            switchToEditMediaPane( media );
        }
    }
}

void BrowserTabWidget::switchToEditMediaPane( relations::dto::Media &media ) {
    LOG( "@BrowserTabWidget::switchToEditMediaPane()" );
    clearLayout( _ui->editMediaPaneLayout );
    _media_edit = new MediaEdit( this, _data_manager, media );
    connect( _media_edit, SIGNAL( cancelMediaEditSignal() ), this, SLOT( cancelEditMedia() ) );
    connect( _media_edit, SIGNAL( saveMediaEditSignal() ), this, SLOT( saveEditMedia() ) );
    _ui->mediaStackedWidget->setCurrentIndex( 1 );
    _ui->editMediaPaneLayout->addWidget( _media_edit );
    _media_edit->show();
}

void BrowserTabWidget::switchToViewMediaPane() {
    LOG( "@BrowserTabWidget::switchToViewMediaPane()" );
    auto selected_row = _ui->mediaTableView->selectionModel()->currentIndex().row();
    if( selected_row >= 0 ) {
        //Getting the correct Media DTO from the TableModel
        auto media = _media_model->getMedia( selected_row );
        switchToViewMediaPane( media );
    }
}

void BrowserTabWidget::switchToViewMediaPane( relations::dto::Media &media ) {
    LOG( "@BrowserTabWidget::switchToViewMediaPane()" );
    clearLayout( _ui->viewMediaPaneLayout );
    _media_view = new MediaView( this, _data_manager, media );
    _ui->mediaStackedWidget->setCurrentIndex( 0 );
    _ui->viewMediaPaneLayout->addWidget( _media_view );
    _media_view->show();
}

void BrowserTabWidget::saveEditMedia() {
    LOG( "@BrowserTabWidget::saveEditMedia()" );
    clearLayout( _ui->editMediaPaneLayout );
    switchToViewMediaPane();
    refreshMediaListModel();
}

void BrowserTabWidget::cancelEditMedia() {
    LOG( "@BrowserTabWidget::cancelEditMedia()" );
    clearLayout( _ui->editMediaPaneLayout );
    switchToViewMediaPane();
}

void BrowserTabWidget::removeMedia() {
    LOG( "@BrowserTabWidget::removeMedia()" );
    auto selected_row = _ui->mediaTableView->selectionModel()->currentIndex().row();
    if( selected_row >= 0 ) {
        auto media = _media_model->getMedia( selected_row );
        _media_view->hide();
        clearLayout( _ui->viewMediaPaneLayout );
        if( !_data_manager->remove( media ) ) {
            LOG_ERROR( "[relations::Ui::BrowserTabWidget::removeMedia()] Failed to remove Media [", media._id, "]." );
        }
        refreshMediaListModel();
        if( selected_row < _media_model->rows() ) {
            _ui->mediaTableView->selectRow( selected_row );
        } else if( selected_row > 0 ) {
            _ui->mediaTableView->selectRow( selected_row - 1 );
        }
    }
}

void BrowserTabWidget::refreshAllViews() {
    LOG_TRACE( "[relations::Ui::BrowserTabWidget::refreshAllViews()] Refreshing all Data views." );
    _ui->personStackedWidget->setCurrentIndex( 0 );
    refreshPersonListModel();
}

void BrowserTabWidget::openPersonView( int64_t id ) {
    LOG( "@BrowserTabWidget::openPersonView( int64_t id )" );
    setCurrentIndex( 0 ); //Person tab
    refreshPersonListModel();
    auto row = _graph_model->getRowId( id );
    if( row >= 0 ) {
        _ui->peopleTableView->selectRow( row );
    }
}

void BrowserTabWidget::refreshPersonListModel() {
    LOG_TRACE( "[relations::Ui::BrowserTabWidget::refreshPersonListModel()] Creating new graph model." );
    clearLayout( _ui->viewPersonPaneLayout );
    _graph_model = new DMDGraphModel( _data_manager );
    LOG_TRACE( "[relations::Ui::BrowserTabWidget::refreshPersonListModel()] ", _data_manager->personCount(), " Person(s) found in Graph." );
    _ui->peopleTableView->setContextMenuPolicy(Qt::CustomContextMenu);
    connect( _ui->peopleTableView,
             SIGNAL( customContextMenuRequested( QPoint) ),
             this,
             SLOT( personRightClickMenu( QPoint ) )
    );
    _ui->peopleTableView->setModel( _graph_model );
    QItemSelectionModel *selection_model = _ui->peopleTableView->selectionModel();
    connect( selection_model,
             SIGNAL( currentRowChanged( QModelIndex, QModelIndex ) ),
             this,
             SLOT( switchToViewPersonPane() )
    );
    _ui->peopleTableView->resizeColumnsToContents();
    _ui->peopleTableView->horizontalHeader()->setSectionResizeMode( 1, QHeaderView::Stretch );
}

void BrowserTabWidget::refreshEventListModel() {
    LOG_TRACE( "[relations::Ui::BrowserTabWidget::refreshEventListModel()] Creating new EventTableModel." );
    clearLayout( _ui->viewEventPaneLayout );
    _event_model = new EventTableModel( _data_manager );
    _ui->eventsTableView->setModel( _event_model );
    QItemSelectionModel *selection_model = _ui->eventsTableView->selectionModel();
    connect( selection_model,
             SIGNAL( currentRowChanged( QModelIndex, QModelIndex ) ),
             this,
             SLOT( switchToViewEventPane() ) );
    _ui->eventsTableView->resizeColumnsToContents();
    _ui->eventsTableView->horizontalHeader()->setSectionResizeMode( 3, QHeaderView::Stretch );
}

void BrowserTabWidget::refreshMediaListModel() {
    LOG_TRACE( "[relations::Ui::BrowserTabWidget::refreshMediaListModel()] Creating new MediaTableModel." );
    clearLayout( _ui->viewMediaPaneLayout );
    _media_model = new MediaTableModel( _data_manager );
    _ui->mediaTableView->setModel( _media_model );
    QItemSelectionModel *selection_model = _ui->mediaTableView->selectionModel();
    connect( selection_model,
             SIGNAL( currentRowChanged( QModelIndex, QModelIndex ) ),
             this,
             SLOT( switchToViewMediaPane() ) );
    _ui->mediaTableView->resizeColumnsToContents();
    _ui->mediaTableView->horizontalHeader()->setSectionResizeMode( 2, QHeaderView::Stretch );
}

void BrowserTabWidget::onTabChanged( int index ) {
    switch( index ) {
        case 0: //People tab
            refreshPersonListModel();
            break;
        case 1:
            refreshEventListModel();
            break;
        case 2:
            refreshMediaListModel();
            break;
        default:
            break;
    }
}

void BrowserTabWidget::clearLayout( QLayout *layout ) {
    if( layout ) {
        QLayoutItem *item;
        while( ( item = layout->takeAt( 0 ) ) != NULL ) {
            delete item->widget();
            delete item;
        }
    }
}
