#ifndef RELATIONS_GRAPHFORM_H
#define RELATIONS_GRAPHFORM_H

#include <QWidget>
#include <src/gui/view/graph/bloodgraph/BloodGraph.h>
#include "src/gui/view/graph/generic/GenericGraph.h"

namespace relations::Ui {
    class GraphForm;
}

class GraphForm : public QWidget {
  Q_OBJECT

  public:
    GraphForm( QWidget *parent, std::shared_ptr<relations::io::DataManager> data_manager );
    ~GraphForm();

  signals:
    void showPersonInformationSignal( int64_t );

  public slots:
    void loadBloodGraph( const int64_t &subject_id );
    void loadGenericGraph( const int64_t &subject_id, const relations::dto::RelationshipType &relationship_type );

  private:
    void clearLayout( QLayout *layout );
    relations::Ui::GraphForm                   *_ui;
    std::shared_ptr<relations::io::DataManager> _data_manager;
    GenericGraph                               *_generic_graph;
    BloodGraph                                 *_blood_graph;
};

#endif //RELATIONS_GRAPHFORM_H
