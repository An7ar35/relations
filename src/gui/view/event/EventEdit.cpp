#include <QtWidgets/QMessageBox>
#include "EventEdit.h"
#include "ui_EventEdit.h"

EventEdit::EventEdit( QWidget *parent,
                      std::shared_ptr<relations::io::DataManager> data_manager,
                      const relations::dto::Event &event ) :
    QWidget( parent ),
    _ui( new relations::Ui::EventEdit ),
    _data_manager( data_manager ),
    _data_cache( _data_manager->getDataCache() ),
    _event( event )
{
    _ui->setupUi( this );
    //Populating the combo boxes
    for( auto e : _data_cache->_event_types ) {
        _ui->typeComboBox->addItem( QString::fromStdString( e._value ),
                                    QString::number( e._id )
        );
        if( e._type == _event._type ) {
            _ui->typeComboBox->setCurrentIndex( _ui->typeComboBox->count() - 1 );
        }
    }

    //Setting fields
    _ui->idDisplayLabel->setText( QString::number( _event._id ) );
    if( _event._date ) {
        _ui->formalLineEdit->setText( QString::fromStdString( _event._date->_formal ) );
        _ui->dateLineEdit->setText( QString::fromStdString( _event._date->_original ) );
    }
    if( _event._location ) {
        _ui->descriptionLineEdit->setText( QString::fromStdString( _event._location->_description ) );
        _ui->addressLineEdit->setText( QString::fromStdString( _event._location->_address ) );
        _ui->postcodeLineEdit->setText( QString::fromStdString( _event._location->_postcode ) );
        _ui->cityLineEdit->setText( QString::fromStdString( _event._location->_city ) );
        _ui->countyLineEdit->setText( QString::fromStdString( _event._location->_county ) );
        _ui->countryLineEdit->setText( QString::fromStdString( _event._location->_country ) );
        _ui->longitudeLineEdit->setText( QString::fromStdString( _event._location->_longitude ) );
        _ui->latitudeLineEdit->setText( QString::fromStdString( _event._location->_latitude ) );
    }
    if( _event._note ) {
        _ui->noteSubjectEdit->setText( QString::fromStdString( _event._note->_subject ) );
        _ui->noteTextEdit->appendPlainText( QString::fromStdString( _event._note->_text ) );
    }

    //Creating Table Model
    _event_roles_model = new EventParticipantsTableModel( _data_manager, _event );
    _ui->participantTableView->setModel( _event_roles_model );
    _ui->participantTableView->resizeColumnsToContents();

    //Creating Editors for the tabled data
    _event_role_edit_dialog = new EventRoleEdit( this, _data_manager, _event._id );

    //Connecting actions
    connect( _ui->saveEventButton, SIGNAL( clicked() ), this, SLOT( saveEventDetails() ) );
    connect( _ui->cancelEventButton, SIGNAL( clicked() ), this, SIGNAL( cancelEventEditSignal() ) );

    connect( _ui->addRoleButton, SIGNAL( clicked() ), this, SLOT( addEventRole() ) );
    connect( _ui->editRoleButton, SIGNAL( clicked() ), this, SLOT( editEventRole() ) );
    connect( _ui->removeRoleButton, SIGNAL( clicked() ), this, SLOT( removeEventRole() ) );
    connect( _event_role_edit_dialog, SIGNAL( accepted() ), this, SLOT( saveEventRole() ) );

    connect( _ui->typeComboBox , SIGNAL( currentIndexChanged( int ) ), this, SLOT( setEventType() ) );
}

EventEdit::~EventEdit() {
    delete _ui;
}

void EventEdit::saveEventDetails() {
    LOG( "@PersonEdit::saveEventDetails()" );
    if( !checkFields() ) {
        return;
    }
    //Event's note
    auto note_subject = _ui->noteSubjectEdit->text().toStdString();
    auto note_text    = _ui->noteTextEdit->toPlainText().toStdString();
    if( note_subject.empty() && note_text.empty() ) {
        if( _event._note ) {
            _event._note.reset();
        }
    } else {
        if( _event._note ) {
            _event._note->_subject = note_subject;
            _event._note->_text    = note_text;
        } else {
            auto note = new relations::dto::Note( note_subject, note_text );
            _event._note = std::unique_ptr<relations::dto::Note>( note );
        }
    }
    //Event's Date
    auto date_original = _ui->dateLineEdit->text().toStdString();
    auto date_formal   = _ui->formalLineEdit->text().toStdString();
    if( _event._date ) {
        _event._date->_original = date_original;
        _event._date->_formal   = date_formal;
    } else {
        auto date = new relations::dto::Date( -1, date_formal, date_original );
        _event._date = std::unique_ptr<relations::dto::Date>( date );
    }
    //Event's Location
    auto location_description = _ui->descriptionLineEdit->text().toStdString();
    auto location_address = _ui->addressLineEdit->text().toStdString();
    auto location_postcode = _ui->postcodeLineEdit->text().toStdString();
    auto location_city = _ui->cityLineEdit->text().toStdString();
    auto location_county = _ui->countyLineEdit->text().toStdString();
    auto location_country = _ui->countryLineEdit->text().toStdString();
    auto location_longitude = _ui->longitudeLineEdit->text().toStdString();
    auto location_latitude = _ui->latitudeLineEdit->text().toStdString();
    if( _event._location ) {
        _event._location->_description = location_description;
        _event._location->_address     = location_address;
        _event._location->_postcode    = location_postcode;
        _event._location->_city        = location_city;
        _event._location->_county      = location_county;
        _event._location->_country     = location_country;
        _event._location->_longitude   = location_longitude;
        _event._location->_latitude    = location_latitude;
    } else {
        auto location = new relations::dto::Location( -1, location_description,
                                                      location_address, location_postcode, location_city, location_county, location_country,
                                                      location_longitude, location_latitude );
        _event._location = std::unique_ptr<relations::dto::Location>( location );
    }
    //Event
    if( _event._id < 0 ) { //new event
        if( !_data_manager->add( _event ) ) {
            LOG_ERROR( "[relations::Ui::PersonEdit::saveEventDetails()] Problem encountered whilst committing change(s) to new Event." );
        }
        _event_roles_model->setEventId( _event._id );
        if( !_event_roles_model->commitChangesToDb() ) {
            LOG_ERROR( "[relations::Ui::PersonEdit::saveEventDetails()] Problem(s) encountered whilst committing change(s) to EventRole(s)." );
        }
    } else { //updated person
        _event_roles_model->setEventId( _event._id );
        if( !_event_roles_model->commitChangesToDb() ) {
            LOG_ERROR( "[relations::Ui::PersonEdit::saveEventDetails()] Problem(s) encountered whilst committing change(s) to EventRole(s)." );
        }
        if( !_data_manager->update( _event ) ) {
            LOG_ERROR( "[relations::Ui::PersonEdit::saveEventDetails()] Problems encountered whilst committing change(s) to Event." );
        }
    }
    emit saveEventEditSignal();
}

void EventEdit::addEventRole() {
    LOG( "@EventEdit::addEventRole()" );
    _event_role_edit_dialog->setFields();
    _event_role_edit_dialog->exec();
}

void EventEdit::editEventRole() {
    LOG( "@EventEdit::editEventRole()" );
    if( _ui->participantTableView->selectionModel() ) {
        auto selected_row = _ui->participantTableView->selectionModel()->currentIndex().row();
        if( selected_row >= 0 ) {
            auto person_id = _event_roles_model->getPersonId( selected_row );
            auto type = _event_roles_model->getRoleType( selected_row );
            QString details = _event_roles_model->getDetails( selected_row );

            _event_role_edit_dialog->setFields( type, person_id, details );
            _event_role_edit_dialog->exec();
        }
    }
}

void EventEdit::removeEventRole() {
    LOG( "@EventEdit::removeEventRole()" );
    if( _ui->participantTableView->selectionModel() ) {
        auto selected_row = _ui->participantTableView->selectionModel()->currentIndex().row();
        if( selected_row >= 0 ) {
            if( !_event_roles_model->removeParticipant( selected_row ) ) {
                LOG_ERROR( "[relations::Ui::EventEdit::removeEventRole()] Could not remove Event Role at row ", selected_row );
            }
            _ui->participantTableView->resizeColumnsToContents();
        }
    }
}

void EventEdit::saveEventRole() {
    LOG( "@EventEdit::saveEventRole()" );
    auto selected_row = _ui->participantTableView->selectionModel()->currentIndex().row();
    auto person_id    = _event_role_edit_dialog->getPersonID();
    auto type         = _event_role_edit_dialog->getType();
    auto details      = _event_role_edit_dialog->getDetails();

    if( _event_role_edit_dialog->isNew() ) { //New Event Participant
        _event_roles_model->addParticipant( selected_row, person_id, _event._id, type, details );
    } else { //Edited relationship
        _event_roles_model->updateParticipant( selected_row, person_id, _event._id, type, details );
    }
    _ui->participantTableView->resizeColumnsToContents();
}

void EventEdit::setEventType() {
    using relations::enums::to_EventType;
    auto index = _ui->typeComboBox->currentIndex();
    _event._type = to_EventType( _ui->typeComboBox->itemData( index ).toInt() );
}

void EventEdit::setCustomEventType() {
    //TODO Future release: implement customtype
}

bool EventEdit::checkFields() {
    bool error_flag = false;
    if( _ui->dateLineEdit->text().isEmpty() ) {
        error_flag = true;
        _ui->dateLineEdit->setStyleSheet( "QLineEdit { background: rgb(255, 120, 120); }" );
    } else {
        _ui->dateLineEdit->setStyleSheet( "" );
    }
    if( _ui->descriptionLineEdit->text().isEmpty() ) {
        error_flag = true;
        _ui->descriptionLineEdit->setStyleSheet( "QLineEdit { background: rgb(255, 120, 120); }" );
    } else {
        _ui->descriptionLineEdit->setStyleSheet( "" );
    }
    if( _ui->noteSubjectEdit->text().isEmpty() ) {
        error_flag = true;
        _ui->noteSubjectEdit->setStyleSheet( "QLineEdit { background: rgb(255, 120, 120); }" );
    } else {
        _ui->noteSubjectEdit->setStyleSheet( "" );
    }
    if( error_flag ) {
        QMessageBox msg_box;
        msg_box.setWindowTitle( "ReLations Error" );
        msg_box.setText( "Date, Location description, and a note heading with the title of the event is the minimum required." );
        msg_box.exec();
        return false;
    } else {
        return true;
    }
}
