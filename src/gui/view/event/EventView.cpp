#include "EventView.h"
#include "ui_EventView.h"

EventView::EventView( QWidget *parent,
                      std::shared_ptr<relations::io::DataManager> data_manager,
                      relations::dto::Event &event ) :
    QWidget( parent ),
    _ui( new relations::Ui::EventView ),
    _data_manager( data_manager ),
    _data_cache( _data_manager->getDataCache() ),
    _event( &event )
{
    _ui->setupUi( this );
    _ui->idDisplay->setText( QString::number( _event->_id ) );
    auto type = std::find_if( _data_cache->_event_types.begin(),
                              _data_cache->_event_types.end(),
                              ( [&]( const relations::dto::EnumType<relations::enums::EventType> &etype ) {
                                  return etype._type == _event->_type;
                              } )
    );
    if( type != _data_cache->_event_types.end() ) {
        _ui->typeDisplay->setText( QString::fromStdString( type->_value ) );
    }

    if( _event->_date ) {
        _ui->formalDateDisplay->setText( QString::fromStdString( _event->_date->_formal ) );
        _ui->dateDisplay->setText( QString::fromStdString( _event->_date->_original ) );
    }
    if( _event->_location ) {
        _ui->descriptionDisplay->setText( QString::fromStdString( _event->_location->_description ) );
        _ui->addressDisplay->setText( QString::fromStdString( _event->_location->_address ) );
        _ui->postcodeDisplay->setText( QString::fromStdString( _event->_location->_postcode ) );
        _ui->cityDisplay->setText( QString::fromStdString( _event->_location->_city ) );
        _ui->countyDisplay->setText( QString::fromStdString( _event->_location->_county ) );
        _ui->countryDisplay->setText( QString::fromStdString( _event->_location->_country ) );
        _ui->longitudeDisplay->setText( QString::fromStdString( _event->_location->_longitude ) );
        _ui->latitudeDisplay->setText( QString::fromStdString( _event->_location->_latitude ) );
    }
    if( _event->_note ) {
        _ui->noteSubjectLabel->setText( QString::fromStdString( _event->_note->_subject ) );
        _ui->noteTextEdit->document()->setPlainText( QString::fromStdString( _event->_note->_text ) );
    }

    //Creating Table Model
    _event_roles_model = new EventParticipantsTableModel( _data_manager, *_event );
    _ui->participantTableView->setModel( _event_roles_model );
    _ui->participantTableView->resizeColumnsToContents();
}

EventView::~EventView() {
    delete _ui;
}