#ifndef RELATIONS_EVENTEDIT_H
#define RELATIONS_EVENTEDIT_H

#include <QWidget>
#include <eadlib/logger/Logger.h>
#include <src/gui/model/event/EventParticipantsTableModel.h>
#include "src/io/DataManager.h"
#include "src/gui/view/event/dialog/EventRoleEdit.h"

namespace relations::Ui {
    class EventEdit;
    class EventParticipantsTableModel;
    class EventRoleEdit;
}

class EventEdit : public QWidget {
  Q_OBJECT

  public:
    EventEdit( QWidget *parent,
               std::shared_ptr<relations::io::DataManager> data_manager,
               const relations::dto::Event &event );
    ~EventEdit();

  public slots:
    void saveEventDetails();

  signals:
    void cancelEventEditSignal();
    void saveEventEditSignal();

  private slots:
    void addEventRole();
    void editEventRole();
    void removeEventRole();
    void saveEventRole();

    void setEventType();
    void setCustomEventType();

  private:
    bool checkFields();
    relations::Ui::EventEdit                   *_ui;
    EventParticipantsTableModel                *_event_roles_model;
    EventRoleEdit                              *_event_role_edit_dialog;
    std::shared_ptr<relations::io::DataManager> _data_manager;
    relations::dto::Event                       _event;
    relations::io::DataCache                   *_data_cache;
};


#endif //RELATIONS_EVENTEDIT_H
