#ifndef RELATIONS_EVENTVIEW_H
#define RELATIONS_EVENTVIEW_H

#include <QWidget>
#include <eadlib/logger/Logger.h>
#include <src/gui/model/event/EventParticipantsTableModel.h>
#include "src/io/DataManager.h"

namespace relations::Ui {
    class EventView;
    class EventParticipantsTableModel;
}

class EventView : public QWidget {
  Q_OBJECT

  public:
    EventView( QWidget *parent,
               std::shared_ptr<relations::io::DataManager> data_manager,
               relations::dto::Event &event );
    ~EventView();

  private:
    relations::Ui::EventView                   *_ui;
    EventParticipantsTableModel                *_event_roles_model;
    std::shared_ptr<relations::io::DataManager> _data_manager;
    relations::io::DataCache                   *_data_cache;
    relations::dto::Event                      *_event;
};


#endif //RELATIONS_EVENTVIEW_H
