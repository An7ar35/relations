#include <QtWidgets/QMessageBox>
#include "EventRoleEdit.h"
#include "ui_EventRoleEdit.h"

EventRoleEdit::EventRoleEdit( QWidget *parent, std::shared_ptr<relations::io::DataManager> data_manager, const int64_t &parent_id ) :
    QDialog( parent ),
    _ui( new relations::Ui::EventRoleEdit ),
    _data_manager( data_manager ),
    _new_flag( true ),
    _parent_id( parent_id )
{
    _ui->setupUi( this );
    auto data_cache = _data_manager->getDataCache();

    for( auto e : data_cache->_role_types ) {
        _ui->roleTypeComboBox->addItem( QString::fromStdString( e._value ),
                                        QString::number( e._id )
        );
    }

    for( auto it = _data_manager->cbegin(); it != _data_manager->cend(); ++it ) {
        QString text = QString( "%1 (%2)" )
            .arg( QString::fromStdString( _data_manager->getPreferedName( it->second.value ) ) )
            .arg( QString::number( it->second.value._id ) );

        _ui->personComboBox->addItem( text,
                                      QString::number( it->second.value._id )
        );
    }
}

EventRoleEdit::~EventRoleEdit() {
    delete _ui;
}

int64_t EventRoleEdit::getPersonID() {
    return _ui->personComboBox->itemData( _ui->personComboBox->currentIndex() ).toInt();
}

relations::enums::RoleType EventRoleEdit::getType() {
    auto selected_id = _ui->roleTypeComboBox->itemData( _ui->roleTypeComboBox->currentIndex() ).toInt();
    return relations::enums::to_RoleType( selected_id );
}

QString EventRoleEdit::getDetails() {
    return _ui->detailsLineEdit->text();
}

bool EventRoleEdit::isNew() {
    return _new_flag;
}

void EventRoleEdit::setFields() {
    _new_flag = true;
    auto enum_int = eadlib::enum_int<relations::enums::RoleType>( relations::enums::RoleType::PARTICIPANT );
    _ui->roleTypeComboBox->setCurrentIndex(  _ui->roleTypeComboBox->findData( QVariant::fromValue( enum_int ) ) );
    _ui->roleTypeComboBox->setDisabled( false );
    _ui->personComboBox->setDisabled( false );
}

void EventRoleEdit::setFields( const relations::enums::RoleType &role_type,
                               int64_t &person_id,
                               QString &details )
{
    _new_flag = false;
    auto enum_int = eadlib::enum_int<relations::enums::RoleType>( role_type );
    _ui->roleTypeComboBox->setCurrentIndex(  _ui->roleTypeComboBox->findData( QVariant::fromValue( enum_int ) ) );
    _ui->personComboBox->setCurrentIndex( _ui->personComboBox->findData( QVariant::fromValue( person_id ) ) );
    _ui->roleTypeComboBox->setDisabled( true );
    _ui->personComboBox->setDisabled( true );
    _ui->detailsLineEdit->setText( details );
}

void EventRoleEdit::done( int i ) {
    if( QDialog::Accepted == i ) {
        bool error_flag = false;
        if( getDetails().isEmpty() ) {
            error_flag = true;
            _ui->detailsLineEdit->setStyleSheet( "QLineEdit { background: rgb(255, 120, 120); }" );
        } else {
            _ui->detailsLineEdit->setStyleSheet( "" );
        }
        if( error_flag ) {
            QMessageBox msg_box;
            msg_box.setWindowTitle( "ReLations Error" );
            msg_box.setText( "Detail of participant's role is required." );
            msg_box.exec();
            return;
        } else {
            QDialog::done( i );
        }
    } else {
        QDialog::done( i );
        return;
    }
}
