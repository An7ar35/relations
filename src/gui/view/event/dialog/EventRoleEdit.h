#ifndef RELATIONS_EVENTROLEEDIT_H
#define RELATIONS_EVENTROLEEDIT_H

#include <QDialog>
#include <src/io/DataManager.h>
#include <src/gui/model/event/EventParticipantsTableModel.h>

namespace relations::Ui {
    class EventRoleEdit;
    class EventParticipantsTableModel;
}

class EventRoleEdit : public QDialog {
  Q_OBJECT

  public:
    EventRoleEdit( QWidget *parent,
                   std::shared_ptr<relations::io::DataManager> data_manager,
                   const int64_t &parent_id );
    ~EventRoleEdit();

    int64_t getPersonID();
    relations::enums::RoleType getType();
    QString getDetails();

    bool isNew();

  public slots:
    void setFields();

    void setFields( const relations::enums::RoleType &role_type,
                    int64_t &person_id,
                    QString &details );

    void done( int i ) override;

  private:
    relations::Ui::EventRoleEdit               *_ui;
    std::shared_ptr<relations::io::DataManager> _data_manager;
    bool                                        _new_flag;
    int64_t                                     _parent_id;
};


#endif //RELATIONS_EVENTROLEEDIT_H
