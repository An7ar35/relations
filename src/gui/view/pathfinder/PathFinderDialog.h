#ifndef RELATIONS_PATHFINDER_H
#define RELATIONS_PATHFINDER_H

#include <QDialog>
#include <src/io/DataManager.h>
#include <src/gui/model/pathfinder/PathFinderModel.h>

namespace relations::Ui {
    class PathFinderDialog;
    class PathFinderModel;
}

class PathFinderDialog : public QDialog {
  Q_OBJECT

  public:
    PathFinderDialog( QWidget *parent, std::shared_ptr<relations::io::DataManager> data_manager );
    ~PathFinderDialog();

  public slots:
    void searchPath();

  private:
    relations::Ui::PathFinderDialog            *_ui;
    std::shared_ptr<relations::io::DataManager> _data_manager;
    PathFinderModel                            *_path_finder_model;
};


#endif //RELATIONS_PATHFINDER_H
