#include <QtWidgets/QHeaderView>
#include "PathFinderDialog.h"
#include "ui_PathFinderDialog.h"

PathFinderDialog::PathFinderDialog( QWidget *parent,
                                    std::shared_ptr<relations::io::DataManager> data_manager ) :
    QDialog( parent ),
    _ui( new relations::Ui::PathFinderDialog ),
    _data_manager( data_manager )
{
    _ui->setupUi( this );
    //Fill combo boxes
    for( auto it = _data_manager->cbegin(); it != _data_manager->cend(); ++it ) {
        QString name = QString::fromStdString( _data_manager->getPreferedName( it->second.value ) );
        _ui->fromComboBox->addItem( QString( "%1 (%2)" ).arg( name ).arg( it->second.value._id ),
                                    QString::number( it->second.value._id )
        );
        _ui->toComboBox->addItem( QString( "%1 (%2)" ).arg( name ).arg( it->second.value._id ),
                                  QString::number( it->second.value._id )
        );
    }
    //Connect buttons with actions
    connect( _ui->goButton, SIGNAL( clicked() ), this, SLOT( searchPath() ) );
    connect( _ui->closeButton, SIGNAL( clicked() ), this, SLOT( close() ) );
}

PathFinderDialog::~PathFinderDialog() {
    delete _ui;
}

void PathFinderDialog::searchPath() {
    auto from_index = _ui->fromComboBox->currentIndex();
    auto to_index   = _ui->toComboBox->currentIndex();
    if( from_index >= 0 && to_index >= 0 ) {
        try {
            int64_t from_id = _ui->fromComboBox->itemData( from_index ).toInt();
            int64_t to_id   = _ui->toComboBox->itemData( to_index ).toInt();
            auto from_name  = QString::fromStdString( _data_manager->getPreferedName( _data_manager->at( from_id ) ) );
            auto to_name    = QString::fromStdString( _data_manager->getPreferedName( _data_manager->at( to_id ) ) );
            _ui->fromDisplayLabel->setText( QString( "%1 (%2)" ).arg( from_name ).arg( from_id ) );
            _ui->toDisplayLabel->setText( QString( "%1 (%2)" ).arg( to_name ).arg( to_id ) );
            _path_finder_model = new PathFinderModel( _data_manager, from_id, to_id );
            _ui->pathTableView->setModel( _path_finder_model );
            _ui->pathTableView->resizeColumnsToContents();
        } catch( std::out_of_range ) {
            _ui->fromDisplayLabel->clear();
            _ui->toDisplayLabel->clear();
            LOG_ERROR( "[relations::Ui::PathFinderDialog::searchPath()] Problems accessing data manager data." );
        }
    }
}


