#ifndef RELATIONS_MEDIAACTOREDIT_H
#define RELATIONS_MEDIAACTOREDIT_H

#include <QDialog>
#include <src/dto/MediaRelation.h>
#include <src/io/DataManager.h>

namespace relations::Ui {
    class MediaActorEditDialog;
}

class MediaActorEditDialog : public QDialog {
  Q_OBJECT

  public:
    MediaActorEditDialog( QWidget *parent, std::shared_ptr<relations::io::DataManager> data_manager );
    ~MediaActorEditDialog();

    int64_t getPersonId();

  public slots:
    void setFields();
    void done( int i ) override;

  private:
    relations::Ui::MediaActorEditDialog        *_ui;
    std::shared_ptr<relations::io::DataManager> _data_manager;
};


#endif //RELATIONS_MEDIAACTOREDIT_H
