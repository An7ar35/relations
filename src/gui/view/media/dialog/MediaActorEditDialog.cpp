#include <QMessageBox>
#include "MediaActorEditDialog.h"
#include "ui_MediaActorEditDialog.h"

MediaActorEditDialog::MediaActorEditDialog( QWidget *parent,
                                std::shared_ptr<relations::io::DataManager> data_manager ) :
    QDialog( parent ),
    _ui( new relations::Ui::MediaActorEditDialog ),
    _data_manager( data_manager )
{
    _ui->setupUi( this );
}

MediaActorEditDialog::~MediaActorEditDialog() {
    delete _ui;
}

int64_t MediaActorEditDialog::getPersonId() {
    return _ui->personComboBox->itemData( _ui->personComboBox->currentIndex() ).toInt();
}

void MediaActorEditDialog::setFields() {
    _ui->personComboBox->clear();
    for( auto it = _data_manager->cbegin(); it != _data_manager->cend(); ++it ) {
        QString name = QString::fromStdString( _data_manager->getPreferedName( it->second.value ) );
        _ui->personComboBox->addItem( QString( "%1 (%2)" ).arg( name ).arg( it->second.value._id ),
                                      QString::number( it->second.value._id )
        );
    }
}

void MediaActorEditDialog::done( int i ) {
    if( QDialog::Accepted == i ) {
        if( _ui->personComboBox->currentIndex() < 0 ) {
            QMessageBox msg_box;
            msg_box.setWindowTitle( "ReLations Error" );
            msg_box.setText( "A person must be chosen." );
            msg_box.exec();
            return;
        } else {
            QDialog::done( i );
            return;
        }
    } else {
        QDialog::done( i );
        return;
    }
}