#include <QMessageBox>
#include "MediaLinkEventDialog.h"
#include "ui_MediaLinkEventDialog.h"


MediaLinkEventDialog::MediaLinkEventDialog( QWidget *parent, std::shared_ptr<relations::io::DataManager> data_manager ) :
    QDialog( parent ),
    _ui( new relations::Ui::MediaLinkEventDialog ),
    _data_manager( data_manager )
{
    _ui->setupUi( this );
}

MediaLinkEventDialog::~MediaLinkEventDialog() {
    delete _ui;
}

int64_t MediaLinkEventDialog::getEventSubjectID() {
    return _ui->eventListComboBox->itemData( _ui->eventListComboBox->currentIndex() ).toInt();
}

int64_t MediaLinkEventDialog::getEventID() {
    auto subject_id = _ui->eventListComboBox->itemData( _ui->eventListComboBox->currentIndex() ).toInt();
    try {
        return _subject_event_map.at( subject_id );
    } catch( std::out_of_range ) {
        LOG_ERROR( "[relations::Ui::MediaLinkEventDialog::getEventID()] "
                       "Subject [", subject_id, "] has not been mapped to a event id at construction." );
        return -1;
    }
}

void MediaLinkEventDialog::setFields() {
    _subject_event_map.clear();
    _ui->eventListComboBox->clear();
    auto events = _data_manager->getEvents();
    for( auto it = events->begin(); it != events->end(); ++it ) {
        QString event_heading = QString::fromStdString( it->_note->_subject );
        _ui->eventListComboBox->addItem( QString( "%1 (%2)" ).arg( event_heading ).arg( it->_id ),
                                         QString::number( it->_subject_id )
        );
        _subject_event_map.insert( { it->_subject_id, it->_id } );
    }
    connect( _ui->eventListComboBox , SIGNAL( currentIndexChanged( int ) ), this, SLOT( loadFields() ) );
    if( _ui->eventListComboBox->currentIndex() >= 0 ) {
        loadFields();
    }
}

void MediaLinkEventDialog::loadFields() {
    LOG( "@MediaLinkEventDialog::loadFields()" );
    auto index     = _ui->eventListComboBox->currentIndex();
    auto event_id  = getEventID();
    try {
        auto event = _data_manager->getEvent( event_id );
        _ui->dateDisplayLabel->setText( event->_date
                                        ? QString::fromStdString( event->_date->_original )
                                        : "No date found."
        );
        _ui->locationDescDisplayLabel->setText( event->_location
                                                ? QString::fromStdString( event->_location->_description )
                                                : "No description found."
        );
        _ui->noteSubjectDisplayLabel->setText( event->_note
                                               ? QString::fromStdString( event->_note->_subject )
                                               : "No note found."
        );
    } catch( std::out_of_range ) {
        LOG_ERROR( "[relations::Ui::MediaLinkEventDialog::loadFields()] Event with ID [", event_id, "] does not exist in the records." );
    }
}

void MediaLinkEventDialog::done( int i ) {
    if( QDialog::Accepted == i ) {
        if( _ui->eventListComboBox->currentIndex() < 0 ) {
            QMessageBox msg_box;
            msg_box.setWindowTitle( "ReLations Error" );
            msg_box.setText( "An event must be chosen." );
            msg_box.exec();
            return;
        } else {
            QDialog::done( i );
            return;
        }
    } else {
        QDialog::done( i );
        return;
    }
}