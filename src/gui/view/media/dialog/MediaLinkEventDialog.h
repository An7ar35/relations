#ifndef RELATIONS_MEDIALINKEVENTDIALOG_H
#define RELATIONS_MEDIALINKEVENTDIALOG_H

#include <QDialog>
#include <src/io/DataManager.h>

namespace relations::Ui {
    class MediaLinkEventDialog;
}

class MediaLinkEventDialog : public QDialog {
  Q_OBJECT

  public:
    MediaLinkEventDialog( QWidget *parent,
                   std::shared_ptr<relations::io::DataManager> data_manager );
    ~MediaLinkEventDialog();

    int64_t getEventSubjectID();
    int64_t getEventID();

  public slots:
    void setFields();
    void done( int i ) override;

  private slots:
    void loadFields();

  private:
    relations::Ui::MediaLinkEventDialog        *_ui;
    std::shared_ptr<relations::io::DataManager> _data_manager;
    std::unordered_map<int64_t, int64_t>        _subject_event_map;
};


#endif //RELATIONS_MEDIALINKEVENTDIALOG_H
