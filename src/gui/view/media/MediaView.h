#ifndef RELATIONS_MEDIAVIEW_H
#define RELATIONS_MEDIAVIEW_H

#include <QWidget>
#include <eadlib/logger/Logger.h>
#include <src/io/DataManager.h>
#include <src/gui/model/media/MediaActorsTableModel.h>
#include <src/gui/view/media/widget/MediaViewEvent.h>

namespace relations::Ui {
    class MediaView;
    class MediaActorsTableModel;
    class MediaViewEvent;
}

class MediaView : public QWidget {
  Q_OBJECT

  public:
    MediaView( QWidget *parent,
        std::shared_ptr<relations::io::DataManager> data_manager,
        relations::dto::Media &media );
    ~MediaView();

    private slots:
  void openMediaWithExternal();

  private:
    relations::Ui::MediaView                   *_ui;
    std::shared_ptr<relations::io::DataManager> _data_manager;
    relations::io::DataCache                   *_data_cache;
    relations::dto::Media                      *_media;
    MediaActorsTableModel                      *_actors_model;
    MediaViewEvent                             *_view_event_widget;
};

#endif //RELATIONS_MEDIAVIEW_H
