#include <QDesktopServices>
#include <QApplication>
#include <QtCore/QUrl>
#include "MediaView.h"
#include "ui_MediaView.h"

MediaView::MediaView( QWidget *parent,
                      std::shared_ptr<relations::io::DataManager> data_manager,
                      relations::dto::Media &media ) :
    QWidget( parent ),
    _ui( new relations::Ui::MediaView ),
    _data_manager( data_manager ),
    _data_cache( _data_manager->getDataCache() ),
    _media( &media )
{
    _ui->setupUi( this );
    _ui->eventGroupBox->hide();
    _ui->mediaIdDisplayLabel->setText( QString::number( media._id ) );
    auto type = std::find_if( _data_cache->_media_types.begin(),
                              _data_cache->_media_types.end(),
                              ( [&]( const relations::dto::EnumType<relations::enums::MediaType> &mtype ) {
                                  return mtype._type == media._type;
                              } )
    );
    if( type != _data_cache->_media_types.end() ) {
        _ui->mediaTypeDisplayLabel->setText( QString::fromStdString( type->_value ) );
    }
    _ui->mediaDescDisplayLabel->setText( QString::fromStdString( media._description ) );
    _ui->mediaFileNameDisplayLabel->setText( QString::fromStdString( media._file ) );

    //Creating Table Model
    _actors_model = new MediaActorsTableModel( _data_manager, *_media );
    _ui->actorsTableView->setModel( _actors_model );
    _ui->actorsTableView->resizeColumnsToContents();
    _ui->actorsTableView->horizontalHeader()->setSectionResizeMode( 1, QHeaderView::Stretch );
    //Creating EventView (if any is linked)
    try {
        auto event = _data_manager->getMediaEvent( *_media );
        _view_event_widget = new MediaViewEvent( this, _data_manager, *event );
        _ui->mediaEventLayout->addWidget( _view_event_widget );
        _ui->eventGroupBox->show();
    } catch( std::out_of_range ) {
        LOG_DEBUG( "[relations::Ui::MediaView::MediaView(..)] No linked Event found for Media [", media._id, "]." );
    }

    connect( _ui->openMediaButton, SIGNAL( clicked() ), this, SLOT( openMediaWithExternal() ) );
    _ui->openMediaButton->setEnabled( false ); //TODO temporary until the picasa section is created
}

MediaView::~MediaView() {
    delete _ui;
}

void MediaView::openMediaWithExternal() {
    LOG( "@MediaView::openMediaWithExternal()" );
    //TODO
}
