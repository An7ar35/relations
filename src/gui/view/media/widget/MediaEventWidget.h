#ifndef RELATIONS_MEDIAEVENTWIDGET_H
#define RELATIONS_MEDIAEVENTWIDGET_H

#include <QWidget>
#include <eadlib/logger/Logger.h>
#include <src/gui/view/media/dialog/MediaLinkEventDialog.h>
#include "src/io/DataManager.h"
#include "MediaLinkEvent.h"
#include "MediaUnlinkEvent.h"
#include "MediaViewEvent.h"
#include <src/datastructure/UpdateTracker.h>

namespace relations::Ui {
    class MediaEventWidget;
    class MediaLinkEvent;
    class MediaUnlinkEvent;
    class MediaViewEvent;
    class MediaLinkEventDialog;
}

class MediaEventWidget : public QWidget {
  Q_OBJECT

  public:
    MediaEventWidget( QWidget *parent,
                      std::shared_ptr<relations::io::DataManager> data_manager );
    ~MediaEventWidget();

    void setMediaId( const int64_t &id );
    bool commitChangesToDb();

  public slots:
    void loadView( const relations::dto::Media &media );

  signals:

  private slots:
    void linkEvent();
    void setLinkedEvent();
    void unlinkEvent();
    void reloadView();
    void clearLayout( QLayout *layout );


  private:
    relations::Ui::MediaEventWidget            *_ui;
    std::shared_ptr<relations::io::DataManager> _data_manager;

    int64_t                                     _linked_event_id;
    int64_t                                     _linked_event_sid;
    MediaLinkEvent                             *_link_event_widget;
    MediaUnlinkEvent                           *_unlink_event_widget;
    MediaViewEvent                             *_view_event_widget;
    MediaLinkEventDialog                       *_event_link_dialog;

    bool                                        _media_ids_are_set_flag { false };
    relations::UpdateTracker<int64_t, relations::dto::MediaRelation> _updates;
};


#endif //RELATIONS_MEDIAEVENTWIDGET_H
