#include "MediaLinkEvent.h"
#include "ui_MediaLinkEvent.h"


MediaLinkEvent::MediaLinkEvent( QWidget *parent ) :
    QWidget( parent ),
    _ui( new relations::Ui::MediaLinkEvent )
{
    _ui->setupUi( this );
    connect( _ui->linkEventButton, SIGNAL( clicked() ), this, SLOT( linkEvent() ) );
}

MediaLinkEvent::~MediaLinkEvent() {
    delete _ui;
}

void MediaLinkEvent::linkEvent() {
    emit linkEventSignal();
}
