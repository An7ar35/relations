#include <src/gui/view/media/dialog/MediaLinkEventDialog.h>
#include "MediaEventWidget.h"
#include "ui_MediaEventWidget.h"


MediaEventWidget::MediaEventWidget( QWidget *parent,
                                    std::shared_ptr<relations::io::DataManager> data_manager ) :
    QWidget( parent ),
    _ui( new relations::Ui::MediaEventWidget ),
    _data_manager( data_manager ),
    _linked_event_sid( -1 )
{
    _ui->setupUi( this );
    _ui->mediaEventWidgetLayout->setEnabled( false );
}

MediaEventWidget::~MediaEventWidget() {
    delete _ui;
}

void MediaEventWidget::setMediaId( const int64_t &id ) {
    for( auto it = _updates.additions_begin(); it != _updates.additions_end(); ++it ) {
        it->second._media_id = id;
    }
    for( auto it = _updates.removals_begin(); it != _updates.removals_end(); ++it ) {
        it->second._media_id = id;
    }
    _media_ids_are_set_flag = true;
}

bool MediaEventWidget::commitChangesToDb() {
    if( !_media_ids_are_set_flag ) {
        LOG_ERROR( "[relations::Ui::MediaEventWidget::commitChangesToDb()] "
                       "Media IDs have not been set or changes have been made since they were." );
        return false;
    }
    auto add_count     = 0;
    auto removal_count = 0;
    auto add_total     = _updates.additionsCount();
    auto update_total  = _updates.updatesCount();
    auto removal_total = _updates.removalCount();
    //Additions
    auto addition_it = _updates.additions_begin();
    while( addition_it != _updates.additions_end() ) {
        if( _data_manager->add( addition_it->second ) ) {
            addition_it = _updates.additions_erase( addition_it );
            add_count++;
        } else {
            addition_it = std::next( addition_it );
        }
    }
    //Removals
    auto removal_it = _updates.removals_begin();
    while( removal_it != _updates.removals_end() ) {
        if( _data_manager->remove( removal_it->second ) ) {
            removal_it = _updates.removals_erase( removal_it );
            removal_count++;
        } else {
            removal_it = std::next( removal_it );
        }
    }
    LOG( "[relations::Ui::MediaEventWidget::commitChangesToDb()] ", add_count, "/", add_total, " MediaRelation(s) added." );
    LOG( "[relations::Ui::MediaEventWidget::commitChangesToDb()] ", removal_count, "/", removal_total, " MediaRelation(s) removed." );
    if( !_updates.empty() ) {
        LOG_ERROR( "[relations::Ui::MediaEventWidget::commitChangesToDb()] "
                       "Could not commit some/all the changes. ", _updates.size(), " remain." );
        return false;
    }
    return true;
}

void MediaEventWidget::loadView( const relations::dto::Media &media ) {
    if( media._id > 0 ) {
        try {
            auto event = _data_manager->getMediaEvent( media );
            _view_event_widget   = new MediaViewEvent( this, _data_manager, *event );
            _unlink_event_widget = new MediaUnlinkEvent( this );
            connect( _unlink_event_widget, SIGNAL( unlinkEventSignal() ), this, SLOT( unlinkEvent() ) );
            _ui->mediaEventWidgetLayout->addWidget( _view_event_widget );
            _ui->mediaEventWidgetLayout->addWidget( _unlink_event_widget );
            _linked_event_sid = event->_subject_id;
        } catch( std::out_of_range ) {
            LOG_DEBUG( "[relations::Ui::MediaEventWidget::loadView( const dto::Media & )] "
                           "No linked Event found for Media [", media._id, "]." );
            _link_event_widget = new MediaLinkEvent( this );
            connect( _link_event_widget, SIGNAL( linkEventSignal() ), this, SLOT( linkEvent() ) );
            _ui->mediaEventWidgetLayout->addWidget( _link_event_widget );
        }
    } else {
        LOG_DEBUG( "[relations::Ui::MediaEventWidget::loadView( const dto::Media & )] "
                       "New Media [", media._id, "]. No Events linked yet." );
        _link_event_widget = new MediaLinkEvent( this );
        connect( _link_event_widget, SIGNAL( linkEventSignal() ), this, SLOT( linkEvent() ) );
        _ui->mediaEventWidgetLayout->addWidget( _link_event_widget );
    }
}

void MediaEventWidget::linkEvent() {
    LOG( "@MediaEventWidget::linkEvent()" );
    _event_link_dialog = new MediaLinkEventDialog( this, _data_manager );
    connect( _event_link_dialog, SIGNAL( accepted() ), this, SLOT( setLinkedEvent() ) );
    _event_link_dialog->setFields();
    _event_link_dialog->exec();
}

void MediaEventWidget::setLinkedEvent() {
    LOG( "@MediaEventWidget::setLinkedEvent()" );
    if( _event_link_dialog ) {
        _linked_event_id  = _event_link_dialog->getEventID();
        _linked_event_sid = _event_link_dialog->getEventSubjectID();
        LOG_DEBUG( "[relations::Ui::MediaEventWidget::setLinkedEvent()] Event set to [", _linked_event_id, "]." );
        _updates.toAdd( _linked_event_sid, relations::dto::MediaRelation( _linked_event_sid, -1 ) );
        reloadView();
    }
}

void MediaEventWidget::unlinkEvent() {
    LOG( "@MediaEventWidget::unlinkEvent()" );
    clearLayout( _ui->mediaEventWidgetLayout );
    _updates.toRemove( _linked_event_sid, relations::dto::MediaRelation( _linked_event_sid, -1 ) );
    _linked_event_id  = -1;
    _linked_event_sid = -1;
    _link_event_widget = new MediaLinkEvent( this );
    connect( _link_event_widget, SIGNAL( linkEventSignal() ), this, SLOT( linkEvent() ) );
    _ui->mediaEventWidgetLayout->addWidget( _link_event_widget );
}

void MediaEventWidget::reloadView() {
    if( _linked_event_sid > 0 ) {
        clearLayout( _ui->mediaEventWidgetLayout );
        try {
            auto event = _data_manager->getEvent( _linked_event_id );
            _view_event_widget   = new MediaViewEvent( this, _data_manager, *event );
            _unlink_event_widget = new MediaUnlinkEvent( this );
            connect( _unlink_event_widget, SIGNAL( unlinkEventSignal() ), this, SLOT( unlinkEvent() ) );
            _ui->mediaEventWidgetLayout->addWidget( _view_event_widget );
            _ui->mediaEventWidgetLayout->addWidget( _unlink_event_widget );
        } catch( std::out_of_range ) {
            LOG_ERROR( "[relations::Ui::MediaEventWidget::loadView( const dto::Media & )] "
                           "Event [", _linked_event_sid, "] does not exist in records." );
            _link_event_widget = new MediaLinkEvent( this );
            connect( _link_event_widget, SIGNAL( linkEventSignal() ), this, SLOT( linkEvent() ) );
            _ui->mediaEventWidgetLayout->addWidget( _link_event_widget );
        }
    }
}

void MediaEventWidget::clearLayout( QLayout *layout ) {
    if( layout ) {
        QLayoutItem *item;
        while( ( item = layout->takeAt( 0 ) ) != NULL ) {
            delete item->widget();
            delete item;
        }
    }
}