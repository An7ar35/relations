#ifndef RELATIONS_MEDIAUNLINKEVENT_H
#define RELATIONS_MEDIAUNLINKEVENT_H

#include <QWidget>
#include <eadlib/logger/Logger.h>

namespace relations::Ui {
    class MediaUnlinkEvent;
}

class MediaUnlinkEvent : public QWidget {
  Q_OBJECT

  public:
    MediaUnlinkEvent( QWidget *parent );
    ~MediaUnlinkEvent();

  signals:
    void unlinkEventSignal();

  public slots:
    void unlinkEvent();

  private:
    relations::Ui::MediaUnlinkEvent *_ui;
};

#endif //RELATIONS_MEDIAUNLINKEVENT_H
