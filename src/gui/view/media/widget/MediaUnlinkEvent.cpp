#include "MediaUnlinkEvent.h"
#include "ui_MediaUnlinkEvent.h"

MediaUnlinkEvent::MediaUnlinkEvent( QWidget *parent ) :
    QWidget( parent ),
    _ui( new relations::Ui::MediaUnlinkEvent )
{
    _ui->setupUi( this );
    connect( _ui->unlinkButton, SIGNAL( clicked() ), this, SLOT( unlinkEvent() ) );
}

MediaUnlinkEvent::~MediaUnlinkEvent() {
    delete _ui;
}

void MediaUnlinkEvent::unlinkEvent() {
    emit unlinkEventSignal();
}
