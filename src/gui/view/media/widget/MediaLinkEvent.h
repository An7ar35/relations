#ifndef RELATIONS_MEDIALINKEVENT_H
#define RELATIONS_MEDIALINKEVENT_H

#include <QWidget>
#include <eadlib/logger/Logger.h>

namespace relations::Ui {
    class MediaLinkEvent;
}

class MediaLinkEvent : public QWidget {
  Q_OBJECT

  public:
    MediaLinkEvent( QWidget *parent );
    ~MediaLinkEvent();

  signals:
    void linkEventSignal();

  public slots:
    void linkEvent();

  private:
    relations::Ui::MediaLinkEvent *_ui;
};


#endif //RELATIONS_MEDIALINKEVENT_H
