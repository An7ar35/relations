#ifndef RELATIONS_MEDIAVIEWEVENT_H
#define RELATIONS_MEDIAVIEWEVENT_H

#include <QWidget>
#include <eadlib/logger/Logger.h>
#include <src/gui/model/event/EventParticipantsTableModel.h>
#include "src/io/DataManager.h"

namespace relations::Ui {
    class MediaViewEvent;
    class EventParticipantsTableModel;
}

class MediaViewEvent : public QWidget {
  Q_OBJECT

  public:
    MediaViewEvent( QWidget *parent,
                    std::shared_ptr<relations::io::DataManager> data_manager,
                    relations::dto::Event &event );
    ~MediaViewEvent();

  private:
    relations::Ui::MediaViewEvent              *_ui;
    std::shared_ptr<relations::io::DataManager> _data_manager;
    relations::io::DataCache                   *_data_cache;
    EventParticipantsTableModel                *_event_roles_model;
    relations::dto::Event                      *_event;
};


#endif //RELATIONS_MEDIAVIEWEVENT_H
