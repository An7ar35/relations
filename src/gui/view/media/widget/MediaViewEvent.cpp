#include "MediaViewEvent.h"
#include "ui_MediaViewEvent.h"

MediaViewEvent::MediaViewEvent( QWidget *parent,
                                std::shared_ptr<relations::io::DataManager> data_manager,
                                relations::dto::Event &event ) :
    QWidget( parent ),
    _ui( new relations::Ui::MediaViewEvent ),
    _data_manager( data_manager ),
    _data_cache( _data_manager->getDataCache() ),
    _event( &event )
{
    _ui->setupUi( this );
    _ui->eventIdDisplayLabel->setText( QString::number( _event->_id ) );
    auto type = std::find_if( _data_cache->_event_types.begin(),
                              _data_cache->_event_types.end(),
                              ( [&]( const relations::dto::EnumType<relations::enums::EventType> &etype ) {
                                  return etype._type == _event->_type;
                              } )
    );
    if( type != _data_cache->_event_types.end() ) {
        _ui->eventTypeDisplayLabel->setText( QString::fromStdString( type->_value ) );
    }

    if( _event->_date ) {
        _ui->eventDateDisplayLabel->setText( QString::fromStdString( _event->_date->_original ) );
    }
    if( _event->_location ) {
        _ui->eventLocationDescDisplayLabel->setText( QString::fromStdString( _event->_location->_description ) );
    }
    if( _event->_note ) {
        _ui->noteSubjectDisplayLabel->setText( QString::fromStdString( _event->_note->_subject ) );
        _ui->notePlainTextEdit->document()->setPlainText( QString::fromStdString( _event->_note->_text ) );
    }

    //Creating Table Model
    _event_roles_model = new EventParticipantsTableModel( _data_manager, *_event );
    _ui->participantTableView->setModel( _event_roles_model );
    _ui->participantTableView->resizeColumnsToContents();
}

MediaViewEvent::~MediaViewEvent() {
    delete _ui;
}
