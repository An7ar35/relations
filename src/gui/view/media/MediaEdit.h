#ifndef RELATIONS_MEDIAEDIT_H
#define RELATIONS_MEDIAEDIT_H

#include <QWidget>
#include <eadlib/logger/Logger.h>
#include <src/gui/view/media/dialog/MediaActorEditDialog.h>
#include "src/io/DataManager.h"
#include "src/gui/model/media/MediaActorsTableModel.h"
#include "src/gui/view/media/widget/MediaEventWidget.h"

namespace relations::Ui {
    class MediaEdit;
    class MediaActorsTableModel;
    class MediaEventWidget;
}

class MediaEdit : public QWidget {
  Q_OBJECT

  public:
    MediaEdit( QWidget *parent,
               std::shared_ptr<relations::io::DataManager> data_manager,
               const relations::dto::Media &media );
    ~MediaEdit();

  public slots:
    void saveMediaDetails();

  signals:
    void cancelMediaEditSignal();
    void saveMediaEditSignal();

  private slots:
    void addMediaActor();
    void removeMediaActor();
    void saveMediaActor();

    void setMediaType();
    void setMediaFile();

  private:
    bool checkFields();
    relations::Ui::MediaEdit                   *_ui;
    MediaActorsTableModel                      *_media_actors_model;
    std::shared_ptr<relations::io::DataManager> _data_manager;
    relations::dto::Media                       _media;
    relations::io::DataCache                   *_data_cache;

    MediaEventWidget                           *_media_event_widget;
    MediaActorEditDialog                       *_media_actor_dialog;
};


#endif //RELATIONS_MEDIAEDIT_H
