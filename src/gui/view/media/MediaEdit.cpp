#include <QtWidgets/QFileDialog>
#include <QtWidgets/QMessageBox>
#include "MediaEdit.h"
#include "ui_MediaEdit.h"

MediaEdit::MediaEdit( QWidget *parent,
                      std::shared_ptr<relations::io::DataManager> data_manager,
                      const relations::dto::Media &media ) :
    QWidget( parent ),
    _ui( new relations::Ui::MediaEdit ),
    _data_manager( data_manager ),
    _data_cache( _data_manager->getDataCache() ),
    _media_event_widget( new MediaEventWidget( this, _data_manager ) ),
    _media( media )
{
    _ui->setupUi( this );
    //Populating the combo box
    for( auto e : _data_cache->_media_types ) {
        _ui->mediaTypeComboBox->addItem( QString::fromStdString( e._value ),
                                    QString::number( e._id )
        );
        if( e._type == _media._type ) {
            _ui->mediaTypeComboBox->setCurrentIndex( _ui->mediaTypeComboBox->count() - 1 );
        }
    }
    //Setting fields
    _ui->mediaIdDisplayLabel->setText( QString::number( _media._id ) );
    _ui->mediaDescLineEdit->setText( QString::fromStdString( _media._description ) );
    _ui->mediaFilePathLineEdit->setText( QString::fromStdString( _media._file ) );
    //Getting actors in media
    _media_actors_model = new MediaActorsTableModel( _data_manager, _media );
    _ui->actorsTableView->setModel( _media_actors_model );
    _ui->actorsTableView->resizeColumnsToContents();
    _ui->actorsTableView->horizontalHeader()->setSectionResizeMode( 1, QHeaderView::Stretch );
    //Getting Event (if any)
    _ui->mediaEventLayout->addWidget( _media_event_widget );
    _media_event_widget->loadView( _media );

    //Creating Editors for the tabled data
    _media_actor_dialog = new MediaActorEditDialog( this, _data_manager );

    //Connecting actions
    connect( _ui->getMediaFileButton, SIGNAL( clicked() ), this, SLOT( setMediaFile() ) );
    connect( _ui->mediaTypeComboBox , SIGNAL( currentIndexChanged( int ) ), this, SLOT( setMediaType() ) );

    connect( _ui->saveButton, SIGNAL( clicked() ), this, SLOT( saveMediaDetails() ) );
    connect( _ui->cancelButton, SIGNAL( clicked() ), this, SIGNAL( cancelMediaEditSignal() ) );

    connect( _ui->addActorButton, SIGNAL( clicked() ), this, SLOT( addMediaActor() ) );
    connect( _ui->removeActorButton, SIGNAL( clicked() ), this, SLOT( removeMediaActor() ) );
    connect( _media_actor_dialog, SIGNAL( accepted() ), this, SLOT( saveMediaActor() ) );
}

MediaEdit::~MediaEdit() {
    delete _ui;
}

void MediaEdit::saveMediaDetails() {
    LOG( "@MediaEdit::saveMediaDetails()" );
    if( checkFields() ) {
        _media._description = _ui->mediaDescLineEdit->text().toStdString();
        _media._file        = _ui->mediaFilePathLineEdit->text().toStdString();
        if( _media._id < 0 ) { //new media
            if( !_data_manager->add( _media ) ) {
                LOG_ERROR( "[relations::Ui::MediaEdit::saveMediaDetails()] Problem encountered whilst committing change(s) to new Media." );
            }
            _media_actors_model->setMediaId( _media._id );
            if( !_media_actors_model->commitChangesToDb() ) {
                LOG_ERROR( "[relations::Ui::MediaEdit::saveMediaDetails()] Problem(s) encountered whilst committing change(s) to Media actor(s)." );
            }
            _media_event_widget->setMediaId( _media._id );
            if( !_media_event_widget->commitChangesToDb() ) {
                LOG_ERROR( "[relations::Ui::MediaEdit::saveMediaDetails()] Problem(s) encountered whilst committing change(s) to linked events." );
            }
        } else { //updated media
            _media_actors_model->setMediaId( _media._id );
            if( !_media_actors_model->commitChangesToDb() ) {
                LOG_ERROR( "[relations::Ui::MediaEdit::saveMediaDetails()] Problem(s) encountered whilst committing change(s) to Media actor(s)." );
            }
            _media_event_widget->setMediaId( _media._id );
            if( !_media_event_widget->commitChangesToDb() ) {
                LOG_ERROR( "[relations::Ui::MediaEdit::saveMediaDetails()] Problem(s) encountered whilst committing change(s) to linked events." );
            }
            if( !_data_manager->update( _media ) ) {
                LOG_ERROR( "[relations::Ui::MediaEdit::saveMediaDetails()] Problems encountered whilst committing change(s) to Media." );
            }
        }
        emit saveMediaEditSignal();
    }
}

void MediaEdit::addMediaActor() {
    LOG( "@MediaEdit::addMediaActor()" );
    _media_actor_dialog->setFields();
    _media_actor_dialog->exec();
}

void MediaEdit::removeMediaActor() {
    LOG( "@MediaEdit::removeMediaActor()" );
    auto selected_row = _ui->actorsTableView->selectionModel()->currentIndex().row();
    if( selected_row >= 0 ) {
        if( !_media_actors_model->removeParticipant( selected_row ) ) {
            LOG_ERROR( "[relations::Ui::MediaEdit::removeMediaActor()] Could not remove actor at row ", selected_row );
        }
    }
    _ui->actorsTableView->resizeColumnsToContents();
    _ui->actorsTableView->horizontalHeader()->setSectionResizeMode( 1, QHeaderView::Stretch );
}

void MediaEdit::saveMediaActor() {
    auto person_id = _media_actor_dialog->getPersonId();
    if( _media_actors_model->exists( person_id ) ) {
        QMessageBox msg_box;
        msg_box.setWindowTitle( "ReLations Error" );
        msg_box.setText( "Already in list of actors. Person was not added." );
        msg_box.exec();
        return;
    } else {
        auto selected_row = _ui->actorsTableView->selectionModel()->currentIndex().row();
        _media_actors_model->addParticipant( selected_row, person_id );
        _ui->actorsTableView->resizeColumnsToContents();
        _ui->actorsTableView->horizontalHeader()->setSectionResizeMode( 1, QHeaderView::Stretch );
    }
}

void MediaEdit::setMediaType() {
    using relations::enums::to_MediaType;
    auto index = _ui->mediaTypeComboBox->currentIndex();
    _media._type = to_MediaType( _ui->mediaTypeComboBox->itemData( index ).toInt() );
}

void MediaEdit::setMediaFile() {
    auto file_name = QFileDialog::getOpenFileName( this,
                                                   tr( "Link to media file" ),
                                                   "./"
    );
    if( file_name.isEmpty() ) {
        return;
    }
    _ui->mediaFilePathLineEdit->setText( file_name );
}

bool MediaEdit::checkFields() {
    bool error_flag = false;
    if( _ui->mediaTypeComboBox->currentIndex() < 0 ) {
        error_flag = true;
    }
    if( _ui->mediaFilePathLineEdit->text().isEmpty() ) {
        error_flag = true;
    }
    if( error_flag ) {
        QMessageBox msg_box;
        msg_box.setWindowTitle( "ReLations Error" );
        msg_box.setText( "A type and a file must be provided." );
        msg_box.exec();
    }
    return !error_flag;
}