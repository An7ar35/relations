#include "GraphForm.h"
#include "ui_GraphForm.h"

GraphForm::GraphForm( QWidget *parent, std::shared_ptr<relations::io::DataManager> data_manager ) :
    QWidget( parent ),
    _ui( new relations::Ui::GraphForm ),
    _data_manager( data_manager ),
    _blood_graph( new BloodGraph( this, _data_manager ) ),
    _generic_graph( new GenericGraph( this, _data_manager ) )
{
    _ui->setupUi( this );
}

GraphForm::~GraphForm() {
    delete _ui;
}

void GraphForm::loadBloodGraph( const int64_t &subject_id ) {
    clearLayout( _ui->graphLayout );
    _blood_graph = new BloodGraph( this, _data_manager );
    _ui->graphLayout->addWidget( _blood_graph );
    connect( _blood_graph,
             SIGNAL( showProfileSignal( int64_t ) ),
             this,
             SIGNAL( showPersonInformationSignal( int64_t ) )
    );
    _blood_graph->loadGraph( subject_id );
}

void GraphForm::loadGenericGraph( const int64_t &subject_id,
                                  const relations::dto::RelationshipType &relationship_type )
{
    clearLayout( _ui->graphLayout );
    _generic_graph = new GenericGraph( this, _data_manager );
    _ui->graphLayout->addWidget( _generic_graph );
    connect( _generic_graph,
             SIGNAL( showProfileSignal( int64_t ) ),
             this,
             SIGNAL( showPersonInformationSignal( int64_t ) )
    );
    _generic_graph->loadGraph( subject_id, relationship_type );
}

void GraphForm::clearLayout( QLayout *layout ) {
    if( layout ) {
        QLayoutItem *item;
        while( ( item = layout->takeAt( 0 ) ) != NULL ) {
            delete item->widget();
            delete item;
        }
    }
}