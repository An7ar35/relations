#include <src/gui/model/people/NameTableModel.h>
#include <QMessageBox>
#include "PersonEdit.h"
#include "ui_PersonEdit.h"

PersonEdit::PersonEdit( QWidget *parent,
                        std::shared_ptr<relations::io::DataManager> data_manager,
                        int64_t person_id ) :
    QWidget( parent ),
    _ui( new relations::Ui::PersonEdit ),
    _data_manager( data_manager ),
    _data_cache( data_manager->getDataCache() ),
    _person_id( person_id ),
    _person( person_id > 0
             ? _data_manager->at( person_id )
             : relations::dto::Person( relations::enums::GenderBioType::UNKNOWN,
                                       relations::enums::GenderIdentityType::UNSPECIFIED )
    )
{
    _ui->setupUi( this );

    //Populating the combo boxes
    for( auto e : _data_cache->_gender_bio_types ) {
        _ui->bioGenderComboBox->addItem( QString::fromStdString( e._value ),
                                         QString::number( e._id )
        );
    }
    for( auto e : _data_cache->_gender_identity_types ) {
        _ui->genderIdentityComboBox->addItem( QString::fromStdString( e._value ),
                                              QString::number( e._id )
        );
    }

    //Creating Table Models
    if( person_id > 0 ) {
        _name_model = new NameTableModel( _data_manager,
                                          _data_manager->getNames( _person )
        );
        _relationship_model = new RelationshipsTableModel( _data_manager,
                                                           std::move( _data_manager->getChildRelationships( _person ) ),
                                                           std::move( _data_manager->getParentRelationships( _person ) )
        );
    } else {
        _name_model = new NameTableModel( _data_manager );
        _relationship_model = new RelationshipsTableModel( _data_manager );
    }
    _ui->namesTableView->setModel( _name_model );
    _ui->namesTableView->resizeColumnsToContents();
    _ui->relationshipTableView->setModel( _relationship_model );
    _ui->relationshipTableView->setColumnHidden( 4, true );
    _ui->relationshipTableView->resizeColumnsToContents();

    //Creating Editors for the tabled data
    _name_edit_dialog = new NameEdit( this, _data_manager );
    _relationship_edit_dialog = new RelationshipEdit( this, _data_manager, _relationship_model, person_id );

    //Setting fields
    _ui->idDisplayLabel->setText( QString::number( _person._id ) );
    _ui->nameDisplayLabel->setText( QString::fromStdString( _data_manager->getPreferedName( _person ) ) );
    _ui->bioGenderComboBox->setCurrentIndex( eadlib::enum_int<relations::enums::GenderBioType>( _person._gender_biological ) - 1 );
    _ui->genderIdentityComboBox->setCurrentIndex( eadlib::enum_int<relations::enums::GenderIdentityType>( _person._gender_identity ) - 1 );
    if( _person._note ) {
        _ui->noteSubjectEdit->setText( QString::fromStdString( _person._note->_subject ) );
        _ui->noteTextEdit->appendPlainText( QString::fromStdString( _person._note->_text ) );
    }
    //Connecting actions
    connect( _ui->saveEditPersonButton, SIGNAL( clicked() ), this, SLOT( savePersonDetails() ) );
    connect( _ui->cancelEditPersonButton, SIGNAL( clicked() ), this, SIGNAL( cancelPersonEditSignal() ) );

    connect( _ui->newNameButton, SIGNAL( clicked() ), this, SLOT( addPersonName() ) );
    connect( _ui->editNameButton, SIGNAL( clicked() ), this, SLOT( editPersonName() ) );
    connect( _ui->setDefaultNameButton, SIGNAL( clicked() ), this, SLOT( setDefaultName() ) );
    connect( _ui->removeNameButton, SIGNAL( clicked() ), this, SLOT( removePersonName() ) );
    connect( _name_edit_dialog, SIGNAL( accepted() ), this, SLOT( savePersonName() ) );

    connect( _ui->newRelationshipButton, SIGNAL( clicked() ), this, SLOT( addPersonRelationship() ) );
    connect( _ui->editRelationshipButton, SIGNAL( clicked() ), this, SLOT( editPersonRelationship() ) );
    connect( _ui->removeRelationshipButton, SIGNAL( clicked() ), this, SLOT( removePersonRelationship() ) );
    connect( _relationship_edit_dialog, SIGNAL( accepted() ), this, SLOT( savePersonRelationship() ) );

    connect( _ui->bioGenderComboBox , SIGNAL( currentIndexChanged( int ) ), this, SLOT( setGender_Bio() ) );
    connect( _ui->genderIdentityComboBox , SIGNAL( currentIndexChanged( int ) ), this, SLOT( setGender_Identity() ) );
}

PersonEdit::~PersonEdit() {
    delete _ui;
}

void PersonEdit::addPersonName() {
    _name_edit_dialog->setFields();
    _name_edit_dialog->exec();
}

void PersonEdit::editPersonName() {
    auto selected_row = _ui->namesTableView->selectionModel()->currentIndex().row();
    if( selected_row >= 0 ) {
        auto    id       = _name_model->getName( selected_row )._id;
        auto    type     = _name_model->getName( selected_row )._type;
        QString prefix   = _name_model->data( _name_model->index( selected_row, 0 ), Qt::DisplayRole ).toString();
        QString first    = _name_model->data( _name_model->index( selected_row, 1 ), Qt::DisplayRole ).toString();
        QString middle   = _name_model->data( _name_model->index( selected_row, 2 ), Qt::DisplayRole ).toString();
        QString last     = _name_model->data( _name_model->index( selected_row, 3 ), Qt::DisplayRole ).toString();
        QString suffix   = _name_model->data( _name_model->index( selected_row, 4 ), Qt::DisplayRole ).toString();
        QString display  = _name_model->data( _name_model->index( selected_row, 5 ), Qt::DisplayRole ).toString();
        QString language = _name_model->data( _name_model->index( selected_row, 6 ), Qt::DisplayRole ).toString();

        _name_edit_dialog->setFields( id,
                                      type,
                                      prefix,
                                      first,
                                      middle,
                                      last,
                                      suffix,
                                      display,
                                      language
        );
        _name_edit_dialog->exec();
    }
}

void PersonEdit::savePersonName() {
    LOG( "@PersonEdit::savePersonName()" );
    auto selected_row   = _ui->namesTableView->selectionModel()->currentIndex().row();
    auto name_id        = _name_edit_dialog->getId();
    auto type           = _name_edit_dialog->getType();
    auto preferred_flag = _name_model->empty(); //Preferred if 1st name of person
    auto language       = _name_edit_dialog->getLanguage();
    auto display        = _name_edit_dialog->getDisplay();
    auto prefix         = _name_edit_dialog->getPrefix();
    auto first          = _name_edit_dialog->getFirst();
    auto middle         = _name_edit_dialog->getMiddle();
    auto last           = _name_edit_dialog->getLast();
    auto suffix         = _name_edit_dialog->getSuffix();

    if( _name_edit_dialog->isNew() ) { //New name
        _name_model->addName( selected_row, _person_id,
                              type, preferred_flag, language, display,
                              prefix, first, middle, last, suffix
        );
    } else { //Edited name
        preferred_flag = _name_model->getPreferedFlag( selected_row );
        _name_model->updateName( selected_row, _person_id, name_id,
                                 type, preferred_flag, language, display,
                                 prefix, first, middle, last, suffix
        );
    }
    if( preferred_flag ) {
        _ui->nameDisplayLabel->setText( display );
    }
    _ui->namesTableView->resizeColumnsToContents();
}

void PersonEdit::setDefaultName() {
    LOG( "@PersonEdit::setDefaultName()" );
    int selected_row = _ui->namesTableView->selectionModel()->currentIndex().row();
    if( selected_row >= 0 && _name_model->setDefault( selected_row ) ) {
        _ui->nameDisplayLabel->setText( _name_model->data( _name_model->index( selected_row, 5 ), Qt::DisplayRole ).toString() );
    }
}

void PersonEdit::removePersonName() {
    LOG( "@PersonEdit::removePersonName()" );
    int selected_row = _ui->namesTableView->selectionModel()->currentIndex().row();
    if( selected_row >= 0 ) {
        _name_model->removeName( selected_row );
    }
    _ui->namesTableView->resizeColumnsToContents();
}

void PersonEdit::addPersonRelationship() {
    LOG( "@PersonEdit::addPersonRelationship()" );
    _relationship_edit_dialog->setFields();
    _relationship_edit_dialog->exec();
}

void PersonEdit::editPersonRelationship() {
    LOG( "@PersonEdit::editPersonRelationship()" );
    auto selected_row = _ui->relationshipTableView->selectionModel()->currentIndex().row();
    if( selected_row >= 0 ) {
        auto relationship = _relationship_model->getRelationship( selected_row );
        auto type_id      = relationship._type_id;
        auto target       = relationship._person2;
        auto note_subject = relationship._note
                            ? QString::fromStdString( relationship._note->_subject )
                            : QString();
        auto note_text    = relationship._note
                            ? QString::fromStdString( relationship._note->_text )
                            : QString();
        _relationship_edit_dialog->setFields( type_id, target, note_subject, note_text );
        _relationship_edit_dialog->exec();
    }
}

void PersonEdit::savePersonRelationship() {
    LOG( "@PersonEdit::savePersonRelationship()" );
    auto selected_row = _ui->relationshipTableView->selectionModel()->currentIndex().row();
    auto target_id    = _relationship_edit_dialog->getRelationshipTargetId();
    auto type_id      = _relationship_edit_dialog->getType()._id;
    auto note_subject = _relationship_edit_dialog->getNoteSubject().toStdString();
    auto note_text    = _relationship_edit_dialog->getNoteText().toStdString();

    if( _relationship_edit_dialog->isNew() ) { //New relationship
        _relationship_model->addRelationship( selected_row, _person_id, type_id, target_id, note_subject, note_text );
    } else { //Edited relationship
        _relationship_model->updateRelationship( selected_row, type_id, target_id, note_subject, note_text );
    }
    _ui->relationshipTableView->resizeColumnsToContents();
}

void PersonEdit::removePersonRelationship() {
    LOG( "@PersonEdit::removePersonRelationship()" );
    auto selected_row = _ui->relationshipTableView->selectionModel()->currentIndex().row();
    if( selected_row >= 0 ) {
        if( !_relationship_model->removeRelationship( selected_row ) ) {
            LOG_ERROR( "[relations::Ui::PersonEdit::removePersonRelationship()] Could not remove relationship at row ", selected_row );
        }
    }
    _ui->relationshipTableView->resizeColumnsToContents();
}

void PersonEdit::setGender_Bio() {
    using relations::enums::to_GenderBioType;
    auto index = _ui->bioGenderComboBox->currentIndex();
    _person._gender_biological = to_GenderBioType( _ui->bioGenderComboBox->itemData( index ).toInt() );
}

void PersonEdit::setGender_Identity() {
    using relations::enums::to_GenderIdentityType;
    auto index = _ui->genderIdentityComboBox->currentIndex();
    _person._gender_identity = to_GenderIdentityType( _ui->genderIdentityComboBox->itemData( index ).toInt() );
}

void PersonEdit::savePersonDetails() {
    LOG( "@PersonEdit::savePersonDetails()" );
    //Person's note
    auto note_subject = _ui->noteSubjectEdit->text().toStdString();
    auto note_text    = _ui->noteTextEdit->toPlainText().toStdString();
    if( note_subject.empty() && note_text.empty() ) {
        if( _person._note ) {
            _person._note.reset();
        }
    } else {
        if( _person._note ) {
            _person._note->_subject = note_subject;
            _person._note->_text    = note_text;
        } else {
            auto note = new relations::dto::Note( note_subject, note_text );
            _person._note = std::unique_ptr < relations::dto::Note > ( note );
        }
    }
    //Person
    if( _person._id < 0 ) { //new person
        if( !_data_manager->add( _person ) ) {
            LOG_ERROR( "[relations::Ui::PersonEdit::savePersonDetails()] Problem encountered whilst committing change(s) to new Person." );
        }
        _name_model->setPersonID( _person._id );
        _relationship_model->setPersonID( _person._id );
        if( !_name_model->commitChangesToDb() ) {
            LOG_ERROR( "[relations::Ui::PersonEdit::savePersonDetails()] Problem(s) encountered whilst committing change(s) to Name(s)." );
        }
        if( !_relationship_model->commitChangesToDb() ) {
            LOG_ERROR( "[relations::Ui::PersonEdit::savePersonDetails()] Problem(s) encountered whilst committing change(s) to Relationship(s)." );
        }
    } else { //updated person
        _name_model->setPersonID( _person._id );
        _relationship_model->setPersonID( _person._id );
        if( !_name_model->commitChangesToDb() ) {
            LOG_ERROR( "[relations::Ui::PersonEdit::savePersonDetails()] Problem(s) encountered whilst committing change(s) to Name(s)." );
        }
        if( !_relationship_model->commitChangesToDb() ) {
            LOG_ERROR( "[relations::Ui::PersonEdit::savePersonDetails()] Problem(s) encountered whilst committing change(s) to Relationship(s)." );
        }
        if( !_data_manager->update( _person ) ) {
            LOG_ERROR( "[relations::Ui::PersonEdit::savePersonDetails()] Problems encountered whilst committing change(s) to Person." );
        }
    }
    emit savePersonEditSignal();
}
