#include <QtWidgets/QMenu>
#include "PersonView.h"
#include "ui_PersonView.h"

PersonView::PersonView( QWidget *parent, std::shared_ptr<relations::io::DataManager> data_manager, relations::dto::Person &person ) :
    QWidget( parent ),
    _ui( new relations::Ui::PersonView ),
    _person( person ),
    _data_manager( data_manager ),
    _data_cache( data_manager->getDataCache() )
{
    _ui->setupUi( this );
    _ui->personToMedia_Button->setEnabled( false );
    _ui->personToEvent_Button->setEnabled( false );

    QMenu * graphButtonMenu = new QMenu( this );
    QMenu * graphSubMenu = new QMenu( this );
    for( auto r : _data_cache->_relationship_types ) {
        //if( r._value != "ParentChild" )
        graphSubMenu->addAction( QString::fromStdString( r._value ),
                                 this,
                                 [ this, person, r ]() { showGenericGraph( person._id, r._id ); }
        );
    }
    graphButtonMenu->addAction( QIcon( ":/resource/open-iconic/person.svg"),
                               "Bloodline Graph.",
                               this,
                               [this, person]() { showBloodGraph( person._id ); }
    );
    graphSubMenu->setTitle( "Show graph for.." );
    graphButtonMenu->addMenu( graphSubMenu );
    _ui->graphButton->setMenu( graphButtonMenu );

    //Details
    _ui->personId->setText( QString::number( _person._id ) );
    try {
        auto index = _data_manager->getPreferredNameIndex( _person );
        _ui->personDisplayName->setText( QString::fromStdString( _person._names.at( index )._name_form->_full_text ) );
    } catch( std::out_of_range ) {
        LOG_ERROR( "[relations::Ui::BPersonView::PersonView( [", _person._id, "] )] Person does not have a preferred name." );
    }

    //Gender
    using relations::dto::EnumType;
    using relations::enums::GenderIdentityType;
    using relations::enums::GenderBioType;
    auto gender_bio = std::find_if( _data_cache->_gender_bio_types.begin(),
                                    _data_cache->_gender_bio_types.end(),
                                    ( [&]( const EnumType<GenderBioType> &type ) { return type._type == _person._gender_biological; } )
    );
    auto gender_identity = std::find_if( _data_cache->_gender_identity_types.begin(),
                                         _data_cache->_gender_identity_types.end(),
                                         ( [&]( const EnumType<GenderIdentityType> &type ) { return type._type == _person._gender_identity; } )
    );
    if( gender_bio != _data_cache->_gender_bio_types.end() ) {
        if( _person._gender_identity != GenderIdentityType::UNSPECIFIED ) {
            if( gender_identity != _data_cache->_gender_identity_types.end() ) {
                _ui->personGender->setText( QString( "%1 (%2)" )
                                                .arg( QString::fromStdString( gender_identity->_value ) )
                                                .arg( QString::fromStdString( gender_bio->_value ) )
                );
            }
        } else {
            _ui->personGender->setText( QString::fromStdString( gender_bio->_value ) );
        }
    }

    //Names
    _name_model = new NameTableModel( _data_manager, _data_manager->getNames( _person ) );
    _ui->namesTableView->setModel( _name_model );
    _ui->namesTableView->resizeColumnsToContents();

    //Relationships
    _relationship_model = new RelationshipsTableModel( _data_manager,
                                                       std::move( _data_manager->getParentRelationships( _person ) ),
                                                       std::move( _data_manager->getChildRelationships( _person ) )
    );
    _ui->relationshipTableView->setModel( _relationship_model );
    _ui->relationshipTableView->resizeColumnsToContents();
    _ui->relationshipTableView->setColumnHidden( 4, true );

    //Subject Note
    if( _person._note ) {
        if( !_person._note->_subject.empty() ) {
            _ui->noteSubjectLabel->setText( QString::fromStdString( _person._note->_subject ) );
        }
        _ui->noteTextEdit->setText( QString::fromStdString( _person._note->_text ) );
    }
}

PersonView::~PersonView() {
    delete _ui;
}

void PersonView::showBloodGraph( int64_t person_id ) {
    LOG( "@PersonView::showBloodGraph( ", person_id, ")" );
    emit showBloodGraphSignal( person_id );
}

void PersonView::showGenericGraph( int64_t person_id, int64_t relationship_id ) {
    LOG( "@PersonView::showGenericGraph( ", person_id, ", ", relationship_id, " )" );
    emit showGenericGraphSignal( person_id, relationship_id );
}
