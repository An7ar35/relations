#ifndef RELATIONS_PERSONVIEW_H
#define RELATIONS_PERSONVIEW_H

#include <QWidget>
#include <eadlib/logger/Logger.h>
#include "src/io/DataManager.h"
#include <src/gui/model/people/NameTableModel.h>
#include <src/gui/model/people/RelationshipsTableModel.h>

namespace relations::Ui {
    class PersonView;
    class NameTableModel;
    class RelationshipsTableModel;
}

class PersonView : public QWidget {
  Q_OBJECT

  public:
    PersonView( QWidget *parent,
                std::shared_ptr<relations::io::DataManager> data_manager,
                relations::dto::Person &person );
    ~PersonView();

  signals:
    void showBloodGraphSignal( int64_t );
    void showGenericGraphSignal( int64_t, int64_t );

  public slots:
    void showBloodGraph( int64_t person_id );
    void showGenericGraph( int64_t person_id, int64_t relationship_id );

  private:
    relations::Ui::PersonView                  *_ui;
    NameTableModel                             *_name_model;
    RelationshipsTableModel                    *_relationship_model;
    std::shared_ptr<relations::io::DataManager> _data_manager;
    const relations::dto::Person               &_person;
    relations::io::DataCache                   *_data_cache;
};

#endif //RELATIONS_PERSONVIEW_H
