#ifndef RELATIONS_NAMEEDIT_H
#define RELATIONS_NAMEEDIT_H

#include <QDialog>
#include <src/dto/Name.h>
#include <src/io/DataManager.h>

namespace relations::Ui {
    class NameEdit;
}

class NameEdit : public QDialog {
  Q_OBJECT

  public:
    NameEdit( QWidget *parent, std::shared_ptr<relations::io::DataManager> data_manager );
    ~NameEdit();

    int64_t getId();
    relations::enums::NameType getType();
    QString getPrefix();
    QString getFirst();
    QString getMiddle();
    QString getLast();
    QString getSuffix();
    QString getDisplay();
    QString getLanguage();

    bool isNew();

  public slots:
    void setFields();

    void setFields( const int64_t &name_id,
                    const relations::enums::NameType &type,
                    QString &prefix,
                    QString &first,
                    QString &middle,
                    QString &last,
                    QString &suffix,
                    QString &display,
                    QString &language );

    void done( int i ) override;

  private:
    relations::Ui::NameEdit                    *_ui;
    std::shared_ptr<relations::io::DataManager> _data_manager;
    int64_t                                     _id;
    bool                                        _new_flag;
};

#endif //RELATIONS_NAMEEDIT_H
