#include "RelationshipEdit.h"
#include "QMessageBox"
#include "ui_RelationshipEdit.h"

RelationshipEdit::RelationshipEdit( QWidget *parent,
                                    std::shared_ptr<relations::io::DataManager> data_manager,
                                    RelationshipsTableModel *model,
                                    const int64_t &parent_id ) :
    QDialog( parent ),
    _ui( new relations::Ui::RelationshipEdit ),
    _data_manager( data_manager ),
    _model( model ),
    _new_flag( true ),
    _parent_relationship_flag( false ),
    _parent_id( parent_id )
{
    _ui->setupUi( this );
    auto data_cache = _data_manager->getDataCache();

    for( auto e : data_cache->_relationship_types ) {
        _ui->typeComboBox->addItem( QString::fromStdString( e._value ),
                                    QString::number( e._id )
        );
    }

    for( auto it = _data_manager->cbegin(); it != _data_manager->cend(); ++it ) {
        QString text = QString( "%1 (%2)" )
            .arg( QString::fromStdString( _data_manager->getPreferedName( it->second.value ) ) )
            .arg( QString::number( it->second.value._id ) );

        _ui->withComboBox->addItem( text,
                                    QString::number( it->second.value._id )
        );

        //Disabling item in ComboBox is the same as parent or relationship
        if( parent_id >= 0 && parent_id == it->second.value._id ) {
            _ui->withComboBox->setItemData( _ui->withComboBox->count() - 1, 0, Qt::UserRole - 1 );
        }
    }
}

RelationshipEdit::~RelationshipEdit() {
    delete _ui;
}

const relations::dto::RelationshipType & RelationshipEdit::getType() {
    auto data_cache  = _data_manager->getDataCache();
    auto selected_id = _ui->typeComboBox->itemData( _ui->typeComboBox->currentIndex() ).toInt();
    auto search_type = std::find_if( data_cache->_relationship_types.begin(),
                                     data_cache->_relationship_types.end(),
                                     ( [&]( const relations::dto::RelationshipType &type ) { return selected_id == type._id; } )
    );
    if( search_type == data_cache->_relationship_types.end() ) {
        LOG_ERROR( "[relations::Ui::RelationshipEdit::getType()] Could not find RelationshipType [", selected_id, "] in the cache." );
    }
    return *search_type;
}

int64_t RelationshipEdit::getRelationshipTargetId() {
    return _ui->withComboBox->itemData( _ui->withComboBox->currentIndex() ).toInt();
}

QString RelationshipEdit::getNoteSubject() {
    return _ui->noteSubjectEdit->text();
}

QString RelationshipEdit::getNoteText() {
    return _ui->noteTextEdit->toPlainText();
}

bool RelationshipEdit::isNew() {
    return _new_flag;
}

void RelationshipEdit::setFields() {
    _new_flag = true;
    _ui->typeComboBox->setCurrentIndex( 0 );
    _ui->withComboBox->setCurrentIndex( 0 );
    _ui->noteSubjectEdit->setText( "" );
    _ui->noteTextEdit->clear();
}

void RelationshipEdit::setFields( int64_t &type_id,
                                  int64_t &target_id,
                                  QString &note_subject,
                                  QString &note_text )
{
    _new_flag                 = false;
    _parent_relationship_flag = target_id == _parent_id;
    _original_type_id         = type_id;
    _original_target_id       = target_id;
    auto type_index = _ui->typeComboBox->findData( QVariant::fromValue( type_id ) );
    _ui->typeComboBox->setCurrentIndex( type_index >= 0 ? type_index : 0 );
    _ui->typeComboBox->setDisabled( _parent_relationship_flag );
    auto with_index = _ui->withComboBox->findData( QVariant::fromValue( target_id ) );
    _ui->withComboBox->setCurrentIndex( with_index >= 0 ? with_index : 0 );
    _ui->withComboBox->setDisabled( _parent_relationship_flag );
    _ui->noteSubjectEdit->setText( note_subject );
    _ui->noteTextEdit->document()->setPlainText( note_text );

}

void RelationshipEdit::done( int i ) {
    if( QDialog::Accepted == i ) {
        if( !_parent_relationship_flag && _parent_id == _ui->withComboBox->itemData( _ui->withComboBox->currentIndex() ).toInt() ) {
            QMessageBox msg_box;
            msg_box.setWindowTitle( "ReLations Error" );
            msg_box.setText( "Relationship can't be self-referencing." );
            msg_box.exec();
            return;
        }
        if( isNew() && _model->exists( _parent_id, getType()._id ,getRelationshipTargetId() ) ) {
            QMessageBox msg_box;
            msg_box.setWindowTitle( "ReLations Error" );
            msg_box.setText( "Relationship already exists." );
            msg_box.exec();
            return;
        }
        if( !isNew() &&
            !( _original_target_id == _ui->withComboBox->itemData( _ui->withComboBox->currentIndex() ).toInt()
               && _original_type_id == _ui->typeComboBox->itemData( _ui->typeComboBox->currentIndex() ).toInt() ) ) {
            if( _model->exists( _parent_id, getType()._id, getRelationshipTargetId() ) ) {
                QMessageBox msg_box;
                msg_box.setWindowTitle( "ReLations Error" );
                msg_box.setText( "Relationship already exists." );
                msg_box.exec();
                return;
            }
        }
    }
    QDialog::done( i );
    return;
}
