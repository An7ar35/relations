#include "NameEdit.h"
#include "QMessageBox"
#include "ui_NameEdit.h"

NameEdit::NameEdit( QWidget *parent,
                    std::shared_ptr<relations::io::DataManager> data_manager ) :
    QDialog( parent ),
    _ui( new relations::Ui::NameEdit ),
    _data_manager( data_manager ),
    _new_flag( true ),
    _id( -1 )
{
    _ui->setupUi( this );
    auto data_cache = _data_manager->getDataCache();
    for( auto e : data_cache->_name_types ) {
        _ui->typeComboBox->addItem( QString::fromStdString( e._value ),
                                    QString::number( e._id )
        );
    }
}

NameEdit::~NameEdit() {
    delete _ui;
}

int64_t NameEdit::getId() {
    return _id;
}

relations::enums::NameType NameEdit::getType() {
    auto id = _ui->typeComboBox->itemData( _ui->typeComboBox->currentIndex() ).toInt();
    return relations::enums::to_NameType( id );
}

QString NameEdit::getPrefix() {
    return _ui->prefixLineEdit->text();
}

QString NameEdit::getFirst() {
    return _ui->nameLineEdit->text();
}

QString NameEdit::getMiddle() {
    return _ui->middleLineEdit->text();
}

QString NameEdit::getLast() {
    return _ui->famillyLineEdit->text();
}

QString NameEdit::getSuffix() {
    return _ui->suffixLineEdit->text();
}

QString NameEdit::getDisplay() {
    return _ui->fullTextLineEdit->text();
}

QString NameEdit::getLanguage() {
    return _ui->languageLineEdit->text();
}

void NameEdit::setFields() {
    _new_flag = true;
    auto enum_int = eadlib::enum_int<relations::enums::NameType>( relations::enums::NameType::ALSOKNOWNAS );
    auto index = _ui->typeComboBox->findData( QVariant::fromValue( enum_int ) );
    _ui->typeComboBox->setCurrentIndex( index >= 0 ? index : 0 );
    _ui->prefixLineEdit->setText( "" );
    _ui->nameLineEdit->setText( "" );
    _ui->middleLineEdit->setText( "" );
    _ui->famillyLineEdit->setText( "" );
    _ui->suffixLineEdit->setText( "" );
    _ui->fullTextLineEdit->setText( "" );
    _ui->languageLineEdit->setText( "" );
}

void NameEdit::setFields( const int64_t &name_id,
                          const relations::enums::NameType &type,
                          QString &prefix,
                          QString &first,
                          QString &middle,
                          QString &last,
                          QString &suffix,
                          QString &display,
                          QString &language ) {
    _new_flag = false;
    _id = name_id;
    auto enum_int = eadlib::enum_int<relations::enums::NameType>( type );
    auto index = _ui->typeComboBox->findData( QVariant::fromValue( enum_int ) );
    _ui->typeComboBox->setCurrentIndex( index );
    _ui->prefixLineEdit->setText( prefix );
    _ui->nameLineEdit->setText( first );
    _ui->middleLineEdit->setText( middle );
    _ui->famillyLineEdit->setText( last );
    _ui->suffixLineEdit->setText( suffix );
    _ui->fullTextLineEdit->setText( display );
    _ui->languageLineEdit->setText( language );
}

bool NameEdit::isNew() {
    return _new_flag;
}

void NameEdit::done( int i ) {
    if( QDialog::Accepted == i ) {
        bool error_flag = false;
        if( getFirst().isEmpty() ) {
            error_flag = true;
            _ui->nameLineEdit->setStyleSheet( "QLineEdit { background: rgb(255, 120, 120); }" );
        } else {
            _ui->languageLineEdit->setStyleSheet( "" );
        }
        if( getLanguage().isEmpty() ) {
            error_flag = true;
            _ui->languageLineEdit->setStyleSheet( "QLineEdit { background: rgb(255, 120, 120); }" );
        } else {
            _ui->languageLineEdit->setStyleSheet( "" );
        }
        if( getDisplay().isEmpty() ) {
            error_flag = true;
            _ui->fullTextLineEdit->setStyleSheet( "QLineEdit { background: rgb(255, 120, 120); }" );
        } else {
            _ui->languageLineEdit->setStyleSheet( "" );
        }
        if( error_flag ) {
            QMessageBox msg_box;
            msg_box.setWindowTitle( "ReLations Error" );
            msg_box.setText( "Name, Display and Language is the minimum required." );
            msg_box.exec();
            return;
        } else {
            QDialog::done( i );
        }
    } else {
        QDialog::done( i );
        return;
    }
}

