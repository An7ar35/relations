#ifndef RELATIONS_RELATIONSHIPEDIT_H
#define RELATIONS_RELATIONSHIPEDIT_H

#include <QDialog>
#include <src/gui/model/people/RelationshipsTableModel.h>
#include <src/dto/RelationshipType.h>
#include <src/io/DataManager.h>

namespace relations::Ui {
    class RelationshipEdit;
    class RelationshipsTableModel;
}

class RelationshipEdit : public QDialog {
  Q_OBJECT

  public:
    RelationshipEdit( QWidget *parent,
                      std::shared_ptr<relations::io::DataManager> data_manager,
                      RelationshipsTableModel *model,
                      const int64_t &parent_id );
    ~RelationshipEdit();

    const relations::dto::RelationshipType & getType();
    int64_t getRelationshipTargetId();
    QString getNoteSubject();
    QString getNoteText();

    bool isNew();

  public slots:
    void setFields();

    void setFields( int64_t &type,
                    int64_t &target_id,
                    QString &note_subject,
                    QString &note_text );

    void done( int i ) override;

  private:
    relations::Ui::RelationshipEdit            *_ui;
    std::shared_ptr<relations::io::DataManager> _data_manager;
    RelationshipsTableModel                    *_model;
    bool                                        _new_flag;
    bool                                        _parent_relationship_flag; //Person is not the origin of the relationship edge in the graph
    int64_t                                     _parent_id; //Person ID from which the dialog is opened
    int64_t                                     _original_type_id;
    int64_t                                     _original_target_id;
};

#endif //RELATIONS_RELATIONSHIPEDIT_H
