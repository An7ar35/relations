#ifndef RELATIONS_PERSONEDIT_H
#define RELATIONS_PERSONEDIT_H

#include <QWidget>
#include <eadlib/logger/Logger.h>
#include <src/gui/model/people/DMDGraphModel.h>
#include <src/gui/model/people/NameTableModel.h>
#include <src/gui/model/people/RelationshipsTableModel.h>
#include "src/gui/view/people/dialog/NameEdit.h"
#include "src/gui/view/people/dialog/RelationshipEdit.h"
#include "src/io/DataManager.h"

namespace relations::Ui {
    class PersonEdit;
    class NameEdit;
    class RelationshipEdit;
    class NameTableModel;
    class RelationshipsTableModel;
    class DMDGraphModel;
}

class PersonEdit : public QWidget {
  Q_OBJECT

  public:
    PersonEdit( QWidget *parent,
                std::shared_ptr<relations::io::DataManager> data_manager,
                int64_t person_id );
    ~PersonEdit();

  public slots:
    void savePersonDetails();

  signals:
    void cancelPersonEditSignal();
    void savePersonEditSignal();

  private slots:
    void addPersonName();
    void editPersonName();
    void savePersonName();
    void setDefaultName();
    void removePersonName();

    void addPersonRelationship();
    void editPersonRelationship();
    void savePersonRelationship();
    void removePersonRelationship();
    void setGender_Bio();
    void setGender_Identity();

  private:
    relations::Ui::PersonEdit                  *_ui;
    NameEdit                                   *_name_edit_dialog;
    RelationshipEdit                           *_relationship_edit_dialog;
    NameTableModel                             *_name_model;
    RelationshipsTableModel                    *_relationship_model;
    std::shared_ptr<relations::io::DataManager> _data_manager;
    int64_t                                     _person_id;
    relations::dto::Person                      _person;
    relations::io::DataCache                   *_data_cache;
};

#endif //RELATIONS_PERSONEDIT_H
