#include "tools.h"

/**
 * Unpacks a set of NamePart DTOs into a QString
 * @param container Set of NamePart DTOs
 * @return QString
 */
QString relations::Ui::tool::unpack( const std::vector<relations::dto::NamePart> &container ) {
    QString s;
    size_t count = 0;
    for( auto part : container ) {
        count++;
        s.append( QString::fromStdString( part._value ) );
        if( count != container.size() ) {
            s += ", ";
        }
    }
    return s;
}

/**
 * Converts a PartType enum into a QString value from the cache
 * @param _data_cache DataCache instance pointer
 * @param part_type   NamePartType enum
 * @return QString value
 */
QString relations::Ui::tool::unpack( relations::io::DataCache *_data_cache, const relations::enums::NameType &part_type ) {
    auto index = eadlib::enum_int<enums::NameType>( part_type ) - 1;
    if( index >= _data_cache->_name_types.size() ) {
        LOG_ERROR( "[relations::Ui::tool::unpack( io::DataCache *, enums::NameType & )] "
                       "Name type [", index, "] is not within the bounds of the NameType cache size." );
        return QString();
    } else {
        return QString::fromStdString( _data_cache->_name_types.at( index )._value );
    }
}

/**
 * Converts a comma separated values into a list
 * @param string Concatenated comma separated values
 * @return List of values
 */
std::list<std::string> relations::Ui::tool::unpack( const std::string &string ) {
    std::istringstream ss( string );
    std::string temp;
    std::list<std::string> list;

    while(std::getline(ss, temp, ',')) {
        auto begin = temp.find_first_not_of( "\t " );
        auto end   = temp.find_last_not_of( " \t" );
        list.emplace_back( temp.substr( begin, end + 1 ) );
    }
    return list;
}
