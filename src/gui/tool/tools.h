#ifndef RELATIONS_DTODISPLAYTOOL_H
#define RELATIONS_DTODISPLAYTOOL_H

#include <iostream>
#include <sstream>
#include <list>
#include <src/dto/NamePart.h>
#include <src/dto/Name.h>
#include <QString>
#include <src/io/DataCache.h>

namespace relations::Ui::tool {
    QString unpack( const std::vector<relations::dto::NamePart> &container );
    QString unpack( relations::io::DataCache *_data_cache, const relations::enums::NameType &part_type );
    std::list<std::string> unpack( const std::string &string );
}

#endif //RELATIONS_DTODISPLAYTOOL_H
