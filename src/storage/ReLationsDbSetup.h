#ifndef RELATIONS_RELATIONSDBSETUP_H
#define RELATIONS_RELATIONSDBSETUP_H

#include <cstdio>
#include <string>
#include <eadlib/wrapper/SQLite/SQLite.h>

namespace relations::storage {
    class ReLationsDbSetup {
      public:
        bool checkTableStructure( eadlib::wrapper::SQLite &db ) const;
        bool buildTableStructure( eadlib::wrapper::SQLite &db );
      private:
        //Variables
        size_t _table_count { 23 };
        //Checks
        bool checkTableStructure( eadlib::wrapper::SQLite &db,
                                  const std::string &table_name,
                                  const std::vector<std::string> &column_names ) const;

        bool checkTable_MediaType( eadlib::wrapper::SQLite &db ) const;
        bool checkTable_Media( eadlib::wrapper::SQLite &db ) const;

        bool checkTable_Note( eadlib::wrapper::SQLite &db ) const;
        bool checkTable_Subject( eadlib::wrapper::SQLite &db ) const;
        bool checkMediaRelation( eadlib::wrapper::SQLite &db ) const;

        bool checkTable_NamePartType( eadlib::wrapper::SQLite &db ) const;
        bool checkTable_NamePart( eadlib::wrapper::SQLite &db ) const;
        bool checkTable_NameForm( eadlib::wrapper::SQLite &db ) const;
        bool checkTable_NameType( eadlib::wrapper::SQLite &db ) const;
        bool checkTable_NameComposition( eadlib::wrapper::SQLite &db ) const;
        bool checkTable_GenderIdentity( eadlib::wrapper::SQLite &db ) const;
        bool checkTable_GenderBiological( eadlib::wrapper::SQLite &db ) const;
        bool checkTable_Person( eadlib::wrapper::SQLite &db ) const;
        bool checkTable_Name( eadlib::wrapper::SQLite &db ) const;
        bool checkTable_RelationshipType( eadlib::wrapper::SQLite &db ) const;
        bool checkTable_Relationship( eadlib::wrapper::SQLite &db ) const;

        bool checkTable_Date( eadlib::wrapper::SQLite &db ) const;
        bool checkTable_Location( eadlib::wrapper::SQLite &db ) const;

        bool checkTable_RoleType( eadlib::wrapper::SQLite &db ) const;
        bool checkTable_EventType( eadlib::wrapper::SQLite &db ) const;
        bool checkTable_CustomEventType( eadlib::wrapper::SQLite &db ) const;
        bool checkTable_EventRole( eadlib::wrapper::SQLite &db ) const;
        bool checkTable_Event( eadlib::wrapper::SQLite &db ) const;
        //Builds
        bool buildTable_MediaType( eadlib::wrapper::SQLite &db ); //enum
        bool buildTable_Media( eadlib::wrapper::SQLite &db );

        bool buildTable_Note( eadlib::wrapper::SQLite &db );
        bool buildTable_Subject( eadlib::wrapper::SQLite &db );
        bool buildMediaRelation( eadlib::wrapper::SQLite &db );

        bool buildTable_NamePartType( eadlib::wrapper::SQLite &db ); //enum
        bool buildTable_NamePart( eadlib::wrapper::SQLite &db );
        bool buildTable_NameForm( eadlib::wrapper::SQLite &db );
        bool buildTable_NameType( eadlib::wrapper::SQLite &db ); //enum
        bool buildTable_NameComposition( eadlib::wrapper::SQLite &db );
        bool buildTable_GenderIdentity( eadlib::wrapper::SQLite &db ); //enum
        bool buildTable_GenderBiological( eadlib::wrapper::SQLite &db ); //enum
        bool buildTable_Person( eadlib::wrapper::SQLite &db );
        bool buildTable_Name( eadlib::wrapper::SQLite &db );
        bool buildTable_RelationshipType( eadlib::wrapper::SQLite &db ); //enum
        bool buildTable_Relationship( eadlib::wrapper::SQLite &db );

        bool buildTable_Date( eadlib::wrapper::SQLite &db );
        bool buildTable_Location( eadlib::wrapper::SQLite &db );

        bool buildTable_RoleType( eadlib::wrapper::SQLite &db ); //enum
        bool buildTable_EventType( eadlib::wrapper::SQLite &db ); //enum
        bool buildTable_CustomEventType( eadlib::wrapper::SQLite &db );
        bool buildTable_EventRole( eadlib::wrapper::SQLite &db );
        bool buildTable_Event( eadlib::wrapper::SQLite &db );
    };
}

#endif //RELATIONS_RELATIONSDBSETUP_H
