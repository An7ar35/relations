#include "DatabaseAccess.h"

/**
 * Constructor
 */
relations::storage::DatabaseAccess::DatabaseAccess() :
    _db( eadlib::wrapper::SQLite() )
{}

/**
 * Destructor
 */
relations::storage::DatabaseAccess::~DatabaseAccess() {
    if( _db.connected() ) {
        if( _db.close() ) {
            LOG_FATAL( "[relations::storage::DatabaseAccess::~DatabaseAccess()] Could not close connection to DB." );
        } else {
            LOG_DEBUG( "[relations::storage::DatabaseAccess::~DatabaseAccess()] Successfully closed connection to DB." );
        }
    }
}

/**
 * Asks the DB wrapper for a connection to the database file
 * @param db_file Database file to connected to
 * @return Success
 */
bool relations::storage::DatabaseAccess::connect( const std::string &db_file ) {
    bool open_flag = _db.open( db_file );
    if( open_flag ) _db.push( "PRAGMA foreign_keys = ON;" );
    return open_flag;
}

/**
 * Asks the DB wrapper for disconnection to the current database file
 * @return Success
 */
bool relations::storage::DatabaseAccess::disconnect() {
    return !_db.close();
}

/**
 * Checks connection state
 * @return Opened state
 */
bool relations::storage::DatabaseAccess::connected() {
    return _db.connected();
}

/**
 * Forwards a string query to the db wrapper
 * @param query SQL Query
 * @param table Storage container for the returned data
 * @return Number of rows attained from the query
 */
size_t relations::storage::DatabaseAccess::pull( const std::string &query, eadlib::TableDB &table ) {
    return _db.pull( query, table );
}

/**
 * Forwards a string query to the db wrapper
 * @param query SQL Query (no returned rows expected)
 * @return Success
 */
bool relations::storage::DatabaseAccess::push( const std::string &query ) {
    return _db.push( query );
}

/**
 * Forwards a query for metadata on a table to the db wrapper
 * @param table_name Table name
 * @param table Storage container for the returned data
 * @return Success
 */
bool relations::storage::DatabaseAccess::pullMetaData( const std::string &table_name, eadlib::TableDB &table ) {
    return _db.pullMetaData( table_name, table );
}

/**
 * Checks the integrity of the expected table structure in the database
 * @return Integrity state
 */
bool relations::storage::DatabaseAccess::checkDbIntegrity() {
    auto dbSetupEngine = ReLationsDbSetup();
    return dbSetupEngine.checkTableStructure( _db );
}

/**
 * Creates the complete table structures for a new database file
 * @return Success
 */
bool relations::storage::DatabaseAccess::createNewStructure() {
    auto dbSetupEngine = ReLationsDbSetup();
    return dbSetupEngine.buildTableStructure( _db );
}

/**
 * Gets the connected status from the wrapper
 * @return Connected status
 */
bool relations::storage::DatabaseAccess::isConnected() {
    return _db.connected();
}

/**
 * Gets the last inserted row id
 * @return Last row id inserted
 */
int64_t relations::storage::DatabaseAccess::getLastInsertedRowIndex() const {
    return _db.lastInsertedRowID();
}

/**
 * Gets the current file name
 * @return File name
 */
std::string relations::storage::DatabaseAccess::fileName() const {
    return _db.getFileName();
}


