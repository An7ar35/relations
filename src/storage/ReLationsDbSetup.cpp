#include "ReLationsDbSetup.h"

/**
 * Checks the database structure
 * @param db Db connection instance
 * @return Success
 */
bool relations::storage::ReLationsDbSetup::checkTableStructure( eadlib::wrapper::SQLite &db ) const {
    size_t tables_checked { 0 };
    if( !db.connected() ) {
        LOG_ERROR( "[relations::storage::ReLationsDbSetup::checkTableStructure()] Database is not connected. Aborting." );
        return false;
    }
    if( checkTable_MediaType( db ) ) tables_checked++;
    if( checkTable_Media( db ) ) tables_checked++;
    if( checkTable_Note( db ) ) tables_checked++;
    if( checkTable_Subject( db ) ) tables_checked++;
    if( checkMediaRelation( db ) ) tables_checked++;
    if( checkTable_NamePartType( db ) ) tables_checked++;
    if( checkTable_NamePart( db ) ) tables_checked++;
    if( checkTable_NameForm( db ) ) tables_checked++;
    if( checkTable_NameType( db ) ) tables_checked++;
    if( checkTable_NameComposition( db ) ) tables_checked++;
    if( checkTable_Name( db ) ) tables_checked++;
    if( checkTable_GenderIdentity( db ) ) tables_checked++;
    if( checkTable_GenderBiological( db ) ) tables_checked++;
    if( checkTable_Person( db ) ) tables_checked++;
    if( checkTable_RelationshipType( db ) ) tables_checked++;
    if( checkTable_Relationship( db ) ) tables_checked++;
    if( checkTable_Date( db ) ) tables_checked++;
    if( checkTable_Location( db ) ) tables_checked++;
    if( checkTable_RoleType( db ) ) tables_checked++;
    if( checkTable_EventType( db ) ) tables_checked++;
    if( checkTable_CustomEventType( db ) ) tables_checked++;
    if( checkTable_Event( db ) ) tables_checked++;
    if( checkTable_EventRole( db ) ) tables_checked++;
    LOG( "[relations::storage::ReLationsDbSetup::checkTableStructure( ", db.getFileName() ," )] ", tables_checked, "/", _table_count," tables pass checks." );
    return tables_checked == _table_count;
}

/**
 * Builds all tables from scratch in the database
 * @param db Db connection instance
 * @return Success
 */
bool relations::storage::ReLationsDbSetup::buildTableStructure( eadlib::wrapper::SQLite &db ) {
    size_t tables_built { 0 };
    if( !db.connected() ) {
        LOG_ERROR( "[relations::storage::ReLationsDbSetup::buildTableStructure()] Database is not connected. Aborting." );
        return false;
    }
    if( buildTable_MediaType( db ) ) tables_built++;
    if( buildTable_Media( db ) ) tables_built++;
    if( buildTable_Note( db ) ) tables_built++;
    if( buildTable_Subject( db ) ) tables_built++;
    if( buildMediaRelation( db ) ) tables_built++;
    if( buildTable_NamePartType( db ) ) tables_built++;
    if( buildTable_NamePart( db ) ) tables_built++;
    if( buildTable_NameForm( db ) ) tables_built++;
    if( buildTable_NameType( db ) ) tables_built++;
    if( buildTable_NameComposition( db ) ) tables_built++;
    if( buildTable_Name( db ) ) tables_built++;
    if( buildTable_GenderIdentity( db ) ) tables_built++;
    if( buildTable_GenderBiological( db ) ) tables_built++;
    if( buildTable_Person( db ) ) tables_built++;
    if( buildTable_RelationshipType( db ) ) tables_built++;
    if( buildTable_Relationship( db ) ) tables_built++;
    if( buildTable_Date( db ) ) tables_built++;
    if( buildTable_Location( db ) ) tables_built++;
    if( buildTable_RoleType( db ) ) tables_built++;
    if( buildTable_EventType( db ) ) tables_built++;
    if( buildTable_CustomEventType( db ) ) tables_built++;
    if( buildTable_Event( db ) ) tables_built++;
    if( buildTable_EventRole( db ) ) tables_built++;
    LOG( "[relations::storage::ReLationsDbSetup::buildTableStructure( ", db.getFileName() ," )] ", tables_built, "/", _table_count," tables built." );
    return tables_built == _table_count;
}

//================================================================================================================================================//
//=============================================================[ CHECK TABLE METHODS ]============================================================//
//================================================================================================================================================//
/**
 * Checks the structure of a table
 * @param db Db connection instance
 * @param table_name Name of the table to check
 * @param column_names Names of all the column expected in order
 * @return Success
 */
bool relations::storage::ReLationsDbSetup::checkTableStructure( eadlib::wrapper::SQLite &db,
                                                                    const std::string &table_name,
                                                                    const std::vector<std::string> &column_names ) const {
    auto expected_col_count = column_names.size();
    auto table = eadlib::TableDB();
    if( db.pull( "PRAGMA table_info(" + table_name + ");", table ) != expected_col_count ) {
        LOG_ERROR( "\t   Structure: [X]" );
        return false;
    }
    auto table_it  = table.begin();
    auto column_it = column_names.cbegin();
    for( ; table_it != table.end() && column_it != column_names.cend(); ++table_it, ++column_it ) {
        if( table_it->at( 1 ).getString() != *column_it ) {
            LOG_ERROR( "\t   Structure: [X]" );
            return false;
        }
    }
    LOG( "\t   Structure: [\u2713]" );
    return true;
}

bool relations::storage::ReLationsDbSetup::checkTable_MediaType( eadlib::wrapper::SQLite &db ) const {
    LOG( "[DB]-> Checking 'MediaType' table in '", db.getFileName(), "'." );
    auto expected_cols  = std::vector<std::string>( { "id", "value" } );
    if( !checkTableStructure( db, "MediaType", expected_cols ) ) return false;
    //Content check
    auto table = eadlib::TableDB();
    if( db.pull( "SELECT value FROM MediaType", table ) < 3 ) {
        LOG_ERROR( "\t   Content  : [X]" );
        return false;
    }
    auto expected_val = std::vector<std::string>( { "Photo", "Audio", "Video", "Document" } );
    auto table_it     = table.begin();
    auto column_it    = expected_val.begin();
    for( ; table_it != table.end() && column_it != expected_val.end(); ++table_it, ++column_it ) {
        if( table_it->at( 0 ).getString() != *column_it ) {
            std::cout << table_it->at( 0 ).getString() << " != " << *column_it << std::endl;
            LOG_ERROR( "\t   Content  : [X]" );
            std::cout << table << std::endl;
            return false;
        }
    }
    LOG( "\t   Content  : [\u2713]" );
    return true;
}

bool relations::storage::ReLationsDbSetup::checkTable_Media( eadlib::wrapper::SQLite &db ) const {
    LOG( "[DB]-> Checking 'Media' table in '", db.getFileName(), "'." );
    auto expected_cols = std::vector<std::string>( { "id", "description", "media_type", "file" } );
    return checkTableStructure( db, "Media", expected_cols );
}

bool relations::storage::ReLationsDbSetup::checkTable_Note( eadlib::wrapper::SQLite &db ) const {
    LOG( "[DB]-> Checking 'Note' table in '", db.getFileName(), "'." );
    auto expected_cols = std::vector<std::string>( { "id", "subject", "text" } );
    return checkTableStructure( db, "Note", expected_cols );
}

bool relations::storage::ReLationsDbSetup::checkTable_Subject( eadlib::wrapper::SQLite &db ) const {
    LOG( "[DB]-> Checking 'Subject' table in '", db.getFileName(), "'." );
    auto expected_cols = std::vector<std::string>( { "id", "note" } );
    return checkTableStructure( db, "Subject", expected_cols );
}

bool relations::storage::ReLationsDbSetup::checkMediaRelation( eadlib::wrapper::SQLite &db ) const {
    LOG( "[DB]-> Checking 'MediaRelation' table in '", db.getFileName(), "'." );
    auto expected_cols = std::vector<std::string>( { "subject_id", "media_id" } );
    return checkTableStructure( db, "MediaRelation", expected_cols );
}

bool relations::storage::ReLationsDbSetup::checkTable_NamePartType( eadlib::wrapper::SQLite &db ) const {
    LOG( "[DB]-> Checking 'NamePartType' table in '", db.getFileName(), "'." );
    auto expected_cols = std::vector<std::string>( { "id", "value" } );
    if( !checkTableStructure( db, "NamePartType", expected_cols ) ) return false;
    //Content check
    auto table = eadlib::TableDB();
    if( db.pull( "SELECT value FROM NamePartType", table ) != 5 ) {
        LOG_ERROR( "\t   Content  : [X]" );
        return false;
    }
    auto expected_val = std::vector<std::string>( { "Prefix", "Suffix", "First", "Middle", "Surname" } );
    auto table_it     = table.begin();
    auto column_it    = expected_val.begin();
    for( ; table_it != table.end() && column_it != expected_val.end(); ++table_it, ++column_it ) {
        if( table_it->at( 0 ).getString() != *column_it ) {
            LOG_ERROR( "\t   Content  : [X]" );
            return false;
        }
    }
    LOG( "\t   Content  : [\u2713]" );
    return true;
}

bool relations::storage::ReLationsDbSetup::checkTable_NamePart( eadlib::wrapper::SQLite &db ) const {
    LOG( "[DB]-> Checking 'NamePart' table in '", db.getFileName(), "'." );
    auto expected_cols = std::vector<std::string>( { "id", "value", "type" } );
    return checkTableStructure( db, "NamePart", expected_cols );
}

bool relations::storage::ReLationsDbSetup::checkTable_NameForm( eadlib::wrapper::SQLite &db ) const {
    LOG( "[DB]-> Checking 'NameForm' table in '", db.getFileName(), "'." );
    auto expected_cols = std::vector<std::string>( { "id", "language", "full_text" } );
    return checkTableStructure( db, "NameForm", expected_cols );
}

bool relations::storage::ReLationsDbSetup::checkTable_NameType( eadlib::wrapper::SQLite &db ) const {
    LOG( "[DB]-> Checking 'NameType' table in '", db.getFileName(), "'." );
    auto expected_cols  = std::vector<std::string>( { "id", "value" } );
    if( !checkTableStructure( db, "NameType", expected_cols ) ) return false;
    //Content check
    auto table = eadlib::TableDB();
    if( db.pull( "SELECT value FROM NameType", table ) != 7 ) {
        LOG_ERROR( "\t   Content  : [X]" );
        return false;
    }
    auto expected_val = std::vector<std::string>( { "BirthName", "MarriedName", "AlsoKnownAs", "Nickname",
                                                    "AdoptiveName", "FormalName", "ReligiousName" } );
    auto table_it     = table.begin();
    auto column_it    = expected_val.begin();
    for( ; table_it != table.end() && column_it != expected_val.end(); ++table_it, ++column_it ) {
        if( table_it->at( 0 ).getString() != *column_it ) {
            LOG_ERROR( "\t   Content  : [X]" );
            return false;
        }
    }
    LOG( "\t   Content  : [\u2713]" );
    return true;
}

bool relations::storage::ReLationsDbSetup::checkTable_NameComposition( eadlib::wrapper::SQLite &db ) const {
    LOG( "[DB]-> Checking 'NameComposition' table in '", db.getFileName(), "'." );
    auto expected_cols = std::vector<std::string>( { "name_part_id", "name_form_id" } );
    return checkTableStructure( db, "NameComposition", expected_cols );
}

bool relations::storage::ReLationsDbSetup::checkTable_Name( eadlib::wrapper::SQLite &db ) const {
    LOG( "[DB]-> Checking 'Name' table in '", db.getFileName(), "'." );
    auto expected_cols = std::vector<std::string>( { "id", "type", "preferred", "name_form", "person" } );
    return checkTableStructure( db, "Name", expected_cols );
}

bool relations::storage::ReLationsDbSetup::checkTable_GenderIdentity( eadlib::wrapper::SQLite &db ) const {
    LOG( "[DB]-> Checking 'GenderIdentity' table in '", db.getFileName(), "'." );
    auto expected_cols  = std::vector<std::string>( { "id", "value" } );
    if( !checkTableStructure( db, "GenderIdentity", expected_cols ) ) return false;
    //Content check
    auto table = eadlib::TableDB();
    if( db.pull( "SELECT value FROM GenderIdentity", table ) != 16 ) {
        LOG_ERROR( "\t   Content  : [X]" );
        return false;
    }
    auto expected_val = std::vector<std::string>( { "Unspecified", "Agender", "Androgyne",
                                                    "Bi-Gender", "Non-Binary",  "Genderbender",
                                                    "Hijra", "Pangender", "Queer-Heterosexual",
                                                    "Third Gender", "Trans-Man", "Tans-Woman",
                                                    "Trans-Masculine", "Trans-Feminine", "Tri-Gender",
                                                    "Two-Spirit" } );
    auto table_it     = table.begin();
    auto column_it    = expected_val.begin();
    for( ; table_it != table.end() && column_it != expected_val.end(); ++table_it, ++column_it ) {
        if( table_it->at( 0 ).getString() != *column_it ) {
            LOG_ERROR( "\t   Content  : [X]" );
            return false;
        }
    }
    LOG( "\t   Content  : [\u2713]" );
    return true;
}

bool relations::storage::ReLationsDbSetup::checkTable_GenderBiological( eadlib::wrapper::SQLite &db ) const {
    LOG( "[DB]-> Checking 'GenderBiological' table in '", db.getFileName(), "'." );
    auto expected_cols  = std::vector<std::string>( { "id", "value" } );
    if( !checkTableStructure( db, "GenderBiological", expected_cols ) ) return false;
    //Content check
    auto table = eadlib::TableDB();
    if( db.pull( "SELECT value FROM GenderBiological", table ) < 4 ) {
        LOG_ERROR( "\t   Content  : [X]" );
        return false;
    }
    auto expected_val = std::vector<std::string>( { "Male", "Female", "Intersex", "Unknown" } );
    auto table_it     = table.begin();
    auto column_it    = expected_val.begin();
    for( ; table_it != table.end() && column_it != expected_val.end(); ++table_it, ++column_it ) {
        if( table_it->at( 0 ).getString() != *column_it ) {
            LOG_ERROR( "\t   Content  : [X]" );
            return false;
        }
    }
    LOG( "\t   Content  : [\u2713]" );
    return true;
}

bool relations::storage::ReLationsDbSetup::checkTable_Person( eadlib::wrapper::SQLite &db ) const {
    LOG( "[DB]-> Checking 'Person' table in '", db.getFileName(), "'." );
    auto expected_cols = std::vector<std::string>( { "id", "subject", "gender_bio", "gender_identity" } );
    return checkTableStructure( db, "Person", expected_cols );
}

bool relations::storage::ReLationsDbSetup::checkTable_RelationshipType( eadlib::wrapper::SQLite &db ) const {
    LOG( "[DB]-> Checking 'RelationshipType' table in '", db.getFileName(), "'." );
    auto expected_cols  = std::vector<std::string>( { "id", "value" } );
    if( !checkTableStructure( db, "RelationshipType", expected_cols ) ) return false;
    //Content check
    auto table = eadlib::TableDB();
    if( db.pull( "SELECT value FROM RelationshipType", table ) < 6 ) {
        LOG_ERROR( "\t   Content  : [X]" );
        return false;
    }
    auto expected_val = std::vector<std::string>( { "Couple", "ParentChild", "Friend", "Acquaintance", "Colleague", "Classmate" } );
    auto table_it     = table.begin();
    auto column_it    = expected_val.begin();
    for( ; table_it != table.end() && column_it != expected_val.end(); ++table_it, ++column_it ) {
        if( table_it->at( 0 ).getString() != *column_it ) {
            LOG_ERROR( "\t   Content  : [X]" );
            return false;
        }
    }
    LOG( "\t   Content  : [\u2713]" );
    return true;
}

bool relations::storage::ReLationsDbSetup::checkTable_Relationship( eadlib::wrapper::SQLite &db ) const {
    LOG( "[DB]-> Checking 'Relationship' table in '", db.getFileName(), "'." );
    auto expected_cols = std::vector<std::string>( { "person1_id", "person2_id", "subject", "type" } );
    return checkTableStructure( db, "Relationship", expected_cols );
}

bool relations::storage::ReLationsDbSetup::checkTable_Date( eadlib::wrapper::SQLite &db ) const {
    LOG( "[DB]-> Checking 'Date' table in '", db.getFileName(), "'." );
    auto expected_cols = std::vector<std::string>( { "id", "formal", "original" } );
    return checkTableStructure( db, "Date", expected_cols );
}

bool relations::storage::ReLationsDbSetup::checkTable_Location( eadlib::wrapper::SQLite &db ) const {
    LOG( "[DB]-> Checking 'Location' table in '", db.getFileName(), "'." );
    auto expected_cols = std::vector<std::string>( { "id", "description", "address", "postcode", "city",
                                                     "county", "country", "longitude", "latitude" } );
    return checkTableStructure( db, "Location", expected_cols );
}

bool relations::storage::ReLationsDbSetup::checkTable_RoleType( eadlib::wrapper::SQLite &db ) const {
    LOG( "[DB]-> Checking 'RoleType' table in '", db.getFileName(), "'." );
    auto expected_cols  = std::vector<std::string>( { "id", "value" } );
    if( !checkTableStructure( db, "RoleType", expected_cols ) ) return false;
    //Content check
    auto table = eadlib::TableDB();
    if( db.pull( "SELECT value FROM RoleType", table ) != 4 ) {
        LOG_ERROR( "\t   Content  : [X]" );
        return false;
    }
    auto expected_val = std::vector<std::string>( { "Principal", "Participant", "Official", "Witness" } );
    auto table_it     = table.begin();
    auto column_it    = expected_val.begin();
    for( ; table_it != table.end() && column_it != expected_val.end(); ++table_it, ++column_it ) {
        if( table_it->at( 0 ).getString() != *column_it ) {
            LOG_ERROR( "\t   Content  : [X]" );
            return false;
        }
    }
    LOG( "\t   Content  : [\u2713]" );
    return true;
}

bool relations::storage::ReLationsDbSetup::checkTable_EventType( eadlib::wrapper::SQLite &db ) const {
    LOG( "[DB]-> Checking 'EventType' table in '", db.getFileName(), "'." );
    auto expected_cols  = std::vector<std::string>( { "id", "value" } );
    if( !checkTableStructure( db, "EventType", expected_cols ) ) return false;
    //Content check
    auto table = eadlib::TableDB();
    if( db.pull( "SELECT value FROM EventType", table ) != 9 ) {
        LOG_ERROR( "\t   Content  : [X]" );
        return false;
    }
    auto expected_val = std::vector<std::string>( { "Adoption", "Birth", "Burial", "Census",
                                                    "Christening", "Death", "Divorce", "Marriage", "Other" } );
    auto table_it     = table.begin();
    auto column_it    = expected_val.begin();
    for( ; table_it != table.end() && column_it != expected_val.end(); ++table_it, ++column_it ) {
        if( table_it->at( 0 ).getString() != *column_it ) {
            LOG_ERROR( "\t   Content  : [X]" );
            return false;
        }
    }
    LOG( "\t   Content  : [\u2713]" );
    return true;
}

bool relations::storage::ReLationsDbSetup::checkTable_CustomEventType( eadlib::wrapper::SQLite &db ) const {
    LOG( "[DB]-> Checking 'CustomEventType' table in '", db.getFileName(), "'." );
    auto expected_cols = std::vector<std::string>( { "id", "value" } );
    return checkTableStructure( db, "CustomEventType", expected_cols );
}

bool relations::storage::ReLationsDbSetup::checkTable_Event( eadlib::wrapper::SQLite &db ) const {
    LOG( "[DB]-> Checking 'Event' table in '", db.getFileName(), "'." );
    auto expected_cols = std::vector<std::string>( { "id", "subject", "event_type",
                                                     "custom_event_type", "date", "location" } );
    return checkTableStructure( db, "Event", expected_cols );
}

bool relations::storage::ReLationsDbSetup::checkTable_EventRole( eadlib::wrapper::SQLite &db ) const {
    LOG( "[DB]-> Checking 'EventRole' table in '", db.getFileName(), "'." );
    auto expected_cols = std::vector<std::string>( { "event_id", "person_id", "role_type", "details" } );
    return checkTableStructure( db, "EventRole", expected_cols );
}

//================================================================================================================================================//
//=============================================================[ BUILD TABLE METHODS ]============================================================//
//================================================================================================================================================//
bool relations::storage::ReLationsDbSetup::buildTable_MediaType( eadlib::wrapper::SQLite &db ) {
    LOG( "[DB]-> Building 'Qualifier' table in '", db.getFileName(), "'." );
    std::stringstream ss1;
    ss1 << "CREATE TABLE MediaType( "
        << "id INTEGER PRIMARY KEY AUTOINCREMENT, "
        << "value VARCHAR(10) NOT NULL, "
        << "UNIQUE( value ) "
        << ")";
    std::stringstream ss2;
    ss2 << "INSERT INTO MediaType( value ) VALUES "
        << "(\"Photo\"), "
        << "(\"Audio\"), "
        << "(\"Video\"), "
        << "(\"Document\")";
    return db.push( ss1.str() ) && db.push( ss2.str() );
}

bool relations::storage::ReLationsDbSetup::buildTable_Media( eadlib::wrapper::SQLite &db ) {
    LOG( "[DB]-> Building 'Media' table in '", db.getFileName(), "'." );
    std::stringstream ss;
    ss << "CREATE TABLE Media( "
       << "id INTEGER PRIMARY KEY AUTOINCREMENT, "
       << "description VARCHAR(255) NOT NULL, "
       << "media_type INTEGER NOT NULL, "
       << "file VARCHAR(4352) NOT NULL, "
       << "FOREIGN KEY(media_type) REFERENCES MediaType(id) "
       << ")";
    return db.push( ss.str() );
}

bool relations::storage::ReLationsDbSetup::buildTable_Note( eadlib::wrapper::SQLite &db ) {
    LOG( "[DB]-> Building 'Note' table in '", db.getFileName(), "'." );
    std::stringstream ss;
    ss << "CREATE TABLE Note( "
       << "id INTEGER PRIMARY KEY AUTOINCREMENT, "
       << "subject VARCHAR(255) , "
       << "text TEXT "
       << ")";
    return db.push( ss.str() );
}

bool relations::storage::ReLationsDbSetup::buildTable_Subject( eadlib::wrapper::SQLite &db ) {
    LOG( "[DB]-> Building 'Subject' table in '", db.getFileName(), "'." );
    std::stringstream ss;
    ss << "CREATE TABLE Subject( "
       << "id INTEGER PRIMARY KEY AUTOINCREMENT, "
       << "note INTEGER, "
       << "FOREIGN KEY(note) REFERENCES Note(id) ON DELETE CASCADE "
       << ")";
    return db.push( ss.str() );
}

bool relations::storage::ReLationsDbSetup::buildMediaRelation( eadlib::wrapper::SQLite &db ) {
    LOG( "[DB]-> Building 'MediaRelation' table in '", db.getFileName(), "'." );
    std::stringstream ss;

    ss << "CREATE TABLE MediaRelation( "
       << "subject_id INTEGER NOT NULL, "
       << "media_id INTEGER NOT NULL, "
       << "FOREIGN KEY(subject_id) REFERENCES Subject(id) ON DELETE CASCADE, "
       << "FOREIGN KEY(media_id) REFERENCES Media(id), "
       << "UNIQUE( subject_id, media_id )"
       << ")";
    return db.push( ss.str() );
}

bool relations::storage::ReLationsDbSetup::buildTable_NamePartType( eadlib::wrapper::SQLite &db ) {
    LOG( "[DB]-> Building 'NamePartType' table in '", db.getFileName(), "'." );
    std::stringstream ss1;
    ss1 << "CREATE TABLE NamePartType( "
        << "id INTEGER PRIMARY KEY AUTOINCREMENT, "
        << "value VARCHAR(7) NOT NULL, "
        << "UNIQUE( value ) "
        << ")";
    std::stringstream ss2;
    ss2 << "INSERT INTO NamePartType( value ) VALUES "
        << "(\"Prefix\"), "
        << "(\"Suffix\"), "
        << "(\"First\"), "
        << "(\"Middle\"), "
        << "(\"Surname\")";
    return db.push( ss1.str() ) && db.push( ss2.str() );
}

bool relations::storage::ReLationsDbSetup::buildTable_NamePart( eadlib::wrapper::SQLite &db ) {
    LOG( "[DB]-> Building 'NamePart' table in '", db.getFileName(), "'." );
    std::stringstream ss;
    ss << "CREATE TABLE NamePart( "
       << "id INTEGER PRIMARY KEY AUTOINCREMENT, "
       << "value VARCHAR(50) NOT NULL, "
       << "type INTEGER NOT NULL, "
       << "FOREIGN KEY(type) REFERENCES NamePartType(id) "
       << ")";
    return db.push( ss.str() );
}

bool relations::storage::ReLationsDbSetup::buildTable_NameForm( eadlib::wrapper::SQLite &db ) {
    LOG( "[DB]-> Building 'NameForm' table in '", db.getFileName(), "'." );
    std::stringstream ss;
    ss << "CREATE TABLE NameForm( "
       << "id INTEGER PRIMARY KEY AUTOINCREMENT, "
       << "language VARCHAR(50) NOT NULL, "
       << "full_text TEXT NOT NULL "
       << ")";
    return db.push( ss.str() );
}

bool relations::storage::ReLationsDbSetup::buildTable_NameType( eadlib::wrapper::SQLite &db ) {
    LOG( "[DB]-> Building 'NameType' table in '", db.getFileName(), "'." );
    std::stringstream ss1;
    ss1 << "CREATE TABLE NameType( "
        << "id INTEGER PRIMARY KEY AUTOINCREMENT, "
        << "value VARCHAR(7) NOT NULL, "
        << "UNIQUE( value ) "
        << ")";
    std::stringstream ss2;
    ss2 << "INSERT INTO NameType( value ) VALUES "
        << "(\"BirthName\"), "
        << "(\"MarriedName\"), "
        << "(\"AlsoKnownAs\"), "
        << "(\"Nickname\"), "
        << "(\"AdoptiveName\"), "
        << "(\"FormalName\"), "
        << "(\"ReligiousName\")";
    return db.push( ss1.str() ) && db.push( ss2.str() );
}

bool relations::storage::ReLationsDbSetup::buildTable_NameComposition( eadlib::wrapper::SQLite &db ) {
    LOG( "[DB]-> Building 'NameComposition' table in '", db.getFileName(), "'." );
    std::stringstream ss;
    ss << "CREATE TABLE NameComposition( "
       << "name_part_id INTEGER, "
       << "name_form_id INTEGER, "
       << "FOREIGN KEY(name_part_id) REFERENCES NamePart(id) ON DELETE CASCADE, "
       << "FOREIGN KEY(name_form_id) REFERENCES NameForm(id), "
       << "UNIQUE( name_part_id, name_form_id )"
       << ")";
    return db.push( ss.str() );
}

bool relations::storage::ReLationsDbSetup::buildTable_GenderIdentity( eadlib::wrapper::SQLite &db ) {
    LOG( "[DB]-> Building 'Gender' table in '", db.getFileName(), "'." );
    std::stringstream ss1;
    ss1 << "CREATE TABLE GenderIdentity( "
        << "id INTEGER PRIMARY KEY AUTOINCREMENT, "
        << "value VARCHAR(20) NOT NULL, "
        << "UNIQUE( value ) "
        << ")";
    std::stringstream ss2;
    ss2 << "INSERT INTO GenderIdentity( value ) VALUES "
        << "(\"Unspecified\"), "
        << "(\"Agender\"), "
        << "(\"Androgyne\"), "
        << "(\"Bi-Gender\"), "
        << "(\"Non-Binary\"), "
        << "(\"Genderbender\"), "
        << "(\"Hijra\"), "
        << "(\"Pangender\"), "
        << "(\"Queer-Heterosexual\"), "
        << "(\"Third Gender\"), "
        << "(\"Trans-Man\"), "
        << "(\"Tans-Woman\"), "
        << "(\"Trans-Masculine\"), "
        << "(\"Trans-Feminine\"), "
        << "(\"Tri-Gender\"), "
        << "(\"Two-Spirit\")";
    return db.push( ss1.str() ) && db.push( ss2.str() );
}

bool relations::storage::ReLationsDbSetup::buildTable_GenderBiological( eadlib::wrapper::SQLite &db ) {
    LOG( "[DB]-> Building 'GenderBiological' table in '", db.getFileName(), "'." );
    std::stringstream ss1;
    ss1 << "CREATE TABLE GenderBiological( "
        << "id INTEGER PRIMARY KEY AUTOINCREMENT, "
        << "value VARCHAR(8) NOT NULL, "
        << "UNIQUE( value ) "
        << ")";
    std::stringstream ss2;
    ss2 << "INSERT INTO GenderBiological( value ) VALUES "
        << "(\"Male\"), "
        << "(\"Female\"), "
        << "(\"Intersex\"), "
        << "(\"Unknown\")";
    return db.push( ss1.str() ) && db.push( ss2.str() );
}

bool relations::storage::ReLationsDbSetup::buildTable_Person( eadlib::wrapper::SQLite &db ) {
    LOG( "[DB]-> Building 'Person' table in '", db.getFileName(), "'." );
    std::stringstream ss;
    ss << "CREATE TABLE Person( "
       << "id INTEGER PRIMARY KEY AUTOINCREMENT, "
       << "subject INTEGER NOT NULL, "
       << "gender_bio INTEGER NOT NULL, "
       << "gender_identity INTEGER DEFAULT 0, "
       << "FOREIGN KEY(subject) REFERENCES Subject(id) ON DELETE CASCADE, "
       << "FOREIGN KEY(gender_bio) REFERENCES GenderBiological(id), "
       << "FOREIGN KEY(gender_identity) REFERENCES GenderIdentity(id) "
       << ")";
    return db.push( ss.str() );
}

bool relations::storage::ReLationsDbSetup::buildTable_Name( eadlib::wrapper::SQLite &db ) {
    LOG( "[DB]-> Building 'Name' table in '", db.getFileName(), "'." );
    std::stringstream ss;
    ss << "CREATE TABLE Name( "
       << "id INTEGER PRIMARY KEY AUTOINCREMENT, "
       << "type INTEGER NOT NULL, "
       << "preferred BOOLEAN DEFAULT 0, "
       << "name_form INTEGER NOT NULL, "
       << "person INTEGER NOT NULL, "
       << "FOREIGN KEY(type) REFERENCES NameType(id), "
       << "FOREIGN KEY(name_form) REFERENCES NameForm(id) ON DELETE CASCADE, "
       << "FOREIGN KEY(person) REFERENCES Person(id) "
       << ")";
    return db.push( ss.str() );
}

bool relations::storage::ReLationsDbSetup::buildTable_RelationshipType( eadlib::wrapper::SQLite &db ) {
    LOG( "[DB]-> Building 'RelationshipType' table in '", db.getFileName(), "'." );
    std::stringstream ss1;
    ss1 << "CREATE TABLE RelationshipType( "
        << "id INTEGER PRIMARY KEY AUTOINCREMENT, "
        << "value VARCHAR(50) NOT NULL, "
        << "UNIQUE( value ) "
        << ")";
    std::stringstream ss2;
    ss2 << "INSERT INTO RelationshipType( value ) VALUES "
        << "(\"Couple\"), "
        << "(\"ParentChild\"), "
        << "(\"Friend\"), "
        << "(\"Acquaintance\"), "
        << "(\"Colleague\"), "
        << "(\"Classmate\")";
    return db.push( ss1.str() ) && db.push( ss2.str() );
}

bool relations::storage::ReLationsDbSetup::buildTable_Relationship( eadlib::wrapper::SQLite &db ) {
    LOG( "[DB]-> Building 'Relationship' table in '", db.getFileName(), "'." );
    std::stringstream ss;
    ss << "CREATE TABLE Relationship( "
       << "person1_id INTEGER NOT NULL, "
       << "person2_id INTEGER NOT NULL, "
       << "subject INTEGER NOT NULL, "
       << "type INTEGER NOT NULL, "
       << "PRIMARY KEY( person1_id, person2_id, type ), "
       << "FOREIGN KEY(person1_id) REFERENCES Person(id), "
       << "FOREIGN KEY(person2_id) REFERENCES Person(id), "
       << "FOREIGN KEY(subject) REFERENCES Subject(id) ON DELETE CASCADE, "
       << "FOREIGN KEY(type) REFERENCES RelationshipType(id), "
       << "UNIQUE( person1_id, person2_id, type ) "
       << ")";
    return db.push( ss.str() );
}

bool relations::storage::ReLationsDbSetup::buildTable_Date( eadlib::wrapper::SQLite &db ) {
    LOG( "[DB]-> Building 'Date' table in '", db.getFileName(), "'." );
    std::stringstream ss;
    ss << "CREATE TABLE Date( "
       << "id INTEGER PRIMARY KEY AUTOINCREMENT, "
       << "formal VARCHAR(50), "
       << "original VARCHAR(50) "
       << ")";
    return db.push( ss.str() );
}

bool relations::storage::ReLationsDbSetup::buildTable_Location( eadlib::wrapper::SQLite &db ) {
    LOG( "[DB]-> Building 'Place' table in '", db.getFileName(), "'." );
    std::stringstream ss;
    ss << "CREATE TABLE Location( "
       << "id INTEGER PRIMARY KEY AUTOINCREMENT, "
       << "description TEXT NOT NULL, "
       << "address TEXT, "
       << "postcode VARCHAR(15), "
       << "city VARCHAR(45), "
       << "county VARCHAR(45), "
       << "country VARCHAR(45), "
       << "longitude VARCHAR(45), "
       << "latitude VARCHAR(45) "
       << ")";
    return db.push( ss.str() );
}

bool relations::storage::ReLationsDbSetup::buildTable_RoleType( eadlib::wrapper::SQLite &db ) {
    LOG( "[DB]-> Building 'RoleType' table in '", db.getFileName(), "'." );
    std::stringstream ss1;
    ss1 << "CREATE TABLE RoleType( "
        << "id INTEGER PRIMARY KEY AUTOINCREMENT, "
        << "value VARCHAR(15) NOT NULL, "
        << "UNIQUE( value ) "
        << ")";
    std::stringstream ss2;
    ss2 << "INSERT INTO RoleType( value ) VALUES "
        << "(\"Principal\"), "
        << "(\"Participant\"), "
        << "(\"Official\"), "
        << "(\"Witness\")";
    return db.push( ss1.str() ) && db.push( ss2.str() );
}

bool relations::storage::ReLationsDbSetup::buildTable_EventType( eadlib::wrapper::SQLite &db ) {
    LOG( "[DB]-> Building 'EventType' table in '", db.getFileName(), "'." );
    std::stringstream ss1;
    ss1 << "CREATE TABLE EventType( "
        << "id INTEGER PRIMARY KEY AUTOINCREMENT, "
        << "value VARCHAR(15) NOT NULL, "
        << "UNIQUE( value ) "
        << ")";
    std::stringstream ss2;
    ss2 << "INSERT INTO EventType( value ) VALUES "
        << "(\"Adoption\"), "
        << "(\"Birth\"), "
        << "(\"Burial\"), "
        << "(\"Census\"), "
        << "(\"Christening\"), "
        << "(\"Death\"), "
        << "(\"Divorce\"), "
        << "(\"Marriage\"), "
        << "(\"Other\")";
    return db.push( ss1.str() ) && db.push( ss2.str() );
}

bool relations::storage::ReLationsDbSetup::buildTable_CustomEventType( eadlib::wrapper::SQLite &db ) {
    LOG( "[DB]-> Building 'CustomEventType' table in '", db.getFileName(), "'." );
    std::stringstream ss1;
    ss1 << "CREATE TABLE CustomEventType( "
        << "id INTEGER PRIMARY KEY AUTOINCREMENT, "
        << "value VARCHAR(50) NOT NULL, "
        << "UNIQUE( value ) "
        << ")";
    return db.push( ss1.str() );
}

bool relations::storage::ReLationsDbSetup::buildTable_Event( eadlib::wrapper::SQLite &db ) {
    LOG( "[DB]-> Building 'Event' table in '", db.getFileName(), "'." );
    std::stringstream ss;
    ss << "CREATE TABLE Event( "
       << "id INTEGER PRIMARY KEY AUTOINCREMENT, "
       << "subject INTEGER NOT NULL, "
       << "event_type INTEGER NOT NULL, "
       << "custom_event_type INTEGER, "
       << "date INTEGER NOT NULL, "
       << "location INTEGER NOT NULL, "
       << "FOREIGN KEY(subject) REFERENCES Subject(id) ON DELETE CASCADE, "
       << "FOREIGN KEY(event_type) REFERENCES EventType(id), "
       << "FOREIGN KEY(custom_event_type) REFERENCES CustomEventType(id), "
       << "FOREIGN KEY(date) REFERENCES Date(id), "
       << "FOREIGN KEY(location) REFERENCES Location(id) "
       << ")";
    return db.push( ss.str() );
}

bool relations::storage::ReLationsDbSetup::buildTable_EventRole( eadlib::wrapper::SQLite &db ) {
    LOG( "[DB]-> Building 'EventRole' table in '", db.getFileName(), "'." );
    std::stringstream ss;
    ss << "CREATE TABLE EventRole( "
       << "event_id INTEGER NOT NULL, "
       << "person_id INTEGER NOT NULL, "
       << "role_type INTEGER NOT NULL, "
       << "details TEXT, "
       << "FOREIGN KEY(event_id) REFERENCES Event(id), "
       << "FOREIGN KEY(person_id) REFERENCES Person(id), "
       << "FOREIGN KEY(role_type) REFERENCES RoleType(id), "
       << "UNIQUE( event_id, person_id, role_type ) "
       << ")";
    return db.push( ss.str() );
}