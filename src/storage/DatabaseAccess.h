#ifndef RELATIONS_DATABASEACCESS_H
#define RELATIONS_DATABASEACCESS_H

#include <string>
#include <eadlib/wrapper/SQLite/SQLite.h>
#include <unordered_map>

#include "ReLationsDbSetup.h"

namespace relations::storage {
    class DatabaseAccess {
      public:
        DatabaseAccess();
        ~DatabaseAccess();

        bool   connect( const std::string &db_file );
        bool   disconnect();
        bool   connected();
        size_t pull( const std::string &query, eadlib::TableDB &table );
        bool   push( const std::string &query );
        int64_t getLastInsertedRowIndex() const;
        bool   pullMetaData( const std::string &table_name, eadlib::TableDB &table );

        bool checkDbIntegrity();
        bool createNewStructure();

        bool isConnected();
        std::string fileName() const;
      private:
        eadlib::wrapper::SQLite _db;
    };
}

#endif //RELATIONS_DATABASEACCESS_H
