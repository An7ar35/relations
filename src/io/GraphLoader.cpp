#include <src/dto/RelationshipType.h>
#include "GraphLoader.h"

/**
 * Populates the graph with Persons in records
 * @param progress Progress tracker
 * @param db       DatabaseAccess instance to get records from
 * @param graph    DMDGraph instance to load into
 * @return Success
 */
bool relations::io::GraphLoader::populateGraphNodes( eadlib::tool::Progress &progress,
                                                     relations::storage::DatabaseAccess &db,
                                                     relations::io::GraphLoader::RelationsGraph_t &graph ) {
    eadlib::TableDB   table;
    size_t            rows { 0 };
    std::stringstream ss;
    //Getting the records from the database
    ss << "SELECT "
       << "Person.id, Person.subject AS \"subject_id\", Person.gender_bio, Person.gender_identity "
       << "FROM Person";

    if( !( rows = db.pull( ss.str(), table ) ) ) {
        LOG_WARNING( "[relations::io::GraphLoader::populateGraphNodes(..)] No Person data in records." );
        return true;
    }
    //Loading the Persons into the graph
    progress.begin( rows );
    int64_t current_person { 0 };
    for( auto r : table ) {
        if( current_person != r.at( 0 ).getInt() ) { //New Person ID
            current_person = r.at( 0 ).getInt();
            try {
                auto person_id  = r.at( 1 ).getInt();
                auto gender_bio = enums::to_GenderBioType( r.at( 2 ).getInt() );
                auto gender_id  = enums::to_GenderIdentityType( r.at( 3 ).getInt() );
                auto graph_it   = graph.emplaceNode( current_person,
                                                     dto::Person( current_person,
                                                                  person_id,
                                                                  gender_bio,
                                                                  gender_id )
                ).first;
                dto::Person * person = &graph_it->second.value;
                if( !this->loadSubject( db, *person ) || !this->loadNames( db, *person ) ) {
                    LOG_ERROR("[relations::io::GraphLoader::populateGraphNodes(..)] "
                                  "Could not get Person [", current_person,"]'s dependent data from records.");
                }
            } catch( std::invalid_argument ) {
                LOG_ERROR("[relations::io::GraphLoader::populateGraphNodes(..)] Person's Gender types are not valid in the records.");
            }
        }
        progress++;
    }
    return true;
}

/**
 * Populates the graph with the relationships (edges)
 * @param progress           Progress tracker
 * @param db                 DatabaseAccess instance
 * @param graph              Graph to load relationships into
 * @param types Container to load into the relationship types found in DB
 * @return Success
 */
bool relations::io::GraphLoader::populateGraphEdge( eadlib::tool::Progress &progress,
                                                    relations::storage::DatabaseAccess &db,
                                                    relations::io::GraphLoader::RelationsGraph_t &graph,
                                                    std::vector<dto::RelationshipType> &types ) {
    eadlib::TableDB   relationship_type_table, relationship_table;
    size_t            rows1 { 0 }, rows2 { 0 };
    std::stringstream ss1, ss2;
    //Getting the records from the database
    ss1 << "SELECT * FROM RelationshipType";
    if( !( rows1 = db.pull( ss1.str(), relationship_type_table ) ) ) {
        LOG_FATAL("[relations::io::GraphLoader::populateGraphEdge(..)] Could not get RelationshipType records." );
        return false;
    }

    ss2 << "SELECT person1_id, person2_id, subject, type FROM Relationship";
    if( !( rows2 = db.pull( ss2.str(), relationship_table ) ) ) {
        LOG_WARNING("[relations::io::GraphLoader::populateGraphEdge(..)] No Relationship records found." );
        return true;
    }
    //Loading the relationships into the graph
    auto total_rows = rows1 + rows2;
    progress.begin( static_cast<unsigned>( total_rows ) ); //TODO when eadlib::Progress is updated with size_t support remove the horrible cast here
    for( auto r : relationship_type_table ) {
        types.emplace_back( dto::RelationshipType( r.at( 0 ).getInt(),     //id
                                                   r.at( 1 ).getString() ) //value
        );
        progress++;
    }
    for( auto r : relationship_table ) {
        auto person1_id = r.at( 0 ).getInt();
        auto person2_id = r.at( 1 ).getInt();
        auto type_id    = r.at( 3 ).getInt();

        auto type = std::find_if( types.begin(),
                                  types.end(),
                                  ( [&type_id]( const dto::RelationshipType &t ) { return type_id == t._id; } )
        );

        if( type == types.end() ) {
            LOG_ERROR("[relations::io::GraphLoader::populateGraphEdge(..)] "
                          "Relationship [", person1_id, ", ", person2_id, "]'s type [", type_id, "] is not found the records.");
        } else {
            if( !graph.createDirectedEdge( person1_id, person2_id, *type ) ) {
                LOG_ERROR("[relations::io::GraphLoader::populateGraphEdge(..)] "
                              "Could not create Relationship [", person1_id, ", ", person2_id, "] of type [", type_id, "] in Graph.");
            }
        }
        progress++;
    }
    return true;
}

/**
 * Loads the names of a Person DTO from the records
 * @param db     DatabaseAccess instance
 * @param person Person DTO to load names into
 * @return Success
 */
bool relations::io::GraphLoader::loadNames( relations::storage::DatabaseAccess &db, relations::dto::Person &person ) {
    eadlib::TableDB table;
    std::stringstream ss;
    //Getting the records from the database
    ss << "SELECT "
       << "Name.id AS \"name_id\", Name.type AS \"name_type\", Name.preferred, "
       << "NameForm.id AS \"name_form_id\", NameForm.language, NameForm.full_text, "
       << "NamePart.id AS \"name_part_id\", NamePart.type AS \"name_part_type\", NamePart.value "
       << "FROM Name "
       << "LEFT OUTER JOIN NameForm ON Name.name_form = NameForm.id "
       << "LEFT OUTER JOIN NameComposition ON NameForm.id = NameComposition.name_form_id "
       << "LEFT OUTER JOIN NamePart ON NameComposition.name_part_id = NamePart.id "
       << "WHERE Name.person = " << person._id;
    if( !db.pull( ss.str(), table ) ) {
        LOG_WARNING( "[relations::io::GraphLoader::loadNames(..)] Person's [", person._id, "] has no Name records." );
        //TODO change to false when having a name is enforced during creation of a person in the upper layers
        return true;
    }
    //Loading Name(s)
    person._names.clear();
    table.sort( 0, []( const eadlib::TableDBCell &a, const eadlib::TableDBCell &b ) { return a < b; } ); //Sort by Name ID
    int64_t name_id = -1;
    auto    name_it = person._names.begin();
    for( auto r : table ) {
        if( name_id != r.at( 0 ).getInt() ) { //Need to create new Name
            auto form = new dto::NameForm( r.at( 3 ).getInt(),                  //Form ID
                                           ( r.at( 4 ).getString() != "NULL" )  //Language
                                           ? r.at( 4 ).getString()
                                           : "",
                                           ( r.at( 5 ).getString() != "NULL" )  //Full Text
                                           ? r.at( 5 ).getString()
                                           : ""
            );
            name_it = person._names.insert( name_it,
                                            dto::Name( ( name_id = r.at( 0 ).getInt() ),         //Name ID
                                                       person._id,                               //Person ID
                                                       enums::to_NameType( r.at( 1 ).getInt() ), //NameType
                                                       r.at( 2 ).getBool(),                      //Preferred flag
                                                       form )                                    //NameForm
            );
        }
        //Adding NamePart(s)
        auto part_type = enums::to_NamePartType( r.at( 7 ).getInt() );
        switch( part_type ) {
            case enums::NamePartType::PREFIX:
                name_it->_name_form->_name_parts_prefix.emplace_back( dto::NamePart( r.at( 6 ).getInt(),  //NamePart ID
                                                                      r.at( 8 ).getString(),              //NamePart Value
                                                                      part_type )                         //NamePartType
                );
                break;
            case enums::NamePartType::FIRST:
                name_it->_name_form->_name_parts_first.emplace_back( dto::NamePart( r.at( 6 ).getInt(),   //NamePart ID
                                                                              r.at( 8 ).getString(),      //NamePart Value
                                                                              part_type )                 //NamePartType
                );
                break;
            case enums::NamePartType::MIDDLE:
                name_it->_name_form->_name_parts_middle.emplace_back( dto::NamePart( r.at( 6 ).getInt(),  //NamePart ID
                                                                              r.at( 8 ).getString(),      //NamePart Value
                                                                              part_type )                 //NamePartType
                );
                break;
            case enums::NamePartType::SURNAME:
                name_it->_name_form->_name_parts_surname.emplace_back( dto::NamePart( r.at( 6 ).getInt(), //NamePart ID
                                                                              r.at( 8 ).getString(),      //NamePart Value
                                                                              part_type )                 //NamePartType
                );
                break;
            case enums::NamePartType::SUFFIX:
                name_it->_name_form->_name_parts_suffix.emplace_back( dto::NamePart( r.at( 6 ).getInt(),  //NamePart ID
                                                                              r.at( 8 ).getString(),      //NamePart Value
                                                                              part_type )                 //NamePartType
                );
                break;
        }
    }
    return true;
}

/**
 * Loads the Subject information from the records
 * @param db      DatabaseAccess instance
 * @param subject Subject DTO to load information into
 * @return Success
 */
bool relations::io::GraphLoader::loadSubject( relations::storage::DatabaseAccess &db, relations::dto::Subject &subject ) {
    eadlib::TableDB   table;
    std::stringstream ss;
    //Getting the records from the database
    ss << "SELECT "
       << "Subject.id AS \"subject_id\", Subject.note AS \"note_id\", "
       << "Note.subject AS \"note_subject\", Note.text AS \"note_text\" "
       << "FROM Subject "
       << "LEFT JOIN Note ON Subject.note = Note.id "
       << "WHERE Subject.id = " << subject._subject_id << ";";
    if( !db.pull( ss.str(), table ) ) {
        LOG_ERROR( "[relations::io::GraphLoader::loadSubject(..)] Could not get Subject [", subject._subject_id, "] Person records." );
        return false;
    }
    //Loading the Subject's Note
    using eadlib::TableDBCell;
    if( table.at( 1, 0 ).getType() != TableDBCell::DataType::NONE ) {
        int64_t     note_id      = table.at( 1, 0 ).getInt();
        std::string note_subject = table.at( 2, 0 ).getString();
        std::string note_text    = table.at( 3, 0 ).getString();
        if( subject._note ) { //Check and update note content
            if( note_subject != "NULL" && subject._note->_subject != note_subject )
                subject._note->_subject = note_subject;
            if( note_text != "NULL" && subject._note->_text != note_text )
                subject._note->_text = note_text;
        } else {
            if( note_subject != "NULL" || note_text != "NULL" ) {
                LOG_TRACE( "[relations::io::GraphLoader::loadSubject(..)] Adding Note to Subject [", subject._subject_id, "]" );
                subject._note = std::make_unique<dto::Note>( note_id,
                                                             note_subject == "NULL" ? "" : note_subject,
                                                             note_text == "NULL" ? "" : note_text );
            }
        }
    } else {
        LOG_DEBUG( "[relations::io::GraphLoader::loadSubject(..)] No Note for [", subject._subject_id, "] in records." );
    }
    return true;
}