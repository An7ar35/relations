#ifndef RELATIONS_DATACACHE_H
#define RELATIONS_DATACACHE_H

#include <vector>
#include "src/dto/RelationshipType.h"
#include "src/dto/EnumType.h"
#include "src/enums/EnumeratedTypes.h"

namespace relations::io {
    struct DataCache {
        std::vector<dto::RelationshipType>                    _relationship_types;
        std::vector<dto::EnumType<enums::EventType>>          _event_types;
        std::vector<dto::EnumType<enums::MediaType>>          _media_types;
        std::vector<dto::EnumType<enums::RoleType>>           _role_types;
        std::vector<dto::EnumType<enums::GenderBioType>>      _gender_bio_types;
        std::vector<dto::EnumType<enums::GenderIdentityType>> _gender_identity_types;
        std::vector<dto::EnumType<enums::NameType>>           _name_types;
        std::vector<dto::EnumType<enums::NamePartType>>       _name_part_types;
    };
}

#endif //RELATIONS_DATACACHE_H
