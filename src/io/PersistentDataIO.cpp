#include "PersistentDataIO.h"
#include <external/eadlib/exception/corruption.h>

//-----------------------------------------------------------------------------------------------------------------
// PersistentData class public method implementations
//-----------------------------------------------------------------------------------------------------------------
/**
 * Constructor
 * @param db Database Access instance
 */
relations::io::PersistentDataIO::PersistentDataIO( std::unique_ptr<storage::DatabaseAccess> &db ) :
    _db( std::move( db ) )
{}

/**
 * Destructor
 */
relations::io::PersistentDataIO::~PersistentDataIO() {}

//==================================================================================================================================================//
// [Public] DatabaseAccess transparency methods
//==================================================================================================================================================//
/**
 * Opens a connection to a database file
 * @param file_name File name
 * @return Success
 */
bool relations::io::PersistentDataIO::open( const std::string &file_name ) {
    return _db->connect( file_name );
}

/**
 * Closes a current connection to a database file
 * @return Success
 */
bool relations::io::PersistentDataIO::close() {
    return _db->disconnect();
}

/**
 * Checks file connection state
 * @return File opened state
 */
bool relations::io::PersistentDataIO::connected() {
    return _db->connected();
}

/**
 * Creates the table structures for a new database file
 * @return Success
 */
bool relations::io::PersistentDataIO::createTableStructure() {
    return _db->createNewStructure();
}

/**
 * Checks existing table structures in a database file
 * @return Success
 */
bool relations::io::PersistentDataIO::checkTableStructure() {
    return _db->checkDbIntegrity();
}

/**
 * Gets the currently connected DB file name
 * @return File name
 */
std::string relations::io::PersistentDataIO::fileName() const {
    return _db->fileName();
}

/**
 * Gets a pointer to the DatabaseAccess instance
 * @return DatabaseAccess instance
 */
relations::storage::DatabaseAccess *relations::io::PersistentDataIO::getDbAccess() {
    return _db.get();
}

//==================================================================================================================================================//
// [Public] Record creation methods
//==================================================================================================================================================//
/**
 * Adds a Person to the records
 * @param person Person to add
 * @return Success
 */
bool relations::io::PersistentDataIO::add( dto::Person &person ) {
    if( !addSubject( person ) ) {
        LOG_ERROR("[relations::io::PersistentDataIO::add( dto::Person & )] Could not add Subject of Person to database." );
        return false;
    }
    std::stringstream ss;
    ss << "INSERT INTO Person( subject, gender_bio, gender_identity ) "
       << "VALUES( " << person._subject_id << ", " << person._gender_biological << ", " << person._gender_identity << " )";
    if( !_db->push( ss.str() ) ) {
        LOG_ERROR("[relations::io::PersistentDataIO::add( dto::Person & )] Could not add Person to database." );
        return false;
    }
    person._id = _db->getLastInsertedRowIndex();
    LOG_TRACE( "[relations::io::PersistentDataIO::add( dto::Person & )] Person ID returned as ", person._id );
    return true;

}

/**
 * Adds a Name to the records
 * @param name Name to add
 * @return Success
 */
bool relations::io::PersistentDataIO::add( dto::Name &name ) {
    if( !add( *name._name_form ) ) {
        LOG_ERROR("[relations::io::PersistentDataIO::add( dto::Name & )] Could not add NameForm of name to database." );
        return false;
    }
    std::stringstream ss;
    ss << "INSERT INTO Name( type, preferred, name_form, person ) "
       << "VALUES( "
       << eadlib::enum_int<enums::NameType>( name._type ) << ", "
       << name._preferred_flag << ", "
       << name._name_form->_id << ", "
       << name._person_id
       << " )";
    if( !_db->push( ss.str() ) ) {
        LOG_ERROR("[relations::io::PersistentDataIO::add( dto::Name & )] Could not add name to database." );
        return false;
    }
    name._id = _db->getLastInsertedRowIndex();
    LOG_TRACE( "[relations::io::PersistentDataIO::add( dto::Name & )] Name ID returned as ", name._id );
    return true;
}

/**
 * Adds a Media to the records
 * @param media Media to add
 * @return Success
 */
bool relations::io::PersistentDataIO::add( dto::Media &media ) {
    std::stringstream ss;
    ss << "INSERT INTO Media( description, media_type, file ) "
       << "VALUES( "
       << "\"" << media._description << "\", "
       << eadlib::enum_int<enums::MediaType>( media._type ) << ", "
       << "\"" << media._file << "\""
       << " )";
    if( !_db->push( ss.str() ) ) {
        LOG_ERROR( "[relations::io::PersistentDataIO::add( dto::Media & )] Could not add media to database." );
        return false;
    }
    media._id = _db->getLastInsertedRowIndex();
    LOG_TRACE( "[relations::io::PersistentDataIO::add( dto::Media & )] Media ID returned as ", media._id );
    return true;
}

/**
 * Adds an Event to the records
 * @param event Event to add
 * @return Success
 */
bool relations::io::PersistentDataIO::add( dto::Event &event ) {
    if( !event._date || !event._location ) {
        LOG_ERROR("[relations::io::PersistentDataIO::add( dto::Event & )] Event malformed (missing Date or Location)." );
        return false;
    }
    if( event._date->_id < 0 ) {
        if( !add( *event._date ) ) {
            LOG_ERROR("[relations::io::PersistentDataIO::add( dto::Event & )] Could not add new Date for Event [", event._id, "]." );
            return false;
        }
    }
    if( event._location->_id < 0 ) {
        if( !add( *event._location ) ) {
            LOG_ERROR("[relations::io::PersistentDataIO::add( dto::Event & )] Could not add new Location for Event [", event._id, "]." );
            return false;
        }
    }
    if( !addSubject( event ) ) {
        LOG_ERROR("[relations::io::PersistentDataIO::add( dto::Event & )] Could not add Subject of event to database." );
        return false;
    }
    std::stringstream ss;
    if( event._type == enums::EventType::OTHER ) {
        ss << "INSERT INTO Event( subject, event_type, custom_event_type, date, location ) "
           << "VALUES( "
           << event._subject_id << ", "
           << eadlib::enum_int<enums::EventType>( event._type ) << ", ";
        if( event._custom_type > 0 )
            ss << event._custom_type << ", ";
        else
            ss << "NULL, ";
        ss << event._date->_id << ", "
           << event._location->_id
           << " )";
    } else {
        ss << "INSERT INTO Event( subject, event_type, date, location ) "
           << "VALUES( "
           << event._subject_id << ", "
           << eadlib::enum_int<enums::EventType>( event._type ) << ", "
           << event._date->_id << ", "
           << event._location->_id
           << " )";
    }
    if( !_db->push( ss.str() ) ) {
        LOG_ERROR( "[relations::io::PersistentDataIO::add( dto::Event & )] Could not add event to database." );
        return false;
    }
    event._id = _db->getLastInsertedRowIndex();
    LOG_TRACE( "[relations::io::PersistentDataIO::add( dto::Event & )] Event ID returned as ", event._id );
    return true;
}

/**
 * Adds an EventRole to the records
 * @param role Event role to add
 * @return Success
 */
bool relations::io::PersistentDataIO::add( dto::EventRole &role ) {
    std::stringstream ss;
    ss << "INSERT INTO EventRole( event_id, person_id, role_type, details ) "
       << "VALUES( "
       << role._event_id << ", "
       << role._person_id << ", "
       << eadlib::enum_int<enums::RoleType>( role._type ) << ", "
       << "\"" << role._details << "\""
       << " )";
    if( !_db->push( ss.str() ) ) {
        LOG_ERROR( "[relations::io::PersistentDataIO::add( dto::EventRole & )] Could not add event role to database." );
        return false;
    }
    LOG_TRACE( "[relations::io::PersistentDataIO::add( dto::EventRole & )] "
                   "EventRole [", role._event_id, ", ", role._person_id, ", ", eadlib::enum_int<enums::RoleType>( role._type ), "] successfully added. )" );
    return true;
}

/**
 * Adds a relationship to the record
 * @param relationship Relationship to add
 * @return Success
 */
bool relations::io::PersistentDataIO::add( dto::Relationship &relationship ) {
    if( !addSubject( relationship ) ) {
        LOG_ERROR("[relations::io::PersistentDataIO::add( dto::Relationship & )] Could not add Subject of relationship to database." );
        return false;
    }
    std::stringstream ss;
    ss << "INSERT INTO Relationship( person1_id, person2_id, subject, type ) "
       << "VALUES( "
       << relationship._person1 << ", "
       << relationship._person2 << ", "
       << relationship._subject_id << ", "
       << relationship._type_id
       << " )";
    if( !_db->push( ss.str() ) ) {
        LOG_ERROR("[relations::io::PersistentDataIO::add( dto::Relationship & )] Could not add relationship to database." );
        return false;
    }
    LOG_TRACE( "[relations::io::PersistentDataIO::add( dto::Relationship & )] Relationship's Subject ID is ", relationship._subject_id );
    return true;
}

/**
 * Adds a media-subject relation
 * @param media_relation MediaRelations DTO
 * @return Success
 */
bool relations::io::PersistentDataIO::add( relations::dto::MediaRelation &media_relation ) {
    std::stringstream ss;
    ss << "INSERT INTO MediaRelation( subject_id, media_id ) "
       << "VALUES( "
       << media_relation._subject_id << ", "
       << media_relation._media_id
       << " )";
    if( !_db->push( ss.str() ) ) {
        LOG_ERROR( "[relations::io::PersistentDataIO::add( dto::MediaRelation & )] "
                       "Failed to add Subject[", media_relation._subject_id, "]->Media[", media_relation._media_id, "] relation." );
        return false;
    }
    LOG_TRACE( "[relations::io::PersistentDataIO::add( dto::MediaRelation & )] "
                   "Added Subject[", media_relation._subject_id, "]->Media[", media_relation._media_id, "] relation." );
    return true;
}

/**
 * Adds a custom RelationshipType to the records
 * @param relationship_type Relationship type to add
 * @return Success
 */
bool relations::io::PersistentDataIO::add( dto::RelationshipType &relationship_type ) {
    std::stringstream ss;
    ss << "INSERT INTO RelationshipType( value ) "
       << "VALUES( \"" << relationship_type._value << "\" )";
    if( !_db->push( ss.str() ) ) {
        LOG_ERROR("[relations::io::PersistentDataIO::add( dto::RelationshipType & )] Could not add relationship type to database." );
        return false;
    }
    relationship_type._id = _db->getLastInsertedRowIndex();
    LOG_TRACE( "[relations::io::PersistentDataIO::add( dto::RelationshipType & )] RelationshipType ID returned as ", relationship_type._id );
    return true;
}

/**
 * Adds a custom Event Type to the records
 * @param event_type Event type to add
 * @return Success
 */
bool relations::io::PersistentDataIO::add( relations::dto::CustomEventType &event_type ) {
    std::stringstream ss;
    ss << "INSERT INTO CustomEventType( value ) "
       << "VALUES( \"" << event_type._value << "\" )";
    if( !_db->push( ss.str() ) ) {
        LOG_ERROR("[relations::io::PersistentDataIO::add( dto::CustomEventType & )] Could not add CustomEventType to database." );
        return false;
    }
    event_type._id = _db->getLastInsertedRowIndex();
    LOG_TRACE( "[relations::io::PersistentDataIO::add( dto::CustomEventType & )] CustomEventType ID returned as ", event_type._id );
    return true;
}

/**
 * Tries to create a RelationshipType in the records if not present
 * @param relationship_type RelationshipType to check/create
 * @return Success (it exists)
 */
bool relations::io::PersistentDataIO::try_add( dto::RelationshipType &relationship_type ) {
    return exists( relationship_type ) ? true : add( relationship_type );
}

/**
 * Tries to create a CustomEventType in the records if not present
 * @param event_type CustomEventType to check/create
 * @return Success (it exists)
 */
bool relations::io::PersistentDataIO::try_add( relations::dto::CustomEventType &event_type ) {
    return exists( event_type ) ? true : add( event_type );
}

//==================================================================================================================================================//
// [Public] Record update methods
//==================================================================================================================================================//
/**
 * Updates Person
 * \nNames not included in update. Do them separately using 'update( dto::Name &, dto::Name & )'.
 * @param person Original Person
 * @param update Updated Person
 * @return Success
 */
bool relations::io::PersistentDataIO::update( dto::Person &person, dto::Person &update ) {
    LOG_DEBUG( "[relations::io::PersistentDataIO::update( dto::Person &, dto::Person & )] Starting update on Person [", person._id, "]." );
    std::list<std::string> updates;
    size_t                 update_count { 0 };
    std::stringstream      ss;
    //Nested updates
    if( !this->updateSubject( person, update ) ) {
        LOG_ERROR( "[relations::io::PersistentDataIO::update( dto::Person &, dto::Person & )] Could not update Person [", person._id, "]'s Note." );
        return false;
    }
    //Select updates
    if( person._gender_biological != update._gender_biological )
        updates.emplace_back( "gender_bio = " + eadlib::to_string<enums::GenderBioType>( update._gender_biological ) );
    if( person._gender_identity != update._gender_identity )
        updates.emplace_back( "gender_identity = " + eadlib::to_string<enums::GenderIdentityType>( update._gender_identity ) );

    if( ( update_count = updates.size() ) < 1 ) {
        LOG_DEBUG( "[relations::io::PersistentDataIO::update( dto::Person &, dto::Person & )] Found nothing to update in Person [", person._id, "]." );
        return true;
    }
    //Assemble the update query
    size_t count { 0 };
    ss << "UPDATE Person SET ";
    for( auto u : updates ) {
        count++;
        ss << u;
        if( count != update_count )
            ss << ", ";
    }
    ss << " WHERE id = " << person._id;
    //Commit query
    if( !_db->push( ss.str() ) ) {
        LOG_ERROR( "[relations::io::PersistentDataIO::update( dto::Person &, dto::Person & )] Could not update Person [", person._id, "]." );
        return false;
    }
    LOG_DEBUG( "[relations::io::PersistentDataIO::update( dto::Person &, dto::Person & )] Person [", person._id, "] updated." );
    return true;
}

/**
 * Updates Name
 * @param name   Original Name
 * @param update Updated Name
 * @return Success
 */
bool relations::io::PersistentDataIO::update( dto::Name &name, dto::Name &update ) {
    std::list<std::string> updates;
    size_t                 update_count { 0 };
    std::stringstream      ss;
    //Nested updates
    if( update._name_form ) {
        if( name._name_form ) {
            if( !this->update( *name._name_form, *update._name_form ) ){
                LOG_ERROR( "[relations::io::PersistentDataIO::update( dto::Name &, dto::Name & )] Could not update Name [", name._id, "]'s NameForm." );
                return false;
            }
        } else { //needs new NameForm
            if( !this->add( *update._name_form ) ) {
                LOG_ERROR( "[relations::io::PersistentDataIO::update( dto::Name &, dto::Name & )] Could not add Name [", name._id, "]'s NameForm." );
                return false;
            }
        }
        updates.emplace_back( "name_form = " + std::to_string( update._name_form->_id ) );
    }
    //Select updates
    if( name._type != update._type )
        updates.emplace_back( "type = " + eadlib::to_string<enums::NameType>( update._type ) );
    if( name._preferred_flag != update._preferred_flag )
        updates.emplace_back( "preferred = " + std::to_string( update._preferred_flag ) );
    if( ( update_count = updates.size() ) < 1 ) {
        LOG( "[relations::io::PersistentDataIO::update( dto::Name &, dto::Name & )] Found nothing to update in Name [", name._id, "]." );
        return true;
    }
    //Assemble the update query
    size_t count { 0 };
    ss << "UPDATE Name SET ";
    for( auto u : updates ) {
        count++;
        ss << u;
        if( count != update_count )
            ss << ", ";
    }
    ss << " WHERE id = " << name._id;
    //Commit query
    if( !_db->push( ss.str() ) ) {
        LOG_ERROR( "[relations::io::PersistentDataIO::update( dto::Name &, dto::Name & )] Could not update Name [", name._id, "]." );
        return false;
    }
    LOG_DEBUG( "[relations::io::PersistentDataIO::update( dto::Name &, dto::Name & )] Name [", name._id, "] updated." );
    return true;
}

/**
 * Updates Media
 * @param media  Original Media
 * @param update Updated Media
 * @return Success
 */
bool relations::io::PersistentDataIO::update( relations::dto::Media &media, relations::dto::Media &update ) {
    std::list<std::string> updates;
    size_t                 update_count { 0 };
    std::stringstream      ss;
    //Select updates
    if( media._file != update._file )
        updates.emplace_back( "file = \"" + update._file + "\"" );
    if( media._type != update._type )
        updates.emplace_back( "media_type = " + eadlib::to_string<enums::MediaType >( update._type ) );
    if( media._description != update._description )
        updates.emplace_back( "description = \"" + update._description + "\"" );

    if( ( update_count = updates.size() ) < 1 ) {
        LOG( "[relations::io::PersistentDataIO::update( dto::Media &, dto::Media & )] Found nothing to update in Media [", media._id, "]." );
        return true;
    }
    //Assemble the update query
    size_t count { 0 };
    ss << "UPDATE Media SET ";
    for( auto u : updates ) {
        count++;
        ss << u;
        if( count != update_count )
            ss << ", ";
    }
    ss << " WHERE id = " << media._id;
    //Commit query
    if( !_db->push( ss.str() ) ) {
        LOG_ERROR( "[relations::io::PersistentDataIO::update( dto::Media &, dto::Media & )] Could not update Media [", media._id, "]." );
        return false;
    }
    LOG_DEBUG( "[relations::io::PersistentDataIO::update( dto::Media &, dto::Media & )] Media [", media._id, "] updated." );
    return true;
}

/**
 * Updates Event
 * @param event  Original Event
 * @param update Updated Event
 * @return Success
 */
bool relations::io::PersistentDataIO::update( relations::dto::Event &event, relations::dto::Event &update ) {
    std::list<std::string> updates;
    size_t                 update_count { 0 };
    std::stringstream      ss;
    //Nested updates
    if( !this->updateSubject( event, update ) ) {
        LOG_ERROR( "[relations::io::PersistentDataIO::update( dto::Event &, dto::Event & )] Could not update Subject [", event._subject_id, "]." );
    }
    if( update._date ) {
        if( event._date ) {
            if( !this->update( *event._date, *update._date ) ) {
                LOG_ERROR( "[relations::io::PersistentDataIO::update( dto::Person &, dto::Event & )] Could not update Event [", event._id, "]'s Date." );
                return false;
            }
        } else { //needs new Date
            if( !this->add( *update._date ) ) {
                LOG_ERROR( "[relations::io::PersistentDataIO::update( dto::Event &, dto::Event & )] Could not add Event [", event._id, "]'s Date." );
                return false;
            }
            updates.emplace_back( "date = " + std::to_string( update._date->_id ) );
        }
    }
    if( update._location ) {
        if( event._location ) {
            if( !this->update( *event._location, *update._location ) ) {
                LOG_ERROR( "[relations::io::PersistentDataIO::update( dto::Person &, dto::Event & )] Could not update Event [", event._id, "]'s Location." );
                return false;
            }
        } else { //need new Location
            if( !this->add( *update._location ) ) {
                LOG_ERROR( "[relations::io::PersistentDataIO::update( dto::Event &, dto::Event & )] Could not add Event [", event._id, "]'s Location." );
                return false;
            }
            updates.emplace_back( "location = " + std::to_string( update._location->_id ) );
        }
    }
    //Select updates
    if( event._type != update._type )
        updates.emplace_back( "event_type = " + eadlib::to_string<enums::EventType>( update._type ) );
    if( event._custom_type != update._custom_type )
        updates.emplace_back( "custom_event_type = " + std::to_string( update._custom_type ) );
    if( ( update_count = updates.size() ) < 1 ) {
        LOG( "[relations::io::PersistentDataIO::update( dto::Event &, dto::Event & )] Found nothing to update in Event [", event._id, "]." );
        return true;
    }
    //Assemble the update query
    size_t count { 0 };
    ss << "UPDATE Event SET ";
    for( auto u : updates ) {
        count++;
        ss << u;
        if( count != update_count )
            ss << ", ";
    }
    ss << " WHERE id = " << event._id;
    //Commit query
    if( !_db->push( ss.str() ) ) {
        LOG_ERROR( "[relations::io::PersistentDataIO::update( dto::Event &, dto::Event & )] Could not update Event [", event._id, "]." );
        return false;
    }
    LOG_DEBUG( "[relations::io::PersistentDataIO::update( dto::Event &, dto::Event & )] Event [", event._id, "] updated." );
    return true;
}

/**
 * Updates EventRole
 * @param role   Original EventRole
 * @param update Updated EventRole
 * @return Success
 */
bool relations::io::PersistentDataIO::update( relations::dto::EventRole &role, relations::dto::EventRole &update ) {
    std::list<std::string> updates;
    size_t                 update_count { 0 };
    if( !( role._event_id == update._event_id //Check if [P,E,T] composite key is different in the update
        && role._person_id == update._person_id
        && role._type == update._type  ) ) {
        //Check the different composite key exists already in records
        std::stringstream ss1;
        eadlib::TableDB   table;
        ss1 << "SELECT * FROM EventRole "
            << "WHERE "
            << "event_id = " << update._event_id << " AND "
            << "person_id = " << update._person_id << " AND "
            << "type = " << eadlib::to_string<enums::RoleType>( update._type );
        _db->pull( ss1.str(), table );
        if( table.getRowCount() > 0 ) {
            LOG_ERROR( "[relations::io::PersistentDataIO::update( dto::EventRole &, dto::EventRole & )] "
                           "EventRole [", update._event_id, ", ", update._person_id, ", ", eadlib::to_string<enums::RoleType>( update._type ), "] "
                           "already exists in records." );
            return false;
        }
        //Composite key updates
        if( role._event_id != update._event_id )
            updates.emplace_back( "event_id = " + std::to_string( update._event_id ) );
        if( role._event_id != update._event_id )
            updates.emplace_back( "person_id = " + std::to_string( update._person_id ) );
        if( role._event_id != update._event_id )
            updates.emplace_back( "role_type = " + eadlib::to_string<enums::RoleType>( update._type ) );
    }

    //Details updates
    if( role._details != update._details )
        updates.emplace_back( "details = \"" + update._details + "\"" );

    //Assemble the update query
    std::stringstream      ss2;
    size_t count { 0 };
    ss2 << "UPDATE EventRole SET ";
    for( auto u : updates ) {
        count++;
        ss2 << u;
        if( count != update_count )
            ss2 << ", ";
    }
    ss2 << " WHERE "
        << "event_id = " << update._event_id << " AND "
        << "person_id = " << update._person_id << " AND "
        << "type = " << eadlib::to_string<enums::RoleType>( update._type );
    //Commit query
    if( !_db->push( ss2.str() ) ) {
        LOG_ERROR( "[relations::io::PersistentDataIO::update( dto::EventRole &, dto::EventRole & )] "
                       "Could not update EventRole [", update._event_id, ", ", update._person_id, ", ", eadlib::to_string<enums::RoleType>( update._type ), "]." );
        return false;
    }
    LOG_DEBUG( "[relations::io::PersistentDataIO::update( dto::EventRole &, dto::EventRole & )] "
                   "EventRole [", update._event_id, ", ", update._person_id, ", ", eadlib::to_string<enums::RoleType>( update._type ), "] updated." );
    return true;
}

/**
 * Updates a Relationship's type
 * @param relationship Original Relationship
 * @param update       Updated Relationship
 * @return Success
 */
bool relations::io::PersistentDataIO::update( relations::dto::Relationship &relationship, relations::dto::Relationship &update ) {
    //Nested updates
    if( !this->updateSubject( relationship, update ) ) {
        LOG_ERROR( "[relations::io::PersistentDataIO::update( dto::Relationship &, dto::Relationship & )] "
                       "Could not update Subject [", relationship._subject_id, "]." );
    }
    //Check update
    if( relationship._person1 == update._person1
        && relationship._person2 == update._person2
        && relationship._type_id == update._type_id )
    {
        LOG( "[relations::io::PersistentDataIO::update( dto::Relationship &, dto::Relationship & )] "
                 "Found nothing to update in Relationship [", relationship._person1, ", ", relationship._person2, "]:[", relationship._type_id, "]." );
        return true;
    } else {
        //Check updated value is not already in records
        std::stringstream ss1;
        eadlib::TableDB   table;
        ss1 << "SELECT * FROM Relationship "
            << "WHERE "
            << "person1_id = " << update._person1 << " AND "
            << "person2_id = " << update._person2 << " AND "
            << "type = " << update._type_id;
        if( _db->pull( ss1.str(), table ) > 0 ) {
            LOG_ERROR( "[relations::io::PersistentDataIO::update( dto::Relationship &, dto::Relationship & )] "
                           "Relationship [", update._person1, ", ", update._person2, "]:[", update._type_id, "] already exists in records." );
            return false;
        }
        //Assemble the update query
        std::stringstream ss2;
        ss2 << "UPDATE Relationship "
            << "SET "
            << "person1_id = " << update._person1 << ", "
            << "person2_id = " << update._person2 << ", "
            << "type = " << update._type_id << " WHERE "
            << "person1_id = " << relationship._person1 << " AND "
            << "person2_id = " << relationship._person2 << " AND "
            << "type = " << relationship._type_id;
        //Commit query
        if( !_db->push( ss2.str() ) ) {
            LOG_ERROR( "[relations::io::PersistentDataIO::update( dto::Relationship &, dto::Relationship & )] "
                           "Could not update Relationship [", relationship._person1, ", ", relationship._person2, "]:[", relationship._type_id, "} "
                           "with type [", update._type_id, "]." );
            return false;
        }
        LOG_DEBUG( "[relations::io::PersistentDataIO::update( dto::Relationship &, dto::Relationship & )] "
                       "Relationship [", relationship._person1, ", ", relationship._person2, "]:[", relationship._type_id, "] "
                       "updated to [", update._person1, ", ", update._person2, "]:[", update._type_id, "] " );
        return true;
    }
}

/**
 * Updates RelationshipType
 * @param relationship_type Original RelationshipType
 * @param update            Updated RelationshipType
 * @return Success
 */
bool relations::io::PersistentDataIO::update( relations::dto::RelationshipType &relationship_type, relations::dto::RelationshipType &update ) {
    //Check update
    if( relationship_type._value == update._value ) {
        LOG( "[relations::io::PersistentDataIO::update( dto::RelationshipType &, dto::RelationshipType & )] "
                 "Found nothing to update in RelationshipType [", relationship_type._id, "]." );
        return true;
    } else {
        //Check updated value is not already in records
        std::stringstream ss1;
        eadlib::TableDB   table;
        ss1 << "SELECT * FROM RelationshipType WHERE value = \"" << update._value << "\";";
        if( _db->pull( ss1.str(), table ) > 0 ) {
            LOG_ERROR( "[relations::io::PersistentDataIO::update( dto::RelationshipType &, dto::RelationshipType & )] "
                           "Value '", update._value, "' already exists in records." );
            return false;
        }
        //Assemble the update query
        std::stringstream ss2;
        ss2 << "UPDATE RelationshipType "
            << "SET value = \"" << update._value << "\" "
            << "WHERE id = " << relationship_type._id;
        //Commit query
        if( !_db->push( ss2.str() ) ) {
            LOG_ERROR( "[relations::io::PersistentDataIO::update( dto::RelationshipType &, dto::RelationshipType & )] "
                           "Could not update RelationshipType [", relationship_type._id, "]." );
            return false;
        }
        LOG_DEBUG( "[relations::io::PersistentDataIO::update( dto::RelationshipType &, dto::RelationshipType & )] "
                       "RelationshipType [", relationship_type._id, "] updated." );
        return true;
    }
}

/**
 * Updates CustomEventType
 * @param event_type Original CustomEventType
 * @param update     Updated CustomEventType
 * @return Success
 */
bool relations::io::PersistentDataIO::update( dto::CustomEventType &event_type, dto::CustomEventType &update ) {
    if( event_type == update ) {
        LOG( "[relations::io::PersistentDataIO::update( dto::CustomEventType &, dto::CustomEventType & )] "
                 "Found nothing to update in CustomEventType [", event_type._id, "]." );
        return true;
    } else {
        //Check updated value is not already in records
        std::stringstream ss1;
        eadlib::TableDB   table;
        ss1 << "SELECT * FROM CustomEventType WHERE value = \"" << update._value << "\";";
        if( _db->pull( ss1.str(), table ) > 0 ) {
            LOG_ERROR( "[relations::io::PersistentDataIO::update( dto::CustomEventType &, dto::CustomEventType & )] "
                           "Value '", update._value, "' already exists in records." );
            return false;
        }
        //Assemble the update query
        std::stringstream ss2;
        ss2 << "UPDATE CustomEventType "
            << "SET value = \"" << update._value << "\" "
            << "WHERE id = " << event_type._id;
        //Commit query
        if( !_db->push( ss2.str() ) ) {
            LOG_ERROR( "[relations::io::PersistentDataIO::update( dto::CustomEventType &, dto::CustomEventType & )] "
                           "Could not update CustomEventType [", event_type._id, "]." );
            return false;
        }
        LOG_DEBUG( "[relations::io::PersistentDataIO::update( dto::CustomEventType &, dto::CustomEventType & )] "
                       "CustomEventType [", event_type._id, "] updated." );
        return true;
    }
}

//==================================================================================================================================================//
// [Public] Record removal methods
//==================================================================================================================================================//
/**
 * Deletes a Person
 * @param person Person DTO
 * @return Success
 */
bool relations::io::PersistentDataIO::remove( dto::Person &person ) {
    LOG_DEBUG( "[relations::io::PersistentDataIO::remove( dto::Person & )] Beginning Person [", person._id, "]'s removal from records." );
    //Remove referencing entries
    bool error_flag = false;
    for( auto name : person._names ) {
        if( !remove( name ) )
            error_flag = true;
    }
    //Remove relationships
    if( !removeRelationships( person ) )
        error_flag = true;
    //Remove Person entry
    std::stringstream ss;
    ss << "DELETE FROM Person WHERE id = " << person._id;
    if( !_db->push( ss.str() ) ) {
        LOG_ERROR( "[relations::io::PersistentDataIO::remove( dto::Person & )] Could not delete Person [", person._id, "]." );
        return false;
    }
    if( !removeSubject( person ) ) {
        LOG_ERROR( "[relations::io::PersistentDataIO::remove( dto::Person & )] "
                       "Could not remove Person [", person._id, "]'s Subject entry [", person._subject_id, "]." );
        error_flag = true;
    }
    if( !error_flag )
        LOG_DEBUG( "[relations::io::PersistentDataIO::remove( dto::Person & )] Person [", person._id, "]'s record fully removed." );
    return true;
}

/**
 * Deletes a Name
 * @param name Name DTO
 * @return Success
 */
bool relations::io::PersistentDataIO::remove( dto::Name &name ) {
    //Remove referencing entries
    bool error_flag = false;
    if( !remove( *name._name_form ) ) error_flag = true;
    //Remove Name entry
    std::stringstream ss;
    ss << "DELETE FROM Name WHERE id = " << name._id;
    if( !_db->push( ss.str() ) ) {
        LOG_ERROR( "[relations::io::PersistentDataIO::remove( dto::Name & )] Could not delete Name [", name._id, "]." );
        return false;
    }
    if( !error_flag )
        LOG_DEBUG( "[relations::io::PersistentDataIO::remove( dto::Person & )] Name [", name._id, "]'s record fully removed." );
    return true;
}

/**
 * Deletes a Media
 * @param media Media DTO
 * @return Success
 */
bool relations::io::PersistentDataIO::remove( dto::Media &media ) {
    std::stringstream ss;
    ss << "DELETE FROM Media WHERE id = " << media._id;
    if( !_db->push( ss.str() ) ) {
        LOG_ERROR( "[relations::io::PersistentDataIO::remove( dto::Media & )] Could not delete Media [", media._id, "]." );
        return false;
    }
    LOG_DEBUG( "[relations::io::PersistentDataIO::remove( dto::Media & )] Media [", media._id, "] deleted." );
    return true;
}

/**
 * Deletes an Event
 * @param event Event DTO
 * @return Success
 */
bool relations::io::PersistentDataIO::remove( dto::Event &event ) {
    bool error_flag = false;
    std::stringstream ss1, ss2, ss3;
    eadlib::TableDB   table;
    size_t            row_count { 0 };
    //Remove all EventRole(s) referencing the Event from records
    ss1 << "SELECT person_id, role_type FROM EventRole WHERE event_id = " << event._id;
    if( ( row_count = _db->pull( ss1.str(), table ) ) ) {
        LOG_DEBUG( "[relations::io::PersistentDataIO::remove( dto::Event & )] "
                       "Found ", row_count, " EventRole(s) referencing Event [", event._id, "]." );
        ss2 << "DELETE FROM EventRole WHERE event_id = " << event._id;
        if( !_db->push( ss2.str() ) ) {
            LOG_ERROR( "[relations::io::PersistentDataIO::remove( dto::Event & )] Could not delete EventRoles referencing Event [", event._id, "]." );
            return false;
        }
    }
    //Remove Event from records
    ss3 << "DELETE FROM Event WHERE id = " << event._id;
    if( !_db->push( ss3.str() ) ) {
        LOG_ERROR( "[relations::io::PersistentDataIO::remove( dto::Event & )] Could not delete Event [", event._id, "]." );
        return false;
    }
    //Remove Subject
    if( !removeSubject( event ) ) {
        LOG_ERROR( "[relations::io::PersistentDataIO::remove( dto::Event & )] "
                       "Could not remove Event [", event._id, "]'s Subject entry [", event._subject_id, "]." );
        error_flag = true;
    }
    if( !error_flag )
        LOG_DEBUG( "[relations::io::PersistentDataIO::remove( dto::Event & )] Event [", event._id, "]'s record fully removed." );
    return true;
}

/**
 * Deletes an EventRole
 * @param role EventRole DTO
 * @return Success
 */
bool relations::io::PersistentDataIO::remove( dto::EventRole &role ) {
    std::stringstream ss;
    ss << "DELETE FROM EventRole WHERE "
       << "person_id = " << role._person_id << " AND "
       << "event_id = " << role._event_id << " AND "
       << "role_type = " << eadlib::enum_int<enums::RoleType>( role._type );
    if( !_db->push( ss.str() ) ) {
        LOG_ERROR( "[relations::io::PersistentDataIO::remove( dto::EventRole & )] Could not delete EventRole "
                       "[", role._event_id, ", ", role._person_id, ", ", eadlib::enum_int<enums::RoleType>( role._type ), "]." );
        return false;
    }
    LOG_DEBUG( "[relations::io::PersistentDataIO::remove( dto::EventRole & )] EventRole "
                   "[", role._event_id, ", ", role._person_id, ", ", eadlib::enum_int<enums::RoleType>( role._type ), "] deleted." );
    return true;
}

/**
 * Deletes a Relationship
 * @param relationship Relationship DTO
 * @return Success
 */
bool relations::io::PersistentDataIO::remove( dto::Relationship &relationship ) {
    bool error_flag = false;
    std::stringstream ss;
    ss << "DELETE FROM Relationship WHERE "
       << "person1_id = " << relationship._person1 << " AND "
       << "person2_id = " << relationship._person2 << " AND "
       << "type = " << relationship._type_id;
    if( !_db->push( ss.str() ) ) {
        LOG_ERROR( "[relations::io::PersistentDataIO::remove( dto::Relationship & )] Could not delete Relationship "
                       "[", relationship._person1, ", ", relationship._person2, "]:[", relationship._type_id, "]." );
        return false;
    }
    //Remove Subject
    if( !removeSubject( relationship ) ) {
        LOG_ERROR( "[relations::io::PersistentDataIO::remove( dto::Event & )] "
                       "Could not remove Relationship [", relationship._person1, ", ", relationship._person2, "]'s Subject entry [", relationship._subject_id, "]." );
        error_flag = true;
    }
    if( !error_flag )
        LOG_DEBUG( "[relations::io::PersistentDataIO::remove( dto::Relationship & )] "
                       "Relationship [", relationship._person1, ", ", relationship._person2, "]:[", relationship._type_id, "]'s record fully removed." );
    return true;
}

/**
 * Deletes a RelationshipType
 * @param relationship_type RelationshipType DTO
 * @return Success
 */
bool relations::io::PersistentDataIO::remove( dto::RelationshipType &relationship_type ) {
    if( relationship_type._id < 7 ) { //Preset types guard
        LOG_WARNING( "[relations::io::PersistentDataIO::remove( dto::RelationshipType & )] "
                         "Trying to remove non-removable preset '", relationship_type, "'!" );
        return false;
    }
    std::stringstream ss;
    ss << "DELETE FROM RelationshipType WHERE id = " << relationship_type._id;
    if( !_db->push( ss.str() ) ) {
        LOG_ERROR( "[relations::io::PersistentDataIO::remove( dto::RelationshipType & )] Could not delete RelationshipType [", relationship_type, "]." );
        return false;
    }
    LOG_DEBUG( "[relations::io::PersistentDataIO::remove( dto::RelationshipType & )] RelationshipType [", relationship_type, "] deleted." );
    return true;
}

/**
 * Deletes a MediaRelation
 * @param media_relation MediaRelation DTO
 * @return Success
 */
bool relations::io::PersistentDataIO::remove( dto::MediaRelation &media_relation ) {
    std::stringstream ss;
    ss << "DELETE FROM MediaRelation WHERE "
       << "subject_id = " << media_relation._subject_id << " AND "
       << "media_id = " << media_relation._media_id;
    if( !_db->push( ss.str() ) ) {
        LOG_ERROR( "[relations::io::PersistentDataIO::remove( dto::MediaRelation & )] "
                       "Could not delete MediaRelation [", media_relation._subject_id, ", ", media_relation._media_id, "]." );
        return false;
    }
    LOG_DEBUG( "[relations::io::PersistentDataIO::remove( dto::MediaRelation & )] "
                   "MediaRelation [", media_relation._subject_id, ", ", media_relation._media_id, "] deleted." );
    return true;
}

/**
 * Deletes a CustomEventType
 * @param event_type CustomEventType DTO
 * @return Success
 */
bool relations::io::PersistentDataIO::remove( dto::CustomEventType &event_type ) {
    std::stringstream ss1, ss2;
    size_t            row_count { 0 };
    eadlib::TableDB   table;
    //Check Events don't use this type
    ss1 << "SELECT id FROM Event WHERE custom_event_type = " << event_type._id;
    if( ( row_count = _db->pull( ss1.str(), table ) ) > 0 ) {
        LOG_WARNING( "[relations::io::PersistentDataIO::remove( dto::CustomEventType & )] "
                         "Found ", row_count, " Events referencing EventType '", event_type._value, "' [", event_type._id, "]." );
        return false;
    }
    //Remove CustomEventType from records
    ss2 << "DELETE FROM CustomEventType WHERE id = " << event_type._id;
    if( !_db->push( ss2.str() ) ) {
        LOG_ERROR( "[relations::io::PersistentDataIO::remove( dto::CustomEventType & )] "
                       "Could not delete CustomEventType '", event_type._value, "' [", event_type._id, "]." );
        return false;
    }
    LOG_DEBUG( "[relations::io::PersistentDataIO::remove( dto::CustomEventType & )] "
                   "CustomEventType '", event_type._value, "' [", event_type._id, "] deleted." );
    return true;
}

/**
 * Removes all relationships records for a person
 * @param person Person DTO
 * @return Success
 */
bool relations::io::PersistentDataIO::removeRelationships( relations::dto::Person &person ) {
    //TODO sort out bizarre non working cascade-delete in SQLite
    std::stringstream ss;
    ss << "DELETE FROM Relationship WHERE person1_id = " << person._id << " OR person2_id = " << person._id;
    if( !_db->push( ss.str() ) ) {
        LOG_ERROR( "[relations::io::PersistentDataIO::removeRelationships( dto::Person & )] "
                       "Could not delete Relationship(s) referencing Person [", person._id, "]." );
        return false;
    }
    LOG_DEBUG( "[relations::io::PersistentDataIO::removeRelationships( dto::Person & )] "
                   "Relationship(s) referencing Person [", person._id, "]. deleted." );
    return true;
}

//==================================================================================================================================================//
// [Public] Single record getter methods
//==================================================================================================================================================//
/**
 * Gets a Name form the records
 * @param id Name ID
 * @return Name DTO
 * @throws std::out_of_range when Name is not found in records
 */
std::unique_ptr<relations::dto::Name> relations::io::PersistentDataIO::getName( const int64_t &id ) {
    std::stringstream ss;
    eadlib::TableDB table;
    ss << "SELECT "
       << "Name.id AS \"name_id\", Name.person AS \"person_id\", Name.type AS \"name_type\", Name.preferred, "
       << "NameForm.id AS \"name_form_id\", NameForm.language, NameForm.full_text, "
       << "NamePart.id AS \"name_part_id\", NamePart.type AS \"name_part_type\", NamePart.value "
       << "FROM Name "
       << "LEFT OUTER JOIN NameForm ON Name.name_form = NameForm.id "
       << "LEFT OUTER JOIN NameComposition ON NameForm.id = NameComposition.name_form_id "
       << "LEFT OUTER JOIN NamePart ON NameComposition.name_part_id = NamePart.id "
       << "WHERE Name.id = " << id;
    if( !_db->pull( ss.str(), table ) ) {
        LOG_ERROR( "[relations::io::PersistentDataIO::getName( ", id, " )] Could not get Name from records." );
        throw std::out_of_range( "[relations::io::PersistentDataIO::getName(..)] Could not get Name from records." );
    }

    auto name_id   = table.at( 0, 0 ).getInt();
    auto person_id = table.at( 1, 0 ).getInt();
    auto name_type = enums::to_NameType( table.at( 2, 0 ).getInt() );
    auto preferred = table.at( 3, 0 ).getBool();
    auto form_id   = table.at( 4, 0 ).getInt();
    auto language  = table.at( 5, 0 ).getString();
    auto full_text = table.at( 6, 0 ).getString();
    //Creating Name DTO
    dto::NameForm * form = new dto::NameForm( form_id, language, full_text );
    auto name = std::make_unique<dto::Name>( name_id, person_id, name_type, preferred, form );
    //Adding NamePart(s)
    for( auto r : table ) {
        auto part_id    = r.at( 7 ).getInt();
        auto part_type  = enums::to_NamePartType( r.at( 8 ).getInt() );
        auto part_value = r.at( 9 ).getString();
        switch( part_type ) {
            case enums::NamePartType::PREFIX:
                name->_name_form->_name_parts_prefix.emplace_back( dto::NamePart( part_id, part_value, part_type ) );
                break;
            case enums::NamePartType::FIRST:
                name->_name_form->_name_parts_first.emplace_back( dto::NamePart( part_id, part_value, part_type ) );
                break;
            case enums::NamePartType::MIDDLE:
                name->_name_form->_name_parts_middle.emplace_back( dto::NamePart( part_id, part_value, part_type ) );
                break;
            case enums::NamePartType::SURNAME:
                name->_name_form->_name_parts_surname.emplace_back( dto::NamePart( part_id, part_value, part_type ) );
                break;
            case enums::NamePartType::SUFFIX:
                name->_name_form->_name_parts_suffix.emplace_back( dto::NamePart( part_id, part_value, part_type ) );
                break;
        }
    }
    return name;
}

/**
 * Gets Media from records
 * @param id Media ID
 * @return Media DTO
 * throws std::out_of_range when id is not in records
 * throws eadlib::exception::corruption when type is not translatable as a corresponding enum
 */
std::unique_ptr<relations::dto::Media> relations::io::PersistentDataIO::getMedia( const int64_t &id ) {
    std::stringstream ss;
    eadlib::TableDB table;
    ss << "SELECT * FROM Media WHERE id = " << id;
    if( !_db->pull( ss.str(), table ) ) {
        LOG_ERROR( "[relations::io::PersistentDataIO::getMedia( ", id, " )] No media found in records with this ID." );
        throw std::out_of_range( "[relations::io::PersistentDataIO::getMedia(..)] No media found in records matching the ID." );
    }
    auto media_id    = table.at( 0, 0 ).getInt();
    auto description = table.at( 1, 0 ).getString();
    auto media_type  = table.at( 2, 0 ).getInt();
    auto file        = table.at( 3, 0 ).getString();
    try {
        auto media = std::make_unique<dto::Media>( media_id, description, enums::to_MediaType( media_id ), file );
        return media;
    } catch( std::invalid_argument ) {
        LOG_ERROR( "[relations::io::PersistentDataIO::getMedia( ", id, " )] Could not translate type [", media_type, "] as enum." );
        throw eadlib::exception::corruption( "[relations::io::PersistentDataIO::getMedia(..)] Could not translate type as enum." );
    }
}

/**
 * Gets Event from records
 * @param id Event ID
 * @return Event DTO
 * throws std::out_of_range when id is not in records
 * throws eadlib::exception::corruption when type is not translatable as a corresponding enum
 */
std::unique_ptr<relations::dto::Event> relations::io::PersistentDataIO::getEvent( const int64_t &id ) {
    std::stringstream ss;
    eadlib::TableDB table;
    ss << "SELECT * FROM Event WHERE id = " << id;
    if( !_db->pull( ss.str(), table ) ) {
        LOG_ERROR( "[relations::io::PersistentDataIO::getEvent( ", id, " )] No Event found in records with this ID." );
        throw std::out_of_range( "[relations::io::PersistentDataIO::getEvent(..)] No Event found in records matching the ID." );
    }
    auto event_id    = table.at( 0, 0 ).getInt();
    auto subject     = table.at( 1, 0 ).getInt();
    auto event_type  = table.at( 2, 0 ).getInt();
    auto custom_type = table.at( 3, 0 ).getString() == "NULL" ? 0 : table.at( 3, 0 ).getInt();
    auto date_id     = table.at( 4, 0 ).getInt();
    auto location_id = table.at( 5, 0 ).getInt();
    try {
        auto event = std::make_unique<dto::Event>( event_id,
                                                   subject,
                                                   enums::to_EventType( event_type ),
                                                   custom_type,
                                                   getDate( date_id ).release(),
                                                   getLocation( location_id ).release()
        );
        if( !loadSubject( *event ) ) {
            LOG_ERROR( "[relations::io::PersistentDataIO::getEvent( ", id, " )] "
                "Could not load subject [", subject, "] details into Event [", event_id, "]." );
            throw eadlib::exception::corruption( "[relations::io::PersistentDataIO::getEvent(..)] Could not load subject details into Event." );
        }
        return event;
    } catch( std::out_of_range ) {
        LOG_ERROR( "[relations::io::PersistentDataIO::getEvent( ", id, " )] Bad references in Event [", event_id, "]." );
        throw eadlib::exception::corruption( "[relations::io::PersistentDataIO::getEvent(..)] Bad references in Event." );
    }
}

/**
 * Gets EventRole from records
 * @param person_id EventRole ID
 * @param event_id  Event ID
 * @param type      RoleType
 * @return EventRole DTOs
 * throws std::out_of_range when composite key is not in records
 */
std::unique_ptr<relations::dto::EventRole> relations::io::PersistentDataIO::getEventRole( const int64_t &person_id,
                                                                                          const int64_t &event_id,
                                                                                          const enums::RoleType &type )
{
    std::stringstream ss;
    eadlib::TableDB   table;
    auto type_id = eadlib::enum_int<enums::RoleType>( type );
    ss << "SELECT details FROM EventRole WHERE "
       << "person_id = " << person_id << " AND "
       << "event_id = " << event_id << " AND "
       << "role_type = " << type_id;
    if( !_db->pull( ss.str(), table ) ) {
        LOG_ERROR( "[relations::io::PersistentDataIO::getEventRole( ", person_id, ", ", event_id, ", ", type_id, " )] "
            "No EventRole found in records with given [P,E,T] composite key." );
        throw std::out_of_range( "[relations::io::PersistentDataIO::getEventRole(..)] "
                                     "No EventRole found in records with given [P,E,T] composite key." );
    }
    std::string details = table.at( 0, 0 ).getString();
    auto event_role = std::make_unique<dto::EventRole>( person_id, event_id, type, details );
    return event_role;
}

/**
 * Gets a particular relationship as a DTO
 * @param subject_id Relationship's Subject ID
 * @return Relationship DTO
 * throws std::out_of_range when relationship does not exist
 */
std::unique_ptr<relations::dto::Relationship> relations::io::PersistentDataIO::getRelationship( const int64_t &subject_id ) {
    auto table = eadlib::TableDB();
    std::stringstream ss; //Relationship.subject
    ss << "SELECT Subject.id, Subject.note AS \"note_id\", "
       << "Note.subject AS \"note_subject\", Note.text AS \"note_text\", "
       << "Relationship.person1_id, Relationship.person2_id, Relationship.type "
       << "FROM Subject "
       << "LEFT JOIN Note ON Subject.note = Note.id "
       << "LEFT JOIN Relationship ON Relationship.subject = Subject.id "
       << "WHERE "
       << "Subject.id = " << subject_id;
    if( !_db->pull( ss.str(), table ) ) {
        LOG_DEBUG( "[relations::io::PersistentDataIO::getRelationship( ", subject_id, " )] "
            "No Relationships matching this Subject ID found." );
        throw std::out_of_range( "[relations::io::PersistentDataIO::getRelationship(..)] No Relationships matching this Subject ID found." );
    }
    //Creating DTO
    auto note_id      = table.at( 1, 0 ).getInt();
    auto note_subject = table.at( 2, 0 ).getString();
    auto note_text    = table.at( 3, 0 ).getString();
    auto person1_id   = table.at( 4, 0 ).getInt();
    auto person2_id   = table.at( 5, 0 ).getInt();
    auto type         = table.at( 6, 0 ).getInt();

    auto relationship = std::make_unique<dto::Relationship>( subject_id, person1_id, person2_id, type );
    if( !note_subject.empty() || !note_text.empty() ) {
        relationship->_note.reset( new dto::Note( note_id, note_subject, note_text ) );
    }

    return relationship;
}

//==================================================================================================================================================//
// [Public] Multiple record getter methods
//==================================================================================================================================================//
/**
 * Gets all events
 * @return Events data in a TableDB
 */
std::unique_ptr<eadlib::TableDB> relations::io::PersistentDataIO::getEvents() const {
    std::stringstream ss;
    auto table = std::make_unique<eadlib::TableDB>();
    size_t row_count { 0 };
    ss << "SELECT "
       << "Event.id, Event.event_type, Event.custom_event_type, "
       << "Date.id, Date.formal, Date.original, "
       << "Location.id, Location.description, Location.address, Location.postcode, Location.city, Location.county, Location.country, "
       << "Location.longitude, Location.latitude, "
       << "Event.subject AS \"subject_id\", Subject.note AS \"note_id\", "
       << "Note.subject AS \"note_subject\", Note.text AS \"note_text\" "
       << "FROM Event "
       << "LEFT JOIN Date ON Event.date = Date.id "
       << "LEFT JOIN Location ON Event.location = Location.id "
       << "LEFT JOIN Subject ON Event.subject = Subject.id "
       << "LEFT JOIN Note ON Subject.note = Note.id";
    if( !( row_count = _db->pull( ss.str(), *table ) ) )
        LOG_WARNING( "[relations::io::PersistentDataIO::getEvents()] No Event found in records." );
    else
        LOG_DEBUG( "[relations::io::PersistentDataIO::getEvents()] Found ", row_count, " Events." );
    return table;
}

/**
 * Gets all participants of an Event
 * @param event Event DTO
 * @return EventRole data in a TableDB
 */
std::unique_ptr<eadlib::TableDB> relations::io::PersistentDataIO::getEventRoles( const dto::Event &event ) const {
    std::stringstream ss;
    auto table = std::make_unique<eadlib::TableDB>();
    size_t row_count { 0 };
    ss << "SELECT person_id, role_type, details FROM EventRole WHERE event_id = " << event._id;
    if( !( row_count = _db->pull( ss.str(), *table ) ) )
        LOG_ERROR( "[relations::io::PersistentDataIO::getEventRole( dto::Event & )] No participant found in records for Event [", event._id, "]." );
    else
        LOG_DEBUG( "[relations::io::PersistentDataIO::getEventRole( dto::Event & )] Found ", row_count, " EventRole(s) for Event [", event._id, "]." );
    return table;
}

/**
 * Get all children relationships of a Person from the database
 * @param person Person DTO
 * @return Relationships data in a TableDB
 */
std::unique_ptr<eadlib::TableDB> relations::io::PersistentDataIO::getChildRelationships( const dto::Person &person ) const {
    auto table = std::make_unique<eadlib::TableDB>();
    std::stringstream ss;
    ss << "SELECT "
       << "Relationship.person1_id, Relationship.person2_id, Relationship.type, Relationship.subject AS \"subject_id\", "
       << "Subject.note AS \"note_id\", "
       << "Note.subject AS \"note_subject\", Note.text AS \"note_text\" "
       << "FROM Relationship "
       << "LEFT JOIN Subject ON subject_id = Subject.id "
       << "LEFT JOIN Note ON Subject.note = Note.id "
       << "WHERE Relationship.person1_id = " << person._id << ";";
    if( !_db->pull( ss.str(), *table ) ) {
        LOG_DEBUG( "[relations::io::PersistentDataIO::getChildRelationships(..)] No child relationships found in Person [", person._id, "]'s record." );
    }
    return table;
}

/**
 * Get all parent relationships of a Person from the database
 * @param person Person DTO
 * @return Relationships data in a TableDB
 */
std::unique_ptr<eadlib::TableDB> relations::io::PersistentDataIO::getParentRelationships( const dto::Person &person ) const {
    auto table = std::make_unique<eadlib::TableDB>();
    std::stringstream ss;
    ss << "SELECT "
       << "Relationship.person1_id, Relationship.person2_id, Relationship.type, Relationship.subject AS \"subject_id\", "
       << "Subject.note AS \"note_id\", "
       << "Note.subject AS \"note_subject\", Note.text AS \"note_text\" "
       << "FROM Relationship "
       << "LEFT JOIN Subject ON subject_id = Subject.id "
       << "LEFT JOIN Note ON Subject.note = Note.id "
       << "WHERE Relationship.person2_id = " << person._id << ";";
    if( !_db->pull( ss.str(), *table ) ) {
        LOG_DEBUG( "[relations::io::PersistentDataIO::getParentRelationships(..)] No parent relationships found in Person [", person._id, "]'s record." );
    }
    return table;
}

/**
 * Gets all Media
 * @return Media data as a TableDB
 */
std::unique_ptr<eadlib::TableDB> relations::io::PersistentDataIO::getMedia() const {
    auto table = std::make_unique<eadlib::TableDB>();
    std::stringstream ss;
    ss << "SELECT * FROM Media";
    if( !_db->pull( ss.str(), *table ) ) {
        LOG_DEBUG( "[relations::io::PersistentDataIO::getMedia()] No Media found in records." );
    }
    return table;
}

/**
 * Gets actors in a Media
 * @param media Media DTO
 * @return Actor's Person IDs in a TableDB
 */
std::unique_ptr<eadlib::TableDB> relations::io::PersistentDataIO::getMediaActors( const relations::dto::Media &media ) const {
    auto table = std::make_unique<eadlib::TableDB>();
    std::stringstream ss;
    ss << "SELECT Person.id "
       << "FROM Person "
       << "LEFT JOIN Subject ON Person.subject = Subject.id "
       << "LEFT JOIN MediaRelation ON Subject.id = MediaRelation.subject_id "
       << "WHERE MediaRelation.media_id = " << media._id;
    if( !_db->pull( ss.str(), *table ) ) {
        LOG_DEBUG( "[relations::io::PersistentDataIO::getMediaActors( const dto::Media &)] No actors found for Media [", media._id, "] in records." );
    }
    return table;
}

/**
 * Gets Event linked to a Media record
 * @param media Media DTO
 * @return Event DTO
 * @throws std::out_of_range when no Event is linked to Media
 */
std::unique_ptr<relations::dto::Event> relations::io::PersistentDataIO::getMediaEvent( const relations::dto::Media &media ) const {
    //Get record entry
    eadlib::TableDB   table;
    std::stringstream ss;
    size_t row_count { 0 };
    ss << "SELECT "
       << "Event.id, Event.event_type, Event.custom_event_type, "
       << "Date.id, Date.formal, Date.original, "
       << "Location.id, Location.description, Location.address, Location.postcode, Location.city, Location.county, Location.country, "
       << "Location.longitude, Location.latitude, "
       << "Event.subject AS \"subject_id\", Subject.note AS \"note_id\", "
       << "Note.subject AS \"note_subject\", Note.text AS \"note_text\" "
       << "FROM Event "
       << "LEFT JOIN Date ON Event.date = Date.id "
       << "LEFT JOIN Location ON Event.location = Location.id "
       << "LEFT JOIN Subject ON Event.subject = Subject.id "
       << "LEFT JOIN Note ON Subject.note = Note.id "
       << "LEFT JOIN MediaRelation ON Subject.id = MediaRelation.subject_id "
       << "WHERE MediaRelation.media_id = " << media._id;
    if( !( row_count = _db->pull( ss.str(), table ) ) ) {
        LOG_ERROR( "[relations::io::PersistentDataIO::getMediaEvent()] No Event found in records." );
        throw std::out_of_range( "[relations::io::PersistentDataIO::getMediaEvent()] No Event found in records." );
    }
    LOG_DEBUG( "[relations::io::PersistentDataIO::getMediaEvent()] Found ", row_count, " Event(s). Building Event DTO..." );
    //Build Event DTO
    auto event_id      = table.at( 0, 0 ).getInt();
    auto type          = enums::to_EventType( table.at( 1, 0 ).getInt() );
    auto custom_type   = table.at( 2, 0 ).getInt();
    //Date
    auto date_id       = table.at( 3, 0 ).getInt();
    auto date_formal   = table.at( 4, 0 ).getString();
    auto date_original = table.at( 5, 0 ).getString();
    //Location
    auto location_id   = table.at( 6, 0 ).getInt();
    auto description   = table.at( 7, 0 ).getString();
    auto address       = table.at( 8, 0 ).getString();
    auto postcode      = table.at( 9, 0 ).getString();
    auto city          = table.at( 10, 0 ).getString();
    auto county        = table.at( 11, 0 ).getString();
    auto country       = table.at( 12, 0 ).getString();
    auto longitude     = table.at( 13, 0 ).getString();
    auto latitude      = table.at( 14, 0 ).getString();
    //Subject, Note
    auto subject_id    = table.at( 15, 0 ).getInt();
    auto note_id       = table.at( 16, 0 ).getInt();
    auto note_heading  = table.at( 17, 0 ).getString();
    auto note_text     = table.at( 18, 0 ).getString();
    //Sub DTOs
    auto date     = new dto::Date( date_id, date_formal, date_original );
    auto location = new dto::Location( location_id, description,
                                       address, postcode, city, county, country,
                                       longitude, latitude );
    auto note     = new dto::Note( note_id, note_heading, note_text );
    //Creating Event DTO
    auto event = std::make_unique<relations::dto::Event>( event_id, subject_id, type, custom_type,
                                                          date, location, note
    );
    return event;
}

/**
 * Gets a list of Media related to a Subject
 * @param subject Subject DTO
 * @return List of Media
 */
std::list<relations::dto::Media> relations::io::PersistentDataIO::getMedia( const dto::Subject &subject ) {
    std::stringstream  ss;
    eadlib::TableDB    table;
    std::list<dto::Media> media_list;
    ss << "SELECT "
       << "MediaRelations.media_id, "
       << "Media.description, Media.media_type, Media.file "
       << "FROM MediaRelation "
       << "LEFT JOIN Media ON MediaRelations.media_id = Media.id "
       << "WHERE MediaRelations.subject_id = " << subject._subject_id;
    if( !_db->pull( ss.str(), table ) ) {
        LOG( "relations::io::PersistentDataIO::getMedia( const dto::Subject & )] No Media related to Subject [", subject._subject_id, "] found." );
    } else {
        for( auto r : table ) {
            auto id = r.at( 0 ).getInt();
            auto description = r.at( 1 ).getString();
            auto media_type = r.at( 2 ).getInt();
            auto file = r.at( 3 ).getString();
            try {
                media_list.emplace_back( dto::Media( id, description, enums::to_MediaType( media_type ), file ) );
            } catch( std::invalid_argument ) {
                LOG_ERROR( "[relations::io::PersistentDataIO::getMedia( const dto::Subject & )] "
                               "Could not translate type [", media_type, "] as enum for Media[", id, "]." );
            }
        }
    }
    return media_list;
}

/**
 * Gets a list of media IDs related to a Subject
 * @param subject Subject DTO
 * @return List of Media IDs
 */
std::list<int64_t> relations::io::PersistentDataIO::getMediaIDs( const dto::Subject &subject ) {
    std::stringstream  ss;
    eadlib::TableDB    table;
    std::list<int64_t> media_list;
    ss << "SELECT media_id FROM MediaRelation WHERE subject_id = " << subject._subject_id;
    if( !_db->pull( ss.str(), table ) ) {
        LOG( "relations::io::PersistentDataIO::getMediaIDs( const dto::Subject & )] No Media related to Subject [", subject._subject_id, "] found." );
    } else {
        for( auto r : table ) {
            media_list.emplace_back( r.at( 0 ).getInt() );
        }
    }
    return media_list;
}

/**
 * Gets a list of Subject IDs related to the Media
 * @param media
 * @return
 */
std::list<int64_t> relations::io::PersistentDataIO::getSubjectIDs( const dto::Media &media ) {
    std::stringstream  ss;
    eadlib::TableDB    table;
    std::list<int64_t> subject_list;
    ss << "SELECT subject_id FROM MediaRelation WHERE media_id = " << media._id;
    if( !_db->pull( ss.str(), table ) ) {
        LOG( "relations::io::PersistentDataIO::getSubjectIDs( const dto::Media & )] No Subject related to Media [", media._id, "] found." );
    } else {
        for( auto r : table ) {
            subject_list.emplace_back( r.at( 0 ).getInt() );
        }
    }
    return std::list<int64_t>();
}



//==================================================================================================================================================//
// [Public] Record re-loader methods
//==================================================================================================================================================//
/**
 * Reloads a Person's record into the DTO from the database
 * @param person Person DTO
 * @return Success
 */
bool relations::io::PersistentDataIO::reload( dto::Person &person ) {
    eadlib::TableDB   table;
    std::stringstream ss;
    //Getting the records from the database
    ss << "SELECT "
       << "Person.id, Person.gender_bio, Person.gender_identity "
       << "FROM Person "
       << "WHERE Person.id = " << person._id << ";";
    if( !_db->pull( ss.str(), table ) ) {
        LOG_ERROR( "[relations::io::PersistentDataIO::reload( dto::Person & )] Could not get Person [", person._id, "] records." );
        return false;
    }
    //Reloading the Person
    try {
        person._gender_biological = enums::to_GenderBioType( table.at( 1, 0 ).getInt() );
    } catch( std::invalid_argument ) {
        LOG_FATAL( "[relations::io::PersistentDataIO::reload( dto::Person & )] GenderBioType value from DB is not valid." );
        return false;
    }
    try {
        person._gender_identity = enums::to_GenderIdentityType( table.at( 2, 0 ).getInt() );
    } catch ( std::invalid_argument ) {
        LOG_FATAL( "[relations::io::PersistentDataIO::reload( dto::Person & )] GenderIdentityType value from DB is not valid." );
        return false;
    }
    //Reloading Subject and Names
    if( !this->reloadSubject( person ) ) {
        LOG_ERROR( "[relations::io::PersistentDataIO::reload( dto::Person & )] Could not get Person's [", person._id, "] Subject records." );
        return false;
    }
    if( !this->reloadNames( person ) ) {
        LOG_WARNING( "[relations::io::PersistentDataIO::reload( dto::Person & )] Could not get Person's [", person._id, "] Name records." );
    }
    return true;
}

//==================================================================================================================================================//
// [Private] Record existence state checker methods
//==================================================================================================================================================//
/**
 * Checks RelationshipType value's existence in the records
 * @param relationship_type RelationshipType DTO
 * @return Success (Updates the ID to the one found in the records)
 */
bool relations::io::PersistentDataIO::exists( relations::dto::RelationshipType &relationship_type ) {
    std::stringstream ss1;
    auto result = eadlib::TableDB();
    ss1 << "SELECT id FROM RelationshipType "
        << "WHERE value=\"" << relationship_type._value << "\";";
    if( !_db->pull( ss1.str(), result ) ) {
        return false;
    }
    relationship_type._id = result.at( 0, 0 ).getInt();
    LOG_TRACE( "[relations::io::PersistentDataIO::exists( dto::RelationshipType & )] "
                   "RelationshipType ID for '", relationship_type._value, "' found in DB: ", relationship_type._id );
    return true;
}

/**
 * Checks CustomEventType value's existence in the records
 * @param event_type CustomEventType DTO
 * @return Success (Updates the ID to the one found in the records)
 */
bool relations::io::PersistentDataIO::exists( relations::dto::CustomEventType &event_type ) {
    std::stringstream ss1;
    auto result = eadlib::TableDB();
    ss1 << "SELECT id FROM CustomEventType "
        << "WHERE value=\"" << event_type._value << "\";";
    if( !_db->pull( ss1.str(), result ) ) {
        return false;
    }
    event_type._id = result.at( 0, 0 ).getInt();
    LOG_TRACE( "[relations::io::PersistentDataIO::exists( dto::CustomEventType & )] "
                   "CustomEventType ID for '", event_type._value, "' found in DB: ", event_type._id );
    return true;
}

//==================================================================================================================================================//
// [Private] Record creation methods
//==================================================================================================================================================//
/**
 * Adds a Subject
 * @param subject Subject to add
 * @return Success
 */
bool relations::io::PersistentDataIO::addSubject( dto::Subject &subject ) {
    std::stringstream ss;
    if( subject._note ) {
        add( *subject._note );
        ss << "INSERT INTO Subject( note ) "
           << "VALUES( " << subject._note->_id << " )";
    } else {
        ss << "INSERT INTO Subject( note ) "
           << "VALUES( NULL )";
    }
    if( !_db->push( ss.str() ) ) {
        LOG_ERROR( "[relations::io::PersistentDataIO::addSubject( dto::Subject & )] Could not add subject to database." );
        return false;
    }
    subject._subject_id = _db->getLastInsertedRowIndex();
    LOG_TRACE( "[relations::io::PersistentDataIO::addSubject( dto::Subject & )] Subject ID returned as ", subject._subject_id );
    return true;
}

/**
 * Adds a Note
 * @param note Note to add
 * @return Success
 */
bool relations::io::PersistentDataIO::add( dto::Note &note ) {
    std::stringstream ss;
    ss << "INSERT INTO Note( subject, text ) "
       << "VALUES( \"" << note._subject << "\", \"" << note._text << "\" )";
    if( !_db->push( ss.str() ) ) {
        LOG_ERROR("[relations::io::PersistentDataIO::add( dto::Note & )] Could not add note to database." );
        return false;
    }
    note._id = _db->getLastInsertedRowIndex();
    LOG_TRACE( "[relations::io::PersistentDataIO::add( dto::Note & )] Note ID returned as ", note._id );
    return true;
}

/**
 * Adds a NameForm
 * @param name_form NameForm to add
 * @return Success
 */
bool relations::io::PersistentDataIO::add( dto::NameForm &name_form ) {
    auto total_parts = name_form._name_parts_prefix.size()
                       + name_form._name_parts_first.size()
                       + name_form._name_parts_middle.size()
                       + name_form._name_parts_surname.size()
                       + name_form._name_parts_suffix.size();
    if( total_parts == 0 ) {
        LOG_ERROR( "[relations::io::PersistentDataIO::add( dto::NameForm & )] No NameParts were included in the NameForm!" );
        return false;
    }
    size_t name_parts_added { 0 };
    for( dto::NamePart & part : name_form._name_parts_prefix ) {
        if( add( part ) )
            name_parts_added++;
    }
    for( dto::NamePart & part : name_form._name_parts_first ) {
        if( add( part ) )
            name_parts_added++;
    }
    for( dto::NamePart & part : name_form._name_parts_middle ) {
        if( add( part ) )
            name_parts_added++;
    }
    for( dto::NamePart & part : name_form._name_parts_surname ) {
        if( add( part ) )
            name_parts_added++;
    }
    for( dto::NamePart & part : name_form._name_parts_suffix ) {
        if( add( part ) )
            name_parts_added++;
    }
    if( name_parts_added != total_parts ) {
        LOG_ERROR( "[relations::io::PersistentDataIO::add( dto::NameForm & )] Added ", name_parts_added, "/", total_parts, " NameParts." );
        return false;
    }
    std::stringstream ss;
    ss << "INSERT INTO NameForm( language, full_text ) "
       << "VALUES( \"" << name_form._language << "\", \"" << name_form._full_text << "\" )";
    if( !_db->push( ss.str() ) ) {
        LOG_ERROR( "[relations::io::PersistentDataIO::add( dto::NameForm & )] Could not add name form to database." );
        return false;
    }
    name_form._id = _db->getLastInsertedRowIndex();
    for( auto part : name_form._name_parts_prefix ) {
        if( !add( dto::NameComposition( name_form._id, part._id ) ) ) {
            LOG_ERROR( "[relations::io::PersistentDataIO::add( dto::NameForm & )] Problem adding to prefix to NameComposition." );
            return false;
        }
    }
    for( auto part : name_form._name_parts_first ) {
        if( !add( dto::NameComposition( name_form._id, part._id ) ) ) {
            LOG_ERROR( "[relations::io::PersistentDataIO::add( dto::NameForm & )] Problem adding first name to NameComposition." );
            return false;
        }
    }
    for( auto part : name_form._name_parts_middle ) {
        if( !add( dto::NameComposition( name_form._id, part._id ) ) ) {
            LOG_ERROR( "[relations::io::PersistentDataIO::add( dto::NameForm & )] Problem adding middle name to NameComposition." );
            return false;
        }
    }
    for( auto part : name_form._name_parts_surname ) {
        if( !add( dto::NameComposition( name_form._id, part._id ) ) ) {
            LOG_ERROR( "[relations::io::PersistentDataIO::add( dto::NameForm & )] Problem adding surname to NameComposition." );
            return false;
        }
    }
    for( auto part : name_form._name_parts_suffix ) {
        if( !add( dto::NameComposition( name_form._id, part._id ) ) ) {
            LOG_ERROR( "[relations::io::PersistentDataIO::add( dto::NameForm & )] Problem adding suffix to NameComposition." );
            return false;
        }
    }
    LOG_TRACE( "[relations::io::PersistentDataIO::add( dto::NameForm & )] NameForm ID returned as ", name_form._id );
    return true;
}

/**
 * Adds a NamePart
 * @param name_part NamePart to add
 * @return Success
 */
bool relations::io::PersistentDataIO::add( dto::NamePart &name_part ) {
    std::stringstream ss;
    ss << "INSERT INTO NamePart( value, type ) "
       << "VALUES( "
       << "\"" << name_part._value << "\", "
       << eadlib::enum_int<enums::NamePartType>( name_part._type )
       << " )";
    if( !_db->push( ss.str() ) ) {
        LOG_ERROR( "[relations::io::PersistentDataIO::add( dto::NamePart & )] Could not add name part to database." );
        return false;
    }
    name_part._id = _db->getLastInsertedRowIndex();
    LOG_TRACE( "[relations::io::PersistentDataIO::add( dto::NamePart & )] NamePart ID returned as ", name_part._id );
    return true;
}

/**
 * Adds a name composition key pair
 * @param name_composition NameComposition to add
 * @return Success
 */
bool relations::io::PersistentDataIO::add( const dto::NameComposition &name_composition ) {
    std::stringstream ss;
    ss << "INSERT INTO NameComposition( name_form_id, name_part_id ) "
       << "VALUES( " << name_composition._form_id << ", " << name_composition._part_id << " )";
    if( !_db->push( ss.str() ) ) {
        LOG_ERROR( "[relations::io::PersistentDataIO::add( dto::NameComposition & )] "
                       "Could not add NameComposition key pair {", name_composition._form_id, ", ", name_composition._part_id, "} to database." );
        return false;
    }
    LOG_TRACE( "[relations::io::PersistentDataIO::add( dto::NameComposition & )] "
                   "NameComposition {", name_composition._form_id, ", ", name_composition._part_id, "} successfully added to database." );
    return true;
}

/**
 * Adds a Date to the records
 * @param date Date to add
 * @return Success
 */
bool relations::io::PersistentDataIO::add( relations::dto::Date &date ) {
    std::stringstream ss;
    ss << "INSERT INTO Date( formal, original ) "
       << "VALUES( \"" << date._formal << "\", \"" << date._original << "\" )";
    if( !_db->push( ss.str() ) ) {
        LOG_ERROR("[relations::io::PersistentDataIO::add( dto::Date & )] Could not add Date to database." );
        return false;
    }
    date._id = _db->getLastInsertedRowIndex();
    LOG_TRACE( "[relations::io::PersistentDataIO::add( dto::Date & )] Date ID returned as ", date._id );
    return true;
}

/**
 * Adds a Location to the records
 * @param location Location to add
 * @return Success
 */
bool relations::io::PersistentDataIO::add( relations::dto::Location &location ) {
    std::stringstream ss;
    ss << "INSERT INTO Location( description, address, postcode, city, county, country, longitude, latitude ) "
       << "VALUES( "
       << "\"" << location._description << "\", " //NOT NULL
       << "\"" << location._address << "\", "
       << "\"" << location._postcode << "\", "
       << "\"" << location._city << "\", "
       << "\"" << location._county << "\", "
       << "\"" << location._country << "\", "
       << "\"" << location._longitude << "\", "
       << "\"" << location._latitude << "\""
       << " )";
    if( !_db->push( ss.str() ) ) {
        LOG_ERROR("[relations::io::PersistentDataIO::add( dto::Location & )] Could not add Location to database." );
        return false;
    }
    location._id = _db->getLastInsertedRowIndex();
    LOG_TRACE( "[relations::io::PersistentDataIO::add( dto::Location & )] Location ID returned as ", location._id );
    return true;
}

//==================================================================================================================================================//
// [Private] Record update methods
//==================================================================================================================================================//
/**
 * Updates a Subject in the record
 * @param subject Original Subject
 * @param update  Updated Subject
 * @return Success
 */
bool relations::io::PersistentDataIO::updateSubject( relations::dto::Subject &subject, relations::dto::Subject &update ) {
    enum {
        NO_ACTION,
        NEW_NOTE,
        DELETED_NOTE
    } flag = NO_ACTION;
    //Nested updates
    if( update._note ) {
        if( subject._note ) {
            if( !this->update( *subject._note, *update._note ) ) {
                LOG_ERROR( "[relations::io::PersistentDataIO::update( dto::Person &, dto::Event & )] "
                               "Could not update Subject [", subject._subject_id, "]'s Note." );
                return false;
            }
        } else { //needs new Note
            if( !add( *update._note ) ) {
                LOG_ERROR( "[relations::io::PersistentDataIO::update( dto::Event &, dto::Event & )] "
                               "Could not add Subject [", subject._subject_id, "]'s Note." );
                return false;
            }
            flag = NEW_NOTE;
        }
    } else {
        if( subject._note ) { //Note was deleted.
            if( !remove( *subject._note ) ) {
                LOG_ERROR( "[relations::io::PersistentDataIO::update( dto::Event &, dto::Event & )] "
                               "Could not remove Subject [", subject._subject_id, "]'s Note." );
                return false;
            }
            flag = DELETED_NOTE;
        }
    }
    //Subject updates
    std::stringstream ss;
    switch( flag ) {
        case NEW_NOTE:
            ss << "UPDATE Subject SET note = " << std::to_string( update._note->_id );
            break;
        case DELETED_NOTE:
            ss << "UPDATE Subject SET note = NULL";
            break;
        case NO_ACTION:
            return true;
    }
    if( !_db->push( ss.str() ) ) {
        LOG_ERROR( "[relations::io::PersistentDataIO::updateSubject( dto::Subject &, dto::Subject & )] "
                       "Could not update Subject [", subject._subject_id, "]." );
        return false;
    }
    return true;
}

/**
 * Updates a Note in the record
 * @param note   Original note
 * @param update Updated note
 * @return Success
 */
bool relations::io::PersistentDataIO::update( dto::Note &note, dto::Note &update ) {
    std::list<std::string> updates;
    size_t                 update_count { 0 };
    std::stringstream      ss;
    //Select updates
    if( note._subject != update._subject )
        if( update._subject.empty() )
            updates.emplace_back( "subject = NULL" );
        else
            updates.emplace_back( "subject = \"" + update._subject + "\"" );
    if( note._text != update._text )
        if( update._text.empty() )
            updates.emplace_back( "text = NULL" );
        else
            updates.emplace_back( "text = \"" + update._text + "\"" );

    if( ( update_count = updates.size() ) < 1 ) {
        LOG( "[relations::io::PersistentDataIO::update( dto::Note &, dto::Note & )] Found nothing to update in Note [", note._id, "]." );
        return true;
    }
    //Assemble of the update query
    size_t count { 0 };
    ss << "UPDATE Note SET ";
    for( auto u : updates ) {
        count++;
        ss << u;
        if( count != update_count )
            ss << ", ";
    }
    ss << " WHERE id = " << note._id;
    //Commit query
    if( !_db->push( ss.str() ) ) {
        LOG_ERROR( "[relations::io::PersistentDataIO::update( dto::Note &, dto::Note & )] Could not update Note [", note._id, "]." );
        return false;
    }
    LOG_DEBUG( "[relations::io::PersistentDataIO::update( dto::Note &, dto::Note & )] Note [", note._id, "] updated." );
    return true;
}

/**
 * Updates a NameForm in the records
 * @param name_form Original NameForm
 * @param name_form_update   Updated NameForm
 * @return Success
 */
bool relations::io::PersistentDataIO::update( dto::NameForm &name_form, dto::NameForm &name_form_update ) {
    /**
     * Sorts sets of parts by ID
     * @param parts Vector of NamePart DTOs
     */
    auto sort = [&]( std::vector<dto::NamePart> &parts ) {
        std::sort( parts.begin(),
                   parts.end(),
                   ( []( const dto::NamePart &a, const dto::NamePart &b ) { return a._id < b._id; } )
        );
    };

    /**
     * Updates NamePart lists of same type
     * @param parts        Original NamePart list
     * @param parts_update Updated NamePart list
     * @return Success
     */
    auto update = [&]( std::vector<dto::NamePart> &parts, std::vector<dto::NamePart> &parts_update ) {
        auto cursor = parts.begin();
        for( auto part : parts_update ) {
            if( part._id < 0 ) { //New NamePart detected
                if( !add( part ) ) {
                    LOG_ERROR( "[relations::io::PersistentDataIO::name_form_update( dto::NameForm &, dto::NameForm & )] "
                                   "Could not add NameForm [", name_form._id, "]'s NamePart ('", part._value, "')." );
                    return false;
                }
            } else { //Updated NamePart detected
                cursor = std::find_if( cursor,
                                       parts.end(),
                                       ( [&part]( const dto::NamePart &n ) { return part._id == n._id; } )
                );
                if( cursor == parts.end() ) {
                    LOG_ERROR( "[relations::io::PersistentDataIO::name_form_update( dto::NameForm &, dto::NameForm & )] "
                                   "Reached the end of the list of NameParts but there are still updates pending." );
                    return false;
                }
                if( !this->update( *cursor, part ) ) {
                    LOG_ERROR( "[relations::io::PersistentDataIO::name_form_update( dto::NameForm &, dto::NameForm & )] "
                                   "Could not name_form_update NameForm [", name_form._id, "]'s NamePart [", part._id, "]." );
                    return false;
                }
            }
        }
        return true;
    };

    std::list<std::string> updates;
    size_t                 update_count { 0 };
    std::stringstream      ss;
    //Nested updates
    if( ! update( name_form._name_parts_prefix, name_form_update._name_parts_prefix ) )
        return false;
    if( ! update( name_form._name_parts_first, name_form_update._name_parts_first ) )
        return false;
    if( ! update( name_form._name_parts_middle, name_form_update._name_parts_middle ) )
        return false;
    if( ! update( name_form._name_parts_surname, name_form_update._name_parts_surname ) )
        return false;
    if( ! update( name_form._name_parts_suffix, name_form_update._name_parts_suffix ) )
        return false;
    //Select NameForm updates
    if( name_form._language != name_form_update._language )
        updates.emplace_back( "language = \"" + name_form_update._language + "\"" );
    if( name_form._full_text != name_form_update._full_text )
        updates.emplace_back( "full_text = \"" + name_form_update._full_text + "\"" );
    if( ( update_count = updates.size() ) < 1 ) {
        LOG( "[relations::io::PersistentDataIO::name_form_update( dto::NameForm &, dto::NameForm & )] Found nothing to name_form_update in NameForm [", name_form._id, "]." );
        return true;
    }
    //Assemble the name_form_update query
    size_t count { 0 };
    ss << "UPDATE NameForm SET ";
    for( auto u : updates ) {
        count++;
        ss << u;
        if( count != update_count )
            ss << ", ";
    }
    ss << " WHERE id = " << name_form._id;
    //Commit query
    if( !_db->push( ss.str() ) ) {
        LOG_ERROR( "[relations::io::PersistentDataIO::name_form_update( dto::NameForm &, dto::NameForm & )] Could not name_form_update NameForm [", name_form._id, "]." );
        return false;
    }
    LOG_DEBUG( "[relations::io::PersistentDataIO::name_form_update( dto::NameForm &, dto::NameForm & )] NameForm [", name_form._id, "] updated." );
    return true;
}

/**
 * Updates a NamePart in the records
 * @param name_part Original NamePart
 * @param update    Updated NamePart
 * @return Success
 */
bool relations::io::PersistentDataIO::update( dto::NamePart &name_part, dto::NamePart &update ) {
    std::list<std::string> updates;
    size_t                 update_count { 0 };
    std::stringstream      ss;
    //Select updates
    if( name_part._value != update._value )
        updates.emplace_back( "value = \"" + update._value + "\"" );
    if( name_part._type != update._type )
        updates.emplace_back( "type = " + eadlib::to_string<enums::NamePartType>( update._type ) );

    if( ( update_count = updates.size() ) < 1 ) {
        LOG( "[relations::io::PersistentDataIO::update( dto::NamePart &, dto::NamePart & )] Found nothing to update in NamePart [", name_part._id, "]." );
        return true;
    }
    //Assemble of the update query
    size_t count { 0 };
    ss << "UPDATE NamePart SET ";
    for( auto u : updates ) {
        count++;
        ss << u;
        if( count != update_count )
            ss << ", ";
    }
    ss << " WHERE id = " << name_part._id;
    //Commit query
    if( !_db->push( ss.str() ) ) {
        LOG_ERROR( "[relations::io::PersistentDataIO::update( dto::NamePart &, dto::NamePart & )] Could not update NamePart [", name_part._id, "]." );
        return false;
    }
    LOG_DEBUG( "[relations::io::PersistentDataIO::update( dto::NamePart &, dto::NamePart & )] NamePart [", name_part._id, "] updated." );
    return true;
}

/**
 * Updates a Date in the records
 * @param date   Original Date
 * @param update Updated Date
 * @return Success
 */
bool relations::io::PersistentDataIO::update( relations::dto::Date &date, relations::dto::Date &update ) {
    std::list<std::string> updates;
    size_t                 update_count { 0 };
    std::stringstream      ss;
    //Select updates
    if( date._formal != update._formal )
        updates.emplace_back( "formal = \"" + update._formal + "\"" );
    if( date._original != update._original )
        updates.emplace_back( "original = \"" + update._original + "\"" );

    if( ( update_count = updates.size() ) < 1 ) {
        LOG( "[relations::io::PersistentDataIO::update( dto::Date &, dto::Date & )] Found nothing to update in Date [", date._id, "]." );
        return true;
    }
    //Assemble of the update query
    size_t count { 0 };
    ss << "UPDATE Date SET ";
    for( auto u : updates ) {
        count++;
        ss << u;
        if( count != update_count )
            ss << ", ";
    }
    ss << " WHERE id = " << date._id;
    //Commit query
    if( !_db->push( ss.str() ) ) {
        LOG_ERROR( "[relations::io::PersistentDataIO::update( dto::Date &, dto::Date & )] Could not update Date [", date._id, "]." );
        return false;
    }
    LOG_DEBUG( "[relations::io::PersistentDataIO::update( dto::Date &, dto::Date & )] Date [", date._id, "] updated." );
    return true;
}

bool relations::io::PersistentDataIO::update( relations::dto::Location &location, relations::dto::Location &update ) {
    std::list<std::string> updates;
    size_t                 update_count { 0 };
    std::stringstream      ss;
    //Select updates
    if( location._description != update._description ) {
        if( !update._description.empty() )
            updates.emplace_back( "description = \"" + update._description + "\"" );
        else //is NULL
            LOG_WARNING( "[relations::io::PersistentDataIO::update( dto::Location &, dto::Location & )] Updated description must not be empty." );
    }
    if( location._address != update._address )
        updates.emplace_back( "address = \"" + update._address + "\"" );
    if( location._postcode != update._postcode )
        updates.emplace_back( "postcode = \"" + update._postcode + "\"" );
    if( location._city != update._city )
            updates.emplace_back( "city = \"" + update._city + "\"" );
    if( location._county != update._county )
            updates.emplace_back( "county = \"" + update._county + "\"" );
    if( location._country != update._country )
            updates.emplace_back( "country = \"" + update._country + "\"" );
    if( location._longitude != update._longitude )
            updates.emplace_back( "longitude = \"" + update._longitude + "\"" );
    if( location._latitude != update._latitude )
            updates.emplace_back( "latitude = \"" + update._latitude + "\"" );

    if( ( update_count = updates.size() ) < 1 ) {
        LOG( "[relations::io::PersistentDataIO::update( dto::Location &, dto::Location & )] Found nothing to update in Location [", location._id, "]." );
        return true;
    }
    //Assemble of the update query
    size_t count { 0 };
    ss << "UPDATE Location SET ";
    for( auto u : updates ) {
        count++;
        ss << u;
        if( count != update_count )
            ss << ", ";
    }
    ss << " WHERE id = " << location._id;
    //Commit query
    if( !_db->push( ss.str() ) ) {
        LOG_ERROR( "[relations::io::PersistentDataIO::update( dto::Location &, dto::Location & )] Could not update Location [", location._id, "]." );
        return false;
    }
    LOG_DEBUG( "[relations::io::PersistentDataIO::update( dto::Location &, dto::Location & )] Location [", location._id, "] updated." );
    return true;
}

//==================================================================================================================================================//
// [Private] Record removal methods
//==================================================================================================================================================//
/**
 * Removes a Subject from the records
 * @param subject Subject to remove
 * @return Success
 */
bool relations::io::PersistentDataIO::removeSubject( relations::dto::Subject &subject ) {
    bool error_flag = false;
    if( subject._note && !remove( *subject._note ) ) {
        LOG_ERROR( "[relations::io::PersistentDataIO::removeSubject( dto::Person & )] "
                       "Could not remove Subject [", subject._subject_id, "]'s Note entry [", subject._note->_id, "]." );
        error_flag = true;
    }
    std::stringstream ss;
    ss << "DELETE FROM Subject WHERE id = " << subject._subject_id;
    if( !_db->push( ss.str() ) ) {
        LOG_ERROR( "[relations::io::PersistentDataIO::removeSubject( dto::Subject & )] Could not delete Subject [", subject._subject_id, "]." );
        return false;
    }
    if( !error_flag )
        LOG_DEBUG( "[relations::io::PersistentDataIO::removeSubject( dto::Subject & )] Subject [", subject._subject_id, "]'s record fully removed." );
    return true;
}

/**
 * Removes a Note from the records
 * @param note Note to remove
 * @return Success
 */
bool relations::io::PersistentDataIO::remove( relations::dto::Note &note ) {
    std::stringstream ss;
    ss << "DELETE FROM Note WHERE id = " << note._id;
    if( !_db->push( ss.str() ) ) {
        LOG_ERROR( "[relations::io::PersistentDataIO::remove( dto::Note & )] Could not delete Note [", note._id, "]." );
        return false;
    }
    return true;
}

/**
 * Removes a NameForm and all dependents from the records
 * @param name_form NameForm to remove
 * @return Success
 */
bool relations::io::PersistentDataIO::remove( relations::dto::NameForm &name_form ) {
    bool error_flag = false;
    //Remove dependent NamePart entries
    for( auto part : name_form._name_parts_prefix ) {
        if( !remove( part ) ) error_flag = true;
    }
    for( auto part : name_form._name_parts_first ) {
        if( !remove( part ) ) error_flag = true;
    }
    for( auto part : name_form._name_parts_middle ) {
        if( !remove( part ) ) error_flag = true;
    }
    for( auto part : name_form._name_parts_surname ) {
        if( !remove( part ) ) error_flag = true;
    }
    for( auto part : name_form._name_parts_suffix ) {
        if( !remove( part ) ) error_flag = true;
    }
    //Remove NameForm entry
    std::stringstream ss;
    ss << "DELETE FROM NameForm WHERE id = " << name_form._id;
    if( !_db->push( ss.str() ) ) {
        LOG_ERROR( "[relations::io::PersistentDataIO::remove( dto::NameForm & )] Could not delete NameForm [", name_form._id, "]." );
        return false;
    }

    if( !error_flag )
        LOG_DEBUG( "[relations::io::PersistentDataIO::remove( dto::NameForm & )] NameForm [", name_form._id, "]'s record fully removed." );
    return true;
}

/**
 * Removes a NamePart from the records
 * @param name_part
 * @return
 */
bool relations::io::PersistentDataIO::remove( relations::dto::NamePart &name_part ) {
    //Remove NamePart entry
    std::stringstream ss1;
    ss1 << "DELETE FROM NamePart WHERE id = " << name_part._id;
    if( !_db->push( ss1.str() ) ) {
        LOG_ERROR( "[relations::io::PersistentDataIO::remove( dto::NamePart & )] Could not delete NamePart [", name_part._id, "]." );
        return false;
    }
    //Remove NameComposition entry/entries
    std::stringstream ss2;
    ss2 << "DELETE FROM NameComposition WHERE name_part_id = " << name_part._id;
    if( !_db->push( ss1.str() ) ) {
        LOG_ERROR( "[relations::io::PersistentDataIO::remove( dto::NamePart & )] "
                       "Could not delete NamePart [", name_part._id, "]'s entries in NameComposition." );
        return false;
    }
    return true;
}

//==================================================================================================================================================//
// [Private] DTO getter methods
//==================================================================================================================================================//
/**
 * Gets Date from records
 * @param id Date ID
 * @return Date DTO
 * throws std::out_of_range when id is not in records
 */
std::unique_ptr<relations::dto::Date> relations::io::PersistentDataIO::getDate( const int64_t &id ) {
    std::stringstream ss;
    eadlib::TableDB table;
    ss << "SELECT * FROM Date WHERE id = " << id;
    if( !_db->pull( ss.str(), table ) ) {
        LOG_ERROR( "[relations::io::PersistentDataIO::getDate( ", id, " )] No Date found in records with this ID." );
        throw std::out_of_range( "[relations::io::PersistentDataIO::getDate(..)] No Date found in records matching the ID." );
    }
    auto date_id  =  table.at( 0, 0 ).getInt();
    auto formal   = table.at( 1, 0 ).getString();
    auto original = table.at( 2, 0 ).getString();
    return std::make_unique<dto::Date>( date_id, formal, original );
}

/**
 * Gets Location from records
 * @param id Location ID
 * @return Location DTO
 * throws std::out_of_range when id is not in records
 */
std::unique_ptr<relations::dto::Location> relations::io::PersistentDataIO::getLocation( const int64_t &id ) {
    std::stringstream ss;
    eadlib::TableDB table;
    ss << "SELECT * FROM Location WHERE id = " << id;
    if( !_db->pull( ss.str(), table ) ) {
        LOG_ERROR( "[relations::io::PersistentDataIO::getLocation( ", id, " )] No Location found in records with this ID." );
        throw std::out_of_range( "[relations::io::PersistentDataIO::getLocation(..)] No Location found in records matching the ID." );
    }
    auto location_id = table.at( 0, 0 ).getInt();
    auto description = table.at( 1, 0 ).getString();
    auto address     = table.at( 2, 0 ).getString();
    auto postcode    = table.at( 3, 0 ).getString();
    auto city        = table.at( 4, 0 ).getString();
    auto county      = table.at( 5, 0 ).getString();
    auto country     = table.at( 6, 0 ).getString();
    auto longitude   = table.at( 7, 0 ).getString();
    auto latitude    = table.at( 8, 0 ).getString();
    return std::make_unique<dto::Location>( location_id, description, address, postcode, city, county, country, longitude, latitude );
}

//==================================================================================================================================================//
// [Private] Record load/re-loader methods
//==================================================================================================================================================//
/**
 * Loads the Subject information from the records
 * @param subject Subject DTO to load information into
 * @return Success
 */
bool relations::io::PersistentDataIO::loadSubject( dto::Subject &subject ) {
    eadlib::TableDB   table;
    std::stringstream ss;
    //Getting the records from the database
    ss << "SELECT "
       << "Subject.id AS \"subject_id\", Subject.note AS \"note_id\", "
       << "Note.subject AS \"note_subject\", Note.text AS \"note_text\" "
       << "FROM Subject "
       << "LEFT JOIN Note ON Subject.note = Note.id "
       << "WHERE Subject.id = " << subject._subject_id << ";";
    if( !_db->pull( ss.str(), table ) ) {
        LOG_ERROR( "[relations::io::PersistentDataIO::loadSubject(..)] Could not get Subject  [", subject._subject_id, "]'s record." );
        return false;
    }
    //Loading the Subject's Note
    using eadlib::TableDBCell;
    if( table.at( 1, 0 ).getType() != TableDBCell::DataType::NONE ) {
        int64_t     note_id      = table.at( 1, 0 ).getInt();
        std::string note_subject = table.at( 2, 0 ).getString();
        std::string note_text    = table.at( 3, 0 ).getString();
        if( subject._note ) { //Check and update note content
            if( note_subject != "NULL" && subject._note->_subject != note_subject )
                subject._note->_subject = note_subject;
            if( note_text != "NULL" && subject._note->_text != note_text )
                subject._note->_text = note_text;
        } else {
            if( note_subject != "NULL" || note_text != "NULL" ) {
                LOG_TRACE( "[relations::io::PersistentDataIO::loadSubject(..)] Loading note into Subject [", subject._subject_id, "]" );
                subject._note = std::make_unique<dto::Note>( note_id,
                                                             note_subject == "NULL" ? "" : note_subject,
                                                             note_text == "NULL" ? "" : note_text );
            }
        }
    } else {
        LOG_DEBUG( "[relations::io::PersistentDataIO::loadSubject(..)] No Note for [", subject._subject_id, "] in records." );
    }
    return true;
}

/**
 * Reload a Person's names from the records
 * @param person Person DTO
 * @return Success
 */
bool relations::io::PersistentDataIO::reloadNames( relations::dto::Person &person ) {
    eadlib::TableDB table;
    std::stringstream ss;
    ss << "SELECT "
       << "Name.id AS \"name_id\", Name.type AS \"name_type\", Name.preferred, "
       << "NameForm.id AS \"name_form_id\", NameForm.language, NameForm.full_text, "
       << "NamePart.id AS \"name_part_id\", NamePart.type AS \"name_part_type\", NamePart.value "
       << "FROM Name "
       << "LEFT OUTER JOIN NameForm ON Name.name_form = NameForm.id "
       << "LEFT OUTER JOIN NameComposition ON NameForm.id = NameComposition.name_form_id "
       << "LEFT OUTER JOIN NamePart ON NameComposition.name_part_id = NamePart.id "
       << "WHERE Name.person = " << person._id;
    if( !_db->pull( ss.str(), table ) ) {
        LOG_ERROR( "[relations::io::PersistentDataIO::reloadNames( dto::Person & )] Could not get Person's [", person._id, "] Name records." );
        return false;
    }
    //Reloading Name(s)
    person._names.clear();
    table.sort( 0, []( const eadlib::TableDBCell &a, const eadlib::TableDBCell &b ) { return a < b; } ); //Sort by Name ID
    int64_t current_name_id = -1;
    auto    name_it = person._names.begin();
    for( auto r : table ) {
        auto name_id = r.at( 0 ).getInt();
        auto name_type = enums::to_NameType( r.at( 1 ).getInt() );
        auto preferred = r.at( 2 ).getBool();
        auto form_id = r.at( 3 ).getInt();
        auto language = r.at( 4 ).getString();
        auto full_text = r.at( 5 ).getString();

        if( current_name_id != name_id ) { //Need to create new Name
            auto form = new dto::NameForm( form_id, language, full_text );
            name_it = person._names.insert( name_it,
                                            dto::Name( ( current_name_id = name_id ), person._id, name_type, preferred, form )
            );
        }
        //Adding NamePart(s)
        auto part_id    = r.at( 6 ).getInt();
        auto part_type  = enums::to_NamePartType( r.at( 7 ).getInt() );
        auto part_value = r.at( 8 ).getString();
        switch( part_type ) {
            case enums::NamePartType::PREFIX:
                name_it->_name_form->_name_parts_prefix.emplace_back( dto::NamePart( part_id, part_value, part_type ) );
                break;
            case enums::NamePartType::FIRST:
                name_it->_name_form->_name_parts_first.emplace_back( dto::NamePart( part_id, part_value, part_type ) );
                break;
            case enums::NamePartType::MIDDLE:
                name_it->_name_form->_name_parts_middle.emplace_back( dto::NamePart( part_id, part_value, part_type ) );
                break;
            case enums::NamePartType::SURNAME:
                name_it->_name_form->_name_parts_surname.emplace_back( dto::NamePart( part_id, part_value, part_type ) );
                break;
            case enums::NamePartType::SUFFIX:
                name_it->_name_form->_name_parts_suffix.emplace_back( dto::NamePart( part_id, part_value, part_type ) );
                break;
        }
    }
    return true;
}

/**
 * Reload a Subject's Note from the records
 * @param subject Subject DTO (or child DTO)
 * @return Success
 */
bool relations::io::PersistentDataIO::reloadSubject( relations::dto::Subject &subject ) {
    eadlib::TableDB table;
    std::stringstream ss;
    //Getting the records from the database
    ss << "SELECT "
       << "Subject.id AS \"subject_id\", Subject.note AS \"note_id\", "
       << "Note.subject AS \"note_subject\", Note.text AS \"note_text\" "
       << "FROM Subject "
       << "LEFT JOIN Note ON Subject.note = Note.id "
       << "WHERE Subject.id = " << subject._subject_id << ";";
    if( !_db->pull( ss.str(), table ) ) {
        LOG_ERROR( "[relations::io::PersistentDataIO::reloadSubject( dto::Subject & )] Could not get Person  [", subject._subject_id, "] records." );
        return false;
    }
    //Reloading the Subject's Note
    using eadlib::TableDBCell;
    if( table.at( 1, 0 ).getType() != TableDBCell::DataType::NONE ) {
        int64_t note_id = table.at( 1, 0 ).getInt();
        std::string note_subject = table.at( 2, 0 ).getString();
        std::string note_text = table.at( 3, 0 ).getString();
        if( subject._note ) { //Check and update note content
            if( note_subject != "NULL" && subject._note->_subject != note_subject )
                subject._note->_subject = note_subject;
            if( note_text != "NULL" && subject._note->_text != note_text )
                subject._note->_text = note_text;
        } else {
            if( note_subject != "NULL" || note_text != "NULL" ) {
                subject._note = std::make_unique<dto::Note>( note_id,
                                                             note_subject == "NULL" ? "" : note_subject,
                                                             note_text == "NULL" ? "" : note_text );
            }
        }
    } else {
        LOG_DEBUG( "[relations::io::PersistentDataIO::reload( dto::Subject & )] No Note for [", subject._subject_id, "] in records." );
    }
    //Reloading the Subject
    subject._subject_id = table.at( 0, 0 ).getInt();
    return true;
}

///**
// * Gets EventRole(s) from records
// * @param person_id Person ID
// * @param event_id  Event ID
// * @return List of EventRole DTOs
// * throws std::out_of_range when composite key is not in records
// */
//std::list<relations::dto::EventRole> relations::io::PersistentDataIO::getEventRole( const int64_t &person_id,
//                                                                                    const int64_t &event_id ) {
//    std::stringstream ss;
//    eadlib::TableDB   table;
//    size_t            row_count { 0 };
//    ss << "SELECT role_type, details FROM EventRole WHERE "
//       << "person_id = " << person_id << " AND "
//       << "event_id = " << event_id;
//    if( !( row_count = _db->pull( ss.str(), table ) ) ) {
//        LOG_ERROR( "[relations::io::PersistentDataIO::getEventRole( ", person_id, ", ", event_id, " )] "
//            "No EventRole found in records with given [P,E,*] composite key." );
//        throw std::out_of_range( "[relations::io::PersistentDataIO::getEventRole(..)] "
//                                     "No EventRole found in records with given [P,E,*] composite key." );
//    }
//    LOG_DEBUG( "[relations::io::PersistentDataIO::getEventRole( ", person_id, ", ", event_id, " )] Found ", row_count, " EventRole(s)." );
//    auto list = std::list<dto::EventRole>();
//    for( auto r : table ) {
//        auto type    = enums::to_RoleType( r.at( 0 ).getInt() );
//        auto details = r.at( 1 ).getString();
//        list.emplace_back( dto::EventRole( person_id, event_id, type, details ) );
//    }
//    return list;
//}