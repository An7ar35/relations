#ifndef RELATIONS_DBGRAPHDATAIO_H
#define RELATIONS_DBGRAPHDATAIO_H

#include <eadlib/logger/Logger.h>
#include <list>

#include <src/dto/NameComposition.h>
#include <src/dto/CustomEventType.h>
#include <src/dto/MediaRelation.h>
#include "src/dto/Person.h"
#include "src/dto/Media.h"
#include "src/dto/Event.h"
#include "src/dto/EventRole.h"
#include "src/dto/Relationship.h"
#include "src/dto/RelationshipType.h"
#include "src/dto/MediaRelation.h"
#include "src/storage/DatabaseAccess.h"

namespace relations::io {
    class PersistentDataIO {
      public:
        PersistentDataIO( std::unique_ptr<storage::DatabaseAccess> &db );
        ~PersistentDataIO();
        //Transparency to DatabaseAccess
        bool open( const std::string &file_name );
        bool close();
        bool connected();
        bool createTableStructure();
        bool checkTableStructure();
        std::string fileName() const;
        storage::DatabaseAccess * getDbAccess();
        //Add to records
        bool add( dto::Person &person );
        bool add( dto::Name &name );
        bool add( dto::Media &media );
        bool add( dto::Event &event );
        bool add( dto::EventRole &role );
        bool add( dto::Relationship &relationship );
        bool add( dto::MediaRelation &media_relation );
        bool add( dto::RelationshipType &relationship_type );
        bool add( dto::CustomEventType &event_type );
        bool try_add( dto::RelationshipType &relationship_type );
        bool try_add( dto::CustomEventType &event_type );
        //Update records
        bool update( dto::Person &person, dto::Person &update );
        bool update( dto::Name &name, dto::Name &update );
        bool update( dto::Media &media, dto::Media &update );
        bool update( dto::Event &event, dto::Event &update );
        bool update( dto::EventRole &role,dto::EventRole &update );
        bool update( dto::Relationship &relationship, dto::Relationship &update );
        bool update( dto::RelationshipType &relationship_type, dto::RelationshipType &update );
        bool update( dto::CustomEventType &event_type, dto::CustomEventType &update );
        //Remove from records
        bool remove( dto::Person &person );
        bool remove( dto::Name &name );
        bool remove( dto::Media &media );
        bool remove( dto::Event &event );
        bool remove( dto::EventRole &role );
        bool remove( dto::Relationship &relationship );
        bool remove( dto::RelationshipType &relationship_type );
        bool remove( dto::MediaRelation &media_relation );
        bool remove( dto::CustomEventType &event_type );
        bool removeRelationships( dto::Person &person );
        //Single record getters
        std::unique_ptr<dto::Name>         getName( const int64_t &id );
        std::unique_ptr<dto::Media>        getMedia( const int64_t &id );
        std::unique_ptr<dto::Event>        getEvent( const int64_t &id );
        std::unique_ptr<dto::EventRole>    getEventRole( const int64_t &person_id, const int64_t &event_id, const enums::RoleType &type );
        std::unique_ptr<dto::Relationship> getRelationship( const int64_t & subject_id );
        //Multiple records getters
        std::unique_ptr<eadlib::TableDB>   getEvents() const;
        std::unique_ptr<eadlib::TableDB>   getEventRoles( const dto::Event &event ) const;
        std::unique_ptr<eadlib::TableDB>   getChildRelationships( const dto::Person &person ) const;
        std::unique_ptr<eadlib::TableDB>   getParentRelationships( const dto::Person &person ) const;
        std::unique_ptr<eadlib::TableDB>   getMedia() const;
        std::unique_ptr<eadlib::TableDB>   getMediaActors( const dto::Media &media ) const;
        std::unique_ptr<dto::Event>        getMediaEvent( const dto::Media &media ) const;

        std::list<dto::Media> getMedia( const dto::Subject &subject );
        std::list<int64_t> getMediaIDs( const dto::Subject &subject );
        std::list<int64_t> getSubjectIDs( const dto::Media &media );
        //Reloads records into DTO
        bool reload( dto::Person &person );
        //Check existence state
        bool exists( dto::RelationshipType &relationship_type );
        bool exists( dto::CustomEventType &event_type );

      private:
        //Private variables
        std::unique_ptr<storage::DatabaseAccess> _db;
        //Private Add methods
        bool addSubject( dto::Subject &subject );
        bool add( dto::Note &note );
        bool add( dto::NameForm &name_form );
        bool add( dto::NamePart &name_part );
        bool add( const dto::NameComposition &name_composition );
        bool add( dto::Date &date );
        bool add( dto::Location &location );
        //Private Update methods
        bool updateSubject( dto::Subject &subject, dto::Subject &update );
        bool update( dto::Note &note, dto::Note &update );
        bool update( dto::NameForm &name_form, dto::NameForm &name_form_update );
        bool update( dto::NamePart &name_part, dto::NamePart &update );
        bool update( dto::Date &date, dto::Date &update );
        bool update( dto::Location &location, dto::Location &update );
        //Private Remove methods
        bool removeSubject( dto::Subject& subject );
        bool remove( dto::Note &note );
        bool remove( dto::NameForm &name_form );
        bool remove( dto::NamePart &name_part );
        //Private DTO Getters
        std::unique_ptr<dto::Date> getDate( const int64_t &id );
        std::unique_ptr<dto::Location> getLocation( const int64_t &id );
        //Private Load/Reload methods
        bool loadSubject( dto::Subject &subject );
        bool reloadNames( dto::Person &person );
        bool reloadSubject( dto::Subject &subject );
    };
}

#endif //RELATIONS_DBGRAPHDATAIO_H