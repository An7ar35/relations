#include <src/dto/RelationshipType.h>
#include "DataLoader.h"

/**
 * Loads EventType(s) from the records
 * @param db    DatabaseAccess instance
 * @param types Collection for types to be loaded into
 * @return Success
 */
bool relations::io::DataLoader::loadEventTypes( relations::storage::DatabaseAccess &db,
                                                std::vector<dto::EnumType<enums::EventType>> &types ) {
    eadlib::TableDB   event_type_table, custom_event_type_table;
    std::stringstream ss1, ss2;
    //Getting the records from the database
    ss1 << "SELECT * FROM EventType";
    if( !db.pull( ss1.str(), event_type_table ) ) {
        LOG_ERROR( "[relations::io::DataLoader::loadEventTypes(..)] Could not find any EventType records." );
        return false;
    }
    //Loading preset types
    for( auto r : event_type_table ) {
        try {
            auto id    = r.at( 0 ).getInt();
            auto type  = enums::to_EventType( id );
            auto value = r.at( 1 ).getString();
            types.emplace_back( dto::EnumType<enums::EventType>( id, type, value ) );
        } catch( std::invalid_argument ) {
            LOG_FATAL( "[relations::io::DataLoader::loadEventTypes(..)] ID from preset table could not be translated into known enum type." );
            return false;
        }
    }
    //Getting the records from the database
    ss2 << "SELECT * FROM CustomEventType";
    if( !db.pull( ss2.str(), custom_event_type_table ) ) {
        LOG_DEBUG( "[relations::io::DataLoader::loadEventTypes(..)] Could not find any CustomEventType records." );
    }
    //Loading custom types
    for( auto r : custom_event_type_table ) {
        auto id = r.at( 0 ).getInt();
        auto value = r.at( 1 ).getString();
        types.emplace_back( dto::EnumType<enums::EventType>( id, enums::EventType::OTHER, value ) );
    }
    return true;
}

/**
 * Loads MediaType(s) from the records
 * @param db    DatabaseAccess instance
 * @param types Collection for types to be loaded into
 * @return Success
 */
bool relations::io::DataLoader::loadMediaTypes( relations::storage::DatabaseAccess &db,
                                                std::vector<dto::EnumType<enums::MediaType>> &types ) {
    eadlib::TableDB   table;
    std::stringstream ss;
    //Getting the records from the database
    ss << "SELECT * FROM MediaType";
    if( !db.pull( ss.str(), table ) ) {
        LOG_ERROR( "[relations::io::DataLoader::loadMediaTypes(..)] Could not find any MediaType records." );
        return false;
    }
    //Loading preset types
    for( auto r : table ) {
        try {
        auto id    = r.at( 0 ).getInt();
        auto type  = enums::to_MediaType( id );
        auto value = r.at( 1 ).getString();
        types.emplace_back( dto::EnumType<enums::MediaType>( id, type, value ) );
        } catch( std::invalid_argument ) {
            LOG_FATAL( "[relations::io::DataLoader::loadMediaTypes(..)] ID from preset table could not be translated into known enum type." );
            return false;
        }
    }
    return true;
}

/**
 * Loads RoleType(s) from the records
 * @param db    DatabaseAccess instance
 * @param types Collection for types to be loaded into
 * @return Success
 */
bool relations::io::DataLoader::loadRoleTypes( relations::storage::DatabaseAccess &db,
                                               std::vector<relations::dto::EnumType<relations::enums::RoleType>> &types ) {
    eadlib::TableDB   table;
    std::stringstream ss;
    //Getting the records from the database
    ss << "SELECT * FROM RoleType";
    if( !db.pull( ss.str(), table ) ) {
        LOG_ERROR( "[relations::io::DataLoader::loadRoleTypes(..)] Could not find any RoleType records." );
        return false;
    }
    //Loading preset types
    for( auto r : table ) {
        try {
            auto id    = r.at( 0 ).getInt();
            auto type  = enums::to_RoleType( id );
            auto value = r.at( 1 ).getString();
            types.emplace_back( dto::EnumType<enums::RoleType>( id, type, value ) );
        } catch( std::invalid_argument ) {
            LOG_FATAL( "[relations::io::DataLoader::loadRoleTypes(..)] ID from preset table could not be translated into known enum type." );
            return false;
        }
    }
    return true;
}

/**
 * Loads GenderBioTypes(s) from the records
 * @param db    DatabaseAccess instance
 * @param types Collection for types to be loaded into
 * @return Success
 */
bool relations::io::DataLoader::loadGenderBioTypes( relations::storage::DatabaseAccess &db,
                                                    std::vector<dto::EnumType<enums::GenderBioType>> &types ) {
    eadlib::TableDB   table;
    std::stringstream ss;
    //Getting the records from the database
    ss << "SELECT * FROM GenderBiological";
    if( !db.pull( ss.str(), table ) ) {
        LOG_ERROR( "[relations::io::DataLoader::loadGenderBioTypes(..)] Could not find any GenderBiological records." );
        return false;
    }
    //Loading preset types
    for( auto r : table ) {
        try {
            auto id    = r.at( 0 ).getInt();
            auto type  = enums::to_GenderBioType( id );
            auto value = r.at( 1 ).getString();
            types.emplace_back( dto::EnumType<enums::GenderBioType>( id, type, value ) );
        } catch( std::invalid_argument ) {
            LOG_FATAL( "[relations::io::DataLoader::loadGenderBioTypes(..)] ID from preset table could not be translated into known enum type." );
            return false;
        }
    }
    return true;
}

/**
 * Loads GenderIdentityTypes(s) from the records
 * @param db    DatabaseAccess instance
 * @param types Collection for types to be loaded into
 * @return Success
 */
bool relations::io::DataLoader::loadGenderIdentityTypes( relations::storage::DatabaseAccess &db,
                                                         std::vector<dto::EnumType<enums::GenderIdentityType>> &types ) {
    eadlib::TableDB   table;
    std::stringstream ss;
    //Getting the records from the database
    ss << "SELECT * FROM GenderIdentity";
    if( !db.pull( ss.str(), table ) ) {
        LOG_ERROR( "[relations::io::DataLoader::loadGenderIdentityTypes(..)] Could not find any GenderIdentity records." );
        return false;
    }
    //Loading preset types
    for( auto r : table ) {
        try {
            auto id    = r.at( 0 ).getInt();
            auto type  = enums::to_GenderIdentityType( id );
            auto value = r.at( 1 ).getString();
            types.emplace_back( dto::EnumType<enums::GenderIdentityType>( id, type, value ) );
        } catch( std::invalid_argument ) {
            LOG_FATAL( "[relations::io::DataLoader::loadGenderIdentityTypes(..)] ID from preset table could not be translated into known enum type." );
            return false;
        }
    }
    return true;
}

/**
 * Loads NameType(s) from the records
 * @param db    DatabaseAccess instance
 * @param types Collection for types to be loaded into
 * @return Success
 */
bool relations::io::DataLoader::loadNameTypes( relations::storage::DatabaseAccess &db,
                                               std::vector<dto::EnumType<enums::NameType>> &types ) {
    eadlib::TableDB   table;
    std::stringstream ss;
    //Getting the records from the database
    ss << "SELECT * FROM NameType";
    if( !db.pull( ss.str(), table ) ) {
        LOG_ERROR( "[relations::io::DataLoader::loadNameTypes(..)] Could not find any NameType records." );
        return false;
    }
    //Loading preset types
    for( auto r : table ) {
        try {
            auto id    = r.at( 0 ).getInt();
            auto type  = enums::to_NameType( id );
            auto value = r.at( 1 ).getString();
            types.emplace_back( dto::EnumType<enums::NameType>( id, type, value ) );
        } catch( std::invalid_argument ) {
            LOG_FATAL( "[relations::io::DataLoader::loadNameTypes(..)] ID from preset table could not be translated into known enum type." );
            return false;
        }
    }
    return true;
}

/**
 * Loads NamePartType(s) from the records
 * @param db    DatabaseAccess instance
 * @param types Collection for types to be loaded into
 * @return Success
 */
bool relations::io::DataLoader::loadNamePartTypes( relations::storage::DatabaseAccess &db,
                                                   std::vector<dto::EnumType<enums::NamePartType>> &types ) {
    eadlib::TableDB   table;
    std::stringstream ss;
    //Getting the records from the database
    ss << "SELECT * FROM NamePartType";
    if( !db.pull( ss.str(), table ) ) {
        LOG_ERROR( "[relations::io::DataLoader::loadNamePartTypes(..)] Could not find any NameType records." );
        return false;
    }
    //Loading preset types
    for( auto r : table ) {
        try {
            auto id    = r.at( 0 ).getInt();
            auto type  = enums::to_NamePartType( id );
            auto value = r.at( 1 ).getString();
            types.emplace_back( dto::EnumType<enums::NamePartType>( id, type, value ) );
        } catch( std::invalid_argument ) {
            LOG_FATAL( "[relations::io::DataLoader::loadNamePartTypes(..)] ID from preset table could not be translated into known enum type." );
            return false;
        }
    }
    return true;
}

/**
 * Loads RelationshipType(s) from the records
 * @param db    DatabaseAccess instance
 * @param types Collection for types to be loaded into
 * @return Success
 */
bool relations::io::DataLoader::loadRelationshipTypes( relations::storage::DatabaseAccess &db,
                                                       std::vector<dto::RelationshipType> &types ) {
    eadlib::TableDB   table;
    std::stringstream ss;
    //Getting the records from the database
    ss << "SELECT * FROM RelationshipType";
    if( !db.pull( ss.str(), table ) ) {
        LOG_ERROR( "[relations::io::DataLoader::loadRelationshipTypes(..)] Could not find any RelationshipType records." );
        return false;
    }
    //Loading preset types
    for( auto r : table ) {
        auto id    = r.at( 0 ).getInt();
        auto value = r.at( 1 ).getString();
        types.emplace_back( dto::RelationshipType( id, value ) );
    }
    return true;
}
