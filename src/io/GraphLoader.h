#ifndef RELATIONS_GRAPHLOADER_H
#define RELATIONS_GRAPHLOADER_H

#include <eadlib/logger/Logger.h>
#include <eadlib/tool/Progress.h>
#include <src/dto/CustomEventType.h>
#include <src/dto/EnumType.h>
#include "src/datastructure/DMDGraph.h"
#include "src/dto/Person.h"
#include "src/dto/RelationshipType.h"
#include "src/storage/DatabaseAccess.h"

namespace relations::io {
    class GraphLoader {
      public:
        typedef DMDGraph<int64_t, dto::Person, dto::RelationshipType> RelationsGraph_t;

        bool populateGraphNodes( eadlib::tool::Progress &progress,
                                 storage::DatabaseAccess &db,
                                 RelationsGraph_t &graph );

        bool populateGraphEdge( eadlib::tool::Progress &progress,
                                storage::DatabaseAccess &db,
                                RelationsGraph_t &graph,
                                std::vector<dto::RelationshipType> &types );

      private:
        bool loadNames( storage::DatabaseAccess &db, dto::Person &person );
        bool loadSubject( storage::DatabaseAccess &db, dto::Subject &subject );
    };
}

#endif //RELATIONS_GRAPHLOADER_H
