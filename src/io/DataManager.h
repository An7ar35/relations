#ifndef RELATIONS_GRAPHAPI_H
#define RELATIONS_GRAPHAPI_H

#include <src/datastructure/DMDGraph.h>
#include <src/algorithm/containers/EdgeInfo.h>
#include "src/dto/Person.h"
#include "src/dto/RelationshipType.h"
#include "PersistentDataIO.h"
#include "GraphLoader.h"
#include "DataCache.h"

namespace relations::io {
    class DataManager {
      public:
        //Need to add DMDGraph transparency here as well for transversing the graph
        DataManager();
        ~DataManager();
        //========DatabaseAccess transparency========
        bool open( const std::string &file_name );
        bool close();
        bool connected();
        bool createTableStructure();
        bool checkTableStructure();
        //=============Graph transparency============
        typedef DMDGraph<int64_t, dto::Person, dto::RelationshipType> RelationsGraph_t;
        typedef RelationsGraph_t::const_iterator const_iterator;
        const_iterator cbegin() const;
        const_iterator cend() const;
        const_iterator find( const dto::Person &person ) const;
        const dto::Person & at( const int64_t &id ) const;
        std::list<int64_t> getRelationships( const dto::Person &person ) const;
        std::list<int64_t> getChildList( const int64_t &person_id, const int64_t &relationship_type_id ) const;
        std::list<int64_t> getParentList( const int64_t &person_id, const int64_t &relationship_type_id ) const;
        std::list<int64_t> getChildList( const dto::Person &person, const dto::RelationshipType &relationship_type ) const;
        std::list<int64_t> getParentList( const dto::Person &person, const dto::RelationshipType &relationship_type ) const;
        bool exists( const dto::Person &person ) const;
        bool exists( const dto::Relationship &relationship ) const;
        bool exists( const dto::Person &person, const int64_t &relationship_type_id ) const;
        bool exists( const int64_t &person_id, const int64_t &relationship_type_id ) const;
        bool exists( const dto::Person &person, const int64_t &relationship_type_id, const dto::Person &target_person ) const;
        bool exists( const int64_t &person_id, const int64_t &relationship_type_id, const int64_t &target_person_id ) const;
        size_t getInDegree( const dto::Person &person ) const;
        size_t getInDegree( const dto::Person &person, const dto::RelationshipType &relationship_type ) const;
        size_t getOutDegree( const dto::Person &person ) const;
        size_t getOutDegree( const dto::Person &person, const dto::RelationshipType &relationship_type ) const;
        std::unique_ptr<std::list<EdgeInfo<int64_t, dto::RelationshipType>>> getRelationship( const int64_t &from_id, const int64_t &to_id ) const; //TODO
//        bool isReachable( const K &from, const K &to ) const;
        size_t personCount() const;
        size_t relationshipCount() const;
        bool graphEmpty() const;
        void clearGraph();
        //=============Graph construction============
        bool loadEnumTypeCaches();
        bool constructGraph( eadlib::tool::Progress &progress_tracker );
        //================Data action================
        bool add( dto::Person &person );
        bool add( dto::Name &name );
        bool add( dto::Relationship &relationship );
        bool add( dto::Media &media );
        bool add( dto::Event &event );
        bool add( dto::EventRole &role );
        bool add( dto::MediaRelation &media_relation );
        bool add( dto::RelationshipType &relationshipType );
        bool add( dto::CustomEventType &event_type );

        bool update( dto::Person &person );
        bool update( dto::Name &name );
        bool update( relations::dto::Relationship &relationship );
        bool update( dto::Media &media );
        bool update( dto::EventRole &event_role );
        bool update( dto::Event &event );

        bool remove( dto::Person &person );
        bool remove( dto::Name &name );
        bool remove( dto::Relationship &relationship );
        bool remove( dto::Media &media );
        bool remove( dto::EventRole &event_role );
        bool remove( dto::Event &event );
        bool remove( dto::MediaRelation &media_relation );

        std::unique_ptr<dto::Event>                     getEvent( const int64_t &event_id );

        std::unique_ptr<std::vector<dto::Name>>         getNames( const dto::Person &person ) const;
        std::unique_ptr<std::vector<dto::Relationship>> getChildRelationships( const dto::Person &person ) const;
        std::unique_ptr<std::vector<dto::Relationship>> getParentRelationships( const dto::Person &person ) const;
        std::unique_ptr<std::vector<dto::Event>>        getEvents() const;
        std::unique_ptr<std::vector<dto::EventRole>>    getEventParticipants( const dto::Event &event ) const;
        std::unique_ptr<std::vector<dto::Media>>        getMedia() const;
        std::unique_ptr<std::vector<int64_t>>           getMediaActors( const dto::Media &media ) const;
        std::unique_ptr<dto::Event>                     getMediaEvent( const dto::Media &media ) const;
        //===============Cache access================
        DataCache * getDataCache();
        //===============Helper tools================
        std::string getPreferedName( const dto::Person &person );
        size_t getPreferredNameIndex( const dto::Person &person );

      private:
        std::unique_ptr<PersistentDataIO>        _dataIO;
        std::unique_ptr<RelationsGraph_t>        _graph;
        std::shared_ptr<io::DataCache>           _data_cache;
    };
}

#endif //RELATIONS_GRAPHAPI_H
