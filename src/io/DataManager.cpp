#include <src/algorithm/RelationshipPath.h>
#include "DataManager.h"
#include "DataLoader.h"

/**
 * Constructor
 */
relations::io::DataManager::DataManager() :
    _graph( std::make_unique<RelationsGraph_t>() ),
    _data_cache( std::make_shared<DataCache>() )
{
    auto ptr = std::make_unique<storage::DatabaseAccess>();
    _dataIO = std::make_unique<PersistentDataIO>( ptr );
}

/**
 * Destructor
 */
relations::io::DataManager::~DataManager() {}

//==================================================================================================================================================//
// DatabaseAccess transparency methods
//==================================================================================================================================================//
/**
 * Opens a connection to a database file
 * @param file_name File name
 * @return Success
 */
bool relations::io::DataManager::open( const std::string &file_name ) {
    return _dataIO->open( file_name );
}

/**
 * Closes a current connection to a database file
 * @return Success
 */
bool relations::io::DataManager::close() {
    if( _dataIO->close() ) {
        //_data_cache.reset( nullptr );
        return true;
    }
    return false;
}

/**
 * Gets connection state of database file
 * @return Opened state
 */
bool relations::io::DataManager::connected() {
    return _dataIO->connected();
}

/**
 * Creates the table structures for a new database file
 * @return Success
 */
bool relations::io::DataManager::createTableStructure() {
    return _dataIO->createTableStructure();
}

/**
 * Checks existing table structures in a database file
 * @return Success
 */
bool relations::io::DataManager::checkTableStructure() {
    if( _dataIO->checkTableStructure() ) {
        loadEnumTypeCaches();
        return true;
    }
    return false;
}

//==================================================================================================================================================//
// Graph transparency methods
//==================================================================================================================================================//
/**
 * Const begin iterator
 * @return const iterator
 */
relations::io::DataManager::const_iterator relations::io::DataManager::cbegin() const {
    return _graph->cbegin();
}

/**
 * Const end iterator
 * @return const iterator
 */
relations::io::DataManager::const_iterator relations::io::DataManager::cend() const {
    return _graph->cend();
}

/**
 * Finds a person in the graph
 * @param person Person to find
 * @return Const iterator to the person
 */
relations::io::DataManager::const_iterator relations::io::DataManager::find( const dto::Person &person ) const {
    return _graph->find( person._id );
}

/**
 * Graph accessor (const)
 * @param id Person ID
 * @return Person DTO
 */
const relations::dto::Person & relations::io::DataManager::at( const int64_t &id ) const {
    return _graph->at( id );
}

/**
 * Gets IDs for type of relationship that a Person has
 * @param person Person
 * @return List of relationship type IDs
 */
std::list<int64_t> relations::io::DataManager::getRelationships( const dto::Person &person ) const {
    try {
        auto relationships_id = std::list<int64_t>();
        auto relationships = _graph->getRelationships( person._id );
        for( auto r : relationships ) {
            relationships_id.emplace_back( r.first._id );
        }
        return relationships_id;
    } catch( std::out_of_range ) {
        LOG_ERROR( "[relations::io::DataManager::getRelationships( ", person._id, " )] Person ID not in graph." );
        return std::list<int64_t>();
    }
}

/**
 * Gets a list of Person IDs connected from a Person through a relationship
 * @param person_id         Person's ID
 * @param relationship_type RelationshipType
 * @return List of child IDs
 */
std::list<int64_t> relations::io::DataManager::getChildList( const int64_t &person_id,
                                                             const int64_t &relationship_type_id ) const {
    try {
        auto type = std::find_if( _data_cache->_relationship_types.begin(),
                                  _data_cache->_relationship_types.end(),
                                  ( [&]( const dto::RelationshipType &type ) { return type._id == relationship_type_id; } )
        );
        if( type == _data_cache->_relationship_types.end() ) {
            LOG_ERROR( "[relations::io::DataManager::getChildList( ", person_id, ", ", relationship_type_id, " )] "
                "Could not find RelationshipType [", relationship_type_id, "] in cache." );
            return std::list<int64_t>();
        }
        return _graph->getChildList( person_id, *type );
    } catch( std::out_of_range ) {
        LOG_WARNING( "[relations::io::DataManager::getChildList( ", person_id, ", ", relationship_type_id, " )] Person/Relationship Type not in graph." );
        return std::list<int64_t>();
    }
}

/**
 * Gets a list of Person IDs connected to a Person through a relationship
 * @param person_id         Person's ID
 * @param relationship_type RelationshipType
 * @return List of parent IDs
 */
std::list<int64_t> relations::io::DataManager::getParentList( const int64_t &person_id,
                                                              const int64_t &relationship_type_id ) const {
    try {
        auto type = std::find_if( _data_cache->_relationship_types.begin(),
                                  _data_cache->_relationship_types.end(),
                                  ( [&]( const dto::RelationshipType &type ) { return type._id == relationship_type_id; } )
        );
        if( type == _data_cache->_relationship_types.end() ) {
            LOG_ERROR( "[relations::io::DataManager::getParentList( ", person_id, ", ", relationship_type_id, " )] "
                "Could not find RelationshipType [", relationship_type_id, "] in cache." );
            return std::list<int64_t>();
        }
        return _graph->getParentList( person_id, *type );
    } catch( std::out_of_range ) {
        LOG_WARNING( "[relations::io::DataManager::getParentList( ", person_id, ", ", relationship_type_id, " )] Person/Relationship Type not in graph." );
        return std::list<int64_t>();
    }
}

/**
 * Gets a list of Person IDs connected from a Person through a relationship
 * @param person            Person DTO
 * @param relationship_type RelationshipType DTO
 * @return List of child IDs
 */
std::list<int64_t> relations::io::DataManager::getChildList( const dto::Person &person,
                                                             const dto::RelationshipType &relationship_type ) const {
    return getChildList( person._id, relationship_type._id );
}

/**
 * Gets a list of Person IDs connected to a Person through a relationship
 * @param person            Person DTO
 * @param relationship_type Relationship DTO
 * @return List of parent IDs
 */
std::list<int64_t> relations::io::DataManager::getParentList( const dto::Person &person,
                                                              const dto::RelationshipType &relationship_type ) const {
    return getParentList( person._id, relationship_type._id );
}

/**
 * Checks if Person exists in the graph
 * @param person Person
 * @return Existence state
 */
bool relations::io::DataManager::exists( const dto::Person &person ) const {
    return _graph->exists( person._id );
}

/**
 * Checks if Relationship exists in the graph
 * @param relationship Relationship
 * @return Existence state
 */
bool relations::io::DataManager::exists( const dto::Relationship &relationship ) const {
    auto cached_type = std::find_if( _data_cache->_relationship_types.begin(),
                                     _data_cache->_relationship_types.end(),
                                     ( [&]( const dto::RelationshipType &type ) { return type._id == relationship._type_id; } )
    );
    if( cached_type == _data_cache->_relationship_types.end() ) {
        LOG_ERROR( "[relations::io::DataManager::exists( const dto::Relationship & )] "
            "Could not find RelationshipType [", relationship._type_id, "] in cache." );
        return false;
    }
    return _graph->exists( relationship._person1, relationship._person2, *cached_type );
}

/**
 * Checks if Person has a RelationshipType
 * @param person            Person DTO
 * @param relationship_type RelationshipType DTO
 * @return Existence state
 */
bool relations::io::DataManager::exists( const relations::dto::Person &person,
                                         const int64_t &relationship_type_id ) const {
    return exists( person._id, relationship_type_id );
}

/**
 * Checks if Person has a RelationshipType
 * @param person_id         Person ID
 * @param relationship_type RelationshipType DTO
 * @return Existence state
 */
bool relations::io::DataManager::exists( const int64_t &person_id,
                                         const int64_t &relationship_type_id ) const
{
    auto cached_type = std::find_if( _data_cache->_relationship_types.begin(),
                                     _data_cache->_relationship_types.end(),
                                     ( [&]( const dto::RelationshipType &type ) { return type._id == relationship_type_id; } )
    );
    if( cached_type == _data_cache->_relationship_types.end() ) {
        LOG_ERROR( "[relations::io::DataManager::exists( ", person_id, ", ", relationship_type_id, ")] "
                       "Could not find RelationshipType [", relationship_type_id, "] in cache." );
        return false;
    }
    auto relationships = _graph->getRelationships( person_id );
    return relationships.find( *cached_type ) != relationships.end();
}

/**
 * Checks of Person has a RelationshipType with another Person
 * @param person            Origin Person DTO
 * @param relationship_type RelationshipType DTO
 * @param target_person     Target Person DTO
 * @return Existence state
 */
bool relations::io::DataManager::exists( const relations::dto::Person &person,
                                         const int64_t &relationship_type_id,
                                         const relations::dto::Person &target_person ) const
{
    return exists( person._id, relationship_type_id, target_person._id );
}

/**
 * Checks of Person has a RelationshipType with another Person
 * @param person_id         Origin Person ID
 * @param relationship_type RelationshipType DTO
 * @param target_person_id  Target Person ID
 * @return Existence state
 */
bool relations::io::DataManager::exists( const int64_t &person_id,
                                         const int64_t &relationship_type_id,
                                         const int64_t &target_person_id ) const
{
    auto relationships = _graph->getRelationships( person_id );
    auto cached_type  = std::find_if( _data_cache->_relationship_types.begin(),
                                      _data_cache->_relationship_types.end(),
                                      ( [&]( const dto::RelationshipType &type ) { return type._id == relationship_type_id; } )
    );
    if( cached_type == _data_cache->_relationship_types.end() ) {
        LOG_ERROR( "[relations::io::DataManager::exists( ", person_id, ", ", relationship_type_id, ", ", target_person_id, " )] "
            "Could not find RelationshipType [", relationship_type_id, "] in cache." );
        return false;
    }
    auto relationship = relationships.find( *cached_type );
    if( relationship != relationships.end() ) {
        auto target = std::find( relationship->second.childList.begin(),
                                 relationship->second.childList.end(),
                                 target_person_id
        );
        return target != relationship->second.childList.end();
    }
    return false;
}

/**
 * Gets the number of incoming relationships to a person
 * @param person Person DTO
 * @return In-Degree
 * @throws std::out_of_range when key does not exist in graph
 */
size_t relations::io::DataManager::getInDegree( const dto::Person &person ) const
{
    try {
        return _graph->getInDegree( person._id );
    } catch( std::out_of_range ) {
        LOG_ERROR( "[relations::io::DataManager::getInDegree( ", person._id, " )] Person not found in graph." );
        throw std::out_of_range( "[relations::io::DataManager::getInDegree(..)] Person not found in graph." );
    }
}

/**
 * Gets the number of incoming relationships of a type from a person
 * @param person            Person
 * @param relationship_type RelationshipType
 * @return In-Degree
 * @throws std::out_of_range when key does not exist in graph
 */
size_t relations::io::DataManager::getInDegree( const relations::dto::Person &person,
                                                const relations::dto::RelationshipType &relationship_type ) const
{
    try {
        return _graph->getInDegree( person._id, relationship_type );
    } catch( std::out_of_range ) {
        LOG_ERROR( "[relations::io::DataManager::getInDegree( ", person._id, ", ", relationship_type._value, " )] Person not found in graph." );
        throw std::out_of_range( "[relations::io::DataManager::getInDegree(..)] Person not found in graph." );
    }
}

/**
 * Gets the number of outgoing relationships from a person
 * @param person Person
 * @return Out-Degree
 * @throws std::out_of_range when key does not exist in graph
 */
size_t relations::io::DataManager::getOutDegree( const dto::Person &person ) const {
    try{
       return _graph->getOutDegree( person._id );
    } catch( std::out_of_range ) {
        LOG_ERROR( "[relations::io::DataManager::getOutDegree( ", person._id, " )] Person not found in graph." );
        throw std::out_of_range( "[relations::io::DataManager::getOutDegree(..)] Person not found in graph." );
    }
}

/**
 * Gets the number of outgoing relationships of a type from a person
 * @param person            Person
 * @param relationship_type RelationshipType
 * @return Out-Degree
 * @throws std::out_of_range when key does not exist in graph
 */
size_t relations::io::DataManager::getOutDegree( const relations::dto::Person &person,
                                                 const relations::dto::RelationshipType &relationship_type ) const
{
    try {
        return _graph->getOutDegree( person._id, relationship_type );
    } catch( std::out_of_range ) {
        LOG_ERROR( "[relations::io::DataManager::getOutDegree( ", person._id, ", ", relationship_type._value, " )] Person not found in graph." );
        throw std::out_of_range( "[relations::io::DataManager::getOutDegree(..)] Person not found in graph." );
    }
}

/**
 * Gets the relationship path between two persons in the graph
 * @param from_id Origin person ID
 * @param to_id   Target person ID
 * @return List of EdgeInfo describing the path
 */
std::unique_ptr<std::list<relations::EdgeInfo<int64_t, relations::dto::RelationshipType>>>
relations::io::DataManager::getRelationship( const int64_t &from_id,
                                             const int64_t &to_id ) const
{
    LOG( "@io::DataManager::getRelationship( ", from_id, ", ", to_id, " )" );
    auto path_finder = relations::algo::RelationshipPath<int64_t,dto::Person,dto::RelationshipType>();
    auto success = path_finder.init( *_graph, from_id, to_id );
    LOG( "[relations::io::DataManager::getRelationship( ", from_id, ", ", to_id, " )] "
        "Visited ", path_finder.getVisitCount(), " nodes." );
    return std::move( path_finder.getPath( *_graph ) );
}

/**
 * Gets the number of persons in memory
 * @return Number of nodes in DMDGraph
 */
size_t relations::io::DataManager::personCount() const {
    return _graph->nodeCount();
}

/**
 * Gets the number of relationships in the graph
 * @return Number of edges present in the graph
 */
size_t relations::io::DataManager::relationshipCount() const {
    return _graph->size();
}

/**
 * Gets the empty state of the graph
 * @return Empty state
 */
bool relations::io::DataManager::graphEmpty() const {
    return _graph->empty();
}

/**
 * Clears the content from the graph
 */
void relations::io::DataManager::clearGraph() {
    _graph.reset( new RelationsGraph_t() );
}

//==================================================================================================================================================//
// Graph construction methods
//==================================================================================================================================================//
/**
 * Loads all the Type lists caches
 * @param progress_tracker Progress tracker
 * @return Success
 */
bool relations::io::DataManager::loadEnumTypeCaches() {
    auto db = _dataIO->getDbAccess();
    if( _data_cache ) {
        _data_cache.reset( new DataCache );
    }
    auto data_loader = io::DataLoader();
    auto error_flag  = false;
    if( !data_loader.loadEventTypes( *db, _data_cache->_event_types ) )
        error_flag = true;
    if( !data_loader.loadMediaTypes( *db, _data_cache->_media_types ) )
        error_flag = true;
    if( !data_loader.loadRoleTypes( *db, _data_cache->_role_types ) )
        error_flag = true;
    if( !data_loader.loadGenderBioTypes( *db, _data_cache->_gender_bio_types ) )
        error_flag = true;
    if( !data_loader.loadGenderIdentityTypes( *db, _data_cache->_gender_identity_types ) )
        error_flag = true;
    if( !data_loader.loadNameTypes( *db, _data_cache->_name_types ) )
        error_flag = true;
    if( !data_loader.loadNamePartTypes( *db, _data_cache->_name_part_types ) )
        error_flag = true;
    if( !data_loader.loadRelationshipTypes( *db, _data_cache->_relationship_types ) )
        error_flag = true;
    if( error_flag ) {
        LOG_FATAL( "[relations::io::DataManager::loadEnumTypeCaches(..)] Problem encountered during cache loading." );
        return false;
    } else {
        LOG( "[relations::io::DataManager::loadEnumTypeCaches(..)] All Type caches loaded successfully." );
        return true;
    }
}

/**
 * Constructs the Directed Multi-Dimensional relationship graph
 * @param progress_tracker Progress tracker
 * @return Success
 */
bool relations::io::DataManager::constructGraph( eadlib::tool::Progress &progress_tracker ) {
    auto db = _dataIO->getDbAccess();
    auto constructor = io::GraphLoader();
    if( _graph ) {
        _graph = std::make_unique<RelationsGraph_t>();
    }
    if( constructor.populateGraphNodes( progress_tracker, *db, *_graph.get() )
        && constructor.populateGraphEdge( progress_tracker, *db, *_graph.get(), _data_cache->_relationship_types ) ) {
        LOG( "[relations::io::DataManager::constructGraph(..)] ", _graph->nodeCount(), " nodes and ", _graph->size(), " edges added to graph." );
        return true;
    } else {
        LOG_FATAL( "[relations::io::DataManager::constructGraph(..)] Problem encountered during graph construction." );
        return false;
    }
}

//==================================================================================================================================================//
// Data action methods
//==================================================================================================================================================//
/**
 * Adds a person
 * @param person Person DTO
 * @return Success
 */
bool relations::io::DataManager::add( dto::Person &person ) {
    if( _dataIO->add( person ) ) {
        return _graph->addNode( person._id, person );
    }
    return false;
}

/**
 * Adds a Name
 * @param name Name DTO
 * @return Success
 */
bool relations::io::DataManager::add( dto::Name &name ) {
    if( name._person_id < 1 ) {
        LOG_ERROR( "[relations::io::DataManager::add( dto::Name & )] Failed adding Name. Name has no Person ID." );
        return false;
    }
    if( !_dataIO->add( name ) ) {
        LOG_ERROR( "[relations::io::DataManager::add( dto::Name & )] Failed adding Name for Person [", name._person_id, "]." );
        return false;
    }
    auto person_it = _graph->find( name._person_id );
    if( person_it == _graph->end() ) {
        LOG_ERROR( "[relations::io::DataManager::add( dto::Name & )] "
                       "Could not find Person [", name._person_id, "] referenced in name [", name._id, "]." );
        return false;
    }
    person_it->second.value._names.emplace_back( name );
    return true;
}

/**
 * Adds a new relationship
 * @param relationship Relationship DTO
 * @return Success
 */
bool relations::io::DataManager::add( dto::Relationship &relationship ) {
    auto cached_type = std::find_if( _data_cache->_relationship_types.begin(),
                                     _data_cache->_relationship_types.end(),
                                     ( [&]( const dto::RelationshipType &type ) { return type._id == relationship._type_id; } )
    );
    if( cached_type == _data_cache->_relationship_types.end() ) {
        LOG_ERROR( "[relations::io::DataManager::add( dto::Relationship &relationship )] "
                       "Could not find RelationshipType [", relationship._type_id, "] in cache." );
        return false;
    }
    if( _dataIO->try_add( *cached_type ) && _dataIO->add( relationship ) ) {
        return _graph->createDirectedEdge( relationship._person1, relationship._person2, *cached_type );
    }
    return false;
}

/**
 * Adds a new Media
 * @param media Media DTO
 * @return Success
 */
bool relations::io::DataManager::add( dto::Media &media ) {
    return _dataIO->add( media );
}

/**
 * Adds a new Event
 * @param event Event DTO
 * @return Success
 */
bool relations::io::DataManager::add( dto::Event &event ) {
    return _dataIO->add( event );
}

/**
 * Adds a new EventRole
 * @param role EventRole DTO
 * @return Success
 */
bool relations::io::DataManager::add( dto::EventRole &role ) {
    return _dataIO->add( role );
}

/**
 * Adds a new MediaRelation
 * @param media_relation MediaRelation DTO
 * @return Success
 */
bool relations::io::DataManager::add( dto::MediaRelation &media_relation ) {
    return _dataIO->add( media_relation );
}

/**
 * Adds a new RelationshipType
 * @param relationshipType RelationshipType DTO
 * @return Success
 */
bool relations::io::DataManager::add( dto::RelationshipType &relationshipType ) {
    return _dataIO->add( relationshipType );
}

/**
 * Adds a new CustomEventType
 * @param event_type CustomEventType DTO
 * @return Success
 */
bool relations::io::DataManager::add( dto::CustomEventType &event_type ) {
    auto cached_type = std::find_if( _data_cache->_event_types.begin(),
                                     _data_cache->_event_types.end(),
                                     ( [&event_type]( const dto::EnumType<enums::EventType> &type ) { return event_type._value == type._value; } )
    );
    if( cached_type != _data_cache->_event_types.end() ) {
        LOG_WARNING( "[relations::io::DataManager::add( dto::CustomEventType & )] "
                         "EventType '", event_type._value, "' already exists in cache [", cached_type->_id, "]." );
        return false;
    }
    if( !_dataIO->try_add( event_type ) ) {
        LOG_ERROR( "[relations::io::DataManager::add( dto::CustomEventType & )] "
                       "Couldn't add '", event_type._value, "' with ID [", event_type._id, "] to records." );
        return false;
    }
    auto type = enums::to_EventType( event_type._id );
    _data_cache->_event_types.emplace_back( dto::EnumType<enums::EventType>( event_type._id, type, event_type._value ) );
    return true;
}

/**
 * Update a person (minus the Names)
 * @param person Updated Person DTO
 * @return Success
 */
bool relations::io::DataManager::update( relations::dto::Person &person ) {
    try {
        dto::Person & original = _graph->at( person._id );
        //Update Person
        if( !_dataIO->update( original, person ) ) {
            LOG_ERROR( "[relations::io::DataManager::update( relations::dto::Person & )] Problems encountered updating Person [", person._id, "]." );
            return true;
        }
        return true;
    } catch( std::out_of_range ) {
        LOG_ERROR( "[relations::io::DataManager::update( relations::dto::Person & )] Person ID [", person._id, "] is not in Graph." );
    }
    return false;
}

/**
 * Updates a Name
 * @param name Updated Name DTO
 * @return Success
 */
bool relations::io::DataManager::update( relations::dto::Name &name ) {
    //Database update
    try {
        auto original = _dataIO->getName( name._id );
        if( !_dataIO->update( *original, name ) ) {
            LOG_ERROR( "[relations::io::DataManager::update( relations::dto::Name & )] Could not update Name [", name._id, "]." );
            return false;
        }
    } catch( std::out_of_range ) {
        LOG_ERROR( "[relations::io::DataManager::update( relations::dto::Name & )] Name ID [", name._id, "] cannot be found in database." );
        return false;
    }
    //Graph update
    try {
        dto::Person & person = _graph->at( name._person_id );
        auto name_it = std::find_if( person._names.begin(),
                                     person._names.end(),
                                     ( [&]( const dto::Name &n ) { return n._id == name._id; } )
        );
        if( name_it == person._names.end() ) {
            LOG_ERROR( "[relations::io::DataManager::update( relations::dto::Name & )] "
                           "Name [", name._id, "] not found in Person [", name._person_id, "] in graph." );
            return false;
        }
        *name_it = name; //Update graph name
        return true;
    } catch( std::out_of_range ) {
        LOG_ERROR( "[relations::io::DataManager::update( relations::dto::Name & )] "
                       "Person ID [", name._person_id, "] in Name [", name._id, "] not found in graph." );
        return false;
    }
}

/**
 * Updates a relationship
 * @param relationship Updated Relationship DTO
 * @return Success
 */
bool relations::io::DataManager::update( relations::dto::Relationship &relationship ) {
    try {
        //Getting the original Relationship DTO
        auto original = _dataIO->getRelationship( relationship._subject_id );
        if( !_dataIO->update( *original, relationship ) ) {
            LOG_ERROR( "[relations::io::DataManager::update( relations::dto::Relationship & )] Could not update relationship." );
            return false;
        }
        //Getting the type DTOs from cache
        auto original_type = std::find_if( _data_cache->_relationship_types.begin(),
                                           _data_cache->_relationship_types.end(),
                                           ( [&]( const dto::RelationshipType &type ) { return original->_type_id == type._id; } )
        );
        if( original_type == _data_cache->_relationship_types.end() ) {
            LOG_ERROR( "[relations::io::DataManager::update( relations::dto::Relationship & )] "
                           "Original Relationship's Type ID [", original->_type_id, "] not found in cache!" );
            return false;
        }
        auto updated_type = std::find_if( _data_cache->_relationship_types.begin(),
                                          _data_cache->_relationship_types.end(),
                                          ( [&]( const dto::RelationshipType &type ) { return relationship._type_id == type._id; } )
        );
        if( updated_type == _data_cache->_relationship_types.end() ) {
            LOG_ERROR( "[relations::io::DataManager::update( relations::dto::Relationship & )] "
                           "Updated Relationship's Type ID [", original->_type_id, "] not found in cache!" );
            return false;
        }
        //Update to the graph edge
        if( !_graph->removeDirectedEdge( original->_person1, original->_person2, *original_type ) ) {
            LOG_ERROR( "[relations::io::DataManager::update( relations::dto::Relationship & )] "
                           "Failed to remove relationship [", original->_person1, ", ", original->_person2, "]:[", original->_type_id, "] from Graph." );
            return false;
        }
        if( !_graph->createDirectedEdge( relationship._person1, relationship._person2, *updated_type ) ) {
            LOG_ERROR( "[relations::io::DataManager::update( relations::dto::Relationship & )] Failed to create relationship "
                           "[", relationship._person1, ", ", relationship._person2, "]:[", relationship._type_id, "] from Graph." );
            if( !_graph->createDirectedEdge( original->_person1, original->_person2, *original_type ) ) {
                LOG_ERROR( "[relations::io::DataManager::update( relations::dto::Relationship & )] Failed to roll back removal of relationship "
                               "[", original->_person1, ", ", original->_person2, "]:[", original->_type_id, "] from Graph." );
            }
            return false;
        }
        return true;
    } catch( std::out_of_range ) {
        LOG_ERROR( "[relations::io::DataManager::update( relations::dto::Relationship & )] "
                       "Person ID [", relationship._person1, "] or [", relationship._person2, "] is not in Graph." );
        return false;
    }
}

/**
 * Updates a Media
 * @param media Media DTO
 * @return Success
 */
bool relations::io::DataManager::update( relations::dto::Media &media ) {
    try {
        auto original = _dataIO->getMedia( media._id );
        if( !_dataIO->update( *original, media ) ) {
            LOG_ERROR( "[relations::io::DataManager::update( relations::dto::Media & )] "
                           "Could not update Media [", media._id, "]." );
            return false;
        }
        return true;
    } catch( std::out_of_range ) {
        LOG_ERROR( "[relations::io::DataManager::update( relations::dto::Media & )] "
                       "Media [", media._id, "] not in records." );
        return false;
    }
}

/**
 * Updates an EventRole
 * @param event_role EventRole DTO
 * @return Success
 */
bool relations::io::DataManager::update( relations::dto::EventRole &event_role ) {
    try {
        auto original = _dataIO->getEventRole( event_role._person_id, event_role._event_id, event_role._type );
        if( !_dataIO->update( *original, event_role ) ) {
            LOG_ERROR( "[relations::io::DataManager::update( relations::dto::EventRole & )] "
                           "Could not update EventRole [", event_role._person_id, ", ", event_role._event_id, "]." );
            return false;
        }
        return true;
    } catch( std::out_of_range ) {
        LOG_ERROR( "[relations::io::DataManager::update( relations::dto::EventRole & )] "
                       "EventRole [", event_role._person_id, ", ", event_role._event_id, "] with given RoleType is not in records." );
        return false;
    }
}

/**
 * Updates an Event
 * @param event Event DTO
 * @return Success
 */
bool relations::io::DataManager::update( relations::dto::Event &event ) {
    try {
        auto original = _dataIO->getEvent( event._id );
        if( !_dataIO->update( *original, event ) ) {
            LOG_ERROR( "[relations::io::DataManager::update( relations::dto::Event & )] Could not update Event [", event._id, "]." );
            return false;
        }
        return true;
    } catch( std::out_of_range ) {
        LOG_ERROR( "[relations::io::DataManager::update( relations::dto::EventRole & )] Event [", event._id, "] is not in records." );
        return false;
    }
}

/**
 * Removes a Person
 * @param person Person DTO to remove
 * @return Success
 */
bool relations::io::DataManager::remove( relations::dto::Person &person ) {
    LOG_TRACE( "[relations::io::DataManager::remove( relations::dto::Person & )] Removing Person [", person._id, "]." );
    if( !_dataIO->remove( person ) ) {
        LOG_ERROR( "[relations::io::DataManager::remove( relations::dto::Person & )] Could not remove Person [", person._id, "] from database." );
        return false;
    }
    if( !_graph->removeNode( person._id ) ) {
        LOG_ERROR( "[relations::io::DataManager::remove( relations::dto::Person & )] Could not remove Person [", person._id, "] from Graph." );
        return false;
    }
    return true;
}

/**
 * Removes a Name
 * @param name Name DTO to remove
 * @return Success
 */
bool relations::io::DataManager::remove( dto::Name &name ) {
    LOG_TRACE( "[relations::io::DataManager::remove( relations::dto::Name & )] Removing Name [", name._id, "]." );
    if( !_dataIO->remove( name ) ) {
        LOG_ERROR( "[relations::io::DataManager::remove( relations::dto::Person & )] Could not remove Name [", name._id, "] from database." );
        return false;
    }
    try {
        dto::Person & person = _graph->at( name._person_id );
        auto name_it = std::find_if( person._names.begin(),
                                     person._names.end(),
                                     ( [&]( const dto::Name &n ) { return n._id == name._id; } )
        );
        if( name_it == person._names.end() ) {
            LOG_ERROR( "[relations::io::DataManager::remove( relations::dto::Name & )] "
                           "Name [", name._id, "] not found in Person [", name._person_id, "] in graph." );
            return false;
        }
        person._names.erase( name_it ); //Remove graph name
        return true;
    } catch( std::out_of_range ) {
        LOG_ERROR( "[relations::io::DataManager::remove( relations::dto::Name & )] "
                       "Person ID [", name._person_id, "] in Name [", name._id, "] not found in graph." );
        return false;
    }
}

/**
 * Removes a Relationship
 * @param relationship Relationship DTO to remove
 * @return Success
 */
bool relations::io::DataManager::remove( relations::dto::Relationship &relationship ) {
    LOG_TRACE( "[relations::io::DataManager::remove( relations::dto::Relationship & )] Removing Relationship "
                   "[", relationship._person1, ", ", relationship._person2, "]:[", relationship._type_id, "]." );
    //Get Type DTO from cache
    auto type = std::find_if( _data_cache->_relationship_types.begin(),
                              _data_cache->_relationship_types.end(),
                              ( [&]( const dto::RelationshipType &type ) { return relationship._type_id == type._id; } )
    );
    if( type == _data_cache->_relationship_types.end() ) {
        LOG_ERROR( "[relations::io::DataManager::remove( relations::dto::Relationship & )] "
                       "Relationship's Type ID [", relationship._type_id, "] not found in cache!" );
        return false;
    }
    //Remove from database
    if( !_dataIO->remove( relationship ) ) {
        LOG_ERROR( "[relations::io::DataManager::remove( relations::dto::Relationship & )] Failed to remove Relationship "
                       "[", relationship._person1, ", ", relationship._person2, "]:[", relationship._type_id, "] from database" );
        return false;
    }
    //Update to the graph edge
    if( !_graph->removeDirectedEdge( relationship._person1, relationship._person2, *type ) ) {
        LOG_ERROR( "[relations::io::DataManager::update( relations::dto::Relationship & )] Failed to remove relationship "
                       "[", relationship._person1, ", ", relationship._person2, "]:[", relationship._type_id, "] from Graph." );
        return false;
    }
    return true;
}

/**
 * Removes a Media record
 * @param media Media DTO
 * @return Success
 */
bool relations::io::DataManager::remove( relations::dto::Media &media ) {
    return _dataIO->remove( media );
}

/**
 * Removes an EventRole (Participant)
 * @param event_role EventRole DTO
 * @return Success
 */
bool relations::io::DataManager::remove( relations::dto::EventRole &event_role ) {
    return _dataIO->remove( event_role );
}

/**
 * Removes an Event
 * @param event Event DTO
 * @return Success
 */
bool relations::io::DataManager::remove( relations::dto::Event &event ) {
    return _dataIO->remove( event );
}

/**
 * Removes a Subject-Media relationship
 * @param media_relation MediaRelation DTO
 * @return Success
 */
bool relations::io::DataManager::remove( relations::dto::MediaRelation &media_relation ) {
    return _dataIO->remove( media_relation );
}

/**
 * Gets an Event
 * @param event_id Event's ID
 * @return Event DTO
 * @throws std::out_of_range when Event ID does not match any Event in records
 */
std::unique_ptr<relations::dto::Event> relations::io::DataManager::getEvent( const int64_t &event_id ) {
    return _dataIO->getEvent( event_id );
}

/**
 * Gets person's name(s)
 * @param person Person DTO
 * @return List of copied Name DTO(s)
 * @throws std::out_of_range when person is not in Graph
 */
std::unique_ptr<std::vector<relations::dto::Name>> relations::io::DataManager::getNames( const relations::dto::Person &person ) const {
    LOG_TRACE( "[relations::io::DataManager::getNames( const dto::Person & )] "
                   "Getting Person [", person._id, "]'s Name(s)." );
    if( !exists( person ) ) {
        LOG_ERROR( "[relations::io::PersistentDataIO::getNames( const dto::Person & )] Person [", person._id, "] is not in graph." );
        throw std::out_of_range( "[relations::io::PersistentDataIO::getNames( const dto::Person & )] Person is not in graph." );
    }
    auto store = std::make_unique<std::vector<relations::dto::Name>>();
    size_t count { 0 };
    for( auto name : person._names ) {
        store->emplace_back( name );
        count++;
    }
    LOG_TRACE( "[relations::io::DataManager::getNames( const relations::dto::Person & )] "
                   "Got ", count, " Names for Person [", person._id, "]." );
    return store;
}

/**
 * Gets child edge relationships
 * @param person Person DTO
 * @return List of child edge Relationship DTO(s)
 * throws std::out_of_range when person in not in Graph
 */
std::unique_ptr<std::vector<relations::dto::Relationship>> relations::io::DataManager::getChildRelationships( const dto::Person &person ) const {
    LOG_TRACE( "[relations::io::DataManager::getParentRelationships( const dto::Person & )] "
                   "Getting children edges for Person [", person._id, "]'s relationships." );
    if( !exists( person ) ) {
        LOG_ERROR( "[relations::io::PersistentDataIO::getChildRelationships( const dto::Person & )] Person [", person._id, "] is not in graph." );
        throw std::out_of_range( "[relations::io::PersistentDataIO::getChildRelationships( const dto::Person & )] Person is not in graph." );
    }
    auto store = std::make_unique<std::vector<relations::dto::Relationship>>();
    auto table = _dataIO->getChildRelationships( person );
    for( auto row : *table ) {
        auto person1_id   = row.at( 0 ).getInt();
        auto person2_id   = row.at( 1 ).getInt();
        auto type_id      = row.at( 2 ).getInt();
        auto cached_type  = std::find_if( _data_cache->_relationship_types.begin(),
                                          _data_cache->_relationship_types.end(),
                                          ( [&]( const dto::RelationshipType &type ) { return type_id == type._id; } )
        );
        auto subject_id   = row.at( 3 ).getInt();
        auto note_id      = row.at( 4 ).getInt();
        auto note_subject = row.at( 5 ).getString();
        auto note_text    = row.at( 6 ).getString();

        if( cached_type == _data_cache->_relationship_types.end() ) {
            LOG_ERROR( "[relations::io::DataManager::getChildRelationships( const dto::Person & )] "
                           "Could not find RelationshipType [", type_id, "] in cache." );
        } else {
            if( note_subject.empty() && note_text.empty() ) {
                store->emplace_back( dto::Relationship( subject_id, person1_id, person2_id, cached_type->_id ) );
            } else {
                dto::Note *note = new dto::Note( note_id, note_subject, note_text );
                store->emplace_back( dto::Relationship( subject_id, person1_id, person2_id, cached_type->_id, note ) );
            }
        }
    }
    return store;
}

/**
 * Gets parent edge relationships
 * @param person Person DTO
 * @return List of parent edge Relationship DTO(s)
 * throws std::out_of_range when person in not in Graph
 */
std::unique_ptr<std::vector<relations::dto::Relationship>> relations::io::DataManager::getParentRelationships( const dto::Person &person ) const {
    LOG_TRACE( "[relations::io::DataManager::getParentRelationships( const dto::Person & )] "
                   "Getting parent edges for Person [", person._id, "]'s relationships." );
    if( !exists( person ) ) {
        LOG_ERROR( "[relations::io::PersistentDataIO::getChildRelationships( const dto::Person & )] Person [", person._id, "] is not in graph." );
        throw std::out_of_range( "[relations::io::PersistentDataIO::getChildRelationships( const dto::Person & )] Person is not in graph." );
    }
    auto store = std::make_unique<std::vector<relations::dto::Relationship>>();
    auto table = _dataIO->getParentRelationships( person );
    for( auto row : *table ) {
        auto person1_id   = row.at( 0 ).getInt();
        auto person2_id   = row.at( 1 ).getInt();
        auto type_id      = row.at( 2 ).getInt();
        auto cached_type  = std::find_if( _data_cache->_relationship_types.begin(),
                                          _data_cache->_relationship_types.end(),
                                          ( [&]( const dto::RelationshipType &type ) { return type_id == type._id; } )
        );
        auto subject_id   = row.at( 3 ).getInt();
        auto note_id      = row.at( 4 ).getInt();
        auto note_subject = row.at( 5 ).getString();
        auto note_text    = row.at( 6 ).getString();

        if( cached_type == _data_cache->_relationship_types.end() ) {
            LOG_ERROR( "[relations::io::DataManager::getParentRelationships( const dto::Person & )] "
                           "Could not find RelationshipType [", type_id, "] in cache." );
        } else {
            if( note_subject.empty() && note_text.empty() ) {
                store->emplace_back( dto::Relationship( subject_id, person1_id, person2_id, cached_type->_id ) );
            } else {
                dto::Note *note = new dto::Note( note_id, note_subject, note_text );
                store->emplace_back( dto::Relationship( subject_id, person1_id, person2_id, cached_type->_id, note ) );
            }
        }
    }
    return store;
}

/**
 * Gets all Events in records
 * @return List of Events as Event DTOs
 */
std::unique_ptr<std::vector<relations::dto::Event>> relations::io::DataManager::getEvents() const {
    LOG_TRACE( "[relations::io::DataManager::getEvents()] Getting all events." );
    auto store = std::make_unique<std::vector<relations::dto::Event>>();
    auto table = std::move( _dataIO->getEvents() );
    for( auto r : *table ) {
        auto event_id      = r.at( 0 ).getInt();
        auto type          = enums::to_EventType( r.at( 1 ).getInt() );
        auto custom_type   = r.at( 2 ).getInt();
        //Date
        auto date_id       = r.at( 3 ).getInt();
        auto date_formal   = r.at( 4 ).getString();
        auto date_original = r.at( 5 ).getString();
        //Location
        auto location_id   = r.at( 6 ).getInt();
        auto description   = r.at( 7 ).getString();
        auto address       = r.at( 8 ).getString();
        auto postcode      = r.at( 9 ).getString();
        auto city          = r.at( 10 ).getString();
        auto county        = r.at( 11 ).getString();
        auto country       = r.at( 12 ).getString();
        auto longitude     = r.at( 13 ).getString();
        auto latitude      = r.at( 14 ).getString();
        //Subject, Note
        auto subject_id    = r.at( 15 ).getInt();
        auto note_id       = r.at( 16 ).getInt();
        auto note_heading  = r.at( 17 ).getString();
        auto note_text     = r.at( 18 ).getString();
        //Sub DTOs
        auto date     = new dto::Date( date_id, date_formal, date_original );
        auto location = new dto::Location( location_id, description,
                                           address, postcode, city, county, country,
                                           longitude, latitude );
        auto note     = new dto::Note( note_id, note_heading, note_text );
        //Adding to store
        store->emplace_back( dto::Event( event_id, subject_id, type, custom_type,
                                         date, location, note )
        );
    }
    return store;
}

/**
 * Gets EventRoles for the participants of an Event
 * @param event Event DTO
 * @return List of participants as EventRole DTOs
 */
std::unique_ptr<std::vector<relations::dto::EventRole>> relations::io::DataManager::getEventParticipants( const dto::Event &event ) const {
    LOG_TRACE( "[relations::io::DataManager::getEventParticipants( const dto::Event & )] Getting participants for Event [", event._id, "]." );
    auto store = std::make_unique<std::vector<relations::dto::EventRole>>();
    auto table = std::move( _dataIO->getEventRoles( event ) );
    for( auto r : *table ) {
        auto person_id = r.at( 0 ).getInt();
        auto type      = enums::to_RoleType( r.at( 1 ).getInt() );
        auto details   = r.at( 2 ).getString();
        store->emplace_back( dto::EventRole( event._id, person_id, type, details ) );
    }
    return store;
}

/**
 * Gets all Media in records
 * @return List of Media as Media DTOs
 */
std::unique_ptr<std::vector<relations::dto::Media>> relations::io::DataManager::getMedia() const {
    LOG_TRACE( "[relations::io::DataManager::getMedia()] Getting all Media records." );
    auto store = std::make_unique<std::vector<relations::dto::Media>>();
    auto table = std::move( _dataIO->getMedia() );
    for( auto r : *table ) {
        auto media_id    = r.at( 0 ).getInt();
        auto description = r.at( 1 ).getString();
        auto type        = enums::to_MediaType( r.at( 2 ).getInt() );
        auto file        = r.at( 3 ).getString();
        store->emplace_back( dto::Media( media_id, description, type, file ) );
    }
    return store;
}

/**
 * Gets a list of actors in a Media
 * @return List of Person IDs that are connected to the Media
 */
std::unique_ptr<std::vector<int64_t>> relations::io::DataManager::getMediaActors( const dto::Media &media ) const {
    LOG_TRACE( "[relations::io::DataManager::getMediaActors( const dto::Media & )] Getting actors for Media [", media._id, "]." );
    auto store = std::make_unique<std::vector<int64_t>>();
    auto table = std::move( _dataIO->getMediaActors( media ) );
    for( auto r : * table ) {
        store->emplace_back( r.at( 0 ).getInt() );
    }
    return store;
}

/**
 * Gets teh Event linked to the Media
 * @param media Media DTO
 * @return Event DTO
 * throws std::out_of_range when Media record has no Event linked to it
 */
std::unique_ptr<relations::dto::Event> relations::io::DataManager::getMediaEvent( const relations::dto::Media &media ) const {
    try {
        return _dataIO->getMediaEvent( media );
    } catch( std::out_of_range ) {
        LOG_ERROR( "[relations::io::DataManager::getMediaEvent( const dto::Media & )] Media [", media._id, "] has no Event linked to it." );
        throw std::out_of_range( "[relations::io::DataManager::getMediaEvent( const dto::Media & )] Media has no Event linked to it." );
    }
}
//==================================================================================================================================================//
// Cache access method(s)
//==================================================================================================================================================//
/**
 * Gets access to the data cache object
 * @return Pointer to DataCache
 */
relations::io::DataCache * relations::io::DataManager::getDataCache() {
    return _data_cache.get();
}

//==================================================================================================================================================//
// Helper method(s)
//==================================================================================================================================================//
/**
 * Gets the preferred name of a person
 * @param person Person DTO
 * @return Preferred name
 */
std::string relations::io::DataManager::getPreferedName( const relations::dto::Person &person ) {
    auto preferred = std::find_if( person._names.begin(),
                                   person._names.end(),
                                   ( []( const relations::dto::Name &name ) { return name._preferred_flag; } )
    );
    std::string name;
    if( preferred != person._names.end() ) {
        if( !preferred->_name_form->_full_text.empty() ) {
            name = preferred->_name_form->_full_text;
        } else {
            for( auto p : preferred->_name_form->_name_parts_first )
                name.append( p._value );
            for( auto p : preferred->_name_form->_name_parts_surname )
                name.append( p._value );
        }
    }
    if( name.empty() ) {
        name = "N/A";
    }
    return name;
}

/**
 * Gets the index in the person's names that is preferred
 * @param person Person DTO
 * @return Index of preferred name
 * throws std::out_of_range when no preferred name was found for the Person
 */
size_t relations::io::DataManager::getPreferredNameIndex( const relations::dto::Person &person ) {
    size_t index { 0 };
    for( auto e : person._names ) {
        if( e._preferred_flag ) {
            return index;
        }
        index++;
    }
    LOG_ERROR( "[relations::io::DataManager::getPreferredNameIndex( const dto::Person & )] No preferred name found for Person [", person._id, "]." );
    throw std::out_of_range( "[relations::io::DataManager::getPreferredNameIndex( const dto::Person & )] No preferred name found for Person." );
}
