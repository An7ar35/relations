#ifndef RELATIONS_DATALOADER_H
#define RELATIONS_DATALOADER_H

#include <src/storage/DatabaseAccess.h>
#include <src/dto/GenericType.h>
#include <src/dto/EnumType.h>
#include <src/enums/EnumeratedTypes.h>

namespace relations::io {
    class DataLoader {
      public:
        bool loadEventTypes( storage::DatabaseAccess &db, std::vector<dto::EnumType<enums::EventType>> &types );
        bool loadMediaTypes( storage::DatabaseAccess &db, std::vector<dto::EnumType<enums::MediaType>> &types );
        bool loadRoleTypes( storage::DatabaseAccess &db, std::vector<dto::EnumType<enums::RoleType>> &types );
        bool loadGenderBioTypes( storage::DatabaseAccess &db, std::vector<dto::EnumType<enums::GenderBioType>> &types );
        bool loadGenderIdentityTypes( storage::DatabaseAccess &db, std::vector<dto::EnumType<enums::GenderIdentityType>> &types );
        bool loadNameTypes( storage::DatabaseAccess &db, std::vector<dto::EnumType<enums::NameType>> &types );
        bool loadNamePartTypes( storage::DatabaseAccess &db, std::vector<dto::EnumType<enums::NamePartType>> &types );
        bool loadRelationshipTypes( storage::DatabaseAccess &db, std::vector<dto::RelationshipType> &types );
    };
}

#endif //RELATIONS_DATALOADER_H
