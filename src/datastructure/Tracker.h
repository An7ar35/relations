#ifndef RELATIONS_TRACKER_H
#define RELATIONS_TRACKER_H

#include <functional>

namespace relations {
    template<class T, class Hash = std::hash<T>> class Tracker {
      public:
        bool track( const T &value );
        bool tracked( const T &value );

      private:
        std::unordered_map<T,bool,Hash> _tracker;
    };

    /**
     * Adds a value to the tracker
     * @tparam T    Value type
     * @tparam Hash Hasher for Type
     * @param value Value to track
     * @return Value added state (whether or not it was already tracked)
     */
    template<class T, class Hash> bool Tracker<T,Hash>::track( const T &value ) {
        return _tracker.try_emplace( value ).second;
    }

    /**
     * Check tracking status of a value
     * @tparam T    Value type
     * @tparam Hash Hasher for Type
     * @param value Value to check tracking for
     * @return Tracking state of value
     */
    template<class T, class Hash> bool Tracker<T,Hash>::tracked( const T &value ) {
        return _tracker.find( value ) != _tracker.end();
    }
}

#endif //RELATIONS_TRACKER_H
