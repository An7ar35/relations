#ifndef RELATIONS_UPDATETRACKER_H
#define RELATIONS_UPDATETRACKER_H

#include <list>
#include <unordered_map>

namespace relations {
    template<class ID, class T> class UpdateTracker {
      public:
        UpdateTracker();
        ~UpdateTracker();
        //Iterator
        typedef typename std::unordered_map<ID, T>::iterator iterator;
        typedef typename std::unordered_map<ID, T>::const_iterator const_iterator;
        iterator additions_begin();
        iterator additions_end();
        iterator updates_begin();
        iterator updates_end();
        iterator removals_begin();
        iterator removals_end();
        iterator additions_erase( const_iterator pos );
        iterator updates_erase( const_iterator pos );
        iterator removals_erase( const_iterator pos );
        //Tracking methods
        void toAdd( const ID &id, const T &object );
        void toUpdate( const ID &id, const T &object );
        void toRemove( const ID &id, const T &object );
        //States
        bool empty() const;
        size_t size() const;
        size_t additionsCount() const;
        size_t updatesCount() const;
        size_t removalCount() const;
      private:
        std::unordered_map<ID, T> _additions;
        std::unordered_map<ID, T> _updates;
        std::unordered_map<ID, T> _removals;
    };

    //-----------------------------------------------------------------------------------------------------------------
    // UpdateTracker class public method implementations
    //-----------------------------------------------------------------------------------------------------------------
    /**
     * Constructor
     * @tparam ID ID type
     * @tparam T  Tracked Object type
     */
    template<class ID, class T> UpdateTracker<ID, T>::UpdateTracker() {}

    /**
     * Destructor
     * @tparam ID ID type
     * @tparam T  Tracked Object type
     */
    template<class ID, class T> UpdateTracker<ID, T>::~UpdateTracker() {}

    /**
     * Begin() iterator of all objects to add
     * @return Start iterator
     */
    template<class ID, class T> typename UpdateTracker<ID, T>::iterator UpdateTracker<ID, T>::additions_begin() {
        return _additions.begin();
    }

    /**
     * End() iterator of all objects to add
     * @return End iterator
     */
    template<class ID, class T> typename UpdateTracker<ID, T>::iterator UpdateTracker<ID, T>::additions_end() {
        return _additions.end();
    }

    /**
     * Begin() iterator of all objects to update
     * @return Start iterator
     */
    template<class ID, class T> typename UpdateTracker<ID, T>::iterator UpdateTracker<ID, T>::updates_begin() {
        return _updates.begin();
    }

    /**
     * End() iterator of all objects to update
     * @return End iterator
     */
    template<class ID, class T> typename UpdateTracker<ID, T>::iterator UpdateTracker<ID, T>::updates_end() {
        return _updates.end();
    }

    /**
     * Begin() iterator of all objects to remove
     * @return Start iterator
     */
    template<class ID, class T> typename UpdateTracker<ID, T>::iterator UpdateTracker<ID, T>::removals_begin() {
        return _removals.begin();
    }

    /**
     * End() iterator of all objects to remove
     * @return End iterator
     */
    template<class ID, class T> typename UpdateTracker<ID, T>::iterator UpdateTracker<ID, T>::removals_end() {
        return _removals.end();
    }

    /**
     * Removes from the Additions a single element
     * @param pos Iterator pointing to a single element to be removed
     * @return Iterator pointing to the position immediately following the last of the elements erased
     */
    template<class ID, class T> typename UpdateTracker<ID, T>::iterator UpdateTracker<ID, T>::additions_erase( UpdateTracker<ID, T>::const_iterator pos ) {
        return _additions.erase( pos );
    }

    /**
     * Removes from the Updates a single element
     * @param pos Iterator pointing to a single element to be removed
     * @return Iterator pointing to the position immediately following the last of the elements erased
     */
    template<class ID, class T> typename  UpdateTracker<ID, T>::iterator UpdateTracker<ID, T>::updates_erase( UpdateTracker<ID, T>::const_iterator pos ) {
        return _updates.erase( pos );
    }

    /**
     * Removes from the Removals a single element
     * @param pos Iterator pointing to a single element to be removed
     * @return Iterator pointing to the position immediately following the last of the elements erased
     */
    template<class ID, class T> typename UpdateTracker<ID, T>::iterator UpdateTracker<ID, T>::removals_erase( UpdateTracker<ID, T>::const_iterator pos ) {
        return _removals.erase( pos );
    }

    /**
     * Add to the tracker an object to add
     * @param id     Object ID
     * @param object Object
     */
    template<class ID, class T> void UpdateTracker<ID, T>::toAdd( const ID &id, const T &object ) {
        _additions.insert( typename std::unordered_map<ID, T>::value_type( id, object ) );
    }

    /**
     * Add to the tracker an object to update
     * @param id     Object ID
     * @param object Object
     */
    template<class ID, class T> void UpdateTracker<ID, T>::toUpdate( const ID &id, const T &object ) {
        auto it = _additions.find( id );
        if( it != _additions.end() ) {
            it->second = object;
        } else {
            _updates.insert( typename std::unordered_map<ID, T>::value_type( id, object ) );
        }
    }

    /**
     * Add to the tracker an object to remove
     * Note: If object was an addition then it is completely un-tracked
     * @param id     Object ID
     * @param object Object
     */
    template<class ID, class T> void UpdateTracker<ID, T>::toRemove( const ID &id, const T &object ) {
        auto it2 = _updates.find( id );
        if( it2 != _updates.end() ) //Check Updates and delete if found
            _updates.erase( it2 );

        auto it1 = _additions.find( id );
        if( it1 != _additions.end() ) //Check Additions and delete if found
            _additions.erase( it1 );
        else //Add to Removals
            _removals.insert( typename std::unordered_map<ID, T>::value_type( id, object ) );
    }

    /**
     * Checks if there are objects in tracker
     * @return Empty state
     */
    template<class ID, class T> bool UpdateTracker<ID, T>::empty() const {
        return _additions.empty() && _updates.empty() && _removals.empty();
    }

    /**
     * Gets the number of objects in tracker
     * @return Number of tracked objects
     */
    template<class ID, class T> size_t UpdateTracker<ID, T>::size() const {
        return _additions.size() + _updates.size() + _removals.size();
    }

    /**
     * Gets the number of objects in Additions tracker
     * @return Number of tracked objects
     */
    template<class ID, class T> size_t UpdateTracker<ID, T>::additionsCount() const {
        return _additions.size();
    }

    /**
     * Gets the number of objects in Updates tracker
     * @return Number of tracked objects
     */
    template<class ID, class T> size_t UpdateTracker<ID, T>::updatesCount() const {
        return _updates.size();
    }

    /**
     * Gets the number of objects in Removals tracker
     * @return Number of tracked objects
     */
    template<class ID, class T> size_t UpdateTracker<ID, T>::removalCount() const {
        return _removals.size();
    }
}

#endif //RELATIONS_UPDATETRACKER_H
