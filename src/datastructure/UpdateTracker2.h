#ifndef RELATIONS_UPDATETRACKERPAIR_H
#define RELATIONS_UPDATETRACKERPAIR_H

#include <list>
#include <unordered_map>

namespace relations {
    template<class K1, class K2, class T> class UpdateTracker2 {
      public:
        typedef std::unordered_map<K2, T>       K2Map_t;
        typedef std::unordered_map<K1, K2Map_t> K1Map_t;

        UpdateTracker2();
        virtual ~UpdateTracker2();
        //Tracking methods
        void toAdd( const std::pair<K1,K2> &id, const T &object );
        void toUpdate( const std::pair<K1,K2> &id, const T &object );
        void toRemove( const std::pair<K1,K2> &id, const T &object );
        //States
        bool empty() const;
        size_t size() const;
        size_t additionsCount() const;
        size_t updatesCount() const;
        size_t removalCount() const;
      private:
        K1Map_t _additions;
        K1Map_t _updates;
        K1Map_t _removals;

      public:
        class iterator {
          public:
            typedef std::forward_iterator_tag iterator_category;
            typedef T                         value_type;
            typedef ptrdiff_t                 difference_type;
            typedef T*                        pointer;
            typedef T&                        reference;
            friend class UpdateTracker2;
            //Constructors
            iterator();
            iterator( const iterator &it );
            //Operators
            iterator & operator =( const iterator &rhs );
            iterator & operator ++();
            iterator operator ++( int );
            reference operator *(); //dereference e.g.: (*it).first
            pointer operator ->();  //dereference e.g.: it->first
            bool operator ==( const iterator &rhs ) const;
            bool operator !=( const iterator &rhs ) const;
          private:
            struct Index {
                Index( const K1 &key1, const K2  &key2, T *value ) : _key( { key1, key2 } ), _value( value ) {};
                std::pair<K1, K2> _key;
                T *_value;
            };
            //Private variables
            bool                                _new_flag;
            typename std::list<Index>           _list;
            typename std::list<Index>::iterator _pos;
            //Private methods
            void add( std::unordered_map<K1, std::unordered_map<K2, T>> &map );
        };

        iterator additions_begin();
        iterator additions_end();
        iterator updates_begin();
        iterator updates_end();
        iterator removals_begin();
        iterator removals_end();
        iterator additions_erase( iterator &pos );
        iterator updates_erase( iterator &pos );
        iterator removals_erase( iterator &pos );
    };

    //-----------------------------------------------------------------------------------------------------------------
    // UpdateTracker2 class public method implementations
    //-----------------------------------------------------------------------------------------------------------------
    /**
     * Constructor
     * @tparam ID ID type
     * @tparam T  Tracked Object type
     */
    template<class K1, class K2, class T> UpdateTracker2<K1,K2,T>::UpdateTracker2() {}

    /**
     * Destructor
     * @tparam ID ID type
     * @tparam T  Tracked Object type
     */
    template<class K1, class K2, class T> UpdateTracker2<K1,K2,T>::~UpdateTracker2() {}

    /**
     * Begin() iterator of all objects to add
     * @return Start iterator
     */
    template<class K1, class K2, class T> typename UpdateTracker2<K1,K2,T>::iterator UpdateTracker2<K1,K2,T>::additions_begin() {
        iterator it;
        it.add( _additions );
        if( !it._list.empty() ) {
            it._pos = it._list.begin();
        }
        return it;
    }

    /**
     * End() iterator of all objects to add
     * @return End iterator
     */
    template<class K1, class K2, class T> typename UpdateTracker2<K1,K2,T>::iterator UpdateTracker2<K1,K2,T>::additions_end() {
        return iterator();
    }

    /**
     * Begin() iterator of all objects to update
     * @return Start iterator
     */
    template<class K1, class K2, class T> typename UpdateTracker2<K1,K2,T>::iterator UpdateTracker2<K1,K2,T>::updates_begin() {
        iterator it;
        it.add( _updates );
        if( !it._list.empty() ) {
            it._pos = it._list.begin();
        }
        return it;
    }

    /**
     * End() iterator of all objects to update
     * @return End iterator
     */
    template<class K1, class K2, class T> typename UpdateTracker2<K1,K2,T>::iterator UpdateTracker2<K1,K2,T>::updates_end() {
        return iterator();
    }

    /**
     * Begin() iterator of all objects to remove
     * @return Start iterator
     */
    template<class K1, class K2, class T> typename UpdateTracker2<K1,K2,T>::iterator UpdateTracker2<K1,K2,T>::removals_begin() {
        iterator it;
        it.add( _removals );
        if( !it._list.empty() ) {
            it._pos = it._list.begin();
        }
        return it;
    }

    /**
     * End() iterator of all objects to remove
     * @return End iterator
     */
    template<class K1, class K2, class T> typename UpdateTracker2<K1,K2,T>::iterator UpdateTracker2<K1,K2,T>::removals_end() {
        return iterator();
    }

    /**
     * Removes from the Additions a single element
     * @param pos Iterator pointing to a single element to be removed
     * @return Iterator pointing to the position immediately following the last of the elements erased
     */
    template<class K1, class K2, class T> typename UpdateTracker2<K1,K2,T>::iterator UpdateTracker2<K1,K2,T>::additions_erase( iterator &pos ) {
        if( !pos._list.empty() ) {
            auto key = pos._pos->_key;
            auto k1 = std::find_if( _additions.begin(),
                                    _additions.end(),
                                    ( [&]( const std::pair<K1,K2Map_t> &map ) { return map.first == key.first; } )
            );
            if( k1 != _additions.end() ) {
                auto k2 = std::find_if( k1->second.begin(),
                                        k1->second.end(),
                                        ( [&]( const std::pair<K2, T> &map ) { return map.first == key.second; } )
                );
                if( k2 != k1->second.end() ) {
                    k1->second.erase( k2 );
                }
                if( k1->second.empty() )
                    _additions.erase( k1 );
            }
            pos._pos = pos._list.erase( pos._pos );
        }
        return pos;
    }

    /**
     * Removes from the Updates a single element
     * @param pos Iterator pointing to a single element to be removed
     * @return Iterator pointing to the position immediately following the last of the elements erased
     */
    template<class K1, class K2, class T> typename  UpdateTracker2<K1,K2,T>::iterator UpdateTracker2<K1,K2,T>::updates_erase( iterator &pos ) {
        if( !pos._list.empty() ) {
            auto key = pos._pos->_key;
            auto k1 = std::find_if( _updates.begin(),
                                    _updates.end(),
                                    ( [&]( const std::pair<K1,K2Map_t> &map ) { return map.first == key.first; } )
            );
            if( k1 != _updates.end() ) {
                auto k2 = std::find_if( k1->second.begin(),
                                        k1->second.end(),
                                        ( [&]( const std::pair<K2, T> &map ) { return map.first == key.second; } )
                );
                if( k2 != k1->second.end() ) {
                    k1->second.erase( k2 );
                }
                if( k1->second.empty() )
                    _updates.erase( k1 );
            }
            pos._pos = pos._list.erase( pos._pos );
        }
        return pos;
    }

    /**
     * Removes from the Removals a single element
     * @param pos Iterator pointing to a single element to be removed
     * @return Iterator pointing to the position immediately following the last of the elements erased
     */
    template<class K1, class K2, class T> typename UpdateTracker2<K1,K2,T>::iterator UpdateTracker2<K1,K2,T>::removals_erase( iterator &pos ) {
        if( !pos._list.empty() ) {
            auto key = pos._pos->_key;
            auto k1 = std::find_if( _removals.begin(),
                                    _removals.end(),
                                    ( [&]( const std::pair<K1,K2Map_t> &map ) { return map.first == key.first; } )
            );
            if( k1 != _removals.end() ) {
                auto k2 = std::find_if( k1->second.begin(),
                                        k1->second.end(),
                                        ( [&]( const std::pair<K2, T> &map ) { return map.first == key.second; } )
                );
                if( k2 != k1->second.end() ) {
                    k1->second.erase( k2 );
                }
                if( k1->second.empty() )
                    _removals.erase( k1 );
            }
            pos._pos = pos._list.erase( pos._pos );
        }
        return pos;
    }

    /**
     * Add to the tracker an object to add
     * @param id     Object ID
     * @param object Object
     */
    template<class K1, class K2, class T> void UpdateTracker2<K1,K2,T>::toAdd( const std::pair<K1,K2> &id, const T &object ) {
        auto key1_addition_it = _additions.find( id.first );
        if( key1_addition_it == _additions.end() ) {
            key1_addition_it = _additions.insert( typename K1Map_t::value_type( id.first, K2Map_t() ) ).first;
        }
        key1_addition_it->second.insert( typename K2Map_t::value_type( id.second, object ) );
    }

    /**
     * Add to the tracker an object to update
     * @param id     Object ID
     * @param object Object
     */
    template<class K1, class K2, class T> void UpdateTracker2<K1,K2,T>::toUpdate( const std::pair<K1,K2> &id, const T &object ) {
        auto key1_additions_it = _additions.find( id.first );
        if( key1_additions_it != _additions.end() ) {
            auto key2_it = key1_additions_it->second.find( id.second );
            if( key2_it != key1_additions_it->second.end() ) {
                key2_it->second = object;
                return;
            }
        }
        //object never added
        auto key1_updates_it = _updates.find( id.first );
        if( key1_updates_it == _updates.end() ) {
            key1_updates_it = _updates.insert( typename K1Map_t::value_type( id.first, K2Map_t() ) ).first;
        }
        key1_updates_it->second.insert( typename K2Map_t::value_type( id.second, object ) );
    }

    /**
     * Add to the tracker an object to remove
     * Note: If object was an addition then it is completely un-tracked
     * @param id     Object ID
     * @param object Object
     */
    template<class K1, class K2, class T> void UpdateTracker2<K1,K2,T>::toRemove( const std::pair<K1,K2> &id, const T &object ) {
        //Check Updates and delete if found
        auto key1_updates_it = _updates.find( id.first );
        if( key1_updates_it != _updates.end() ) {
            auto key2_updates_it = key1_updates_it->second.find( id.second );
            if( key2_updates_it != key1_updates_it->second.end() ) {
                key1_updates_it->second.erase( key2_updates_it );
                if( key1_updates_it->second.empty() ) {
                    _updates.erase( key1_updates_it );
                }
            }
        }
        //Check Additions and delete if found
        auto key1_additions_it = _additions.find( id.first );
        if( key1_additions_it != _additions.end() ) {
            auto key2_additions_it = key1_additions_it->second.find( id.second );
            if( key2_additions_it != key1_additions_it->second.end() ) {
                key1_additions_it->second.erase( key2_additions_it );
                if( key1_additions_it->second.empty() ) {
                    _additions.erase( key1_additions_it );
                    return;
                }
            }
        }
        //Add to Removals
        auto key1_removals_it = _removals.find( id.first );
        if( key1_removals_it == _removals.end() ) {
            key1_removals_it = _removals.insert( typename K1Map_t::value_type( id.first, K2Map_t() ) ).first;
        }
        key1_removals_it->second.insert( typename K2Map_t::value_type( id.second, object ) );
    }

    /**
     * Checks if there are objects in tracker
     * @return Empty state
     */
    template<class K1, class K2, class T> bool UpdateTracker2<K1,K2,T>::empty() const {
        return _additions.empty() && _updates.empty() && _removals.empty();
    }

    /**
     * Gets the number of objects in tracker
     * @return Number of tracked objects
     */
    template<class K1, class K2, class T> size_t UpdateTracker2<K1,K2,T>::size() const {
        return additionsCount() + updatesCount() + removalCount();
    }

    /**
     * Gets the number of objects in Additions tracker
     * @return Number of tracked objects
     */
    template<class K1, class K2, class T> size_t UpdateTracker2<K1,K2,T>::additionsCount() const {
        size_t count = 0;
        for( auto k1 : _additions )
            for( auto k2 : k1.second )
                count++;
        return count;
    }

    /**
     * Gets the number of objects in Updates tracker
     * @return Number of tracked objects
     */
    template<class K1, class K2, class T> size_t UpdateTracker2<K1,K2,T>::updatesCount() const {
        size_t count = 0;
        for( auto k1 : _updates )
            for( auto k2 : k1.second )
                count++;
        return count;
    }

    /**
     * Gets the number of objects in Removals tracker
     * @return Number of tracked objects
     */
    template<class K1, class K2, class T> size_t UpdateTracker2<K1,K2,T>::removalCount() const {
        size_t count = 0;
        for( auto k1 : _removals )
            for( auto k2 : k1.second )
                count++;
        return count;
    }

    //-----------------------------------------------------------------------------------------------------------------
    // Iterator class method implementations for the UpdateTracker2
    //-----------------------------------------------------------------------------------------------------------------
    /**
     * Constructor
     * @tparam K Key type
     * @tparam V Value type
     */
    template<class K1, class K2, class T> UpdateTracker2<K1,K2,T>::iterator::iterator() :
        _pos( 0 ),
        _new_flag( true )
    {};

    /**
     * Constructor
     * @tparam K Key type
     * @tparam V Value type
     * @param it Iterator to copy over
     */
    template<class K1, class K2, class T> UpdateTracker2<K1,K2,T>::iterator::iterator( const iterator &it ) :
        _list( it._list ),
        _pos( it._pos ),
        _new_flag( it._new_flag )
    {}

    /**
     * Assignment operator
     * @tparam K Key type
     * @tparam V Value type
     * @param rhs Iterator to assign with
     * @return Assigned iterator
     */
    template<class K1, class K2, class T> typename UpdateTracker2<K1,K2,T>::iterator & UpdateTracker2<K1,K2,T>::iterator::operator =( const iterator &rhs ) {
        _list = rhs._list;
        _pos  = rhs._pos;
        _new_flag = rhs._new_flag;
        return *this;
    }

    /**
     * Pre-increment operator
     * @tparam K Key type
     * @tparam V Value type
     * @return Incremented iterator
     */
    template<class K1, class K2, class T> typename UpdateTracker2<K1,K2,T>::iterator & UpdateTracker2<K1,K2,T>::iterator::operator ++() {
        if( _new_flag ) {
            _pos = _list.begin();
            _new_flag = false;
        }
        if( _pos != _list.end() ) {
            _pos = _list.erase( _pos );
        }
        return *this;
    }

    /**
     * Post-increment operator
     * @tparam K Key type
     * @tparam V Value type
     * @return Current iterator
     */
    template<class K1, class K2, class T> typename UpdateTracker2<K1,K2,T>::iterator UpdateTracker2<K1,K2,T>::iterator::operator ++( int ) {
        iterator tmp( *this );
        operator++();
        return tmp;
    }

    /**
     * Dereference operator
     * e.g.: (*it).first
     * @tparam K Key type
     * @tparam V Value type
     * @return Iterator
     */
    template<class K1, class K2, class T> typename UpdateTracker2<K1,K2,T>::iterator::reference & UpdateTracker2<K1,K2,T>::iterator::operator *() {
        return *_pos->_value;
    }

    /**
     * Dereference operator
     * e.g.: it->first
     * @tparam K Key type
     * @tparam V Value type
     * @return Iterator
     */
    template<class K1, class K2, class T> typename UpdateTracker2<K1,K2,T>::iterator::pointer UpdateTracker2<K1,K2,T>::iterator::operator ->() {
        return &*_pos->_value;
    }

    /**
     * Equivalence operator
     * @tparam K Key type
     * @tparam V Value type
     * @param rhs Iterator to compare with
     * @return Equivalence state
     */
    template<class K1, class K2, class T> bool UpdateTracker2<K1,K2,T>::iterator::operator ==( const UpdateTracker2<K1,K2,T>::iterator &rhs ) const {
        return ( _new_flag && rhs._pos == rhs._list.end() )
               || ( _pos == _list.end() && rhs._new_flag )
               || ( _pos == rhs._pos );
    }

    /**
     * Negative equivalence operator
     * @tparam K Key type
     * @tparam V Value type
     * @param rhs Iterator to compare with
     * @return !equivalence state
     */
    template<class K1, class K2, class T> bool UpdateTracker2<K1,K2,T>::iterator::operator !=( const UpdateTracker2<K1,K2,T>::iterator &rhs ) const {
        return !( *this == rhs );
    }

    /**
     * Adds all items from a map
     * @param map Key pair and value map
     */
    template<class K1, class K2, class T>  void UpdateTracker2<K1,K2,T>::iterator::add( std::unordered_map<K1, std::unordered_map<K2, T>> &map ) {
        for( auto it1 = map.begin(); it1 != map.end(); ++it1 ) {
            for( auto it2 = it1->second.begin(); it2 != it1->second.end(); ++it2 ) {
                _list.emplace_back( it1->first, it2->first, &it2->second );
            }
        }
    }
}

#endif //RELATIONS_UPDATETRACKERPAIR_H
