/**
    @class          relations::Graph2
    @brief          [ADT] Multi-typed-edge graph

    MultiGraph with defined edge types

    @dependencies   eadlib::logger::Logger

    @author         E. A. Davison
    @copyright      E. A. Davison 2017
    @license        GNUv2 Public License
**/
#ifndef RELATIONS_GRAPH2_H
#define RELATIONS_GRAPH2_H

#include <list>
#include <string>
#include <vector>
#include <unordered_map>
#include <algorithm>
#include <memory>

namespace datastructure {
    template<class K, class V, class R> class Graph2 {
      public:
        //Sub-class and typedefs
        struct Link {
            R      _relationship;
            Node * _from   { 0 };
            Node * _to     { 0 };
        };
        struct Node {
            std::vector<Link *> _parents  { 0 };
            std::vector<Link *> _children { 0 };
            K _key;
            V _value;
        };
        //Typedefs
        typedef std::unordered_map<R, std::vector<Link>> Links_t;
        typedef std::vector<Node>                        Nodes_t;
        //Constructors/Destructor
        Graph2();
        Graph2( const Graph2 &graph );
        Graph2( Graph2 && graph );
        ~Graph2();
        //Iterator
        iterator begin();
        iterator end();
        const_iterator cbegin() const;
        const_iterator cend() const;
        //Operators
        Graph2 &operator =( const Graph3 &graph );
        Graph2 &operator =( Graph3 &&graph );
        //Graph manipulation
        bool addNode( const K &key, const V &value );
        bool removeNode( const K &key );
        bool createDirectedEdge( const K &from, const K &to, const R &relationship );
        bool createDirectedEdge( const K &from, const std::pair<K, V> &to, const R &relationship );
        bool createDirectedEdge( const std::pair<K, V>, const K &to, const R &relationship );
        bool createDirectedEdge( const std::pair<K, V> &from, const std::pair<K, V> &to, const R &relationship );
        bool removeDirectedEdge( const K &from, const K &to );
        bool removeDirectedEdge( const K &from, const K &to, const R &relationship );
        //Graph access
        V & at( const K &key );
        const Relationships_t &getRelationships( const K &key ) const;
        const std::list<K> & getChildList( const K &key, const R &relationship );
        const std::list<K> & getParentList( const K &key, const R &relationship );
        const_iterator find( const K &key ) const;
        //Graph state
        std::list<std::pair<K,R>> getRelationship( const K &from, const K &to ) const;
        bool isReachable( const K &from, const K &to ) const;
        bool exists( const K &key ) const;
        bool exists( const K &from, const K &to, const R &relationship ) const;
        bool empty() const;
        size_t nodeCount() const;
        size_t size() const; //Edge count
        size_t getInDegree( const K &key ) const;
        size_t getOutDegree( const K &key ) const;


      private:
        Links_t _edges;
        Nodes_t _nodes;
    };
}

#endif //RELATIONS_GRAPH2_H
