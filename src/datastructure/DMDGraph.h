/**
    @class          relations::Graph3
    @brief          [ADT] Directed-Multi-Dimension Graph

    MultiGraph that is able to hold multiple edge types extending graph dimensions

    @dependencies   eadlib::logger::Logger, eadlib::exception::corruption

    @author         E. A. Davison
    @copyright      E. A. Davison 2017
    @license        GNUv2 Public License
**/
#ifndef RELATIONS_GRAPH3_H
#define RELATIONS_GRAPH3_H

#include <list>
#include <string>
#include <ostream>
#include <vector>
#include <unordered_map>
#include <algorithm>
#include <memory>
#include <initializer_list>

#include <eadlib/datastructure/GenericNodePair.h>
#include <eadlib/logger/Logger.h>
#include <eadlib/exception/corruption.h>

namespace relations {
    template<class K, class V, class R> class DMDGraph {
      public:
        //Nested classes and typedefs
        struct Adjacency {
            std::list<K> childList  = std::list<K>();
            std::list<K> parentList = std::list<K>();
        };

        typedef std::unordered_map<R, Adjacency> Relationships_t; //Edge types

        struct NodeData {
            NodeData( const V& value ) : value( value ) {};
            friend std::ostream &operator <<( std::ostream &output, const NodeData node ) {
                output << node.value;
                return output;
            }
            V               value;
            Relationships_t relationships = Relationships_t();
        };

        typedef std::unordered_map<K, NodeData>  Graph_t;
        typedef typename Graph_t::const_iterator const_iterator;
        typedef typename Graph_t::iterator       iterator;
        //Constructors/Destructor
        DMDGraph();
        DMDGraph( const DMDGraph &graph );
        DMDGraph( DMDGraph &&graph );
        ~DMDGraph();
        //Iterator
        iterator begin();
        iterator end();
        const_iterator cbegin() const;
        const_iterator cend() const;
        //Operators
        DMDGraph &operator =( const DMDGraph &graph );
        DMDGraph &operator =( DMDGraph &&graph );
        //Graph manipulation
        bool addNode( const K &key, const V &value );
        std::pair<iterator, bool> emplaceNode( const K &key, const V &value );
        bool removeNode( const K &key );
        bool createDirectedEdge( const K &from, const K &to, const R &relationship );
        bool createDirectedEdge( const K &from, const std::pair<K, V> &to, const R &relationship );
        bool createDirectedEdge( const std::pair<K, V>, const K &to, const R &relationship );
        bool createDirectedEdge( const std::pair<K, V> &from, const std::pair<K, V> &to, const R &relationship );
        bool removeDirectedEdge( const K &from, const K &to );
        bool removeDirectedEdge( const K &from, const K &to, const R &relationship );
        //Graph access
        V & at( const K &key );
        const V & at( const K &key ) const;
        const Relationships_t & getRelationships( const K &key ) const;
        const std::list<K> & getChildList( const K &key, const R &relationship );
        const std::list<K> & getParentList( const K &key, const R &relationship );
        iterator find( const K &key );
        const_iterator find( const K &key ) const;
        //Graph state
        bool isReachable( const K &from, const K &to ) const;
        bool exists( const K &key ) const;
        bool exists( const K &from, const K &to, const R &relationship ) const;
        bool empty() const;
        size_t nodeCount() const;
        size_t size() const; //Edge count
        size_t getInDegree( const K &key ) const;
        size_t getInDegree( const K &key, const R &relationship ) const;
        size_t getOutDegree( const K &key ) const;
        size_t getOutDegree( const K &key, const R &relationship ) const;

      private:
        //Variables
        std::unique_ptr<Graph_t>      _graph;
        std::unordered_map<R, size_t> _edge_count;
        //Methods
        bool removeChild( typename Graph_t::iterator &node_it, const K &key, const R &relationship );
        bool removeParent( typename Graph_t::iterator &node_it, const K &key, const R &relationship );
        size_t removeChildren( typename Graph_t::iterator &node_it, const K &key );
        size_t removeParents( typename Graph_t::iterator &node_it, const K &key );
    };

    //-----------------------------------------------------------------------------------------------------------------
    // DMDGraph class public method implementations
    //-----------------------------------------------------------------------------------------------------------------
    /**
     * Constructor
     * @tparam K Key type
     * @tparam V Value type
     * @tparam R Relationship type
     */
    template<class K, class V, class R> DMDGraph<K,V,R>::DMDGraph() :
        _graph( std::make_unique<Graph_t>() )
    {}

    /**
     * Copy-Constructor
     * @tparam K Key type
     * @tparam V Value type
     * @tparam R Relationship type
     * @param graph Graph to copy over
     */
    template<class K, class V, class R> DMDGraph<K,V,R>::DMDGraph( const DMDGraph &graph ) :
        _graph( std::make_unique<Graph_t>() )
    {
        for( auto it = graph._graph->cbegin(); it != graph._graph->cend(); ++it ) {
            _graph->emplace( it->first, it->second );
        }
        for( auto it = graph._edge_count.begin(); it != graph._edge_count.end(); ++it ) {
            _edge_count.emplace( it->first, it->second );
        }
        LOG_DEBUG( "[relations::DMDGraph<K,V,R>::DMDGraph()] Copy constructor called. Copied ", _graph->size(), " nodes and ", size() ," links." );
    }

    /**
     * Move-Constructor
     * @tparam K Key type
     * @tparam V Value type
     * @tparam R Relationship type
     * @param graph Graph to move over
     */
    template<class K, class V, class R> DMDGraph<K,V,R>::DMDGraph( DMDGraph &&graph ) :
        _graph( std::move( graph._graph ) ),
        _edge_count( std::move( graph._edge_count ) )
    {
        graph._graph  = std::make_unique<Graph_t>();
    }

    /**
     * Destructor
     */
    template<class K, class V, class R> DMDGraph<K,V,R>::~DMDGraph() {}

    /**
     * begin() iterator
     * @return Begin iterator
     */
    template<class K, class V, class R> typename DMDGraph<K,V,R>::iterator DMDGraph<K,V,R>::begin() {
        return _graph->begin();
    }

    /**
     * end() iterator
     * @return End iterator
     */
    template<class K, class V, class R> typename DMDGraph<K,V,R>::iterator DMDGraph<K,V,R>::end() {
        return _graph->end();
    }

    /**
     * cbegin() const iterator
     * @return Begin const iterator
     */
    template<class K, class V, class R> typename DMDGraph<K,V,R>::const_iterator DMDGraph<K,V,R>::cbegin() const {
        return _graph->cbegin();
    }

    /**
     * cend() const iterator
     * @return End const iterator
     */
    template<class K, class V, class R> typename DMDGraph<K,V,R>::const_iterator DMDGraph<K,V,R>::cend() const {
        return _graph->cend();
    }

    /**
     * Copy assignment operator
     * @tparam K Key type
     * @tparam V Value type
     * @tparam R Relationship type
     * @param graph Graph to copy over
     * @return copied graph
     */
    template<class K, class V, class R> DMDGraph<K,V,R> & DMDGraph<K,V,R>::operator =( const DMDGraph &graph ) {
        _graph.reset( std::make_unique<Graph_t>() );
        for( auto it = graph._graph->cbegin(); it != graph._graph->cend(); ++it ) {
            _graph->emplace( it->first, it->second );
        }
        for( auto it = graph._edge_count.begin(); it != graph._edge_count.end(); ++it ) {
            _edge_count.emplace( it->first, it->second );
        }
        return *this;
    }

    /**
     * Move assignment operator
     * @tparam K Key type
     * @tparam V Value type
     * @tparam R Relationship type
     * @param graph Graph to move over
     * @return Moved graph
     */
    template<class K, class V, class R> DMDGraph<K,V,R> & DMDGraph<K,V,R>::operator =( DMDGraph &&graph ) {
        _graph.reset( graph._graph.release );
        _edge_count = std::move( graph._edge_count );
        graph._graph = std::make_unique<Graph_t>();
        LOG_DEBUG( "[relations::DMDGraph<K,V,R>::operator=(..)] Move assignement called. Moved ", _graph->size(), " nodes and ", size() ," links." );
        return *this;
    }

    /**
     * Adds a node
     * @param key Key for node
     * @return Success
     */
    template<class K, class V, class R> bool DMDGraph<K,V,R>::addNode( const K &key, const V &value ) {
        if( _graph->find( key ) == _graph->end() ) {
            _graph->insert( typename Graph_t::value_type( key, NodeData( value ) ) );
            return true;
        } else {
            LOG_ERROR( "[relations::DMDGraph<K,V,R>::addNode( ", key, ", ", value, " )] Key is already in graph." );
            return false;
        }
    }

    /**
     * Emplace a new node if key doesn't exist
     * @param key   Node key
     * @param value Node value
     * @return Pair with iterator pointing either to the newly inserted element in the container or to the element whose key is equivalent and a bool value indicating whether the element was successfully inserted or not..
     */
    template<class K, class V, class R> std::pair<typename DMDGraph<K,V,R>::iterator, bool> DMDGraph<K,V,R>::emplaceNode( const K &key, const V &value ) {
        return _graph->emplace( typename Graph_t::value_type( key, NodeData( value ) ) );
    }

    /**
     * Deletes a node
     * @param key Node key
     * @return Success
     */
    template<class K, class V, class R> bool DMDGraph<K,V,R>::removeNode( const K &key ) {
        auto node_to_delete = _graph->find( key );
        if( node_to_delete == _graph->end() ) {
            LOG_WARNING( "[relations::DMDGraph<K,V,R>::deleteNode( ", key, " )] Key doesn't exist." );
            return false;
        } else {
            //Removal of any references to the key of the node to delete from over nodes
            for( auto node_it = _graph->begin(); node_it != _graph->end(); ++node_it ) {
                if( node_it != node_to_delete ) {
                    Relationships_t * relationshipList = &node_it->second.relationships;
                    for( auto relation = relationshipList->begin(); relation != relationshipList->end(); ) {
                        auto           relation_count = _edge_count.find( relation->first );
                        std::list<K> * childList      = &relation->second.childList;
                        std::list<K> * parentList     = &relation->second.parentList;
                        //Removing reference in current iterated node's child list
                        for( auto child = childList->begin(); child != childList->end(); ) {
                            if( *child == key ) {
                                LOG_DEBUG( "[relations::DMDGraph<K,V,R>::deleteNode( ", key, " )] "
                                    "Removing [", node_it->first, "]-(", relation->first, ")->[", key, "] relationship." );
                                child = childList->erase( child );
                                relation_count->second--;
                            } else {
                                ++child;
                            }
                        }
                        //Removing reference in current iterated node's parent list
                        for( auto parent = parentList->begin(); parent != parentList->end(); ) {
                            if( *parent == key ) {
                                parent = parentList->erase( parent );
                            } else {
                                ++parent;
                            }
                        }
                        //Removing relationship edge count tracker if 0
                        if( relation_count->second < 1 ) {
                            LOG_DEBUG( "[relations::DMDGraph<K,V,R>::deleteNode( ", key, " )] "
                                "Removing (", relation->first, ") dimension from the counter." );
                            relation_count = _edge_count.erase( relation_count );
                        }
                        //Removing relationship type in current iterated node if not used any longer
                        if( childList->empty() && parentList->empty() ) {
                            LOG_DEBUG( "[relations::DMDGraph<K,V,R>::deleteNode( ", key, " )] "
                                           "Removing -(", relation->first, ")- dimension from [", node_it->first, "]" );
                            relation = node_it->second.relationships.erase( relation );
                        } else {
                            ++relation;
                        }
                    }
                }
            }
            //Removal of edges leaving the node to delete
            auto relationshipList = &node_to_delete->second.relationships;
            for( auto relation = relationshipList->begin(); relation != relationshipList->end(); ++relation ) {
                auto relation_count = _edge_count.find( relation->first );
                if( relation_count != _edge_count.end() ) {
                    //Removing from edge count the edges leaving node to delete and removing reference (not really needed but kept for logging purposes)
                    for( auto child = relation->second.childList.begin(); child != relation->second.childList.end(); ++child ) {
                        LOG_DEBUG( "[relations::DMDGraph<K,V,R>::deleteNode( ", key, " )] "
                            "Removing [", key, "]-(", relation->first, ")->[", *child, "] relationship." );
                        relation_count->second--;
                    }
                    //Removing relationship edge count tracker if 0
                    if( relation_count->second < 1 ) {
                        LOG_DEBUG( "[relations::DMDGraph<K,V,R>::deleteNode( ", key, " )] "
                            "Removing (", relation->first, ") dimension from the counter." );
                        relation_count = _edge_count.erase( relation_count );
                    }
                }
            }
            //Removal of the node
            _graph->erase( node_to_delete );
            LOG_DEBUG( "[relations::DMDGraph<K,V,R>::deleteNode( ", key, " )] Node deleted." );
            return true;
        }
    }

    /**
     * Creates a directed edge with the given relationship between two existing nodes
     * @param from         Origin node key
     * @param to           Destination node key
     * @param relationship Relationship type
     * @return Success
     */
    template<class K, class V, class R> bool DMDGraph<K,V,R>::createDirectedEdge( const K &from,
                                                                                  const K &to,
                                                                                  const R &relationship ) {
        //Error control
        auto from_it = _graph->find( from ); //Origin node iterator
        auto to_it   = _graph->find( to );   //Destination node iterator
        if( from_it == _graph->end() ) {
            LOG_ERROR( "[relations::Graph<K,V,R>::createDirectedEdge( ", from, ", ", to, ", ", relationship, " )] Origin key missing in graph." );
            return false;
        }
        if( to_it == _graph->end() ) {
            LOG_ERROR( "[relations::Graph<K,V,R>::createDirectedEdge( ", from, ", ", to, ", ", relationship, " )] Destination key missing in graph." );
            return false;
        }
        Relationships_t * from_relationships = &from_it->second.relationships;
        Relationships_t * to_relationships   = &to_it->second.relationships;
        //Adding relationship type not already there
        auto count_it = _edge_count.try_emplace( relationship, 0 ).first;
        //Adding relationship type to origin and destination nodes
        auto from_relationship_it = from_relationships->try_emplace( relationship, Adjacency() ).first;
        auto to_relationship_it   = to_relationships->try_emplace( relationship, Adjacency() ).first;
        //Checking/Inserting 'to' node in children list of 'from' in given relationship
        auto search_to = std::find( from_relationship_it->second.childList.begin(),
                                    from_relationship_it->second.childList.end(),
                                    to );
        if( search_to == from_relationship_it->second.childList.end() ) {
            from_relationship_it->second.childList.emplace_back( to );
        }
        //Checking/Inserting 'from' in parents list of 'to' in given relationship
        auto search_from = std::find( to_relationship_it->second.parentList.begin(),
                                      to_relationship_it->second.parentList.end(),
                                      from );
        if( search_from == to_relationship_it->second.parentList.end() ) {
            to_relationship_it->second.parentList.emplace_back( from );
        }
        //Updating edge counter for this relationship type
        count_it->second++;
        LOG_DEBUG( "[relations::DMDGraph<K,V,R>::createDirectedEdge( ", from, ", ", to, ", ", relationship, " )]" );
        return true;
    }

    /**
     * Creates a directed edge with the given relationship between on existing node and another to create
     * @param from         Origin node key
     * @param to           Destination node Key/Value pair to create
     * @param relationship Relationship type
     * @return Success
     */
    template<class K, class V, class R> bool DMDGraph<K,V,R>::createDirectedEdge( const K &from,
                                                                                  const std::pair<K, V> &to,
                                                                                  const R &relationship ) {
        LOG_DEBUG( "[relations::DMDGraph<K,V,R>::createDirectedEdge( ", from.first, ", ", "{ ", to.first, ", ", to.second, " }, ", relationship, " )]" );
        _graph->try_emplace( to.first, NodeData( to.second ) );
        return createDirectedEdge( from, to.first, relationship );
    }

    /**
     * Creates a directed edge with the given relationship between on existing node and another to create
     * @param from         Origin node Key/Value pair to create
     * @param to           Destination node key
     * @param relationship Relationship type
     * @return Success
     */
    template<class K, class V, class R> bool DMDGraph<K,V,R>::createDirectedEdge( const std::pair<K, V> from,
                                                                                  const K &to,
                                                                                  const R &relationship ) {
        LOG_DEBUG( "[relations::DMDGraph<K,V,R>::createDirectedEdge( { ", from.first, ", ", from.second, " }, ", to, ", ", relationship, " )]" );
        _graph->try_emplace( to.first, NodeData( to.second ) );
        return createDirectedEdge( from.first, to, relationship );
    }

    /**
     * Creates a directed edge with given relationship including the nodes
     * @param from         Origin node to create
     * @param to           Destination node to create
     * @param relationship Relationship type of the edge (link)
     * @return Success
     */
    template<class K, class V, class R> bool DMDGraph<K,V,R>::createDirectedEdge( const std::pair<K, V> &from,
                                                                                  const std::pair<K, V> &to,
                                                                                  const R &relationship ) {
        //Adding nodes and relationship type to edge counter
        auto from_it  = _graph->try_emplace( from.first, NodeData( from.second ) ).first;
        auto to_it    = _graph->try_emplace( to.first, NodeData( to.second ) ).first;
        auto count_it = _edge_count.try_emplace( relationship, 0 ).first;
        //Adding relationship type to origin and destination nodes
        auto from_relationship_it = from_it->second.relationships.try_emplace( relationship, Adjacency() ).first;
        auto to_relationship_it   = to_it->second.relationships.try_emplace( relationship, Adjacency() ).first;
        //Checking/Inserting 'to' node in children list of 'from' in given relationship
        auto search_to = std::find( from_relationship_it->second.childList.begin(),
                                    from_relationship_it->second.childList.end(),
                                    to.first );
        if( search_to == from_relationship_it->second.childList.end() ) {
            from_relationship_it->second.childList.emplace_back( to.first );
        }
        //Checking/Inserting 'from' in parents list of 'to' in given relationship
        auto search_from = std::find( to_relationship_it->second.parentList.begin(),
                                      to_relationship_it->second.parentList.end(),
                                      from.first );
        if( search_from == to_relationship_it->second.parentList.end() ) {
            to_relationship_it->second.parentList.emplace_back( from.first );
        }
        //Updating edge counter for this relationship type
        count_it->second++;
        LOG_DEBUG( "[relations::DMDGraph<K,V,R>::createDirectedEdge( { ", from.first, ", ", from.second, " }, { ", to.first, ", ", to.second, " }, ", relationship, " )]" );
        return true;
    }

    /**
     * Removes all directed edges between two nodes
     * @param from Origin node key
     * @param to   Destination node key
     * @return Success
     */
    template<class K, class V, class R> bool DMDGraph<K,V,R>::removeDirectedEdge( const K &from, const K &to ) {
        auto origin_it             = _graph->find( from );
        auto destination_it        = _graph->find( to );
        //Error control
        if( origin_it == _graph->end() ) {
            LOG_ERROR( "[relations::DMDGraph<K,V,R>::removeDirectedEdge( ", from, ", ", to, " )] Origin node does not exist." );
            return false;
        }
        if( destination_it == _graph->end() ) {
            LOG_ERROR( "[relations::DMDGraph<K,V,R>::removeDirectedEdge( ", from, ", ", to, " )] Destination node does not exist." );
            return false;
        }
        //Removing
        return ( removeChildren( origin_it, to ) && removeParents( destination_it, from ) );
    }

    /**
     * Removes a directed edge between two node of a particular relationship type
     * @param from         Origin node key
     * @param to           Destination node key
     * @param relationship Relationship type of the edge to delete
     * @return Success
     */
    template<class K, class V, class R> bool DMDGraph<K,V,R>::removeDirectedEdge( const K &from, const K &to, const R &relationship ) {
        auto origin_it             = _graph->find( from );
        auto destination_it        = _graph->find( to );
        auto relationship_count_it = _edge_count.find( relationship );
        //Error control
        if( origin_it == _graph->end() ) {
            LOG_ERROR( "[relations::DMDGraph<K,V,R>::removeDirectedEdge( ", from, ", ", to, ", ", relationship, " )] Origin node does not exist." );
            return false;
        }
        if( destination_it == _graph->end() ) {
            LOG_ERROR( "[relations::DMDGraph<K,V,R>::removeDirectedEdge( ", from, ", ", to, ", ", relationship, " )] Destination node does not exist." );
            return false;
        }
        if( relationship_count_it == _edge_count.end() ) {
            LOG_ERROR( "[relations::DMDGraph<K,V,R>::removeDirectedEdge( ", from, ", ", to, ", ", relationship, " )] Relationship type does not exist." );
            return false;
        }
        return ( removeChild( origin_it, to, relationship ) && removeParent( destination_it, from, relationship ) );
    }

    /**
     * Access value at node
     * @param key Node key
     * @return Value
     * @throws std::out_of_range when key is not in graph
     */
    template<class K, class V, class R> V & DMDGraph<K,V,R>::at( const K &key ) {
        try {
            return _graph->at( key ).value;
        } catch( std::out_of_range ) {
            LOG_ERROR( "[relations::DMDGraph<K,V,R>::at( ", key, " )] Key is not in graph's key:value map." );
            throw std::out_of_range( "[relations::DMDGraph<K,V,R>::at(..)] Key is not in graph's key:value map." );
        }
    }

    /**
     * Access value at node
     * @param key Node key
     * @return Value
     * std::out_of_range when key is not in graph
     */
    template<class K, class V, class R> const V & DMDGraph<K,V,R>::at( const K &key ) const {
        try {
            return _graph->at( key ).value;
        } catch( std::out_of_range ) {
            LOG_ERROR( "[relations::DMDGraph<K,V,R>::at( ", key, " ) const ] Key is not in graph's key:value map." );
            throw std::out_of_range( "[relations::DMDGraph<K,V,R>::at(..)] Key is not in graph's key:value map." );
        }
    }

    /**
     * Access a node in the graph
     * @param key Node key
     * @return Relationships of key
     * @throws std::out_of_range when key is not in graph
     */
    template<class K, class V, class R> const typename DMDGraph<K,V,R>::Relationships_t & DMDGraph<K,V,R>::getRelationships( const K &key ) const {
        try {
            return _graph->at( key ).relationships;
        } catch( std::out_of_range ) {
            LOG_ERROR( "[relations::DMDGraph<K,V,R>::getRelationships( ", key, " )] Key is not in graph." );
            throw std::out_of_range( "[relations::DMDGraph<K,V,R>::getRelationships(..)] Key is not in graph." );
        }
    }

    /**
     * Gets the list of nodes connected to from key in given relationship
     * @param key          Node key
     * @param relationship Relationship type
     * @return List of nodes connected to by this relationship
     * @throws std::out_of_range when key is not in graph
     */
    template<class K, class V, class R> const std::list<K> & DMDGraph<K,V,R>::getChildList( const K &key, const R &relationship ) {
        try {
            return _graph->at( key ).relationships.at( relationship ).childList;
        } catch( std::out_of_range ) {
            LOG_ERROR( "[relations::DMDGraph<K,V,R>::getChildList( ", key, ", ", relationship, " )] K/R is not in graph." );
            throw std::out_of_range( "[relations::DMDGraph<K,V,R>::getChildList(..)] K/R is not in graph." );
        }
    }

    /**
     * Gets the list of nodes connected to the key in given relationship
     * @param key          Node key
     * @param relationship Relationship type
     * @return List of nodes connected by this relationship
     * @throws std::out_of_range when key is not in graph
     */
    template<class K, class V, class R> const std::list<K> & DMDGraph<K,V,R>::getParentList( const K &key, const R &relationship ) {
        try {
            return _graph->at( key ).relationships.at( relationship ).parentList;
        } catch( std::out_of_range ) {
            LOG_ERROR( "[relations::DMDGraph<K,V,R>::getParentList( ", key, ", ", relationship, " )] K/R is not in graph." );
            throw std::out_of_range( "[relations::DMDGraph<K,V,R>::getParentList(..)] K/R is not in graph." );
        }
    }

    /**
     * Finds a node in the graph
     * @param key Node key
     * @return Iterator pointing to element in graph
     */
    template<class K, class V, class R> typename DMDGraph<K,V,R>::iterator DMDGraph<K,V,R>::find( const K &key ) {
        return _graph->find( key );
    }

    /**
     * Finds a node in the graph
     * @param key Node key
     * @return Const Iterator pointing to element in graph
     */
    template<class K, class V, class R> typename DMDGraph<K,V,R>::const_iterator DMDGraph<K,V,R>::find( const K &key ) const {
        return _graph->find( key );
    }

    /**
     * //TODO isReachable( from, to )
     * @param from
     * @param to
     * @return
     */
    template<class K, class V, class R> bool DMDGraph<K,V,R>::isReachable( const K &from, const K &to ) const {
        auto search_from = _graph->find( from );
        auto search_to   = _graph->find( to );
        //Error control
        if( search_from == _graph->end() ) {
            LOG_ERROR( "[relations::DMDGraph<K,V,R>::isReachable( ", from, ", ", to, ")] '", from, "' key does not exist in graph.");
            return false;
        }
        if( search_to == _graph->end() ) {
            LOG_ERROR( "[relations::DMDGraph<K,V,R>::isReachable( ", from, ", ", to, ")] '", to, "' key does not exist in graph.");
            return false;
        }
        if( from == to ) return true;
        //DFS lookup
        if( search_from->second.size() < 1 ) return false; //No child relationships
        for( auto relationship_it = search_from->second.begin(); relationship_it != search_from->end(); ++relationship_it ) {
            if( !relationship_it->second.childList.empty() ) {
                std::unordered_map<K,bool> visited;
                for( typename Graph_t::const_iterator it = _graph.cbegin(); it != _graph.cend(); ++it ) {
                    visited.emplace( it->first, false );
                }
                std::list<K> queue;
                queue.push_back( search_from->first );

                while( !queue.empty() ) {
                    K s = queue.front();
                    queue.pop_front();
                    auto search_node = _graph->find( s );
                    if( search_node == _graph->end() ) {
                        LOG_ERROR( "[eadlib::WeightedGraph<K>::isReachable( ", from, ", ", to, " )] Node '", s, "' does not exist, yet has a directed edge pointing to it." );
                        throw new eadlib::exception::corruption( "[eadlib::WeightedGraph<K>::isReachable(..)] A directed edge points to a non-existent node." );
                    }
                    for( auto i : search_node->second.childrenList ) {
                        if( i == to && search_node->second.weight.at( to ) > 0 ) return true;
                        if( !visited.at( i ) ) {
                            visited.at( i ) = true;
                            queue.push_back( i );
                        }
                    }
                }
            }
        }
        return false;
    }

    /**
     * Gets the existence of a key in the graph
     * @param key Key to check
     * @return Existence state of key
     */
    template<class K, class V, class R> bool DMDGraph<K,V,R>::exists( const K &key ) const {
        return ( _graph->find( key ) != _graph->end() );
    }

    /**
     * Gets the existence of a directed edge between two nodes in the graph
     * @param from Origin node key
     * @param to   Destination node key
     * @return Existence
     */
    template<class K, class V, class R> bool DMDGraph<K,V,R>::exists( const K &from, const K &to, const R &relationship ) const {
        auto origin_it       = _graph->find( from );
        auto destination_it  = _graph->find( to );
        if( origin_it == _graph->end() || destination_it == _graph->end() ) {
            return false;
        }
        auto relationship_it = origin_it->second.relationships.find( relationship );
        if( relationship_it == origin_it->second.relationships.end() ) {
            return false;
        }
        auto search = std::find( relationship_it->second.childList.begin(),
                                 relationship_it->second.childList.end(),
                                 to );
        return ( search != relationship_it->second.childList.end() );
    }

    /**
     * Checks if graph has no nodes
     * @return Empty (no nodes) state
     */
    template<class K, class V, class R> bool DMDGraph<K,V,R>::empty() const {
        return _graph->empty();
    }

    /**
     * Gets the number of nodes (vertices) in the graph
     * @return Number of nodes
     */
    template<class K, class V, class R> size_t DMDGraph<K,V,R>::nodeCount() const {
        return _graph->size();
    }

    /**
     * Gets the number of edges in the graph
     * @return number of edges
     */
    template<class K, class V, class R> size_t DMDGraph<K,V,R>::size() const {
        size_t sum { 0 };
        for( auto e : _edge_count ) {
            sum += e.second;
        }
        return sum;
    }

    /**
     * Gets the in-degree of a node
     * @param key Key of node
     * @return Number of edges connecting into the node
     * @throws std::out_of_range when key does not exist in graph
     */
    template<class K, class V, class R> size_t DMDGraph<K,V,R>::getInDegree( const K &key ) const {
        size_t sum { 0 };
        auto node_it = _graph->find( key );
        if( node_it == _graph->end() ) {
            LOG_ERROR( "[relations::DMDGraph<K,V,R>::getInDegree( ", key, " )] Key not found in graph." );
            throw std::out_of_range( "[relations::DMDGraph<K,V,R>::getInDegree(..)] Key not found in graph." );
        } else {
            for( auto relation : node_it->second.relationships ) {
                sum += relation.second.parentList.size();
            }
        }
        return sum;
    }

    /**
     * Gets the in-degree of a node from a relationship
     * @param key          Key of node
     * @param relationship Relationship
     * @return Number of edges connecting into the node from that relationship
     * @throws std::out_of_range when key does not exist in graph
     */
    template<class K, class V, class R> size_t DMDGraph<K,V,R>::getInDegree( const K &key, const R &relationship ) const {
        auto node_it = _graph->find( key );
        if( node_it == _graph->end() ) {
            LOG_ERROR( "[relations::DMDGraph<K,V,R>::getInDegree( ", key, ", ", relationship, " )] Key not found in graph." );
            throw std::out_of_range( "[relations::DMDGraph<K,V,R>::getInDegree(..)] Key not found in graph." );
        }
        auto relationship_it = node_it->second.relationships.find( relationship );
        if( relationship_it == node_it->second.relationships.end() ) {
            LOG_DEBUG( "[relations::DMDGraph<K,V,R>::getInDegree( ", key, ", ", relationship, " )] Relationship not found at key." );
            return 0;
        } else {
            return relationship_it->second.parentList.size();
        }
    }

    /**
     * Gets the out-degree of a node
     * @param key Key of node
     * @return Number of edges connecting out of the node
     * @throws std::out_of_range when key does not exist in graph
     */
    template<class K, class V, class R> size_t DMDGraph<K,V,R>::getOutDegree( const K &key ) const {
        size_t sum { 0 };
        auto node_it = _graph->find( key );
        if( node_it == _graph->end() ) {
            LOG_ERROR( "[relations::DMDGraph<K,V,R>::getOutDegree( ", key, " )] Key not found in graph." );
            throw std::out_of_range( "[relations::DMDGraph<K,V,R>::getOutDegree(..)] Key not found in graph." );
        } else {
            for( auto relation : node_it->second.relationships ) {
                sum += relation.second.childList.size();
            }
        }
        return sum;
    }

    /**
     * Gets the out-degree of a node from a relationship
     * @param key          Key of node
     * @param relationship Relationship
     * @return Number of edges connecting out the node from that relationship
     * @throws std::out_of_range when key does not exist in graph
     */
    template<class K, class V, class R> size_t DMDGraph<K,V,R>::getOutDegree( const K &key, const R &relationship ) const {
        auto node_it = _graph->find( key );
        if( node_it == _graph->end() ) {
            LOG_ERROR( "[relations::DMDGraph<K,V,R>::getOutDegree( ", key, ", ", relationship, " )] Key not found in graph." );
            throw std::out_of_range( "[relations::DMDGraph<K,V,R>::getInDegree(..)] Key not found in graph." );
        }
        auto relationship_it = node_it->second.relationships.find( relationship );
        if( relationship_it == node_it->second.relationships.end() ) {
            LOG_DEBUG( "[relations::DMDGraph<K,V,R>::getOutDegree( ", key, ", ", relationship, " )] Relationship not found at key." );
            return 0;
        } else {
            return relationship_it->second.childList.size();
        }
    }

    //-----------------------------------------------------------------------------------------------------------------
    // DMDGraph class private method implementations
    //-----------------------------------------------------------------------------------------------------------------
    /**
     * Removes all key reference in a particular relationship's child list in a node
     * @param node_it      Iterator of node to clear out
     * @param key          Key to remove any references of
     * @param relationship Relationship to clear
     * @return Success
     */
    template<class K, class V, class R> bool DMDGraph<K,V,R>::removeChild( typename Graph_t::iterator &node_it,
                                                                           const K &key,
                                                                           const R &relationship ) {
        bool flag { false };
        //Error control
        auto relation = node_it->second.relationships.find( relationship );
        if( relation == node_it->second.relationships.end() ) {
            LOG_ERROR( "[relations::DMDGraph<K,V,R>::removeChild( ", node_it->first, ", ", key, ", ", relationship, " )] "
                "Could not find relationship type in node [", node_it->first, "]." );
            return false;
        }
        auto           relation_count = _edge_count.find( relation->first );
        std::list<K> * childList      = &relation->second.childList;
        std::list<K> * parentList     = &relation->second.parentList;
        //Removing reference in current iterated node relationship's child list
        for( auto child = childList->begin(); child != childList->end(); ) {
            if( *child == key ) {
                LOG_DEBUG( "[relations::DMDGraph<K,V,R>::removeChild( ", node_it->first, ", ", key, " )] "
                    "Removing [", node_it->first, "]-(", relation->first, ")->[", key, "] relationship." );
                child = relation->second.childList.erase( child );
                relation_count->second--;
                flag = true;
            } else {
                ++child;
            }
        }
        //Removing relationship type in the node if not used anymore
        if( childList->empty() && parentList->empty() ) {
            LOG_DEBUG( "[relations::DMDGraph<K,V,R>::removeChild( ", node_it->first, ", ", key, " )] "
                "Removing -(", relation->first, ")- dimension from [", node_it->first, "]" );
            relation = node_it->second.relationships.erase( relation );
        }
        //Removing relationship edge count tracker if 0
        if( relation_count->second < 1 ) {
            LOG_DEBUG( "[relations::DMDGraph<K,V,R>::removeChild( ", node_it->first, ", ", key, " )] "
                "Removing (", relation_count->first, ") dimension from the counter." );
            relation_count = _edge_count.erase( relation_count );
        }
        return flag;
    }

    /**
     * Removes all key reference in a particular relationship's parent list in a node
     * @param node_it      Iterator of node to clear out
     * @param key          Key to remove any references of
     * @param relationship Relationship to clear
     * @return Success
     */
    template<class K, class V, class R> bool DMDGraph<K,V,R>::removeParent( typename Graph_t::iterator &node_it,
                                                                            const K &key,
                                                                            const R &relationship ) {
        bool flag { false };
        //Error control
        auto relation = node_it->second.relationships.find( relationship );
        if( relation == node_it->second.relationships.end() ) {
            LOG_ERROR( "[relations::DMDGraph<K,V,R>::removeParent( ", node_it->first, ", ", key, ", ", relationship, " )] "
                "Could not find relationship type in node [", node_it->first, "]." );
            return false;
        }
        auto           relation_count = _edge_count.find( relation->first );
        std::list<K> * childList      = &relation->second.childList;
        std::list<K> * parentList     = &relation->second.parentList;
        //Removing reference in current iterated node relationship's parent list
        for( auto parent = parentList->begin(); parent != parentList->end(); ) {
            if( *parent == key ) {
                parent = parentList->erase( parent );
                LOG_DEBUG( "[relations::DMDGraph<K,V,R>::removeParent( ", node_it->first, ", ", key, ", ", relationship, " )] "
                    "Removing [", key, "]<-(", relation->first, ")-[", node_it->first, "] relationship." );
                flag = true;
            } else {
                ++parent;
            }
        }
        //Removing relationship type in the node if not used anymore
        if( childList->empty() && parentList->empty() ) {
            LOG_DEBUG( "[relations::DMDGraph<K,V,R>::removeParent( ", node_it->first, ", ", key, ", ", relationship, " )] "
                "Removing -(", relation->first, ")- dimension from [", node_it->first, "]" );
            relation = node_it->second.relationships.erase( relation );
        }
        return flag;
    }

    /**
     * Removes all key reference in all relationship's children lists in a node
     * @param node_it Iterator of node to clear out
     * @param key     Key to remove any references of
     * @return Number of key references removed
     */
    template<class K, class V, class R> size_t DMDGraph<K,V,R>::removeChildren( typename Graph_t::iterator &node_it,
                                                                                const K &key ) {
        size_t count { 0 };
        for( auto relation = node_it->second.relationships.begin(); relation != node_it->second.relationships.end(); ) {
            auto           relation_count = _edge_count.find( relation->first );
            std::list<K> * childList      = &relation->second.childList;
            std::list<K> * parentList     = &relation->second.parentList;
            //Removing reference in current iterated node relationship's child list
            for( auto child = childList->begin(); child != childList->end(); ) {
                if( *child == key ) {
                    LOG_DEBUG( "[relations::DMDGraph<K,V,R>::removeChildren( ", node_it->first, ", ", key, " )] "
                        "Removing [", node_it->first, "]-(", relation->first, ")->[", key, "] relationship." );
                    child = childList->erase( child );
                    relation_count->second--;
                    count++;
                } else {
                    ++child;
                }
            }
            //Removing relationship type in current node if not used anymore
            if( childList->empty() && parentList->empty() ) {
                LOG_DEBUG( "[relations::DMDGraph<K,V,R>::removeChildren( ", node_it->first, ", ", key, " )] "
                    "Removing -(", relation->first, ")- dimension from [", node_it->first, "]" );
                relation = node_it->second.relationships.erase( relation );
            } else {
                ++relation;
            }
            //Removing relationship edge count tracker if 0
            if( relation_count->second < 1 ) {
                LOG_DEBUG( "[relations::DMDGraph<K,V,R>::removeChildren( ", node_it->first, ", ", key, " )] "
                    "Removing (", relation_count->first, ") dimension from the counter." );
                relation_count = _edge_count.erase( relation_count );
            }
        }
        return count;
    }

    /**
     * Removes all key reference in all relationship's parent lists in a node
     * @param node_it Iterator of node to clear out
     * @param key     Key to remove any references of
     * @return Number of key references removed
     */
    template<class K, class V, class R> size_t DMDGraph<K,V,R>::removeParents( typename Graph_t::iterator &node_it,
                                                                               const K &key ) {
        size_t count { 0 };
        for( auto relation = node_it->second.relationships.begin(); relation != node_it->second.relationships.end(); ) {
            auto relation_count = _edge_count.find( relation->first );
            std::list<K> *childList = &relation->second.childList;
            std::list<K> *parentList = &relation->second.parentList;
            //Removing reference in current iterated node relationship's parent list
            for( auto parent = parentList->begin(); parent != parentList->end(); ) {
                if( *parent == key ) {
                    parent = parentList->erase( parent );
                    LOG_DEBUG( "[relations::DMDGraph<K,V,R>::removeParents( ", node_it->first, ", ", key, " )] "
                        "Removing [", key, "]<-(", relation->first, ")-[", node_it->first, "] relationship." );
                    count++;
                } else {
                    ++parent;
                }
            }
            //Removing relationship type in current node if not used anymore
            if( childList->empty() && parentList->empty() ) {
                LOG_DEBUG( "[relations::DMDGraph<K,V,R>::removeParents( ", node_it->first, ", ", key, " )] "
                    "Removing -(", relation->first, ")- dimension from [", node_it->first, "]" );
                relation = node_it->second.relationships.erase( relation );
            } else {
                ++relation;
            }
        }
        return count;
    }
}
#endif //RELATIONS_GRAPH3_H
