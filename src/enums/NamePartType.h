#ifndef RELATIONS_NAMEPARTTYPE_H
#define RELATIONS_NAMEPARTTYPE_H

#include <eadlib/logger/Logger.h>

namespace relations::enums {
    enum class NamePartType {
        PREFIX          = 1,
        SUFFIX          = 2,
        FIRST           = 3,
        MIDDLE          = 4,
        SURNAME         = 5
    };

    /**
     * Converts a base value to a NamePartType enum
     * @param val Value to convert
     * @return Converted value
     * throws std::invalid_argument when value does not match any of the enumerated types
     */
    inline NamePartType to_NamePartType( const int64_t &val ) {
        auto cast = static_cast<NamePartType>( val );
        switch( cast ) {
            case NamePartType::PREFIX:
                return NamePartType::PREFIX;
            case NamePartType::SUFFIX:
                return NamePartType::SUFFIX;
            case NamePartType::FIRST:
                return NamePartType::FIRST;
            case NamePartType::MIDDLE:
                return NamePartType::MIDDLE;
            case NamePartType::SURNAME:
                return NamePartType::SURNAME;
            default:
                LOG_ERROR( "[relations::enums::to_NamePartType( ", val, " )] Value is not valid for the enumerated type." );
                throw std::invalid_argument( "Value not valid for the enumerated type." );
        }
    }
}

#endif //RELATIONS_NAMEPARTTYPE_H
