#ifndef RELATIONS_GENDERBIOTYPE_H
#define RELATIONS_GENDERBIOTYPE_H

#include <eadlib/logger/Logger.h>

namespace relations::enums {
    enum GenderBioType {
        MALE            = 1,
        FEMALE          = 2,
        INTERSEX        = 3,
        UNKNOWN         = 4
    };

    /**
     * Converts a base value to a GenderBioType enum
     * @param val Value to convert
     * @return Converted value
     * throws std::invalid_argument when value does not match any of the enumerated types
     */
    inline GenderBioType to_GenderBioType( const int64_t &val ) {
        auto cast = static_cast<GenderBioType>( val );
        switch( cast ) {
            case GenderBioType::MALE:
                return GenderBioType::MALE;
            case GenderBioType::FEMALE:
                return GenderBioType::FEMALE;
            case GenderBioType::INTERSEX:
                return GenderBioType::INTERSEX;
            case GenderBioType::UNKNOWN:
                return GenderBioType::UNKNOWN;
            default:
                LOG_ERROR( "[relations::enums::to_GenderBioType( ", val, " )] Value is not valid for the enumerated type." );
                throw std::invalid_argument( "Value not valid for the enumerated type." );
        }
    }
}

#endif //RELATIONS_GENDERBIOTYPE_H
