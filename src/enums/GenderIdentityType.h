#ifndef RELATIONS_GENDERIDENTITYTYPE_H
#define RELATIONS_GENDERIDENTITYTYPE_H

#include <eadlib/logger/Logger.h>

namespace relations::enums {
    enum GenderIdentityType {
        UNSPECIFIED     = 1, //Defaults on biological gender in this case
        AGENDER         = 2, //Genderless
        ANDROGYNE       = 3,
        BIGENDER        = 4,
        NON_BINARY      = 5, //Gender-queer
        GENDER_BENDER   = 6,
        HIJRA           = 7,
        PANGENDER       = 8,
        QUEER_HETERO    = 9,
        THIRD_GENDER    = 10,
        TRANS_MAN       = 11,
        TANS_WOMAN      = 12,
        TRANS_MASCULINE = 13,
        TRANS_FEMININE  = 14,
        TRIGENDER       = 15,
        TWO_SPIRIT      = 16,
    };

    /**
     * Converts a base value to a GenderIdentityType enum
     * @param val Value to convert
     * @return Converted value
     * throws std::invalid_argument when value does not match any of the enumerated types
     */
    inline GenderIdentityType to_GenderIdentityType( const int64_t &val ) {
        auto cast = static_cast<GenderIdentityType>( val );
        switch( cast ) {
            case GenderIdentityType::UNSPECIFIED:
                return GenderIdentityType::UNSPECIFIED;
            case GenderIdentityType::AGENDER:
                return GenderIdentityType::AGENDER;
            case GenderIdentityType::ANDROGYNE:
                return GenderIdentityType::ANDROGYNE;
            case GenderIdentityType::BIGENDER:
                return GenderIdentityType::BIGENDER;
            case GenderIdentityType::NON_BINARY:
                return GenderIdentityType::NON_BINARY;
            case GenderIdentityType::GENDER_BENDER:
                return GenderIdentityType::GENDER_BENDER;
            case GenderIdentityType::HIJRA:
                return GenderIdentityType::HIJRA;
            case GenderIdentityType::PANGENDER:
                return GenderIdentityType::PANGENDER;
            case GenderIdentityType::QUEER_HETERO:
                return GenderIdentityType::QUEER_HETERO;
            case GenderIdentityType::THIRD_GENDER:
                return GenderIdentityType::THIRD_GENDER;
            case GenderIdentityType::TRANS_MAN:
                return GenderIdentityType::TRANS_MAN;
            case GenderIdentityType::TANS_WOMAN:
                return GenderIdentityType::TANS_WOMAN;
            case GenderIdentityType::TRANS_MASCULINE:
                return GenderIdentityType::TRANS_MASCULINE;
            case GenderIdentityType::TRANS_FEMININE:
                return GenderIdentityType::TRANS_FEMININE;
            case GenderIdentityType::TRIGENDER:
                return GenderIdentityType::TRIGENDER;
            case GenderIdentityType::TWO_SPIRIT:
                return GenderIdentityType::TWO_SPIRIT;
            default:
                LOG_ERROR( "[relations::enums::to_GenderIdentityType( ", val, " )] Value is not valid for the enumerated type." );
                throw std::invalid_argument( "Value not valid for the enumerated type." );
        }
    }
}

#endif //RELATIONS_GENDERIDENTITYTYPE_H
