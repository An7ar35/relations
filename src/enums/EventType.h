#ifndef RELATIONS_EVENTTYPE_H
#define RELATIONS_EVENTTYPE_H

#include <eadlib/logger/Logger.h>

namespace relations::enums {
    enum class EventType {
        ADOPTION        = 1,
        BIRTH           = 2,
        BURIAL          = 3,
        CENSUS          = 4,
        CHRISTENING     = 5,
        DEATH           = 6,
        DIVORCE         = 7,
        MARRIAGE        = 8,
        OTHER           = 9 //Extends into custom tags (Custom event types)
    };

    /**
     * Converts a base value to a EventType enum
     * @param val Value to convert
     * @return Converted value
     */
    inline EventType to_EventType( const int64_t &val ) {
        auto cast = static_cast<EventType>( val );
        switch( cast ) {
            case EventType::ADOPTION:
                return EventType::ADOPTION;
            case EventType::BIRTH:
                return EventType::BIRTH;
            case EventType::BURIAL:
                return EventType::BURIAL;
            case EventType::CENSUS:
                return EventType::CENSUS;
            case EventType::CHRISTENING:
                return EventType::CHRISTENING;
            case EventType::DEATH:
                return EventType::DEATH;
            case EventType::DIVORCE:
                return EventType::DIVORCE;
            case EventType::MARRIAGE:
                return EventType::MARRIAGE;
            default:
                return EventType::OTHER;
        }
    }
}

#endif //RELATIONS_EVENTTYPE_H
