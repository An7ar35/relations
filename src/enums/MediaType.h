#ifndef RELATIONS_MEDIATYPE_H
#define RELATIONS_MEDIATYPE_H

#include <eadlib/logger/Logger.h>

namespace relations::enums {
    enum class MediaType {
        PHOTO           = 1,
        AUDIO           = 2,
        VIDEO           = 3,
        DOCUMENT        = 4
    };

    /**
 * Converts a base value to a MediaType enum
 * @param val Value to convert
 * @return Converted value
 * throws std::invalid_argument when value does not match any of the enumerated types
 */
    inline MediaType to_MediaType( const int64_t &val ) {
        auto cast = static_cast<MediaType>( val );
        switch( cast ) {
            case MediaType::PHOTO:
                return MediaType::PHOTO;
            case MediaType::AUDIO:
                return MediaType::AUDIO;
            case MediaType::VIDEO:
                return MediaType::VIDEO;
            case MediaType::DOCUMENT:
                return MediaType::DOCUMENT;
            default:
                LOG_ERROR( "[relations::enums::to_MediaType( ", val, " )] Value is not valid for the enumerated type." );
                throw std::invalid_argument( "Value not valid for the enumerated type." );
        }
    }
}

#endif //RELATIONS_MEDIATYPE_H
