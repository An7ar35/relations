#ifndef RELATIONS_NAMETYPE_H
#define RELATIONS_NAMETYPE_H

#include <eadlib/logger/Logger.h>

namespace relations::enums {
    enum class NameType {
        BIRTHNAME       = 1,
        MARRIEDNAME     = 2,
        ALSOKNOWNAS     = 3,
        NICKNAME        = 4,
        ADOPTIVENAME    = 5,
        FORMALNAME      = 6,
        RELIGIOUSNAME   = 7
    };

    /**
     * Converts a base value to a NameType enum
     * @param val Value to convert
     * @return Converted value
     * throws std::invalid_argument when value does not match any of the enumerated types
     */
    inline NameType to_NameType( const int64_t &val ) {
        auto cast = static_cast<NameType>( val );
        switch( cast ) {
            case NameType::BIRTHNAME:
                return NameType::BIRTHNAME;
            case NameType::MARRIEDNAME:
                return NameType::MARRIEDNAME;
            case NameType::ALSOKNOWNAS:
                return NameType::ALSOKNOWNAS;
            case NameType::NICKNAME:
                return NameType::NICKNAME;
            case NameType::ADOPTIVENAME:
                return NameType::ADOPTIVENAME;
            case NameType::FORMALNAME:
                return NameType::FORMALNAME;
            case NameType::RELIGIOUSNAME:
                return NameType::RELIGIOUSNAME;
            default:
                LOG_ERROR( "[relations::enums::to_NameType( ", val, " )] Value is not valid for the enumerated type." );
                throw std::invalid_argument( "Value not valid for the enumerated type." );
        }
    }
}

#endif //RELATIONS_NAMETYPE_H
