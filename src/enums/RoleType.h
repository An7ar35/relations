#ifndef RELATIONS_ROLETYPE_H
#define RELATIONS_ROLETYPE_H

#include <eadlib/logger/Logger.h>

namespace relations::enums {
    enum class RoleType {
        PRINCIPAL       = 1,
        PARTICIPANT     = 2,
        OFFICIAL        = 3,
        WITNESS         = 4
    };

    /**
     * Converts a base value to a RoleType enum
     * @param val Value to convert
     * @return Converted value
     * throws std::invalid_argument when value does not match any of the enumerated types
     */
    inline RoleType to_RoleType( const int64_t &val ) {
        auto cast = static_cast<RoleType>( val );
        switch( cast ) {
            case RoleType::PRINCIPAL:
                return RoleType::PRINCIPAL;
            case RoleType::PARTICIPANT:
                return RoleType::PARTICIPANT;
            case RoleType::OFFICIAL:
                return RoleType::OFFICIAL;
            case RoleType::WITNESS:
                return RoleType::WITNESS;
            default:
                LOG_ERROR( "[relations::enums::to_RoleType( ", val, " )] Value is not valid for the enumerated type." );
                throw std::invalid_argument( "Value not valid for the enumerated type." );
        }
    }
}

#endif //RELATIONS_ROLETYPE_H