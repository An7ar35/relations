#ifndef RELATIONS_ENUMERATEDTYPES_H
#define RELATIONS_ENUMERATEDTYPES_H

#include "EventType.h"
#include "GenderBioType.h"
#include "GenderIdentityType.h"
#include "MediaType.h"
#include "NameType.h"
#include "NamePartType.h"
#include "RoleType.h"

#endif //RELATIONS_ENUMERATEDTYPES_H
