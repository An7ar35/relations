#ifndef RELATIONS_GENERICTYPE_H
#define RELATIONS_GENERICTYPE_H

#include <string>
#include <ostream>

namespace relations::dto {
    struct GenericType {
        GenericType( const int64_t &id,
                     const std::string &value ) :
            _id( id ),
            _value( value )
        {};

        GenericType( const std::string &value ) :
            _id( -1 ),
            _value( value )
        {};

        friend std::ostream &operator <<( std::ostream &output, relations::dto::GenericType const &type ) {
            output << type._value;
            return output;
        }

        virtual bool operator ==( const GenericType &rhs ) const {
            if( _id < 0 || rhs._id < 0 )
                return _value == rhs._value;
            else
                return _id == rhs._id && _value == rhs._value;
        }

        int64_t     _id;
        std::string _value;
    };
}

#endif //RELATIONS_GENERICTYPE_H
