#ifndef RELATIONS_NOTE_H
#define RELATIONS_NOTE_H

#include <string>

namespace relations::dto {
    struct Note {
        Note( const std::string &subject, const std::string &text ) :
            _id( -1 ), _subject( subject ), _text( text )
        {};

        Note( const int64_t &id, const std::string &subject, const std::string &text ) :
            _id( id ), _subject( subject ), _text( text )
        {};

        friend std::ostream &operator <<( std::ostream &output, relations::dto::Note const &note ) {
            output << "Note [" << note._id << "]\n"
                   << "· Subject: " << note._subject << "\n"
                   << "· Text   : \"" << note._text << "\"";
            return output;
        }

        bool operator ==( const Note &rhs ) const {
            return _id == rhs._id
                   && _subject == rhs._subject
                   && _text == rhs._text;
        }

        bool operator !=( const Note &rhs ) const {
            return !( *this == rhs );
        }

        int64_t     _id;
        std::string _subject; //note's heading
        std::string _text;    //note's text
    };
}

#endif //RELATIONS_NOTE_H
