#ifndef RELATIONS_NAMEPART_H
#define RELATIONS_NAMEPART_H

#include <string>
#include <eadlib/datatype/enum.h>
#include <src/enums/EnumeratedTypes.h>

namespace relations::dto {
    struct NamePart {
        NamePart( const std::string &value,
                  const enums::NamePartType &type ) :
            _id( -1 ),
            _value( value ),
            _type( type )
        {};

        NamePart( const int64_t &id,
                  const std::string &value,
                  const enums::NamePartType &type ) :
            _id( id ),
            _value( value ),
            _type( type )
        {};

        NamePart( const NamePart &name_part ) :
            _id( name_part._id ),
            _value( name_part._value ),
            _type( name_part._type )
        {};

        NamePart( NamePart &&name_part ) :
            _id( std::move( name_part._id ) ),
            _value( std::move( name_part._value ) ),
            _type( std::move( name_part._type ) )
        {};

        NamePart & operator =( const NamePart &name_part ) {
            _id    = name_part._id;
            _value = name_part._value;
            _type  = name_part._type;
            return *this;
        }

        NamePart & operator =( NamePart &&name_part ) {
            _id    = std::move( name_part._id );
            _value = std::move( name_part._value );
            _type  = std::move( name_part._type );
            return *this;
        }

        friend std::ostream &operator <<( std::ostream &output, relations::dto::NamePart const &name_part ) {
            output << "NamePart [" << std::to_string( name_part._id ) << "]\n"
                   << "· Value : " << name_part._value << "\n"
                   << "· Type  : [" << eadlib::to_string<enums::NamePartType>( name_part._type ) << "]";
            return output;
        };

        int64_t             _id;
        std::string         _value;
        enums::NamePartType _type;
    };
}

#endif //RELATIONS_NAMEPART_H
