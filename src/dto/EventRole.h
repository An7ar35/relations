#ifndef RELATIONS_EVENTROLE_H
#define RELATIONS_EVENTROLE_H

#include <string>
#include <ostream>
#include <eadlib/datatype/enum.h>

#include "src/enums/EnumeratedTypes.h"

namespace relations::dto {
    struct EventRole {
        EventRole( const int64_t &event_id,
                   const int64_t &person_id,
                   const enums::RoleType &type,
                   const std::string &details ) :
            _event_id( event_id ),
            _person_id( person_id ),
            _type( type ),
            _details( details )
        {};

        EventRole( const int64_t &event_id,
                   const int64_t &person_id,
                   const enums::RoleType &type ) :
            _event_id( event_id ),
            _person_id( person_id ),
            _type( type ),
            _details( "" )
        {};

        friend std::ostream &operator <<( std::ostream &output, relations::dto::EventRole const &role ) {
            output << "EventRole\n"
                   << "· Event ID : [" << role._event_id << "]\n"
                   << "· Person ID: [" << role._person_id << "]\n"
                   << "· Type.....: [" << eadlib::to_string<enums::RoleType>( role._type ) << "]";
            if( !role._details.empty() )
                output << "\n· Details..: " << role._details;
            return output;
        }

        bool operator ==( const EventRole &rhs ) const {
            return _event_id == rhs._event_id
                   && _person_id == rhs._person_id
                   && _type == rhs._type
                   && _details == rhs._details;
        }

        bool operator !=( const EventRole &rhs ) const {
            return !( *this == rhs );
        }

        int64_t         _event_id;
        int64_t         _person_id;
        enums::RoleType _type;
        std::string     _details;
    };
}

#endif //RELATIONS_EVENTROLE_H
