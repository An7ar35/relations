#ifndef RELATIONS_ENUMTYPE_H
#define RELATIONS_ENUMTYPE_H

#include "GenericType.h"

namespace relations::dto {
    template<class Enum> struct EnumType : GenericType {
        EnumType( const int64_t &id,
                  const Enum &type,
                  const std::string &value ) :
            GenericType( id, value ),
            _type( type )
        {};

        friend std::ostream &operator <<( std::ostream &output, relations::dto::EnumType<Enum> const &type ) {
            output << type._value;
            return output;
        }

        bool operator ==( const EnumType<Enum> &rhs ) const {
            if( _id < 0 || rhs._id < 0 )
                return _value == rhs._value;
            else
                return _id == rhs._id
                       && _value == rhs._value
                       && _type == rhs._type;
        }

        Enum _type;
    };
}

#endif //RELATIONS_ENUMTYPE_H
