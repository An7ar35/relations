#ifndef RELATIONS_SUBJECT_H
#define RELATIONS_SUBJECT_H

#include <memory>
#include "Note.h"

namespace relations::dto {
    struct Subject {
        Subject() :
            _subject_id( -1 ),
            _note( nullptr )
        {};

        Subject( Note *note ) :
            _subject_id( -1 ),
            _note( std::unique_ptr<Note>( note ) )
        {};

        Subject( const int64_t &id ) :
            _subject_id( id ),
            _note( nullptr )
        {};

        Subject( const int64_t &id,
                 Note *note ) :
            _subject_id( id ),
            _note( std::unique_ptr<Note>( note ) )
        {};

        Subject( const Subject &subject ) :
            _subject_id( subject._subject_id )
        {
            if( subject._note ) {
                _note = std::make_unique<Note>( subject._note->_id,
                                                subject._note->_subject,
                                                subject._note->_text );
            }
        }

        Subject( Subject &&subject ) :
            _subject_id( subject._subject_id ),
            _note( std::move( subject._note ) )
        {};

        virtual ~Subject() {};

        Subject & operator =( const Subject &subject ) {
            _subject_id = subject._subject_id;
            if( subject._note ) {
                _note = std::make_unique<Note>( subject._note->_id,
                                                subject._note->_subject,
                                                subject._note->_text );
            }
            return *this;
        };

        Subject & operator =( Subject &&subject ) {
            _subject_id = subject._subject_id;
            _note       = std::move( subject._note );
            return *this;
        };

        friend std::ostream &operator <<( std::ostream &output, relations::dto::Subject const &subject ) {
            output << "Subject [" << subject._subject_id << "]\n";
            if( subject._note ) {
                output << "Attached " << *subject._note;
            }
            return output;
        }

        bool operator ==( const Subject &rhs ) const {
            if( _note && rhs._note ) { //Both have notes
                return _subject_id == rhs._subject_id
                       && *_note == *rhs._note;
            }
            if( !_note && !rhs._note ) { //Neither have notes
                return _subject_id == rhs._subject_id;
            }
            return false;
        }

        bool operator !=( const Subject &rhs ) const {
            return !( *this == rhs );
        }

        int64_t                _subject_id;
        std::unique_ptr<Note>  _note;
    };
}

#endif //RELATIONS_SUBJECT_H
