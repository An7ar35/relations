#ifndef RELATIONS_RELATIONSHIP_H
#define RELATIONS_RELATIONSHIP_H

#include <string>
#include <ostream>
#include "RelationshipType.h"

namespace relations::dto {
    struct Relationship : public Subject {
        Relationship( const int64_t &person1,
                      const int64_t &person2,
                      const int64_t &type ) :
            Subject(),
            _person1( person1 ),
            _person2( person2 ),
            _type_id( type )
        {};

        Relationship( const int64_t &person1,
                      const int64_t &person2,
                      const int64_t &type,
                      Note *note ) :
            Subject( note ),
            _person1( person1 ),
            _person2( person2 ),
            _type_id( type )
        {};

        Relationship( const int64_t &subject_id,
                      const int64_t &person1,
                      const int64_t &person2,
                      const int64_t &type ) :
            Subject( subject_id ),
            _person1( person1 ),
            _person2( person2 ),
            _type_id( type )
        {};

        Relationship( const int64_t &subject_id,
                      const int64_t &person1,
                      const int64_t &person2,
                      const int64_t &type,
                      Note *note ) :
            Subject( subject_id, note ),
            _person1( person1 ),
            _person2( person2 ),
            _type_id( type )
        {};

        Relationship( const Relationship &relationship ) :
            Subject( relationship ),
            _person1( relationship._person1 ),
            _person2( relationship._person2 ),
            _type_id( relationship._type_id )
        {}

        Relationship( Relationship &&relationship ) :
            Subject( std::move( relationship ) ),
            _person1( std::move( relationship._person1 ) ),
            _person2( std::move( relationship._person2 ) ),
            _type_id( std::move( relationship._type_id ) )
        {};

        Relationship & operator =( const Relationship &relationship ) {
            Subject::operator=( relationship );
            _person1 = relationship._person1;
            _person2 = relationship._person2;
            _type_id = relationship._type_id;
            return *this;
        }

        Relationship & operator =( Relationship &&relationship ) {
            Subject::operator=( std::move( relationship ) );
            _person1 = std::move( relationship._person1 );
            _person2 = std::move( relationship._person2 );
            _type_id = std::move( relationship._type_id );
            return *this;
        }

        friend std::ostream &operator <<( std::ostream &output, relations::dto::Relationship const &relationship ) {
            output << "Relationship [" << std::to_string( relationship._person1 ) << "]->[" << std::to_string( relationship._person2 ) << "]\n"
                   << "Subject ID: [" << std::to_string( relationship._subject_id ) << "]"
                   << "Type ID   : [" << std::to_string( relationship._type_id ) << "]";
            if( relationship._note )
                output << "\n Attached " << *relationship._note;
            return output;
        }

        int64_t _person1;
        int64_t _person2;
        int64_t _type_id;
    };
}

#endif //RELATIONS_RELATIONSHIP_H
