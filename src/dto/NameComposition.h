#ifndef RELATIONS_NAMECOMPOSITION_H
#define RELATIONS_NAMECOMPOSITION_H

#include <string>
#include <ostream>

namespace relations::dto {
    struct NameComposition {
        NameComposition( const int64_t &name_form_id,
                         const int64_t &name_part_id ) :
            _form_id( name_form_id ),
            _part_id( name_part_id )
        {};

        friend std::ostream &operator <<( std::ostream &output, relations::dto::NameComposition const &name_composition ) {
            output << "{ " << name_composition._form_id << ", " << name_composition._part_id << " }";
            return output;
        };

        int64_t _form_id;
        int64_t _part_id;
    };
}

#endif //RELATIONS_NAMECOMPOSITION_H
