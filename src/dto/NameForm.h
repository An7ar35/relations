#ifndef RELATIONS_NAMEFORM_H
#define RELATIONS_NAMEFORM_H

#include <string>
#include <ostream>
#include <vector>
#include "NamePart.h"

namespace relations::dto {
    struct NameForm {
        NameForm( const int64_t &id,
                  const std::string &language,
                  const std::string &full_text ) :
            _id( id ),
            _language( language ),
            _full_text( full_text )
        {};

        NameForm( const std::string &language,
                  const std::string &full_text ) :
            _id( -1 ),
            _language( language ),
            _full_text( full_text )
        {};

        NameForm( const NameForm &name_form ) :
            _id( name_form._id ),
            _language( name_form._language ),
            _full_text( name_form._full_text )
        {
            std::copy ( _name_parts_prefix.begin(),
                        _name_parts_prefix.end(),
                        std::back_inserter( _name_parts_prefix )
            );
            std::copy ( _name_parts_first.begin(),
                        _name_parts_first.end(),
                        std::back_inserter( _name_parts_first )
            );
            std::copy ( _name_parts_middle.begin(),
                        _name_parts_middle.end(),
                        std::back_inserter( _name_parts_middle )
            );
            std::copy ( _name_parts_surname.begin(),
                        _name_parts_surname.end(),
                        std::back_inserter( _name_parts_surname )
            );
            std::copy ( _name_parts_suffix.begin(),
                        _name_parts_suffix.end(),
                        std::back_inserter( _name_parts_suffix )
            );
        }

        NameForm( NameForm &&name_form ) :
            _id( std::move( name_form._id ) ),
            _language( std::move( name_form._language ) ),
            _full_text( std::move( name_form._full_text ) ),
            _name_parts_prefix( std::move( name_form._name_parts_prefix ) ),
            _name_parts_first( std::move( name_form._name_parts_first ) ),
            _name_parts_middle( std::move( name_form._name_parts_middle ) ),
            _name_parts_surname( std::move( name_form._name_parts_surname ) ),
            _name_parts_suffix( std::move( name_form._name_parts_suffix ) )
        {}

        NameForm & operator =( const NameForm &name_form ) {
            _id                 = name_form._id;
            _language           = name_form._language;
            _full_text          = name_form._full_text;
            _name_parts_prefix  = name_form._name_parts_prefix;
            _name_parts_first   = name_form._name_parts_first;
            _name_parts_middle  = name_form._name_parts_middle;
            _name_parts_surname = name_form._name_parts_middle;
            _name_parts_suffix  = name_form._name_parts_suffix;
            return *this;
        }

        NameForm & operator =( NameForm &&name_form ) {
            _id        = std::move( name_form._id );
            _language  = std::move( name_form._language );
            _full_text = std::move( name_form._full_text );
            _name_parts_prefix  = std::move( name_form._name_parts_prefix );
            _name_parts_first   = std::move( name_form._name_parts_first );
            _name_parts_middle  = std::move( name_form._name_parts_middle );
            _name_parts_surname = std::move( name_form._name_parts_surname );
            _name_parts_suffix  = std::move( name_form._name_parts_suffix );
            return *this;
        }

        friend std::ostream &operator <<( std::ostream &output, relations::dto::NameForm const &name_form ) {
            output << "NameForm [" << std::to_string( name_form._id ) << "]\n"
                   << "· Language  : " << name_form._language << "\n"
                   << "· Full text : " << name_form._full_text;
            if( !name_form._name_parts_prefix.empty() ) {
                output << "\n» Prefix         : \n";
                for( auto e : name_form._name_parts_prefix ) {
                    output << e << " ";
                }
            }
            if( !name_form._name_parts_first.empty() ) {
                output << "\n» First name(s)  : \n";
                for( auto e : name_form._name_parts_first ) {
                    output << e << " ";
                }
            }
            if( !name_form._name_parts_middle.empty() ) {
                output << "\n» Middle name(s) : \n";
                for( auto e : name_form._name_parts_middle ) {
                    output << e << " ";
                }
            }
            if( !name_form._name_parts_surname.empty() ) {
                output << "\n» Surname        : \n";
                for( auto e : name_form._name_parts_surname ) {
                    output << e << " ";
                }
            }
            if( !name_form._name_parts_suffix.empty() ) {
                output << "\n» Suffix         : \n";
                for( auto e : name_form._name_parts_suffix ) {
                    output << e << " ";
                }
            }
            return output;
        };

        int64_t               _id;
        std::string           _language;
        std::string           _full_text;
        std::vector<NamePart> _name_parts_prefix;
        std::vector<NamePart> _name_parts_first;
        std::vector<NamePart> _name_parts_middle;
        std::vector<NamePart> _name_parts_surname;
        std::vector<NamePart> _name_parts_suffix;
    };
}

#endif //RELATIONS_NAMEFORM_H
