#ifndef RELATIONS_LOCATION_H
#define RELATIONS_LOCATION_H

#include <string>
#include <ostream>

namespace relations::dto {
    struct Location {
        Location( const std::string &description ) :
            _id( -1 ),
            _description( description )
        {};

        Location( const int64_t &id, const std::string &description ) :
            _id( id ),
            _description( description )
        {};

        Location( const int64_t &id,
                  const std::string &description,
                  const std::string &address,
                  const std::string &postcode,
                  const std::string &city,
                  const std::string &county,
                  const std::string &country,
                  const std::string &longitude,
                  const std::string &latitude ) :
            _id( id ),
            _description( description ),
            _address( address ),
            _postcode( postcode ),
            _city( city ),
            _county( county ),
            _country( country ),
            _longitude( longitude ),
            _latitude( latitude )
        {};

        friend std::ostream &operator <<( std::ostream &output, relations::dto::Location const &location ) {
            output << "Location [" << location._id << "]\n"
                   << "· Description : " << location._description << "\n";
            if( !location._address.empty() )
                output << "\n· Address.....: " << location._address;
            if( !location._postcode.empty() )
                output << "\n· Postcode....: " << location._postcode;
            if( !location._city.empty() )
                output << "\n· City........: " << location._city;
            if( !location._county.empty() )
                output << "\n· County......: " << location._county;
            if( !location._country.empty() )
                output << "\n· Country.....: " << location._country;
            if( !location._longitude.empty() )
                output << "\n· Longitude...: " << location._longitude;
            if( !location._latitude.empty() )
                output << "\n· Latitude....: " << location._latitude;
            return output;
        }

        bool operator ==( const Location &rhs ) const {
            return ( _id == rhs._id
                     && _description == rhs._description
                     && _address     == rhs._address
                     && _postcode    == rhs._postcode
                     && _city        == rhs._city
                     && _county      == rhs._county
                     && _country     == rhs._country
                     && _longitude   == rhs._longitude
                     && _latitude    == rhs._latitude );
        }

        bool operator !=( const Location &rhs ) const {
            return !( *this == rhs );
        }

        int64_t     _id;
        std::string _description;
        std::string _address;
        std::string _postcode;
        std::string _city;
        std::string _county;
        std::string _country;
        std::string _longitude;
        std::string _latitude;
    };
}

#endif //RELATIONS_LOCATION_H
