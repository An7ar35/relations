#ifndef RELATIONS_CUSTOMEVENTTYPE_H
#define RELATIONS_CUSTOMEVENTTYPE_H

#include "GenericType.h"

namespace relations::dto {
    struct CustomEventType : GenericType {
        CustomEventType( const int64_t &id,
                         const std::string &value ) :
            GenericType( id, value )
        {};

        CustomEventType( const std::string &value ) :
            GenericType( -1 , value )
        {};
    };
}

#endif //RELATIONS_CUSTOMEVENTTYPE_H
