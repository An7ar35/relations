#ifndef RELATIONS_RELATIONSHIPTYPE_H
#define RELATIONS_RELATIONSHIPTYPE_H

#include "GenericType.h"

namespace relations::dto {
    struct RelationshipType : GenericType {
        RelationshipType( const int64_t &id,
                          const std::string &value ) :
            GenericType( id, value )
        {};

        RelationshipType( const std::string &value ) :
            GenericType( -1, value )
        {};
    };
}

namespace std {
    template<> struct hash<relations::dto::RelationshipType> {
        /**
         * Hash specialisation for RelationshipType
         * @param type RelationshipType to hash
         * @return Hashed RelationshipType
         */
        inline size_t operator()(const relations::dto::RelationshipType& type) const {
            return std::hash<int64_t>{}( type._id );
        }
    };
}

#endif //RELATIONS_RELATIONSHIPTYPE_H
