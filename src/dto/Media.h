#ifndef RELATIONS_MEDIA_H
#define RELATIONS_MEDIA_H

#include <string>
#include <ostream>
#include <eadlib/datatype/enum.h>
#include <src/enums/EnumeratedTypes.h>

namespace relations::dto {
    struct Media {
        Media() :
            _id( -1 ),
            _type( enums::MediaType::DOCUMENT ),
            _description( "" ),
            _file( "" )
        {};

        Media( const enums::MediaType &type,
               const std::string &file ) :
            _id( -1 ),
            _type( type ),
            _description( "" ),
            _file( file )
        {};

        Media( const std::string &description,
               const enums::MediaType &type,
               const std::string &file ) :
            _id( -1 ),
            _description( description ),
            _type( type ),
            _file( file )
        {};

        Media( const int64_t &id,
               const enums::MediaType &type,
               const std::string &file ) :
            _id( id ),
            _type( type ),
            _description( "" ),
            _file( file )
        {};

        Media( const int64_t &id,
               const std::string &description,
               const enums::MediaType &type,
               const std::string &file ) :
            _id( id ),
            _description( description ),
            _type( type ),
            _file( file )
        {};

        friend std::ostream &operator <<( std::ostream &output, relations::dto::Media const &media ) {
            output << "Media [" << media._id << "]\n"
                   << "· Type : [" << eadlib::to_string<enums::MediaType>( media._type ) << "]\n"
                   << "· File : " << media._file;
            if( !media._description.empty() )
                output << "\n· Description: " << media._description;
            return output;
        }

        bool operator ==( const Media &rhs ) const {
            return _id == rhs._id
                   && _description == rhs._description
                   && _type == rhs._type
                   && _file == rhs._file;
        }

        bool operator !=( const Media &rhs ) const {
            return !( *this == rhs );
        }

        int64_t _id;
        std::string _description;
        enums::MediaType _type;
        std::string _file;
    };
}

#endif //RELATIONS_MEDIA_H
