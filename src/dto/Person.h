#ifndef RELATIONS_PERSON_H
#define RELATIONS_PERSON_H

#include <string>
#include <ostream>
#include <eadlib/datatype/enum.h>

#include "src/enums/EnumeratedTypes.h"
#include "Subject.h"
#include "Name.h"

namespace relations::dto {
    struct Person : public Subject {
        Person( const enums::GenderBioType &gender_bio,
                const enums::GenderIdentityType &gender_identity )  :
            Subject(),
            _id( -1 ),
            _gender_biological( gender_bio ),
            _gender_identity( gender_identity )
        {};

        Person( const enums::GenderBioType &gender_bio,
                const enums::GenderIdentityType &gender_identity,
                Note *note )  :
            Subject( note ),
            _id( -1 ),
            _gender_biological( gender_bio ),
            _gender_identity( gender_identity )
        {};

        Person( const int64_t &id,
                const int64_t &subject_id,
                const enums::GenderBioType &gender_bio,
                const enums::GenderIdentityType &gender_identity ) :
            Subject( subject_id ),
            _id( id ),
            _gender_biological( gender_bio ),
            _gender_identity( gender_identity )
        {};

        Person( const int64_t &id,
                const int64_t &subject_id,
                const enums::GenderBioType &gender_bio,
                const enums::GenderIdentityType &gender_identity,
                Note *note ) :
            Subject( subject_id, note ),
            _id( id ),
            _gender_biological( gender_bio ),
            _gender_identity( gender_identity )
        {};

        Person( const Person &person ) :
            Subject( person ),
            _id( person._id ),
            _gender_biological( person._gender_biological ),
            _gender_identity( person._gender_identity ),
            _names( person._names )
        {};

        Person( Person &&person ) :
            Subject( std::move( person ) ),
            _id( std::move( person._id ) ),
            _gender_biological( std::move( person._gender_biological ) ),
            _gender_identity( std::move( person._gender_identity ) ),
            _names( std::move( person._names ) )
        {};

        Person & operator =( const Person &person ) {
            Subject::operator=( person );
            _id                = person._id;
            _gender_biological = person._gender_biological;
            _gender_identity   = person._gender_identity;
            _names             = person._names;
            return *this;
        }

        Person & operator =( Person &&person ) {
            Subject::operator=( std::move( person ) );
            _id                = std::move( person._id );
            _gender_biological = std::move( person._gender_biological );
            _gender_identity   = std::move( person._gender_identity );
            _names             = std::move( person._names );
            return *this;
        }

        friend std::ostream &operator <<( std::ostream &output, relations::dto::Person const &person ) {
            output << "Person [" << std::to_string( person._id ) << "]\n"
                   << "· Subject ID        : [" << std::to_string( person._subject_id ) << "]\n"
                   << "· Biological gender : [" << eadlib::to_string<enums::GenderBioType>( person._gender_biological ) << "]\n"
                   << "· Gender Identity   : [" << eadlib::to_string<enums::GenderIdentityType>( person._gender_identity ) << "]";
            if( !person._names.empty() ) {
                output << "\n» -Associated name(s)-";
                for( auto n : person._names )
                    output << "\n" << n;
            } else {
                output << "\n» No names associated with this person.";
            }
            if( person._note ) {
                output << "\nAttached " << *person._note;
            }
            return output;
        };

        int64_t                   _id;
        enums::GenderBioType      _gender_biological;
        enums::GenderIdentityType _gender_identity;
        std::vector<Name>         _names;
    };
}

#endif //RELATIONS_PERSON_H
