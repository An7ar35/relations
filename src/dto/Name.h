#ifndef RELATIONS_NAME_H
#define RELATIONS_NAME_H

#include <string>
#include <ostream>
#include <memory>
#include <src/enums/EnumeratedTypes.h>
#include <eadlib/datatype/enum.h>
#include "NameForm.h"

namespace relations::dto {
    struct Name {
        Name( const int64_t &id,
              const int64_t &person_id,
              const enums::NameType &name_type,
              const bool &preferred,
              NameForm *name_form ) :
            _id( id ),
            _person_id( person_id ),
            _type( name_type ),
            _preferred_flag( preferred ),
            _name_form( std::unique_ptr<NameForm>( name_form ) )
        {};

        Name( const int64_t &person_id,
              const enums::NameType &name_type,
              const bool &preferred,
              NameForm *name_form ) :
            _id( -1 ),
            _person_id( person_id ),
            _type( name_type ),
            _preferred_flag( preferred ),
            _name_form( std::unique_ptr<NameForm>( name_form ) )
        {};

        Name( const Name &name ) :
            _id( name._id ),
            _person_id( name._person_id ),
            _type( name._type ),
            _preferred_flag( name._preferred_flag )
        {
            auto ptr = name._name_form.get();
            if( name._name_form ) {
                _name_form = std::make_unique<NameForm>( name._name_form->_id,
                                                         name._name_form->_language,
                                                         name._name_form->_full_text );
                if( name._name_form ) {
                    std::copy ( name._name_form->_name_parts_prefix.begin(),
                                name._name_form->_name_parts_prefix.end(),
                                std::back_inserter( _name_form->_name_parts_prefix )
                    );
                    std::copy ( name._name_form->_name_parts_first.begin(),
                                name._name_form->_name_parts_first.end(),
                                std::back_inserter( _name_form->_name_parts_first )
                    );
                    std::copy ( name._name_form->_name_parts_middle.begin(),
                                name._name_form->_name_parts_middle.end(),
                                std::back_inserter( _name_form->_name_parts_middle )
                    );
                    std::copy ( name._name_form->_name_parts_surname.begin(),
                                name._name_form->_name_parts_surname.end(),
                                std::back_inserter( _name_form->_name_parts_surname )
                    );
                    std::copy ( name._name_form->_name_parts_suffix.begin(),
                                name._name_form->_name_parts_suffix.end(),
                                std::back_inserter( _name_form->_name_parts_suffix )
                    );
                }
            }
        }

        Name( Name &&name ) :
            _id( name._id ),
            _person_id( name._person_id ),
            _type( name._type ),
            _preferred_flag( name._preferred_flag ),
            _name_form( std::move( name._name_form ) )
        {}

        Name & operator =( const Name &name ) {
            _id             = name._id;
            _person_id      = name._person_id;
            _type           = name._type;
            _preferred_flag = name._preferred_flag;
            if( name._name_form ) {
                _name_form = std::make_unique<NameForm>( name._name_form->_id,
                                                         name._name_form->_language,
                                                         name._name_form->_full_text );
                _name_form->_name_parts_prefix  = name._name_form->_name_parts_prefix;
                _name_form->_name_parts_first   = name._name_form->_name_parts_first;
                _name_form->_name_parts_middle  = name._name_form->_name_parts_middle;
                _name_form->_name_parts_surname = name._name_form->_name_parts_surname;
                _name_form->_name_parts_suffix  = name._name_form->_name_parts_suffix;
            }
            return *this;
        };

        Name & operator =( Name &&name ) {
            _id             = name._id;
            _person_id      = name._person_id;
            _type           = name._type;
            _preferred_flag = name._preferred_flag;
            _name_form      = std::move( name._name_form );
            return *this;
        };

        friend std::ostream &operator <<( std::ostream &output, relations::dto::Name const &name ) {
            if( name._preferred_flag ) {
                output << "Preferred ";
            }
            output << "Name [" << std::to_string( name._id ) << "] for Person [" << std::to_string( name._person_id ) << "]\n"
                   << "· Type: [" << eadlib::to_string<enums::NameType>( name._type ) << "]\n"
                   << *name._name_form;
            return output;
        };

        int64_t                   _id;
        int64_t                   _person_id;
        enums::NameType           _type;
        bool                      _preferred_flag;
        std::unique_ptr<NameForm> _name_form;
    };
}

#endif //RELATIONS_NAME_H