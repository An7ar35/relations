#ifndef RELATIONS_MEDIARELATION_H
#define RELATIONS_MEDIARELATION_H

#include <string>
#include <ostream>

namespace relations::dto {
    struct MediaRelation {
        MediaRelation( const int64_t &subject_id,
                       const int64_t &media_id ) :
            _subject_id( subject_id ),
            _media_id( media_id )
        {};

        friend std::ostream &operator <<( std::ostream &output, relations::dto::MediaRelation const &media_relation ) {
            output << "MediaRelation Person[" << media_relation._subject_id << "]--Media[" << media_relation._media_id << "]";
            return output;
        }

        bool operator ==( const MediaRelation &rhs ) const {
            return _subject_id == rhs._subject_id
                   && _media_id == rhs._media_id;
        }

        bool operator !=( const MediaRelation &rhs ) const {
            return !( *this == rhs );
        }

        int64_t _subject_id;
        int64_t _media_id;
    };
}

#endif //RELATIONS_MEDIARELATION_H
