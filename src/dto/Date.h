#ifndef RELATIONS_DATE_H
#define RELATIONS_DATE_H

#include <string>
#include <ostream>

namespace relations::dto {
    struct Date {
        Date() :
            _id( -1 )
        {};

        Date( const int64_t &id ) :
            _id( id )
        {};

        Date( const int64_t &id,
              const std::string &formal,
              const std::string &original  ) :
            _id( id ),
            _formal( formal ),
            _original( original )
        {};

        friend std::ostream &operator <<( std::ostream &output, relations::dto::Date const &date ) {
            output << "Date [" << date._id << "] " << date._formal << " (" << date._original << ")";
            return output;
        }

        bool operator ==( const Date &rhs ) const {
            return ( _id == rhs._id
                     && _formal == rhs._formal
                     && _original == rhs._original );
        }

        bool operator !=( const Date &rhs ) const {
            return !( *this == rhs );
        }

        //TODO implement date comparators when formal is implemented in future release

        int64_t _id;
        std::string _formal;
        std::string _original;
    };
}

#endif //RELATIONS_DATE_H
