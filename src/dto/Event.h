#ifndef RELATIONS_EVENT_H
#define RELATIONS_EVENT_H

#include <string>
#include <ostream>
#include <eadlib/datatype/enum.h>

#include "src/enums/EventType.h"
#include "src/dto/Date.h"
#include "src/dto/Location.h"

namespace relations::dto {
    struct Event : public Subject {
        Event() :
            Subject(),
            _id( -1 ),
            _type( enums::EventType::OTHER ),
            _custom_type( -1 ),
            _date( std::make_unique<dto::Date>() ),
            _location( std::make_unique<dto::Location>( "New location" ) )
        {};

        Event( const enums::EventType &type,
               const int64_t &custom_type,
               Date *date,
               Location *location  ) :
            Subject(),
            _id( -1 ),
            _type( type ),
            _custom_type( custom_type ),
            _date( std::unique_ptr<Date>( date ) ),
            _location( std::unique_ptr<Location>( location ) )
        {};

        Event( const enums::EventType &type,
               const int64_t &custom_type,
               Date *date,
               Location *location ,
               Note *note ) :
            Subject( note ),
            _id( -1 ),
            _type( type ),
            _custom_type( custom_type ),
            _date( std::unique_ptr<Date>( date ) ),
            _location( std::unique_ptr<Location>( location ) )
        {};

        Event( const int64_t &id,
               const int64_t &subject_id,
               const enums::EventType &type,
               const int64_t &custom_type,
               Date *date,
               Location *location ) :
            Subject( subject_id ),
            _id( id ),
            _type( type ),
            _custom_type( custom_type ),
            _date( std::unique_ptr<Date>( date ) ),
            _location( std::unique_ptr<Location>( location ) )
        {};

        Event( const int64_t &id,
               const int64_t &subject_id,
               const enums::EventType &type,
               const int64_t &custom_type,
               Date *date,
               Location *location,
               Note *note ) :
            Subject( subject_id, note ),
            _id( id ),
            _type( type ),
            _custom_type( custom_type ),
            _date( std::unique_ptr<Date>( date ) ),
            _location( std::unique_ptr<Location>( location ) )
        {};

        Event( const Event &event ) :
            Subject( event ),
            _id( event._id ),
            _type( event._type ),
            _custom_type( event._custom_type ),
            _date( std::make_unique<Date>( *event._date ) ),
            _location( std::make_unique<Location>( *event._location ) )
        {}

        Event( Event &&event ) :
            Subject( std::move( event ) ),
            _id( event._id ),
            _type( event._type ),
            _custom_type( event._custom_type ),
            _date( std::move( event._date ) ),
            _location( std::move( event._location ) )
        {};

        Event & operator =( const Event &event ) {
            Subject::operator=( event );
            _id          = event._id;
            _type        = event._type;
            _custom_type = event._custom_type;
            _date.reset( new Date( *event._date ) );
            _location.reset( new Location( *event._location ) );
            return *this;
        }

        Event & operator =( Event &&event ) {
            Subject::operator=( std::move( event ) );
            _id          = event._id;
            _type        = event._type;
            _custom_type = event._custom_type;
            _date        = std::move( event._date );
            _location    = std::move( event._location );
            return *this;
        }

        friend std::ostream &operator <<( std::ostream &output, relations::dto::Event const &event ) {
            output << "Event [" << event._id << "]\n"
                   << "· Subject ID.....: [" << event._subject_id << "]";
            if( event._type == enums::EventType::OTHER )
                output << "\n· Type (std)...: [" << eadlib::to_string<enums::EventType>( event._type ) << "]";
            else
                output << "\n· Type (custom): [" << std::to_string( event._custom_type ) << "]";
            output << "\n· " << *event._date << "]"
                   << "\n· " << *event._location << "]";
            if( event._note ) {
                output << "\nAttached " << *event._note;
            }
            return output;
        }

        int64_t                   _id;
        enums::EventType          _type;
        int64_t                   _custom_type;
        std::unique_ptr<Date>     _date;
        std::unique_ptr<Location> _location;
    };
}

#endif //RELATIONS_EVENT_H
