#ifndef RELATIONS_CIRCULARPOSITION_H
#define RELATIONS_CIRCULARPOSITION_H

#include <list>
#include <cstddef>
#include <utility>
#include <memory>
#include <src/algorithm/containers/Position.h>

namespace relations::algo {
    std::unique_ptr<std::list<Position<double>>> circularPosition( const size_t &object_count,
                                                                   const relations::Position<int64_t> &offset );
}

#endif //RELATIONS_CIRCULARPOSITION_H
