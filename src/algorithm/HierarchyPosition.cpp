#include "HierarchyPosition.h"
#include <eadlib/datastructure/LinkedList.h>

/**
 * Inserts an index at a specified level
 * @param level Level
 * @param index Index
 */
void relations::algo::HierarchyPosition::Population::insert( const int64_t &level, const int64_t &index ) {
    try {
        std::vector<int64_t> & v = _level_population.at( level );
        v.emplace_back( index );
    } catch( std::out_of_range ) {
        auto it = _level_population.emplace( level, std::vector<int64_t>() ).first;
        it->second.emplace_back( index );
    }
}

/**
 * Constructor
 */
relations::algo::HierarchyPosition::HierarchyPosition() :
    _level_bounds( { 0, 0 } ),
    _width_bounds( { 0, 0 } )
{}

/**
 * Destructor
 */
relations::algo::HierarchyPosition::~HierarchyPosition() {}

/**
 * Const Iterator Begin
 * @return Begin() iterator
 */
relations::algo::HierarchyPosition::const_iterator relations::algo::HierarchyPosition::cbegin() const {
    return _index_map.cbegin();
}

/**
 * Const Iterator End
 * @return End() iterator
 */
relations::algo::HierarchyPosition::const_iterator relations::algo::HierarchyPosition::cend() const {
    return _index_map.cend();
}

/**
 * Initialize with graph
 * @param data_manager DataManager instance
 * @return Success
 */
bool relations::algo::HierarchyPosition::init( std::shared_ptr<io::DataManager> data_manager ) {
    //Pick a node
    auto it = data_manager->cbegin();
    //Error control
    if( it == data_manager->cend() ) {
        LOG_ERROR( "[relations::algo::HierarchyPosition::init(..)] DataManager has no graph data." );
        return false;
    }
    //Get the relationship types we need
    auto parent_child = std::find_if( data_manager->getDataCache()->_relationship_types.cbegin(),
                                      data_manager->getDataCache()->_relationship_types.cend(),
                                      ( []( const relations::dto::RelationshipType &type ) { return type._value == "ParentChild"; } )
    );
    auto couple = std::find_if( data_manager->getDataCache()->_relationship_types.begin(),
                                data_manager->getDataCache()->_relationship_types.end(),
                                ( []( const relations::dto::RelationshipType &type ) { return type._value == "Couple"; } )
    );
    //Start
    Position starting_position { 0 };
    while( it != data_manager->cend() ) {
        if( _index_map.find( it->first ) == _index_map.end() ) {
            //check if 'ParentChild' relationship exists for this person
            if( data_manager->exists( it->first, parent_child->_id ) ) {
                exploreLevels( data_manager, it->first, 0, *parent_child );
            }
            //check if 'Couple' relationship exists for this person
            if( data_manager->exists( it->first, couple->_id ) ) {
                auto partners = data_manager->getChildList( it->first, couple->_id );
                for( auto partner : partners ) {
                    auto partner_in_index_map = _index_map.find( it->first );
                    if( partner_in_index_map != _index_map.end() ) {
                        auto partner_position = partner_in_index_map->second;
                        exploreLevels( data_manager, it->first, partner_position.y, *parent_child );
                    } else {
                        exploreLevels( data_manager, partner, starting_position.y, *parent_child );
                    }
                }
            }
        }
        it = std::next( it );
    }
    horizontalPositioning( data_manager, 0 );
    for( auto e : _population._level_population ) {
        std::cout << "Level " << e.first << " population: ";
        for( auto i : e.second ) {
            std::cout << i << ",";
        }
        std::cout << std::endl;
    }
    return true;
}

/**
 * Gets the level of an index
 * @param index Index (key) of node
 * @return Level of node
 * @throws std::out_of_range when index is not in map
 */
relations::algo::HierarchyPosition::Position relations::algo::HierarchyPosition::at( const int64_t &index ) {
    return _index_map.at( index );
}

/**
 * Gets the number of levels in the graph
 * @return Level count
 */
int64_t relations::algo::HierarchyPosition::levels() const {
    return _level_bounds.second - _level_bounds.first;
}

/**
 * Explores edges departing and arriving at node
 * @param data_manager DataManager instance
 * @param previous_index
 * @param index        Node key (person's ID)
 * @param level        Level of the current node key
 * @param parent_child Parent/Child RelationshipType DTO object
 */
void relations::algo::HierarchyPosition::exploreLevels( std::shared_ptr<io::DataManager> data_manager,
                                                        const int64_t &index,
                                                        const int64_t &level,
                                                        const dto::RelationshipType &parent_child ) {
    if( _index_map.find( index ) == _index_map.end() ) {
        if( level < _level_bounds.first )
            _level_bounds.first = level;
        if( level > _level_bounds.second )
            _level_bounds.second = level;
        std::cout << "Looking at [" << index << "], level bounds { " << _level_bounds.first << ", " << _level_bounds.second << " }" << std::endl;
        _index_map.insert( { index, { 0, level } } );
        _population.insert( level, index );
        auto parents = data_manager->getParentList( index, parent_child._id );
        for( auto parent : parents ) {
            exploreLevels( data_manager, index, parent, level - 1, parent_child );
        }
        auto children = data_manager->getChildList( index, parent_child._id );
        for( auto child : children ) {
            exploreLevels( data_manager, index, child, level + 1, parent_child );
        }
    }
}

void relations::algo::HierarchyPosition::exploreLevels( std::shared_ptr<io::DataManager> data_manager,
                                                        const int64_t &previous_index,
                                                        const int64_t &index,
                                                        const int64_t &level,
                                                        const relations::dto::RelationshipType &parent_child ) {
    if( _index_map.find( index ) == _index_map.end() ) {
        if( level < _level_bounds.first )
            _level_bounds.first = level;
        if( level > _level_bounds.second )
            _level_bounds.second = level;
        std::cout << "Looking at [" << index << "], level bounds { " << _level_bounds.first << ", " << _level_bounds.second << " }" << std::endl;
        _index_map.insert( { index, { 0, level } } );
        _population.insert( level, index );
        auto parents = data_manager->getParentList( index, parent_child._id );
        for( auto parent : parents ) {
            exploreLevels( data_manager, index, parent, level - 1, parent_child );
        }
        auto children = data_manager->getChildList( index, parent_child._id );
        for( auto child : children ) {
            exploreLevels( data_manager, index, child, level + 1, parent_child );
        }
    }
}

void relations::algo::HierarchyPosition::horizontalPositioning( std::shared_ptr<io::DataManager> data_manager, const int64_t &index ) {
    std::vector<int64_t> & lowest_level = _population._level_population.at( _level_bounds.first );

}

/**
 * Explores bloodline branching
 * @param data_manager
 * @param index
 */
void relations::algo::HierarchyPosition::branching( std::shared_ptr<io::DataManager> data_manager, const int64_t &index ) {

}
