#ifndef RELATIONS_BLOODLINE_H
#define RELATIONS_BLOODLINE_H

#include <memory>
#include <unordered_map>
#include <src/datastructure/Tracker.h>
#include <src/io/DataManager.h>
#include <src/algorithm/containers/EdgeInfo.h>

namespace relations::algo {
    class BloodLine {
      public:
        typedef EdgeInfo<int64_t,relations::dto::RelationshipType> EdgeInfo_t;
        typedef std::list<EdgeInfo_t>                              EdgeList_t;

        struct Siblings {
            std::list<int64_t>                     _full; //sibling id
            std::list<std::pair<int64_t, int64_t>> _half; //sibling id, parent id
        };

        BloodLine( std::shared_ptr<io::DataManager> data_manager );
        ~BloodLine();

        std::unique_ptr<std::list<int64_t>> getChildren( const int64_t &person_id ) const;
        std::unique_ptr<std::list<int64_t>> getParents( const int64_t &person_id ) const;
        std::unique_ptr<std::list<int64_t>> getPartners( const int64_t &person_id ) const;
        std::unique_ptr<Siblings>           getSiblings( const int64_t &person_id,
                                                         const std::list<int64_t> &blood_parents ) const;
        std::unique_ptr<std::list<std::pair<int64_t, int64_t>>> generateUniquePairs( const std::list<int64_t> &list ) const;

      private:
        std::shared_ptr<io::DataManager>       _data_manager;
        std::unique_ptr<dto::RelationshipType> _parentchild_type;
        std::unique_ptr<dto::RelationshipType> _couple_type;
    };
}

#endif //RELATIONS_BLOODLINE_H
