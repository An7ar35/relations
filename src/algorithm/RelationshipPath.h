#ifndef RELATIONS_RELATIONSHIPPATH_H
#define RELATIONS_RELATIONSHIPPATH_H

#include <src/algorithm/containers/EdgeInfo.h>
#include <src/datastructure/DMDGraph.h>
#include <external/eadlib/datastructure/LinkedList.h>

namespace relations::algo {
    template<class K, class V, class R> class RelationshipPath {
      public:
        typedef DMDGraph<K,V,R> RelationshipGraph_t;

        RelationshipPath();
        ~RelationshipPath();

        bool init( RelationshipGraph_t &graph, const K &from, const K &to );

        std::unique_ptr<std::list<EdgeInfo<K,R>>> getPath( RelationshipGraph_t &graph ) const;
        std::unique_ptr<std::list<K>>             getKeys() const;
        int64_t                                   getVisitCount() const;

      private:
        //Constants
        int64_t const INFINITY = -1;
        //Structs
        struct Hop {
            int64_t path_depth;
            K       from_key;
        };
        //Private methods
        bool explore( RelationshipPath::RelationshipGraph_t &graph,
                      const K &previous,
                      const K &current,
                      const K &target,
                      int64_t current_path_depth );

        EdgeInfo<K,R> getRelation( RelationshipPath::RelationshipGraph_t &graph,
                                   const K &from,
                                   const K&to ) const;
        //Private variables
        std::unordered_map<K,Hop> _shortest_paths;
        int64_t                   _shortest_path_depth;
        int64_t                   _target { -1 };
        int64_t                   _origin { -1 };
        int _visit_count { 0 };

    };

    /**
     * Constructor
     * @tparam K Key Type
     * @tparam V Value Type
     * @tparam R Relationship Type
     */
    template<class K, class V, class R> RelationshipPath<K,V,R>::RelationshipPath() :
        _shortest_path_depth( INFINITY )
    {}

    /**
     * Destructor
     */
    template<class K, class V, class R> RelationshipPath<K,V,R>::~RelationshipPath() {}

    /**
     * Init method
     * @param graph DMDGraph instance
     * @param from  Origin node key
     * @param to    Destination node key
     * @return      Success
     */
    template<class K, class V, class R> bool RelationshipPath<K,V,R>::init( RelationshipGraph_t &graph, const K &from, const K &to ) {
        if( from == to ) {
            LOG_WARNING( "[RelationshipPath<K,V,R>::init( DMDGraph<K,V,R> &, ", from, ", ", to, " )] "
                "Keys are the same. There's only self-love in this relationship..." );
            return true;
        }
        _target              = to;
        _origin              = from;
        _visit_count         = 0;
        _shortest_path_depth = INFINITY;
        _shortest_paths.clear();
        return explore( graph, -1, from, to, 0 );
    }

    /**
     * Gets the path found
     * @return Path as a list of node keys
     */
    template<class K, class V, class R>
    typename std::unique_ptr<std::list<EdgeInfo<K,R>>> RelationshipPath<K,V,R>::getPath( RelationshipGraph_t &graph ) const {
        auto ordered_key_list  = std::list<K>();
        auto relationship_path = std::make_unique<std::list<EdgeInfo<K,R>>>();
        auto target = _shortest_paths.find( _target );
        if( target != _shortest_paths.end() ) {
            int64_t current = _target;
            do {
                current = target->first;
                ordered_key_list.emplace_front( current );
                target = _shortest_paths.find( target->second.from_key );
            } while( target->first != _origin );
            ordered_key_list.emplace_front( _origin );
        }
        //Get edges in path
        if( !ordered_key_list.empty() ) {
            do {
                auto from = ordered_key_list.front();
                ordered_key_list.pop_front();
                relationship_path->emplace_back( getRelation( graph, from, ordered_key_list.front() ) );
            } while( ordered_key_list.size() > 1 );
        }
        auto count = 1;
        for( auto e : *relationship_path ) {
            LOG_DEBUG( "[relations::algo::RelationshipPath::getPath()] Hop (", count, "): ", e );
            count++;
        }
        LOG( "[relations::algo::RelationshipPath::getPath()] ", _visit_count, " node visits counted." );
        return std::move( relationship_path );
    }

    /**
     * Gets the keys in path in order
     * @return Node keys for path
     */
    template<class K, class V, class R> typename std::unique_ptr<std::list<K>> RelationshipPath<K,V,R>::getKeys() const {
        auto list = std::make_unique<std::list<K>>();
        auto target = _shortest_paths.find( _target );
        if( target != _shortest_paths.end() ) {
            int64_t current = _target;
            do {
                current = target->first;
                list->emplace_front( current );
                target = _shortest_paths.find( target->second.from_key );
            } while( target->first != _origin );
            list->emplace_front( _origin );
        }
        LOG( "[relations::algo::RelationshipPath::getKeys()] ", _visit_count, " node visits counted." );
        return std::move( list );
    }

    /**
     * Explores the graph to find the target key
     * @param graph              DMDGraph instance
     * @param previous           Previous node key
     * @param current            Current node key
     * @param target             Target node key
     * @param current_path_depth Current path depth
     * @return Find success
     */
    template<class K, class V, class R> bool RelationshipPath<K,V,R>::explore( RelationshipPath::RelationshipGraph_t &graph,
                                                                               const K &previous,
                                                                               const K &current,
                                                                               const K &target,
                                                                               int64_t current_path_depth ) {
        current_path_depth++;
        if( _shortest_path_depth != INFINITY && current_path_depth >= _shortest_path_depth ) {
            LOG_DEBUG( "[relations::algo::RelationshipPath::explore( RelationshipGraph_t &, ", previous, ", ", current, ", ", target, ", ", current_path_depth, " )] "
                "Dropped out at [", current, "] as path depth ", current_path_depth, " >= current minimum of ", _shortest_path_depth );
            return false;
        }
        _visit_count++;
        bool new_record_flag = false;
        auto hop_record = _shortest_paths.find( current );
        if( hop_record == _shortest_paths.end() ) { //never visited
            new_record_flag = true;
            LOG_TRACE( "[relations::algo::RelationshipPath::explore( RelationshipGraph_t &, ", previous, ", ", current, ", ", target, ", ", current_path_depth, " )] "
                "Discovered [", current, "] in ", current_path_depth - 1, " hops." );
            hop_record = _shortest_paths.insert( { current, { current_path_depth, previous } } ).first;
        }
        if( current == target ) {
            LOG( "[relations::algo::RelationshipPath::explore( RelationshipGraph_t &, ", previous, ", ", current, ", ", target, ", ", current_path_depth, " )] "
                "Found target [", target, "] in ", current_path_depth - 1, " hops." );
            if( _shortest_path_depth == INFINITY || current_path_depth < _shortest_path_depth )
                _shortest_path_depth = current_path_depth;
        }
        if( new_record_flag && current != target && current_path_depth != _shortest_path_depth ) {
            for( auto relationship : graph.getRelationships( current ) ) {
                for( auto child : graph.getChildList( current, relationship.first ) ) {
                    explore( graph, current, child, target, hop_record->second.path_depth );
                }
                for( auto parent : graph.getParentList( current, relationship.first ) ) {
                    explore( graph, current, parent, target, hop_record->second.path_depth );
                }
            }
        } else {
            if( current_path_depth < hop_record->second.path_depth ) {
                hop_record->second.path_depth = current_path_depth;
                hop_record->second.from_key = previous;
            } else {
                return false;
            }
        }
        return false;
    }

    /**
     * Finds the relationship and direction between two directly connected nodes
     * @param graph DMDGraph instance
     * @param from  Origin node key
     * @param to    Destination node key
     * @return EdgeInfo container
     */
    template<class K, class V, class R> typename relations::EdgeInfo<K,R>
    RelationshipPath<K,V,R>::getRelation( RelationshipPath::RelationshipGraph_t &graph, const K &from, const K &to ) const {
        auto relationships = graph.getRelationships( from );
        for( auto r : relationships ) {
            for( auto c : r.second.childList ) {
                if( c == to ) {
                    return EdgeInfo<K,R>( from, to, r.first, EdgeInfo<K,R>::Direction::CHILD );
                }
            }
            for( auto p : r.second.parentList ) {
                if( p == to ) {
                    return EdgeInfo<K,R>( from, to, r.first, EdgeInfo<K,R>::Direction::PARENT );
                }
            }
        }
    }

    /**
     * Gets the number of visits of nodes during the last path finding
     * @return Visit count
     */
    template<class K, class V, class R> int64_t RelationshipPath<K,V,R>::getVisitCount() const {
        return _visit_count;
    }
}

#endif //RELATIONS_RELATIONSHIPPATH_H
