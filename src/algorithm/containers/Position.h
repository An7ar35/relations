#ifndef RELATIONS_POSITION_H
#define RELATIONS_POSITION_H

#include <cstdint>

namespace relations {
    template<class T> struct Position {
        Position( const T &x_coord, const T &y_coord ) : x( x_coord ), y( y_coord ) {};
        T  x;
        T  y;
    };
}

#endif //RELATIONS_POSITION_H
