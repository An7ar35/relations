#ifndef RELATIONS_EDGEINFO_H
#define RELATIONS_EDGEINFO_H

#include <ostream>

namespace relations {
    template<class K, class R> struct EdgeInfo {
        enum class Direction { CHILD, PARENT };

        EdgeInfo( const K &from, const K& to, const R &relationship, Direction direction ) :
            _origin( from ),
            _destination( to ),
            _relationship( relationship ),
            _direction( direction )
        {}

        EdgeInfo( const EdgeInfo &edge_info ) :
            _origin( edge_info._origin ),
            _destination( edge_info._destination ),
            _relationship( edge_info._relationship ),
            _direction( edge_info._direction )
        {}

        EdgeInfo( EdgeInfo &&edge_info ) :
            _origin( edge_info._origin ),
            _destination( edge_info._destination ),
            _relationship( std::move( edge_info._relationship ) ),
            _direction( edge_info._direction )
        {}

        EdgeInfo & operator =( const EdgeInfo &edge_info ) {
            _origin       = edge_info._origin;
            _destination  = edge_info._destination;
            _relationship = edge_info._relationship;
            _direction    = edge_info._direction;
            return *this;
        }

        EdgeInfo & operator =( EdgeInfo &&edge_info ) {
            _origin       = edge_info._origin;
            _destination  = edge_info._destination;
            _relationship = std::move( edge_info._relationship );
            _direction    = edge_info._direction;
            return *this;
        }

        friend std::ostream &operator <<( std::ostream &output, relations::EdgeInfo<K,R> const &edge_info ) {
            output << edge_info._origin;
            switch( edge_info._direction ) {
                case EdgeInfo::Direction::CHILD:
                    output << "->";
                    break;
                case EdgeInfo::Direction::PARENT:
                    output << "<-";
                    break;
            }
            output << edge_info._destination
                   << ": "
                   << edge_info._relationship;
            return output;
        }

        K         _origin;
        K         _destination;
        R         _relationship;
        Direction _direction;
    };
}

#endif //RELATIONS_EDGEINFO_H
