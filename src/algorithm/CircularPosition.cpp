#include <math.h>
#include "CircularPosition.h"

static double TWO_PI = 2.0 * M_PI;

std::unique_ptr<std::list<relations::Position<double>>> relations::algo::circularPosition( const size_t &object_count,
                                                                                           const relations::Position<int64_t> &offset ) {
    auto list = std::make_unique<std::list<relations::Position<double>>>();
    if( object_count == 1 ) {
        list->emplace_back( Position<double>( 0, offset.y ) );
    } else {
        auto h = sqrt( pow( offset.x, 2 ) + pow( offset.y, 2 ) );
        auto c = object_count * 2 * h;
        auto r = c / TWO_PI;
        for( size_t i = 0; i < object_count; i++ ) {
            auto theta = TWO_PI / object_count;
            auto angle = theta * i;
            list->emplace_back( Position<double>( r * cos( angle ), r * sin( angle ) ) );
        }
    }
    return std::move( list );
}
