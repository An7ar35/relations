#include <set>
#include <src/algorithm/containers/EdgeInfo.h>
#include "BloodLine.h"

/**
 * Constructor
 * @param data_manager DataManager instance
 */
relations::algo::BloodLine::BloodLine( std::shared_ptr<relations::io::DataManager> data_manager ) :
    _data_manager( data_manager )
{
    //Get the relationship types we need
    auto parentchild_type = std::find_if( data_manager->getDataCache()->_relationship_types.cbegin(),
                                      data_manager->getDataCache()->_relationship_types.cend(),
                                      ( []( const relations::dto::RelationshipType &type ) { return type._value == "ParentChild"; } )
    );
    auto couple_type = std::find_if( data_manager->getDataCache()->_relationship_types.begin(),
                                 data_manager->getDataCache()->_relationship_types.end(),
                                 ( []( const relations::dto::RelationshipType &type ) { return type._value == "Couple"; } )
    );
    if( parentchild_type != data_manager->getDataCache()->_relationship_types.cend() )
        _parentchild_type = std::make_unique<dto::RelationshipType>( *parentchild_type );
    if( couple_type != data_manager->getDataCache()->_relationship_types.end() )
        _couple_type = std::make_unique<dto::RelationshipType>( *couple_type );
}

/**
 * Destructor
 */
relations::algo::BloodLine::~BloodLine() {}

/**
 * Gets the children IDs of a Person
 * @param person_id Person ID
 * @return List of children (from 'ParentChild' relationships)
 */
std::unique_ptr<std::list<int64_t>> relations::algo::BloodLine::getChildren( const int64_t &person_id ) const {
    auto list = std::make_unique<std::list<int64_t>>();
    auto children = _data_manager->getChildList( person_id, _parentchild_type->_id );
    for( auto c : children ) {
        list->emplace_back( c );
    }
    return std::move( list );
}

/**
 * Gets the parents IDs of a person
 * @param person_id Person ID
 * @return List of parents (from 'ParentChild' relationships)
 */
std::unique_ptr<std::list<int64_t>> relations::algo::BloodLine::getParents( const int64_t &person_id ) const {
    auto list = std::make_unique<std::list<int64_t>>();
    auto parents = _data_manager->getParentList( person_id, _parentchild_type->_id );
    for( auto p : parents ) {
        auto person = _data_manager->at( p );
        switch( person._gender_biological ) {
            case enums::GenderBioType::MALE:
                list->emplace_back( p );
                break;
            case enums::GenderBioType::FEMALE:
                list->emplace_front( p );
                break;
            default:
                list->emplace_back( p );
        }
    }
    return std::move( list );
}

/**
 * Creates a list with the person's ID and adds the partners to their left ('Couple' relationships)
 * @param person_id Person ID
 * @return List of partners next to the person
 */
std::unique_ptr<std::list<int64_t>> relations::algo::BloodLine::getPartners( const int64_t &person_id ) const {
    auto list = std::make_unique<std::list<int64_t>>();
    if( _data_manager->exists( person_id, _couple_type->_id ) ) {
        auto parent_edges = _data_manager->getChildList( person_id, _couple_type->_id );
        for( auto p : parent_edges ) {
            list->emplace_front( p );
            LOG_TRACE( "[relations::algo::BloodLine::addPartners( ", person_id, " )] Added partner [", p, "] to the left." );
        }
        auto child_edges = _data_manager->getParentList( person_id, _couple_type->_id );
        for( auto c : child_edges ) {
            list->emplace_front( c );
            LOG_TRACE( "[relations::algo::BloodLine::addPartners( ", person_id, " )] Added partner [", c, "] to the left." );
        }
    }
    return std::move( list );
}

/**
 * Gets full/half sibling relationships
 * @param person_id     Person ID
 * @param blood_parents Parents of the person (in order)
 * @return Lists of siblings
 */
std::unique_ptr<relations::algo::BloodLine::Siblings> relations::algo::BloodLine::getSiblings( const int64_t &person_id,
                                                                                               const std::list<int64_t> &blood_parents ) const
{
    auto list = std::make_unique<Siblings>();
    if( !blood_parents.empty() ) {
        relations::Tracker<int64_t>                     sibling_processed;
        std::list<int64_t>                              full_siblings; //List of sibling IDs
        std::unordered_map<int64_t,std::list<int64_t>>  half_siblings; //[Parent]->{list of children} map
        //(1) For each blood parent get children
        for( auto blood_parent : blood_parents ) {
            //(2) For each child get parents
            std::list<int64_t> children = _data_manager->getChildList( blood_parent, _parentchild_type->_id );
            for( auto sibling : children ) {
                if( sibling != person_id && !sibling_processed.tracked( sibling ) ) {
                    auto sibling_parents = _data_manager->getParentList( sibling, _parentchild_type->_id );
                    bool half_sibling_flag = false;
                    //(3) Check all parents match the subject's parents
                    if( sibling_parents.size() == 1 && blood_parents.size() > 1 ) {
                        half_sibling_flag = true; //edge case where sibling has only 1 known parent
                    } else {
                        for( auto sibling_parent : sibling_parents ) {
                            if( std::find( blood_parents.begin(), blood_parents.end(), sibling_parent ) == blood_parents.end() ) {
                                half_sibling_flag = true;
                            }
                        }
                    }
                    //(4) If 1+ parent does not match subject's parent then mark it as half blood sibling
                    //    otherwise mark it as full blood sibling
                    if( half_sibling_flag ) {
                        auto p = half_siblings.try_emplace( blood_parent );
                        p.first->second.emplace_back( sibling );
                    } else {
                        if( std::find( full_siblings.begin(), full_siblings.end(), sibling ) == full_siblings.end() ) {
                            full_siblings.emplace_back( sibling );
                        }
                    }
                    sibling_processed.track( sibling );
                }
            }
        }
        //(5) Add full-blood siblings to the 'full sibling' list
        for( auto s : full_siblings ) {
            list->_full.emplace_back( s );
        }
        //(6) Add half-blood siblings to the 'half sibling' list
        for( auto p : blood_parents ) {
            try {
                auto siblings = half_siblings.find( p );
                if( siblings != half_siblings.end() ) {
                    for( auto s : siblings->second ) {
                        list->_half.emplace_back( std::make_pair( s, p ) ); //fill half siblings in parent order
                    }
                }
            } catch( std::out_of_range ) {
                LOG_ERROR( "[relations::algo::BloodLine::addSiblings( ", person_id, ", .. )] "
                    "No parent '", p, "' exists as a key in the half_siblings collection for subject [", person_id, "]" );
            }
        }
    }
    return std::move( list );
}

/**
 * Generates all possible pairs from a list of IDs - O( n*(n-1)/2 )
 * @param list List of IDs
 * @return Unordered list of pairs
 */
std::unique_ptr<std::list<std::pair<int64_t, int64_t>>> relations::algo::BloodLine::generateUniquePairs( const std::list<int64_t> &list ) const {
    auto pairs = std::make_unique<std::list<std::pair<int64_t,int64_t>>>();
    for( auto it1 = list.begin(); it1 != list.end(); ++it1 ) {
        for( auto it2 = std::next( it1 ); it2 != list.end(); ++it2 ) {
            pairs->emplace_back( std::make_pair( *it1, *it2 ) );
        }
    }
    return std::move( pairs );
}