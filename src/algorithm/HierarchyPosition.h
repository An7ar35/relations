/**
    @class          relations::algo::HierarchyPosition
    @brief          Algorithm for node positioning for paretn/child and couple relationships
    @author         E. A. Davison
    @copyright      E. A. Davison 2017
    @license        GNUv2 Public License
**/

/**
    @class          relations::algo::HierarchyPosition::Population
    @brief          SQLite database wrapper to simplify usage of sqlite3
**/

/**
    @class          relations::algo::HierarchyPosition::Position
    @brief          SQLite database wrapper to simplify usage of sqlite3
**/

#ifndef RELATIONS_HEIRARCHYPOSITION_H
#define RELATIONS_HEIRARCHYPOSITION_H

#include <src/dto/RelationshipType.h>
#include <src/dto/Person.h>
#include <src/io/DataManager.h>

//TODO check lone nodes are included,
//TODO orphan and childless partners are there and have the correct level assigned.

namespace relations::algo {
    class HierarchyPosition {
      public:
        //Structs
        struct Population {
            void insert( const int64_t &level, const int64_t &index );
            std::unordered_map<int64_t, std::vector<int64_t>>  _level_population;
        };
        struct Position {
            int64_t  x;
            int64_t  y;
        };
        //Constructor/Destructor
        HierarchyPosition();
        ~HierarchyPosition();
        //Iterator
        typedef std::unordered_map<int64_t, Position>::const_iterator const_iterator;
        const_iterator cbegin() const;
        const_iterator cend() const;
        //Methods
        bool init( std::shared_ptr<io::DataManager> data_manager );
        Position at( const int64_t &index );
        int64_t levels() const;

      private:
        void exploreLevels( std::shared_ptr<io::DataManager> data_manager,
                            const int64_t &index,
                            const int64_t &level,
                            const dto::RelationshipType &parent_child );
        void exploreLevels( std::shared_ptr<io::DataManager> data_manager,
                            const int64_t &previous_index,
                            const int64_t &index,
                            const int64_t &level,
                            const dto::RelationshipType &parent_child );
        void horizontalPositioning( std::shared_ptr<io::DataManager> data_manager, const int64_t &index );
        void branching( std::shared_ptr<io::DataManager> data_manager, const int64_t &index );
        //Private variables
        Population _population;
        std::unordered_map<int64_t, Position> _index_map;
        std::pair<int64_t, int64_t>           _level_bounds;
        std::pair<int64_t, int64_t>           _width_bounds;
    };
}

#endif //RELATIONS_HEIRARCHYPOSITION_H
