#include <eadlib/wrapper/SQLite/SQLite.h>
#include "src/storage/ReLationsDbSetup.h"
#include "queries.h"

namespace sample {
    const std::string FILE_NAME { "sample.db" };
}
int main() {
    auto db = eadlib::wrapper::SQLite();

    if( std::ifstream( sample::FILE_NAME ) )
        std::remove( sample::FILE_NAME.c_str() );

    if( db.open( sample::FILE_NAME ) ) {
        db.push( "PRAGMA foreign_keys = ON;" );
        auto setup = relations::storage::ReLationsDbSetup();
        if( setup.buildTableStructure( db ) && setup.checkTableStructure( db ) ) {
            if( db.push( sample::queries::noteTable() ) ) {
                LOG( "[\u2713] Note table sample data." );
            }
            if( db.push( sample::queries::subjectTable() ) ) {
                LOG( "[\u2713] Subject table sample data." );
            }
            if( db.push( sample::queries::personTable() ) ) {
                LOG( "[\u2713] Person table sample data." );
            }
            if( db.push( sample::queries::relationshipTable() ) ) {
                LOG( "[\u2713] Relationship table sample data." );
            }
            if( db.push( sample::queries::namePartTable() ) ) {
                LOG( "[\u2713] NamePart table sample data." );
            }
            if( db.push( sample::queries::nameFormTable() ) ) {
                LOG( "[\u2713] NameForm table sample data." );
            }
            if( db.push( sample::queries::nameTable() ) ) {
                LOG( "[\u2713] Name table sample data." );
            }
            if( db.push( sample::queries::nameCompositionTable() ) ) {
                LOG( "[\u2713] NameComposition table sample data." );
            }
        }
        LOG( "Sample Database created.." );
    }

    return 0;
}