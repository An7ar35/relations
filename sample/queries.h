#include <string>
#include <sstream>

namespace sample::queries {
    std::string noteTable() {
        std::stringstream ss;
        ss << "INSERT INTO Note( subject, text ) VALUES "
           << "( \"Person\", \"4th Generation, #1\" ), "
           << "( \"Person\", \"4th Generation, #2\" ), "
           << "( \"Person\", \"4th Generation, #3\" ), "
           << "( \"Person\", \"4th Generation, #4\" ), "
           << "( \"Person\", \"4th Generation, #5\" ), " //5
           << "( \"Person\", \"4th Generation, #6\" ), "
           << "( \"Person\", \"4th Generation, #7\" ), "
           << "( \"Person\", \"4th Generation, #8\" ), "
           << "( \"Person\", \"4th Generation, #9\" ), "
           << "( \"Person\", \"4th Generation, #10\" ), " //10
           << "( \"Person\", \"4th Generation, #11\" ), "
           << "( \"Person\", \"4th Generation, #12\" ), "
           << "( \"Person\", \"4th Generation, #13\" ), "
           << "( \"Person\", \"4th Generation, #14\" ), "
           << "( \"Person\", \"4th Generation, #15\" ), " //15
           << "( \"Person\", \"4th Generation, #16\" ), "
           << "( \"Person\", \"4th Generation, #1\" ), "
           << "( \"Person\", \"3rd Generation, #2\" ), "
           << "( \"Person\", \"3rd Generation, #3\" ), "
           << "( \"Person\", \"3rd Generation, #4\" ), " //20
           << "( \"Person\", \"3rd Generation, #5\" ), "
           << "( \"Person\", \"3rd Generation, #6\" ), "
           << "( \"Person\", \"3rd Generation, #7\" ), "
           << "( \"Person\", \"3rd Generation, #8\" ), "
           << "( \"Person\", \"2nd Generation, #1\" ), " //25
           << "( \"Person\", \"2nd Generation, #2\" ), "
           << "( \"Person\", \"2nd Generation, #3\" ), "
           << "( \"Person\", \"2nd Generation, #4\" ), "
           << "( \"Person\", \"1st Generation, #1\" ), "
           << "( \"Person\", \"1st Generation, #2\" ), " //30
           << "( \"Person\", \"1st Generation, #3\" ), "
           << "( \"Person\", \"1st Generation, #4\" ), "
           << "( \"Person\", \"1st Generation, #5\" ), "
           << "( \"Person\", \"Children, #1\" ), "
           << "( \"Person\", \"Children, #2\" ), " //35
           << "( \"Relationship\", \"Couple 1&2\" ), "
           << "( \"Relationship\", \"Couple 3&4\" ), "
           << "( \"Relationship\", \"Couple 5&6\" ), "
           << "( \"Relationship\", \"Couple 7&8\" ), "
           << "( \"Relationship\", \"Couple 9&10\" ), "//40
           << "( \"Relationship\", \"Couple 11&12\" ), "
           << "( \"Relationship\", \"Couple 13&14\" ), "
           << "( \"Relationship\", \"Couple 15&16\" ), "
           << "( \"Relationship\", \"Couple 17&18\" ), "
           << "( \"Relationship\", \"Couple 19&20\" ), " //45
           << "( \"Relationship\", \"Couple 21&22\" ), "
           << "( \"Relationship\", \"Couple 23&24\" ), "
           << "( \"Relationship\", \"Couple 25&26\" ), "
           << "( \"Relationship\", \"Couple 27&28\" ), "
           << "( \"Relationship\", \"Couple 31&32\" ), " //50
           << "( \"Relationship\", \"Parent-Child 1->17\" ), "
           << "( \"Relationship\", \"Parent-Child 2->17\" ), "
           << "( \"Relationship\", \"Parent-Child 3->18\" ), "
           << "( \"Relationship\", \"Parent-Child 4->18\" ), "
           << "( \"Relationship\", \"Parent-Child 5->19\" ), " //55
           << "( \"Relationship\", \"Parent-Child 6->19\" ), "
           << "( \"Relationship\", \"Parent-Child 7->20\" ), "
           << "( \"Relationship\", \"Parent-Child 8->20\" ), "
           << "( \"Relationship\", \"Parent-Child 9->21\" ), "
           << "( \"Relationship\", \"Parent-Child 10->21\" ), " //60
           << "( \"Relationship\", \"Parent-Child 11->22\" ), "
           << "( \"Relationship\", \"Parent-Child 12->22\" ), "
           << "( \"Relationship\", \"Parent-Child 13->23\" ), "
           << "( \"Relationship\", \"Parent-Child 14->23\" ), "
           << "( \"Relationship\", \"Parent-Child 15->24\" ), " //65
           << "( \"Relationship\", \"Parent-Child 16->24\" ), "
           << "( \"Relationship\", \"Parent-Child 17->25\" ), "
           << "( \"Relationship\", \"Parent-Child 18->25\" ), "
           << "( \"Relationship\", \"Parent-Child 19->26\" ), "
           << "( \"Relationship\", \"Parent-Child 20->26\" ), " //70
           << "( \"Relationship\", \"Parent-Child 21->27\" ), "
           << "( \"Relationship\", \"Parent-Child 22->27\" ), "
           << "( \"Relationship\", \"Parent-Child 23->28\" ), "
           << "( \"Relationship\", \"Parent-Child 24->28\" ), "
           << "( \"Relationship\", \"Parent-Child 25->29\" ), " //75
           << "( \"Relationship\", \"Parent-Child 25->30\" ), "
           << "( \"Relationship\", \"Parent-Child 25->31\" ), "
           << "( \"Relationship\", \"Parent-Child 26->29\" ), "
           << "( \"Relationship\", \"Parent-Child 26->30\" ), "
           << "( \"Relationship\", \"Parent-Child 26->31\" ), " //80
           << "( \"Relationship\", \"Parent-Child 27->32\" ), "
           << "( \"Relationship\", \"Parent-Child 27->33\" ), "
           << "( \"Relationship\", \"Parent-Child 28->32\" ), "
           << "( \"Relationship\", \"Parent-Child 28->33\" ), "
           << "( \"Relationship\", \"Parent-Child 31->34\" ), " //85
           << "( \"Relationship\", \"Parent-Child 31->35\" ), "
           << "( \"Relationship\", \"Parent-Child 32->34\" ), "
           << "( \"Relationship\", \"Parent-Child 32->35\" )"
           << ";";
        return ss.str();
    }

    std::string subjectTable() {
        std::stringstream ss;
        ss << "INSERT INTO Subject( note ) VALUES "
           << "( 1 ), ( 2 ), ( 3 ), ( 4 ), ( 5 ), ( 6 ), ( 7 ), ( 8 ), "
           << "( 9 ), ( 10 ), ( 11 ), ( 12 ), ( 13 ), ( 14 ), ( 15 ), ( 16 ), "
           << "( 17 ), ( 18 ), ( 19 ), ( 20 ), ( 21 ), ( 22 ), ( 23 ), ( 24 ), "
           << "( 25 ), ( 26 ), ( 27 ), ( 28 ), ( 29 ), ( 30 ), ( 31 ), ( 32 ), "
           << "( 33 ), ( 34 ), ( 35 ), " //End of Persons
           << "( 36 ), ( 37 ), ( 38 ), ( 39 ), ( 40 ), ( 41 ), ( 42 ), ( 43 ), "
           << "( 44 ), ( 45 ), ( 46 ), ( 47 ), ( 48 ), ( 49 ), ( 50 ), " //End of Couples
           << "( 51 ), ( 52 ), ( 53 ), ( 54 ), ( 55 ), ( 56 ), ( 57 ), ( 58 ), ( 59 ), ( 60 ), "
           << "( 61 ), ( 62 ), ( 63 ), ( 64 ), ( 65 ), ( 66 ), ( 67 ), ( 68 ), ( 69 ), ( 70 ), "
           << "( 71 ), ( 72 ), ( 73 ), ( 74 ), ( 75 ), ( 76 ), ( 77 ), ( 78 ), ( 79 ), ( 80 ), "
           << "( 81 ), ( 82 ), ( 83 ), ( 76 ), ( 77 ), ( 78 ), ( 79 ), ( 80 ), ( 81 ), ( 82 ), "
           << "( 83 ), ( 84 ), ( 85 ), ( 86 ), ( 87 ), ( 88 )" //End of Parent/Child
           << ";";
        return ss.str();
    }


    std::string personTable() {
        std::stringstream ss;
        ss << "INSERT INTO Person( subject, gender_bio, gender_identity ) VALUES "
           << "( 1, 1, 1 ), ( 2, 2, 1 ), " //4th
           << "( 3, 1, 1 ), ( 4, 2, 1 ), "
           << "( 5, 1, 1 ), ( 6, 2, 1 ), "
           << "( 7, 1, 1 ), ( 8, 2, 1 ), "
           << "( 9, 1, 1 ), ( 10, 2, 1 ), "
           << "( 11, 1, 1 ), ( 12, 2, 1 ), "
           << "( 13, 1, 1 ), ( 14, 2, 1 ), "
           << "( 15, 1, 1 ), ( 16, 2, 1 ), "
           << "( 17, 1, 1 ), ( 18, 2, 1 ), " //3rd
           << "( 19, 1, 1 ), ( 20, 2, 1 ), "
           << "( 21, 1, 1 ), ( 22, 2, 1 ), "
           << "( 23, 1, 1 ), ( 24, 2, 1 ), "
           << "( 25, 1, 1 ), ( 26, 2, 1 ), " //2nd
           << "( 27, 1, 1 ), ( 28, 2, 1 ), "
           << "( 29, 1, 1 ), ( 30, 2, 1 ), " //1st couple
           << "( 31, 1, 1 ), ( 32, 2, 1 ), ( 33, 1, 1 ), " //1st siblings
           << "( 34, 1, 1 ), ( 35, 2, 1 ) " //Children
           << ";";
        return ss.str();
    }

    std::string nameTable() {
        std::stringstream ss;
        ss << "INSERT INTO Name( type, preferred, name_form, person ) VALUES"
           << "( 1, 1, 1, 1 ) "
           << ";";
        return ss.str();
    }

    std::string nameFormTable() {
        std::stringstream ss;
        ss << "INSERT INTO NameForm( language, full_text ) VALUES "
           << "( \"eng\", \"John Smith\" ) "
           << ";";
        return ss.str();
    }

    std::string namePartTable() {
        std::stringstream ss;
        ss << "INSERT INTO NamePart( value, type ) VALUES "
           << "( \"Mr\", 1 ), "
           << "( \"John\", 3 ), "
           << "( \"Peter\", 4 ), "
           << "( \"Bernard\", 4 ), "
           << "( \"Smith\", 5 ) "
           << ";";
        return ss.str();
    }

    std::string nameCompositionTable() {
        std::stringstream ss;
        ss << "INSERT INTO NameComposition( name_part_id, name_form_id ) VALUES "
           << "( 1, 1 ), "
           << "( 2, 1 ), "
           << "( 3, 1 ), "
           << "( 4, 1 ), "
           << "( 5, 1 ) "
           << ";";
        return ss.str();
    }

    std::string relationshipTable() {
        std::stringstream ss;
        ss << "INSERT INTO Relationship( person1_id, person2_id, subject, type ) VALUES "
           << "( 1, 2, 36, 1 ), " //Couples
           << "( 3, 4, 37, 1 ), "
           << "( 5, 6, 37, 1 ), "
           << "( 7, 8, 38, 1 ), "
           << "( 9, 10, 39, 1 ), "
           << "( 11, 12, 40, 1 ), "
           << "( 13, 14, 41, 1 ), "
           << "( 15, 16, 42, 1 ), "
           << "( 17, 18, 43, 1 ), "
           << "( 19, 20, 44, 1 ), "
           << "( 21, 22, 46, 1 ), "
           << "( 23, 24, 47, 1 ), "
           << "( 25, 26, 48, 1 ), "
           << "( 27, 28, 49, 1 ), "
           << "( 31, 32, 50, 1 ), "
           << "( 1, 17, 51, 2 ), " //ParentChild
           << "( 2, 17, 52, 2 ), "
           << "( 3, 18, 53, 2 ), "
           << "( 4, 18, 54, 2 ), "
           << "( 5, 19, 55, 2 ), "
           << "( 6, 19, 56, 2 ), "
           << "( 7, 20, 57, 2 ), "
           << "( 8, 20, 58, 2 ), "
           << "( 9, 21, 59, 2 ), "
           << "( 10, 21, 60, 2 ), "
           << "( 11, 22, 61, 2 ), "
           << "( 12, 22, 62, 2 ), "
           << "( 13, 23, 63, 2 ), "
           << "( 14, 23, 64, 2 ), "
           << "( 15, 24, 65, 2 ), "
           << "( 16, 24, 66, 2 ), "
           << "( 17, 25, 67, 2 ), "
           << "( 18, 25, 68, 2 ), "
           << "( 19, 26, 69, 2 ), "
           << "( 20, 26, 70, 2 ), "
           << "( 21, 27, 71, 2 ), "
           << "( 22, 27, 72, 2 ), "
           << "( 23, 28, 73, 2 ), "
           << "( 24, 28, 74, 2 ), "
           << "( 25, 29, 75, 2 ), "
           << "( 25, 30, 76, 2 ), "
           << "( 25, 31, 77, 2 ), "
           << "( 26, 29, 78, 2 ), "
           << "( 26, 30, 79, 2 ), "
           << "( 26, 31, 80, 2 ), "
           << "( 27, 32, 81, 2 ), "
           << "( 27, 33, 82, 2 ), "
           << "( 28, 32, 83, 2 ), "
           << "( 28, 33, 84, 2 ), "
           << "( 31, 34, 85, 2 ), "
           << "( 31, 35, 86, 2 ), "
           << "( 32, 34, 87, 2 ), "
           << "( 32, 35, 88, 2 ) "
           << ";";
        return ss.str();
    }
}