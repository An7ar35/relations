#!/usr/bin/env bash

OUTPUT=listings.ps

a2ps `find src -name '*.c' -o -name '*.cpp' -o -name '*.h'` \
-1 --pretty-print=cxx --line-numbers=1 --border=no --chars-per-line=120 \
--media=letter --compact=1 --margin=12 -o src_listings.ps --prologue=color --right-title=%p. --left-title \
--left-footer --right-footer --header

a2ps `find external/eadlib/logger -name '*.c' -o -name '*.cpp' -o -name '*.h'` \
`find external/eadlib/wrapper -name '*.c' -o -name '*.cpp' -o -name '*.h'` \
-1 --pretty-print=cxx --line-numbers=1 --border=no --chars-per-line=120 \
--media=letter --compact=1 --margin=12 -o eadlib_listings.ps --prologue=color --right-title=%p. --left-title \
--left-footer --right-footer --header

ps2pdf src_listings.ps src_listings.pdf
ps2pdf eadlib_listings.ps eadlib_listings.pdf
